!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_hammoz_config

  IMPLICIT NONE
  
  PRIVATE

  PUBLIC ::   nmaxclass

  INTEGER, PARAMETER     :: nmaxclass = 20   ! maximum number of aerosol modes or bins

END MODULE mo_hammoz_config

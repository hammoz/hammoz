remap_dust(){

irdir=hammoz/${hres_in}
ordir=hammoz/${grid_name_out}

cdo_remap_nn $irdir/soil_type_all_${hres_in}.nc $ordir/soil_type_all_${grid_name_out}.nc 

cdo_remap_con $irdir/dust_potential_sources_${hres_in}.nc $ordir/dust_potential_sources_${grid_name_out}.nc
cdo_remap_con $irdir/dust_preferential_sources_${hres_in}.nc $ordir/dust_preferential_sources_${grid_name_out}.nc
cdo_remap_con $irdir/msg_pot_sources_${hres_in}.nc $ordir/msg_pot_sources_${grid_name_out}.nc

cdo_remap_nn $irdir/dust_regions_${hres_in}.nc  $ordir/dust_regions_${grid_name_out}.nc

cdo_remap_nn_masked $irdir/surface_rough_12m_${hres_in}.nc $ordir/surface_rough_12m_${grid_name_out}.nc

}

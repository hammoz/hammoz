import os, sys
from utils.util import new_dirs
import pprint

def create_drivers_list( driver_dir ):
    caller="drivers.py"
    drivers_list=[]
    x=new_dirs(caller, driver_dir, driver_dir)
    drivers_list.append(x)
    #for subdir in os.listdir(driver_dir):
    #    subdir_path=os.path.join(driver_dir, subdir)
    #    if os.path.isdir(subdir_path) and subdir != "shared":
    #         x=u.new_dirs(caller,subdir, subdir_path)
    #         drivers_list.append(x)
    return drivers_list

def print_drivers_list( drivers_list ):
    for driver in drivers_list:
        print ( "driver "+driver.name )
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(driver.__dict__)

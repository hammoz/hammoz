from namelists.namelist import prepare_namelist_io

import os,sys
# purpose: create fortran modules for namelist io,etc. based on 
#       variable definitions in the xml files in xml_include_dir 
#       and on host model specific template files in dirvers_dir/*/templates

# store present working directory  
odir=os.getcwd()
# change directory to hammoz/namelists
os.chdir(os.path.dirname(__file__)+"/..")

# directory containing xml files with variable declarations, defaults, etc.
xml_include_dir="./"

# host template files are expected in 
#    ./templates
#    output for these will be written to host_models/*/src
driver_dir="./"


prepare_namelist_io( xml_include_dir, driver_dir )

# cd back to old working directory 
os.chdir(odir)

sys.exit(0)

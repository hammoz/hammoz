!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_hammoz_sedimentation.f90
!!
!! \brief
!! module to interface ECHAM submodules with sedimentation module(s)
!!
!! \author M. Schultz (FZ Juelich)
!!
!! \responsible_coder
!! M. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Schultz (FZ Juelich) - original code (2009-10-26)
!!   -# M. Schultz (FZ Juelich) - improved diag routines (2010-04-16)
!!
!! \limitations
!! All diag_lists must be defined in order to avoid problems with
!! get_diag_pointer in the actual sedi_interface routine. Lists can be empty.
!! Currently there is only one unified interactive sedimentation scheme for
!! aerosols (HAM).
!!
!! \details
!! This module initializes the scheme based on the namelist parameters
!! in submodeldiagctl and creates a stream for variable pointers and 
!! diagnostic quantities used in the sedimentation scheme. It also
!! provides a generic interface to the actual sedimentation routine(s).
!!
!! \bibliographic_references
!! None
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mo_hammoz_sedimentation

  USE mo_kind,             ONLY: dp
  USE mo_submodel_diag,    ONLY: t_diag_list
  USE mo_linked_list,      ONLY: t_var_list

  IMPLICIT NONE

  PRIVATE

  ! public variables  (see declaration below)

  ! subprograms
  PUBLIC                       :: init_sedi_stream, &
                                  sedi_interface

  ! sedi_stream
  INTEGER, PARAMETER           :: nsedivars=2
  CHARACTER(LEN=32)            :: sedivars(1:nsedivars)= &
                                (/'sed             ', &   ! total sedimentation flux
                                  'vsedi           '  /)  ! sedimentation velocity

  TYPE t_sedi_field
    ! variable pointers and diagnostic lists
    TYPE (t_diag_list), PUBLIC   :: sed         ! sedimentation flux
    TYPE (t_diag_list), PUBLIC   :: vsedi       ! sedimentation velocity
  END TYPE t_sedi_field

  TYPE(t_sedi_field), ALLOCATABLE, TARGET :: sedi_field(:)  !< shape: (n_dom)
  TYPE(t_var_list), POINTER      :: sedi_field_list(:)      !< shape: (n_dom)
  TYPE(t_var_list), POINTER      :: sedi_field_list1

  CHARACTER(LEN=*), PARAMETER :: thismodule='mo_hammoz_sedimentation' 

  CONTAINS

  SUBROUTINE init_sedi_stream ( p_patch )

    USE mo_string_utls,         ONLY: st1_in_st2_proof
    USE mo_util_string,         ONLY: tolower
    USE mo_exception,           ONLY: finish, message
    USE mo_ham_m7_trac,         ONLY: ham_get_class_flag
    USE mo_submodel_tracdef,    ONLY: ln, ntrac, trlist, AEROSOL
    USE mo_species,             ONLY: nspec, speclist
    USE mo_ham,                 ONLY: nclass
    USE mo_submodel_streams,    ONLY: sedi_tinterval, sedinam, sedi_keytype
    USE mo_submodel_diag,       ONLY: BYTRACER, BYSPECIES, BYNUMMODE, BYMODE !SF #299 added BYMODE
    USE mo_submodel,            ONLY: lham !SF, see #228

    USE mo_parallel_config,     ONLY: nproma
    USE mo_impl_constants,      ONLY: MAX_CHAR_LENGTH, SUCCESS
    USE mo_model_domain,        ONLY: t_patch

    ! input
    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    INTEGER, PARAMETER             :: ndefault = 2
    CHARACTER(LEN=32)              :: defnam(1:ndefault)   = &   ! default output
                                (/ 'sed             ', &    ! total sedimentation flux
                                   'vsedi           '  /)   ! sedimentation velocity
 
    LOGICAL                        :: tracflag(ntrac), specflag(nspec), modflag(MAX(nclass,1))
    CHARACTER(LEN=ln)              :: tracname(ntrac), specname(nspec), modname(MAX(nclass,1)), &
                                      modnumname(MAX(nclass,1)) !SF #299

!!$    TYPE (t_stream), POINTER       :: ssedi

    CHARACTER(len=MAX_CHAR_LENGTH)  :: listname

    INTEGER                        :: ierr, jt, ndomain, jg, nblks, nlev, ist
    LOGICAL                        :: lpost

    !++mgs: default values and namelist read are done in init_submodel_streams !

    !-- handle ALL, DETAIL and DEFAULT options for sedi output variables
    !-- Note: ALL and DETAIL are identical for sedi output
    IF (TRIM(tolower(sedinam(1))) == 'detail')  sedinam(1:nsedivars) = sedivars(:)
    IF (TRIM(tolower(sedinam(1))) == 'all')     sedinam(1:nsedivars) = sedivars(:)
    IF (TRIM(tolower(sedinam(1))) == 'default') sedinam(1:ndefault) = defnam(:)

    !-- check that all variable names from namelist are valid
    IF (.NOT. st1_in_st2_proof( sedinam, sedivars, ierr=ierr) ) THEN
      IF (ierr > 0) CALL finish ( 'ini_sedi_stream', 'variable '// &
                                  sedinam(ierr)//' does not exist in sedi stream' )
    END IF

    !-- define the flags and names for the diagnostic lists. We need one set of flags and
    !   names for each key_type (BYTRACER, BYSPECIES, BYMODE)
    !   gas-phase tracers will always be defined BYTRACER, for aerosol tracers one of the
    !   following lists will be empty.
    tracflag(:) = .FALSE.
    DO jt = 1,ntrac
      tracname(jt) = trlist%ti(jt)%fullname
      IF (IAND(trlist%ti(jt)%nphase,AEROSOL) /= 0 .AND.       &  !>>dod diagnostics bugfix <<dod
          sedi_keytype == BYTRACER .AND.              &
          nclass > 0) THEN
        tracflag(jt) = trlist%ti(jt)%nsedi > 0
      END IF
    END DO
    specflag(:) = .FALSE.
    DO jt = 1,nspec
      specname(jt) = speclist(jt)%shortname
      IF (sedi_keytype == BYSPECIES .AND.                       &
          IAND(speclist(jt)%nphase, AEROSOL) /= 0 .AND.         &
          nclass > 0) THEN
        specflag(jt) = .TRUE.
      END IF
    END DO
    modflag(:) = .false.
    modname(:) = ''
    !SF #228, adding a condition to check that HAM is active:
    !SF #299, adding a condition to check if BYMODE is relevant:
    IF (lham .AND. nclass > 0 .AND. (sedi_keytype == BYMODE)) THEN
       CALL ham_get_class_flag(nclass, modflag, modname, modnumname, lsedi=.TRUE.) ! get all modes
    END IF


    ! since nesting may be supported in the future, let's just handle this properly
    CALL message(TRIM(thismodule),'Construction of HAM sedimentation state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( sedi_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of sedi_field failed')

    ALLOCATE( sedi_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of sedi_field_list failed')


   DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      sedi_field_list1 => sedi_field_list(jg)

      WRITE(listname,'(a,i2.2)') 'sedi_state_diag_of_domain',jg
      CALL new_sedi_field_list( jg,        nproma,     nlev,        nblks,                 &
                               &  nclass,    ntrac,      nspec,                            &
                               &  tracflag,  specflag,   modflag,                          &
                               &  tracname,  specname,   modname,     modnumname,          &
                               &  TRIM(listname),                                          &
                               &  sedi_field_list1, sedi_field(jg)                          )

    END DO

    CALL message(TRIM(thismodule),'Construction of HAM sedimentation state finished.')


  END SUBROUTINE init_sedi_stream

  SUBROUTINE new_sedi_field_list( k_jg,       kproma,     klev,       kblks,        &
                               &  kclass,     ktrac,      kspec,                    &
                               &  tracflag,   specflag,   modflag,                  &
                               &  tracname,   specname,   modname,     modnumname,  &                      
                               &  listname,                                         &
                               &  field_list, field                                 ) 

    USE mo_kind,                ONLY: dp
    USE mo_exception,           ONLY: message, finish

    USE mo_impl_constants,      ONLY: SUCCESS, MAX_CHAR_LENGTH,  & 
      &                               VINTP_METHOD_PRES,         &
      &                               VINTP_METHOD_LIN,          &
      &                               VINTP_METHOD_LIN_NLEVP1

    USE mo_linked_list,         ONLY: t_var_list

    USE mo_var_list,            ONLY: default_var_list_settings, &
      &                               add_var, add_ref,          &
      &                               new_var_list,              &
      &                               delete_var_list
    USE mo_var_metadata,        ONLY: create_vert_interp_metadata, vintp_types, &
                                      groups
    USE mo_cf_convention,       ONLY: t_cf_var
    USE mo_grib2,               ONLY: t_grib2_var, grib2_var
    USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
      &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
      &                               GRID_UNSTRUCTURED,                 &
      &                               TSTEP_INSTANT, TSTEP_AVG,          &
      &                               cdiDefMissval
    USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
      &                               ZA_HYBRID, ZA_HYBRID_HALF,         &
      &                               ZA_SURFACE
    USE mo_io_config,           ONLY: lnetcdf_flt64_output

    USE mo_fortran_tools,       ONLY: t_ptr_2d3d
   
    USE mo_submodel_diag,       ONLY: new_diag_list, new_diag,  &
                                      BYTRACER, BYSPECIES, BYNUMMODE, BYMODE !SF #299 added BYMODE
    USE mo_submodel_streams,    ONLY: drydep_lpost, drydep_tinterval, drydepnam,  &
                                      drydep_gastrac, drydep_keytype, drydep_ldetail, &    ! ++mgs 20140519
                                      drydep_trac_detail

    USE mo_submodel_tracdef,    ONLY: ln
    USE mo_string_utls,         ONLY: st1_in_st2_proof

    USE mo_submodel_diag,       ONLY: new_diag_list, new_diag,   &
                                      BYTRACER, BYSPECIES, BYNUMMODE, BYMODE !SF #299 added BYMODE
    USE mo_submodel_streams,    ONLY: sedi_lpost, sedinam

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev,  kblks     !< dimension sizes
    INTEGER, INTENT(IN) :: kclass, ktrac, kspec

    LOGICAL, INTENT(IN)            :: tracflag(ktrac), specflag(kspec), modflag(MAX(kclass,1))
    CHARACTER(LEN=ln), INTENT(IN)  :: tracname(ktrac), specname(kspec), modname(MAX(kclass,1)), &
                                      modnumname(MAX(kclass,1)) !SF #299

    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),  POINTER,   INTENT(INOUT) :: field_list
    TYPE(t_sedi_field), INTENT(INOUT)           :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: jt

    LOGICAL :: lpost

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)

    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )

    CALL default_var_list_settings( field_list,  lrestart=.FALSE. )

    !-- sedimentation velocities by tracer or by aerosol mode
    lpost = st1_in_st2_proof( 'vsedi', sedinam) .AND. sedi_lpost
    CALL new_diag_list (field%vsedi, field_list, diagname='vsedi', tsubmname='',        &
                        longname='sedimentation velocity', units='m s-1',               &
                        ndims=2, nmaxkey=(/ktrac, 0, kclass, kclass, 0 /), lpost=lpost, &
                        table=199                                                       )
    CALL new_diag(field%vsedi, ktrac, tracflag, tracname, BYTRACER, shape2d=shape2d )
    IF (ANY(modflag)) THEN !SF #299 added mode mass diags and fix for mode number name
      CALL new_diag(field%vsedi, kclass, modflag, modname, BYMODE, shape2d=shape2d)
      CALL new_diag(field%vsedi, kclass, modflag, modnumname, BYNUMMODE, shape2d=shape2d)
    END IF

    !-- average diagnostic quantities


    !-- total sedi flux
    lpost = st1_in_st2_proof( 'sed', sedinam) .AND. sedi_lpost
    CALL new_diag_list (field%sed,  field_list, diagname='sed', tsubmname='',               &
                        longname='accumulated sedimentation flux', units='kg m-2 s-1',      &
                        ndims=2, nmaxkey=(/ktrac, kspec, kclass, kclass, 0 /), lpost=lpost, &
                        table=199                                                           )

    ! add diagnostic elements only when output is activated
    IF (lpost) THEN
      CALL new_diag(field%sed, ktrac, tracflag, tracname, BYTRACER, shape2d=shape2d)
      CALL new_diag(field%sed, kspec, specflag, specname, BYSPECIES, shape2d=shape2d)
      IF (ANY(modflag)) THEN !SF #299 added mode mass diags and fix for mode number name
        CALL new_diag(field%sed, kclass, modflag, modname, BYMODE, shape2d=shape2d)
        CALL new_diag(field%sed, kclass, modflag, modnumname, BYNUMMODE, shape2d=shape2d)
      END IF
    END IF

  END SUBROUTINE new_sedi_field_list

  !! ---------------------------------------------------------------------------------------
  !! sedi_interface: generic interface routine to sedimentation
  !! currently, the only sedimentation scheme implemented is that of HAM

  SUBROUTINE sedi_interface(jg, kbdim, kproma, klev, krow,      & 
                            pdtime,                             &
                            pdz,                                &
                            pmconv,       prho,                 &
                            pt,           pap,                  &
                            pxtm1,  pxtte                       )


  USE mo_submodel_tracdef,     ONLY: ntrac, trlist
  USE mo_submodel_diag,        ONLY: get_diag_pointer
  USE mo_ham_sedimentation,    ONLY: ham_prep_sedi, ham_sedimentation

  !--- parameters
  INTEGER,  INTENT(in)    :: jg, kbdim, kproma, klev, krow
  REAL(dp), INTENT(in)    :: pdtime,                  & ! time step         
                             pdz(kbdim,klev),         & ! geometric height thickness of layer
                             pmconv(kbdim, klev),     & ! air mass [kg/m2] (either dry or moist)
                             prho(kbdim,klev),        & ! air density
                             pt(kbdim, klev),         & ! temperature
                             pap(kbdim, klev),        & ! full level pressure
                             pxtm1(kbdim,klev,ntrac)    ! tracer mass/number mixing ratio

  REAL(dp), INTENT(inout) :: pxtte(kbdim,klev,ntrac)    ! tracer tendency

  !--- local variables
  INTEGER       :: jt, ierr
  REAL(dp)      :: ztempc(kbdim, klev),   &  ! temp. above melting
                   zvis(kbdim, klev),     &  ! air viscosity
                   zlair(kbdim, klev),    &  ! mean free path
                   zxtp1(kbdim, klev),    &  ! updated tracer(jt) 
                   zxtte(kbdim, klev),    &  ! tracer(jt) tendency
                   zvsedi(kbdim, klev),   &  ! sedimentation velocity
                   zsediflux(kbdim, klev)    ! sedimentation flux
  REAL(dp), POINTER    :: fld2d(:,:)         ! pointer for diagnostics

  !--- calculate tracer independent physical variables
  ! note: IF (ANY(trlist%ti(:)%nsedi)/=0) THEN  not needed -- this is checked upon calling
  CALL ham_prep_sedi(kproma, kbdim, klev, &
                     pt,     pap,         &
                     ztempc, zvis,        &
                     zlair                )

  !-- for icon accum
   DO jt=1, ntrac
      IF (trlist%ti(jt)%nsedi==0) CYCLE    
      CALL get_diag_pointer(sedi_field(jg)%sed, fld2d, jt, ierr=ierr)
      IF (ierr == 0) fld2d(1:kproma,krow) = 0._dp
   END DO

  !--- tracer loop
  DO jt=1, ntrac
    IF (trlist%ti(jt)%nsedi==0) CYCLE    ! do nothing if tracer doesn't sediment

    !--- update tracer concentration
    zxtp1(1:kproma,:) = pxtm1(1:kproma,:,jt) + pxtte(1:kproma,:,jt) * pdtime
    zxtte(1:kproma,:) = pxtte(1:kproma,:,jt)

    !--- call HAM sedimentation routine    
    CALL ham_sedimentation(jg, kproma, kbdim, klev, krow, &
                           pdtime,                        &
                           jt, zvis, zlair, prho,         &
                           pmconv, pdz,                   & 
                           zxtp1, zxtte,                  &
                           zvsedi, zsediflux)

    !--- store diagnostics
    CALL get_diag_pointer(sedi_field(jg)%sed, fld2d, jt, ierr=ierr)
    IF (ierr == 0) fld2d(1:kproma,krow) = fld2d(1:kproma,krow) + zsediflux(1:kproma,klev)
    CALL get_diag_pointer(sedi_field(jg)%vsedi, fld2d, jt, ierr=ierr)
    IF (ierr == 0) fld2d(1:kproma,krow) = zvsedi(1:kproma,klev)

    pxtte(1:kproma,:,jt)=zxtte(1:kproma,:)  !csld write tendency out

  END DO

  END SUBROUTINE sedi_interface


END MODULE mo_hammoz_sedimentation

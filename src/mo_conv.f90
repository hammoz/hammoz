!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_conv

  ! U. Lohmann, ETHZ, 2007-01-24, additional cloud output
  USE mo_kind,                ONLY: dp
  USE mo_fortran_tools,       ONLY: t_ptr_2d3d
  USE mo_linked_list,         ONLY: t_var_list

  USE mo_exception,           ONLY: message, finish

  USE mo_impl_constants,      ONLY: SUCCESS, MAX_CHAR_LENGTH,  & 
    &                               VINTP_METHOD_PRES,         &
    &                               VINTP_METHOD_LIN,          &
    &                               VINTP_METHOD_LIN_NLEVP1

  USE mo_linked_list,         ONLY: t_var_list

  USE mo_var_list,            ONLY: default_var_list_settings, &
    &                               add_var, add_ref,          &
    &                               new_var_list,              &
    &                               delete_var_list
  USE mo_var_metadata,        ONLY: create_vert_interp_metadata, vintp_types, &
                                    groups
  USE mo_cf_convention,       ONLY: t_cf_var
  USE mo_grib2,               ONLY: t_grib2_var, grib2_var
  USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
    &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
    &                               GRID_UNSTRUCTURED,                 &
    &                               TSTEP_INSTANT, TSTEP_AVG,          &
    &                               TSTEP_ACCUM, cdiDefMissval
  USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
    &                               ZA_HYBRID, ZA_HYBRID_HALF,         &
    &                               ZA_SURFACE
  USE mo_parallel_config,     ONLY: nproma
  USE mo_io_config,           ONLY: lnetcdf_flt64_output

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: construct_stream_conv, conv_field

  TYPE t_conv_field
    REAL(dp), POINTER :: na_cv(:,:,:)
    REAL(dp), POINTER :: cdncact_cv(:,:,:)
    REAL(dp), POINTER :: twc_conv(:,:,:)
    REAL(dp), POINTER :: conv_time(:,:,:)
  END TYPE t_conv_field

 TYPE(t_conv_field), ALLOCATABLE, TARGET :: conv_field(:)  !< shape: (n_dom)
 TYPE(t_var_list), ALLOCATABLE :: conv_field_list(:)  !< shape: (n_dom) 

 CHARACTER(LEN=*), PARAMETER :: thismodule='mo_conv'

CONTAINS

  SUBROUTINE construct_stream_conv( p_patch ) 

    ! construct_stream_conv: allocates output streams
    !                        for the activation schemes
    !
    ! Author:
    ! -------
    ! Philip Stier, 2004
    !
    !USE mo_filename,      ONLY: out_filetype
    USE mo_model_domain,            ONLY: t_patch  

    IMPLICIT NONE

    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch 

    CHARACTER(len=MAX_CHAR_LENGTH) :: listname
    INTEGER :: ndomain, jg, ist, nblks, nlev

    CALL message(TRIM(thismodule),'Construction of CONV state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( conv_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of conv_field failed')

    ALLOCATE( conv_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of conv_field_list failed')



    DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      WRITE(listname,'(a,i2.2)') 'conv_state_diag_of_domain',jg
      CALL new_conv_field_list( jg, nproma, nlev, nblks,                                  &
                                 &  TRIM(listname), conv_field_list(jg), conv_field(jg)  )

    END DO

    CALL message(TRIM(thismodule),'Construction of CONV state finished.')

    
  END SUBROUTINE construct_stream_conv
  !--------------------------------------------------------------------
  !>
  SUBROUTINE new_conv_field_list( k_jg, kproma, klev, kblks,       &
                                 & listname, field_list, field    ) 

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev, kblks     !< dimension sizes

    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),   INTENT(INOUT) :: field_list
    TYPE(t_conv_field), INTENT(INOUT) :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    CHARACTER(len=10) :: cbin
    INTEGER :: jw

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)


    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )

    CALL default_var_list_settings( field_list,                &
                                  & lrestart=.TRUE.  )

    ! cloud Properties:
    cf_desc    = t_cf_var('TWC_CONV', 'kg m-3', 'LWC+IWC from detr.+ cloud weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'twc_conv', field%twc_conv,                                &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('CONV_TIME', '1', 'acc. cloud occ. conv time fraction', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'conv_time', field%conv_time,                              &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('CDNCACT_CV', 'm-3', 'activated CDNC in conv', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdncact_cv', field%cdncact_cv,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('NA_CV', 'm-3', 'aerosols for nucleation in conv', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'na_cv', field%na_cv,                                      &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

  END SUBROUTINE new_conv_field_list

END MODULE mo_conv


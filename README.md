README file for HAMMOZ
======================

Sylvaine Ferrachat, ETH Zurich, 2021-07

#------------------------------------------------------
## IMPORTANT

This code corresponds to HAM, as inherited from echam6.3.0-ham2.3-moz1.0
(https://redmine.hammoz.ethz.ch/projects/hammoz/wiki/Echam630-ham23-moz10) and
further adapted for ICON.

In future, this code should become host-model-agnostic. This is not yet the case.

In order to know which icon version is functional with the current specific HAM
version of this branch, please check the file `host_model_version.md`.


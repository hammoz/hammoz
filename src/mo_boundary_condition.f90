!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz, MPI fuer Meteorologie, FZJ 
!>
!! @par Copyright
!! This code is subject to the MPI-M-Software - License - Agreement in it's most recent form.
!! Please see URL http://www.mpimet.mpg.de/en/science/models/model-distribution.html and the
!! file COPYING in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the headers of the routines.
!!
!! handling of boundary conditions
!!
!! concepts of the routines:
!! see document: "ECHAM6 Boundary condition scheme" by M. G. Schultz, S. Schroeder, et al. - August 2009
!! see also: http://hammoz.icg.fz-juelich.de/data/BoundaryConditions
!!
!! @author S. Schroeder, FZ-Juelich
!!
!! $Id: 1423$
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
!! @par Copyright
!! 2009 by MPI-M and FZJ
!! This software is provided for non-commercial use only.
!!
MODULE mo_boundary_condition
  USE mo_kind,                     ONLY: dp
  USE mo_external_field_processor, ONLY: external_field, EF_INACTIVE, &
                                         EF_UNDEFINED, EF_TIMERESOLVED, &
                                         EF_NOINTER
  USE mo_external_field_processor, ONLY: EF_INACTIVE, &
                                         EF_UNDEFINED, EF_TIMERESOLVED, &
                                         EF_NOINTER

  USE mo_linked_list,              ONLY: t_var_list
  USE mo_fortran_tools,            ONLY: t_ptr_2d3d
  USE mo_impl_constants,           ONLY: max_dom

  IMPLICIT NONE

  PRIVATE

! flags of bc_domain

  INTEGER, PARAMETER :: BC_EVERYWHERE = 0
  INTEGER, PARAMETER :: BC_BOTTOM     = 1
  INTEGER, PARAMETER :: BC_TOP        = 2
  INTEGER, PARAMETER :: BC_LEVEL      = 3
  INTEGER, PARAMETER :: BC_ALTITUDE   = 4
  INTEGER, PARAMETER :: BC_PRESSURE   = 5

! flags of bc_mode

  INTEGER, PARAMETER :: BC_REPLACE  = 1
  INTEGER, PARAMETER :: BC_ADD      = 2
  INTEGER, PARAMETER :: BC_RELAX    = 3
  INTEGER, PARAMETER :: BC_SPECIAL  = 4

! flags of bc_vertint

  INTEGER, PARAMETER :: BC_VERTICAL_NONE                   = 0
  INTEGER, PARAMETER :: BC_VERTICAL_INTERPOLATION          = 1
  INTEGER, PARAMETER :: BC_VERTICAL_WEIGHTED_INTERPOLATION = 2
  INTEGER, PARAMETER :: BC_VERTICAL_INTEGRATION            = 3

  CHARACTER(len=*), PARAMETER :: thismodule = 'mo_boundary_conditions'

  TYPE, PUBLIC :: bc_nml
  ! external field properties:
    ! source of the boundary condition
    INTEGER               :: ef_type = EF_INACTIVE   
                             ! possible values:
                             ! EF_INACTIVE = 0
                             ! EF_VALUE = 1 
                             ! EF_FILE = 2
                             ! EF_MODULE = 3
    ! information for type=ef_file
    ! template for filename. Example: bc_ch4.%Y4.%T0.nc
    CHARACTER(LEN=512)    :: ef_template = ''
    ! variable name in file
    CHARACTER(LEN=512)    :: ef_varname= ''
    ! geometry of the file variable
    INTEGER               :: ef_geometry = EF_UNDEFINED
                             ! possible values:
                             ! EF_UNDEFINED = 0
                             ! EF_3D        = 1
                             ! EF_LONLAT    = 2
                             ! EF_LEV       = 3
                             ! EF_SINGLE    = 4
    ! definition of the time values in the file
    INTEGER               :: ef_timedef = EF_TIMERESOLVED  
                             ! possible values:
                             ! EF_TIMERESOLVED = 1
                             ! EF_IGNOREYEAR   = 2
                             ! EF_CONSTANT     = 3
    ! offset to time values (e.g. to shift from mid-month to 1st day)
    REAL(dp)              :: ef_timeoffset = 0.0_dp  ! (time unit)
    ! fixed record number to select a specific time in the file
    INTEGER               :: ef_timeindex = 0
    ! time interpolation
    INTEGER               :: ef_interpolate = EF_NOINTER
                             ! possible values:
                             ! EF_NOINTER = 0
                             ! EF_LINEAR  = 1
                             ! EF_CUBIC   = 2
    ! scaling factor
    REAL(dp)              :: ef_factor = 1.0_dp 
    ! actual unit string in netcdf file
    CHARACTER(LEN=30)     :: ef_actual_unit = ''
    ! information for type=ef_value
    ! globally uniform boundary condition value
    REAL(dp)              :: ef_value       = 0.0_dp
  ! define application of boundary condition
    ! (vertical) domain where field should be applied
    INTEGER              :: bc_domain       = BC_EVERYWHERE
                             ! possible values:
                             ! BC_EVERYWHERE = 0
                             ! BC_BOTTOM     = 1
                             ! BC_TOP        = 2
                             ! BC_LEVEL      = 3
                             ! BC_ALTITUDE   = 4
                             ! BC_PRESSURE   = 5
    ! minimum and maximum model level where field shall be applied
    INTEGER              :: bc_minlev       = -1
    INTEGER              :: bc_maxlev       = 10000
    ! mode of application
    INTEGER              :: bc_mode         = BC_REPLACE
                             ! possible values:
                             ! BC_REPLACE  = 1
                             ! BC_ADD      = 2
                             ! BC_RELAX    = 3
                             ! BC_SPECIAL  = 4
    ! relaxation time for mode = bc_relax
    REAL(dp)             :: bc_relaxtime    = 0._dp
    ! number of vertical levels in input file
    INTEGER              :: ef_nlev = -1
  END TYPE bc_nml

  PUBLIC :: bc_list_read
  PUBLIC :: bc_define
  PUBLIC :: bc_set
  PUBLIC :: bc_apply
  PUBLIC :: bc_query
  PUBLIC :: bc_modify      ! ++mgs 2010-02-25
  PUBLIC :: bc_find
  PUBLIC :: p_bcast_bc
  PUBLIC :: start_boundary_condition
  PUBLIC :: bc_lists_init  

! the following three lines are not supported via ICON style rules...

  PUBLIC :: BC_EVERYWHERE, BC_BOTTOM, BC_TOP, BC_LEVEL, BC_ALTITUDE, BC_PRESSURE                                     ! flags of bc_domain
  PUBLIC :: BC_REPLACE, BC_ADD, BC_RELAX, BC_SPECIAL                                                                 ! flags of bc_mode
  PUBLIC :: BC_VERTICAL_NONE, BC_VERTICAL_INTERPOLATION, BC_VERTICAL_WEIGHTED_INTERPOLATION, BC_VERTICAL_INTEGRATION ! flags of bc_vertint

  INTEGER, PARAMETER             :: MAXNBC = 800 ! Scarlet: for heterogeneous chemistry coupling with SALSA

  TYPE :: boundary_condition
    CHARACTER(LEN=128)   :: bc_name                        = ''             ! just for logging interests
    TYPE(external_field) :: bc_ef
    INTEGER              :: bc_domain                      = BC_EVERYWHERE
    INTEGER              :: bc_minlev                      = -1
    INTEGER              :: bc_maxlev                      = -1
    INTEGER              :: bc_mode                        = BC_REPLACE
    INTEGER              :: bc_ndim                        = -1
    INTEGER              :: bc_vertint                     = BC_VERTICAL_NONE
    REAL(dp)             :: bc_relaxtime                   = 0._dp
    TYPE (t_ptr_2d3d)    :: bc_values_pointer
    TYPE (t_ptr_2d3d)    :: bc_values_pointer_prior
    TYPE (t_ptr_2d3d)    :: bc_values_pointer_later
    LOGICAL              :: bc_ldefined                    = .false.
    LOGICAL              :: bc_lfirst                      = .TRUE.
  END TYPE boundary_condition

  TYPE boundary_conditions ! for later when there is more than one domain
     TYPE(boundary_condition) :: bc_list(MAXNBC) 
  END TYPE boundary_conditions 

  TYPE(boundary_conditions), ALLOCATABLE, TARGET :: bc_lists(:) !< shape: (n_dom)

  TYPE (t_var_list), ALLOCATABLE :: bc_stream(:)  !< shape: (n_dom)
  TYPE (t_var_list), ALLOCATABLE :: bc_stream_prior(:)  !< shape: (n_dom)
  TYPE (t_var_list), ALLOCATABLE :: bc_stream_later(:)  !< shape: (n_dom)

  INTEGER, SAVE                  :: nbc = 0

  LOGICAL, SAVE, DIMENSION(max_dom)    :: lfirst = .TRUE. ! cms needs to be changed for multiple grids 
  LOGICAL, SAVE, DIMENSION(max_dom)    :: called_bc_lists_init = .FALSE.

  INTERFACE bc_set
    MODULE PROCEDURE bc_set1d
    MODULE PROCEDURE bc_set2d
  END INTERFACE

  INTERFACE bc_apply
    MODULE PROCEDURE bc_apply1d
    MODULE PROCEDURE bc_apply2d
  END INTERFACE

  ! subprograms

  CONTAINS

!-----------------------------------------------------------------------
!>
!! expand newly read external field (0d = single) to boundary condition dimension needed
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!! adapted for icon by Marc Salzmann LIM (2017-07-18)
  SUBROUTINE bc_read_0d(bc, jbc)

    USE mo_mpi,                      ONLY: my_process_is_mpi_workroot, p_pe, p_io, p_bcast, p_comm_work
    USE mo_external_field_processor, ONLY: ef_read_single
    TYPE(boundary_conditions), POINTER    :: bc
    INTEGER,  INTENT(in) :: jbc
    REAL(dp) :: val  

     IF ( p_pe == p_io ) THEN
       CALL ef_read_single (bc%bc_list(jbc)%bc_ef, val)
     END IF

    CALL p_bcast(val, p_io, p_comm_work)

    SELECT CASE(bc%bc_list(jbc)%bc_domain)
      CASE (BC_BOTTOM, BC_TOP)
        bc%bc_list(jbc)%bc_values_pointer%p_2d(:,:) = val
      CASE (BC_LEVEL)
        bc%bc_list(jbc)%bc_values_pointer%p_3d(:,:,:) = val  
    END SELECT

  END SUBROUTINE bc_read_0d

!cms previously ef_read_2d

  SUBROUTINE bc_read_2d( p_patch, jbc, bc )

  USE mo_model_domain,              ONLY: t_patch
  USE mo_kind,                      ONLY: dp
  USE mo_exception,                 ONLY: finish, message, em_info
  USE mo_external_field_processor,  ONLY: EF_LONLAT
  USE mo_read_interface,            ONLY: openInputFile, closeFile, read_2D_time, &
    &                                     t_stream_id, on_cells
  USE mo_io_config,                 ONLY: default_read_method

  TYPE(t_patch),                           INTENT( IN )     :: p_patch 
  INTEGER,                                 INTENT( IN )     :: jbc
  TYPE ( boundary_conditions ), POINTER,   INTENT( INOUT )  :: bc
 
  ! local vars
  TYPE ( external_field ) :: efield
  TYPE(t_stream_id) :: stream_id

  REAL(dp), POINTER     :: p3d(:,:,:)

  efield = bc%bc_list(jbc)%bc_ef

  IF (efield%ef_geometry .eq. EF_LONLAT) THEN

    stream_id = openInputFile(efield%ef_file, p_patch, default_read_method)

    CALL read_2D_time( stream_id=stream_id, location=on_cells,    &
                & variable_name=trim(efield%ef_varname),     &
                & return_pointer=p3d, &
                & start_timestep=efield%ef_timeindex, end_timestep=efield%ef_timeindex    )

    bc%bc_list(jbc)%bc_values_pointer%p_2d(:,:)=p3d(:,:,1)
   
    DEALLOCATE(p3d)

    CALL closeFile(stream_id)

  ELSE
   !cms in case this needs to be supported: this used to be  ef_read_2d
   CALL finish(thismodule,"bc_read_2d - unsupported geometry")
  ENDIF
  bc%bc_list(jbc)%bc_values_pointer%p_2d(:,:)=bc%bc_list(jbc)%bc_values_pointer%p_2d(:,:)*efield%ef_factor
  CALL message('ef_read_2d', 'new 2d-field read for variable '//TRIM(efield%ef_varname) &
               //' from file '//TRIM(efield%ef_file), level=em_info)

  END SUBROUTINE bc_read_2d

!cms  previously ef_read_3d

  SUBROUTINE bc_read_3d( jg, p_patch, jbc, bc, lispressure, llevel )

  USE mo_model_domain,              ONLY: t_patch
  USE mo_kind,                      ONLY: dp
  USE mo_exception,                 ONLY: finish, message, em_info, message_text
  USE mo_external_field_processor,  ONLY: ef_get_levname_and_size, EF_3D,  &
                                       &  ef_get_full_and_half_level_information
  USE mo_read_interface,            ONLY: openInputFile, closeFile, read_3D_time, &
    &                                     t_stream_id, on_cells
  USE mo_io_config,                 ONLY: default_read_method

  INTEGER,                                 INTENT( IN )     :: jg  ! domain/grid index
  TYPE(t_patch),                           INTENT( IN )     :: p_patch 
  INTEGER,                                 INTENT( IN )     :: jbc
  TYPE ( boundary_conditions ), POINTER,   INTENT( INOUT )  :: bc
  LOGICAL, OPTIONAL,                       INTENT( OUT )    :: lispressure, llevel

  ! local vars
  TYPE ( external_field ), POINTER :: efield
  CHARACTER(len=5)        :: clevname
  INTEGER                 :: nzval, ilev, ii
  TYPE(t_stream_id)       :: stream_id
  CHARACTER(LEN=4)        :: cnzval
  REAL(dp), POINTER       :: p4d(:,:,:,:)
  LOGICAL                 :: lreverse, lpres, llev


  efield => bc%bc_list(jbc)%bc_ef
 
  CALL ef_get_levname_and_size(efield%ef_file,clevname,nzval)

  IF ( bc%bc_list(jbc)%bc_ef%ef_nzval /= nzval ) THEN
    WRITE(cnzval,FMT='(I3)') nzval
    message_text="Specify bc%ef_nlev (or nlev in emi_spec.txt) for reading "//efield%ef_file//"  "//cnzval
    CALL finish(thismodule, message_text)
  END IF 

  IF (efield%ef_geometry .eq. EF_3D) THEN

    stream_id = openInputFile(efield%ef_file, p_patch, default_read_method)

    CALL read_3D_time( stream_id=stream_id, location=on_cells,    &
                & variable_name=trim(efield%ef_varname),     &
                & return_pointer=p4d, &
                & start_timestep=efield%ef_timeindex, end_timestep=efield%ef_timeindex    )


    CALL closeFile(stream_id)

    p4d(:,:,:,:) = efield%ef_factor * p4d(:,:,:,:)

    CALL ef_get_full_and_half_level_information(jg, p_patch, efield,nzval,clevname,lreverse,lpres,llev)

    IF (lreverse) THEN
      DO ilev = 1, nzval
        ii = nzval - ilev + 1
        bc%bc_list(jbc)%bc_values_pointer%p_3d(:,ilev,:) = p4d(:, ii,:,1)
      END DO
    ELSE
      bc%bc_list(jbc)%bc_values_pointer%p_3d(:,:,:) = p4d(:,:,:,1)
    END IF

    DEALLOCATE(p4d)

  ELSE
    CALL finish(thismodule,"bc_read_3d - unsupported geometry")
  ENDIF
 
  IF ( PRESENT( lispressure ) ) THEN
    lispressure = lpres
  END IF
  IF ( PRESENT( llevel ) ) THEN
    llevel = llev
  END IF

  CALL message('ef_read_3d', 'new 3d-field read for variable '//TRIM(efield%ef_varname) &
               //' from file '//TRIM(efield%ef_file), level=em_info)

  END SUBROUTINE bc_read_3d

!!$
!!$!>
!!$!! expand newly read external field to boundary condition dimension needed
!!$!!
!!$!! Description?
!!$!!
!!$!! @par Revision History
!!$!! code implementation by S. Schroeder (2009-08-25)
!!$!!
!!$  SUBROUTINE bc_expand1d(jbc)
!!$  USE mo_control,                  ONLY: nlon, nlev, ngl
!!$  USE mo_external_field_processor, ONLY: EF_LAT, EF_LEV, value1d
!!$  USE mo_exception,                ONLY: message, em_error
!!$
!!$  INTEGER,              INTENT(in) :: jbc
!!$
!!$  INTEGER  :: ilat, ilev
!!$
!!$  SELECT CASE(bc_list(jbc)%bc_ef%ef_geometry)
!!$  CASE (EF_LAT)
!!$    IF (bc_list(jbc)%bc_domain == BC_LEVEL) THEN
!!$      CALL message ('bc_expand', TRIM(bc_list(jbc)%bc_name)//' bc_domain=BC_LEVEL && ef_geometry=EF_LAT not possible!', &
!!$                    level=em_error)
!!$    ELSE
!!$      ALLOCATE(tmp_glob_values2d(nlon,ngl))
!!$      DO ilat = 1, ngl
!!$        tmp_glob_values2d(:,ilat) = value1d(ilat)
!!$      END DO
!!$    ENDIF
!!$  CASE (EF_LEV)
!!$    ALLOCATE(tmp_glob_values3d(nlon,nlev,ngl))
!!$    DO ilev = 1, nlev
!!$      tmp_glob_values3d(:,ilev,:) = value1d(ilev)
!!$    END DO
!!$  END SELECT
!!$  END SUBROUTINE bc_expand1d
!!$
!!$!>
!!$!! expand newly read external field to boundary condition dimension needed
!!$!!
!!$!! Description?
!!$!!
!!$!! @par Revision History
!!$!! code implementation by S. Schroeder (2009-08-25)
!!$!!
!!$  SUBROUTINE bc_expand2d(jbc)
!!$  USE mo_control,                  ONLY: nlon, nlev, ngl
!!$  USE mo_external_field_processor, ONLY: EF_LONLAT, EF_LATLEV, value2d
!!$
!!$  INTEGER,              INTENT(in) :: jbc
!!$
!!$  INTEGER  :: ilev, ilon
!!$
!!$  IF (bc_list(jbc)%bc_ndim == 2) THEN
!!$    ALLOCATE(tmp_glob_values2d(nlon,ngl))
!!$  ELSE
!!$    ALLOCATE(tmp_glob_values3d(nlon,bc_list(jbc)%bc_ef%ef_nzval,ngl))
!!$  ENDIF
!!$  SELECT CASE(bc_list(jbc)%bc_domain)
!!$  CASE (BC_BOTTOM)
!!$    IF (bc_list(jbc)%bc_ndim == 2) THEN
!!$      tmp_glob_values2d(:,:) = value2d(:,:)
!!$    ELSE
!!$      tmp_glob_values3d(:,1,:) = value2d(:,:)
!!$    ENDIF
!!$  CASE (BC_TOP)
!!$    IF (bc_list(jbc)%bc_ndim == 2) THEN
!!$      tmp_glob_values2d(:,:) = value2d(:,:)
!!$    ELSE
!!$      tmp_glob_values3d(:,nlev,:) = value2d(:,:)
!!$    ENDIF
!!$  CASE (BC_LEVEL)
!!$    SELECT CASE (bc_list(jbc)%bc_ef%ef_geometry)
!!$    CASE (EF_LONLAT)
!!$      DO ilev = bc_list(jbc)%bc_minlev, bc_list(jbc)%bc_maxlev
!!$        tmp_glob_values3d(:,ilev,:) = value2d(:,:)
!!$      END DO
!!$    CASE (EF_LATLEV)
!!$      DO ilon = 1, nlon
!!$        DO ilev = bc_list(jbc)%bc_minlev, bc_list(jbc)%bc_maxlev
!!$          tmp_glob_values3d(ilon,ilev,:) = value2d(:,ilev)
!!$        END DO
!!$      END DO
!!$    END SELECT
!!$  CASE (BC_EVERYWHERE)
!!$    IF (bc_list(jbc)%bc_ndim == 2) THEN
!!$      tmp_glob_values2d(:,:) = value2d(:,:)
!!$    ELSE
!!$      SELECT CASE (bc_list(jbc)%bc_ef%ef_geometry)
!!$      CASE (EF_LONLAT)
!!$        DO ilev = 1, nlev
!!$          tmp_glob_values3d(:,ilev,:) = value2d(:,:)
!!$        END DO
!!$      CASE (EF_LATLEV)
!!$        DO ilon = 1, nlon
!!$          tmp_glob_values3d(ilon,:,:) = TRANSPOSE(value2d(:,:))
!!$        END DO
!!$      END SELECT
!!$    ENDIF
!!$  END SELECT
!!$  END SUBROUTINE bc_expand2d
!!$
!!$!>
!!$!! expand newly read external field to boundary condition dimension needed
!!$!!
!!$!! Description?
!!$!!  
!!$!! @par Revision History
!!$!! code implementation by S. Schroeder (2009-08-25)
!!$!!
!!$  SUBROUTINE bc_expand3d(jbc)
!!$  USE mo_control,                  ONLY: nlon, ngl
!!$  USE mo_external_field_processor, ONLY: value3d
!!$
!!$  INTEGER,              INTENT(in) :: jbc
!!$
!!$  INTEGER :: ilev, nzval
!!$
!!$  nzval = bc_list(jbc)%bc_ef%ef_nzval
!!$  ALLOCATE(tmp_glob_values3d(nlon,nzval,ngl))
!!$  DO ilev = 1,nzval
!!$    tmp_glob_values3d(:,ilev,:) = value3d(:,:,ilev)
!!$  END DO
!!$  END SUBROUTINE bc_expand3d
!!$

  FUNCTION bc_timefac(jbc, bc, current_date) RESULT(timefac)
    ! this is currently called for every single block ...
    USE mtime,                       ONLY: datetime, getTotalMilliSecondsTimeDelta, &
                                           timeDelta, newTimedelta, deallocateTimedelta, &
                                           getTimeDeltaFromDateTime
    USE mo_time_conversion,                ONLY: write_date
    INTEGER,  INTENT(in)  :: jbc
    TYPE(boundary_conditions), POINTER    :: bc
    TYPE(datetime), POINTER :: current_date

    REAL(dp) :: timefac

    ! local vars
    TYPE(datetime), POINTER   :: ztime1, ztime2
    TYPE(timeDelta), POINTER  :: delta_mtime_input,  delta_mtime_elapsed_since_last_input
    REAL(dp) :: delta_t_input,  delta_t_elapsed_since_last_input

    delta_mtime_input  => newTimedelta("PT0S")
    delta_mtime_elapsed_since_last_input =>  newTimedelta("PT0S")

    ztime1 => bc%bc_list(jbc)%bc_ef%ef_times_prior(bc%bc_list(jbc)%bc_ef%ef_timeindex_prior)%datetime
    ztime2 => bc%bc_list(jbc)%bc_ef%ef_times(bc%bc_list(jbc)%bc_ef%ef_timeindex)%datetime

    delta_mtime_input = getTimeDeltaFromDateTime(ztime2,ztime1 ) 
    delta_mtime_elapsed_since_last_input=getTimeDeltaFromDateTime(current_date,ztime1 ) 

    delta_t_input = getTotalMilliSecondsTimeDelta(delta_mtime_input, current_date)
    delta_t_elapsed_since_last_input = getTotalMilliSecondsTimeDelta(delta_mtime_elapsed_since_last_input, current_date)

    CALL deallocateTimedelta(delta_mtime_input)
    CALL deallocateTimedelta(delta_mtime_elapsed_since_last_input)

    timefac= delta_t_elapsed_since_last_input / delta_t_input

  END FUNCTION bc_timefac

  SUBROUTINE bc_in_geom(jg, p_patch, bc, jbc)
!!$  USE mo_control,                  ONLY: nlev
  USE mo_model_domain,             ONLY: t_patch
  USE mo_external_field_processor, ONLY: EF_SINGLE, EF_LEV, EF_LONLAT, EF_3D !, &
                                        !cms ef_read_single !cms, ef_read_1d, ef_read_2d, ef_read_3D, &
!!$                                         value1d, value2d, value3d
  USE mo_mpi,                      ONLY: my_process_is_mpi_workroot, p_pe, p_io, p_bcast, p_comm_work

  INTEGER,                             INTENT( IN ) :: jg  ! domain/grid index
  TYPE(t_patch),                       INTENT( IN ) :: p_patch 
  TYPE(boundary_conditions), POINTER                :: bc


  INTEGER, INTENT(in)    :: jbc
  REAL(dp)               :: value
  LOGICAL                :: lispressure, llevel

  SELECT CASE(bc%bc_list(jbc)%bc_ef%ef_geometry)
  CASE (EF_SINGLE)
    CALL bc_read_0d(bc, jbc)
  CASE (EF_LEV)
    
    write(0,*) "AAAAAAAFFFFFFFFFFF" 
    stop 554
!!$    CALL ef_read_1d (bc_list(jbc)%bc_ef, lispressure, llevel)
!!$! this is the first time to know about bc_domain (BC_ALTITUDE || BC_PRESSURE)
!!$    IF (llevel) THEN
!!$      bc_list(jbc)%bc_domain = BC_LEVEL
!!$      bc_list(jbc)%bc_minlev = 1
!!$      bc_list(jbc)%bc_maxlev = nlev
!!$    ELSE
!!$      IF (lispressure) THEN
!!$        bc_list(jbc)%bc_domain = BC_PRESSURE
!!$      ELSE
!!$        bc_list(jbc)%bc_domain = BC_ALTITUDE
!!$      ENDIF
!!$    ENDIF
!!$    CALL bc_expand1d(jbc)
!!$    DEALLOCATE(value1d)
  CASE (EF_LONLAT)
    CALL bc_read_2d (p_patch, jbc, bc) ! formerly ef_read_2d
  CASE (EF_3D)
    CALL bc_read_3d (jg, p_patch, jbc, bc, lispressure, llevel) 

! this is the first time to know about bc_domain (BC_ALTITUDE || BC_PRESSURE)
    IF (llevel) THEN
      bc%bc_list(jbc)%bc_domain = BC_LEVEL
      bc%bc_list(jbc)%bc_minlev = 1
      bc%bc_list(jbc)%bc_maxlev = p_patch%nlev
    ELSE
      IF (bc%bc_list(jbc)%bc_domain == BC_EVERYWHERE) THEN
        bc%bc_list(jbc)%bc_minlev = 1
        bc%bc_list(jbc)%bc_maxlev = p_patch%nlev
! if bc_domain = BC_PRESSURE/BC_ALTITUDE, bc_minlev/bc_maxlev have to be calculated
! (not yet ready)!
      ENDIF
      IF (lispressure) THEN
        bc%bc_list(jbc)%bc_domain = BC_PRESSURE
      ELSE
        bc%bc_list(jbc)%bc_domain = BC_ALTITUDE
      ENDIF
    ENDIF

  END SELECT
  END SUBROUTINE bc_in_geom

  SUBROUTINE start_boundary_condition( p_patch )

    USE mo_model_domain,         ONLY: t_patch   
    USE mo_impl_constants,       ONLY: SUCCESS 
    USE mo_exception,            ONLY: finish

    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch
    INTEGER :: ndomain, ist

    ndomain = SIZE(p_patch)
    ALLOCATE( bc_lists(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of bc_lists failed')   

  END SUBROUTINE start_boundary_condition
!>
!! 
!! call new_bc_list (create variable lists and allocate arrays for boundary conditions)
!!
!! @par Revision History
!!  initial implementation by Marc Salzmann (LIM) (2017-07-12)
!!
  SUBROUTINE bc_lists_init( p_patch )
    
    USE mo_model_domain,         ONLY: t_patch   
    USE mo_exception,            ONLY: message, finish
    USE mo_impl_constants,       ONLY: SUCCESS, MAX_CHAR_LENGTH
    USE mo_parallel_config,      ONLY: nproma

    IMPLICIT NONE
    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    CHARACTER(len=MAX_CHAR_LENGTH) :: listname(3)
    INTEGER :: ndomain, jg, ist, nblks, nlev 

    CALL message(TRIM(thismodule),'Construction of HAMMOZ BCs started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( bc_stream(ndomain), bc_stream_prior(ndomain), bc_stream_later(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of bc streams failed')

    DO jg = 1,ndomain
      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      WRITE(listname(1),'(a,i2.2)') 'hammoz_boundary_condition_of_domain',jg
      WRITE(listname(2),'(a,i2.2)') 'hammoz_boundary_condition_prior_of_domain',jg
      WRITE(listname(3),'(a,i2.2)') 'hammoz_boundary_condition_later_of_domain',jg

      CALL new_bc_list( jg, nproma, nlev, nblks, listname,                         &
                     &   bc_stream(jg), bc_stream_prior(jg), bc_stream_later(jg),  &
                     &   bc_lists(jg)                                              )

    END DO

    called_bc_lists_init(jg) = .TRUE.

  END  SUBROUTINE bc_lists_init
!>
!! 
!! create variable lists and allocate arrays for boundary conditions
!!
!! @par Revision History
!!  based on code by S. Schroeder - adapted for icon by  Marc Salzmann (LIM) (2017-07-12)
!!
  SUBROUTINE new_bc_list(  k_jg, kproma, klev, kblks, listname,           &
                       &   bc_stream, bc_stream_prior, bc_stream_later,   &
                       &   bc                                             )
 
    USE mo_var_list,                 ONLY: default_var_list_settings, &
      &                                    add_var, add_ref,          &
      &                                    new_var_list

    USE mo_cf_convention,            ONLY: t_cf_var
    USE mo_grib2,                    ONLY: t_grib2_var, grib2_var
    USE mo_cdi,                      ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
      &                                    DATATYPE_FLT32,  DATATYPE_FLT64,   &
      &                                    GRID_UNSTRUCTURED,                 &
      &                                    TSTEP_INSTANT, TSTEP_AVG,          &
      &                                    cdiDefMissval
    USE mo_cdi_constants,            ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
      &                                    ZA_HYBRID, ZA_HYBRID_HALF,         &
      &                                    ZA_SURFACE
    USE mo_io_config,                ONLY: lnetcdf_flt64_output


    USE mo_external_field_processor, ONLY: EF_FILE, EF_VALUE, EF_CONSTANT

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev, kblks     !< dimension sizes

    CHARACTER(len=*),  INTENT(IN) :: listname(3)

    TYPE(t_var_list),  INTENT(INOUT) :: bc_stream, bc_stream_prior, bc_stream_later
    TYPE(boundary_conditions), INTENT(INOUT) :: bc

  ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc


    INTEGER :: jbc
    CHARACTER(len=5)       :: cbc = "bc000"

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)


    CALL new_var_list( bc_stream, TRIM(listname(1)), patch_id=k_jg )
    CALL new_var_list( bc_stream_prior, TRIM(listname(2)), patch_id=k_jg )
    CALL new_var_list( bc_stream_later, TRIM(listname(3)), patch_id=k_jg )

    DO jbc = 1, nbc
      IF ( bc%bc_list(jbc)%bc_lfirst ) THEN
        write(cbc(3:5),'(i3.3)') jbc
        IF (bc%bc_list(jbc)%bc_ndim == 2) THEN ! time interpolation is only done for type EF_FILE (ef_interpolate)
          cf_desc    = t_cf_var(cbc, 'unknown', TRIM(bc%bc_list(jbc)%bc_name), datatype_flt)
          grib2_desc = grib2_var(255, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( bc_stream, cbc,  bc%bc_list(jbc)%bc_values_pointer%p_2d,                    &
                       & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,    &
                       & lrestart=.FALSE., loutput=.FALSE. )

                              
          IF ( bc%bc_list(jbc)%bc_ef%ef_type == EF_FILE .AND. bc%bc_list(jbc)%bc_ef%ef_interpolate /= EF_NOINTER) THEN
            CALL add_var( bc_stream_prior, cbc,  bc%bc_list(jbc)%bc_values_pointer_prior%p_2d,       &
                         & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                         & lrestart=.FALSE., loutput=.FALSE. )

            CALL add_var( bc_stream_later, cbc,  bc%bc_list(jbc)%bc_values_pointer_later%p_2d,       &
                         & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                         & lrestart=.FALSE., loutput=.FALSE. )
          END IF


        ELSE
            shape3d  = (/kproma, bc%bc_list(jbc)%bc_ef%ef_nzval , kblks/)

             cf_desc    = t_cf_var(cbc, 'unknown', TRIM(bc%bc_list(jbc)%bc_name), datatype_flt)
             grib2_desc = grib2_var(255, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
             CALL add_var( bc_stream, cbc, bc%bc_list(jbc)%bc_values_pointer%p_3d,                    &
                         & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d,     &
                         & lrestart=.FALSE., loutput=.FALSE. )

             IF ( bc%bc_list(jbc)%bc_ef%ef_type == EF_FILE .AND. bc%bc_list(jbc)%bc_ef%ef_interpolate /= EF_NOINTER) THEN
               CALL add_var( bc_stream_prior, cbc, bc%bc_list(jbc)%bc_values_pointer_prior%p_3d,       &
                            & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d,   &
                            & lrestart=.FALSE., loutput=.FALSE. )
               CALL add_var( bc_stream_later, cbc, bc%bc_list(jbc)%bc_values_pointer_later%p_3d,       &
                            & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d,   &
                            & lrestart=.FALSE., loutput=.FALSE. )
             END IF
        END IF
      bc%bc_list(jbc)%bc_lfirst = .FALSE.
      END IF
    END DO  

  END SUBROUTINE new_bc_list

!>
!! update all boundary conditions (if needed: read new ones from external fields)
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE bc_list_read( jg, p_patch, current_date )
   
    USE mo_model_domain,             ONLY: t_patch
    USE mo_mpi,                      ONLY: p_pe, my_process_is_mpi_workroot
    USE mo_external_field_processor, ONLY: EF_FILE, EF_VALUE, EF_CONSTANT, ef_get_first_timestep, ef_get_next_timestep
    USE mo_exception,                ONLY: finish, number_of_errors
    USE mo_time_conversion,               ONLY: datetimes, write_date ! datetimes is datetime array
    USE mtime,                      ONLY: datetime, OPERATOR(==), OPERATOR(>)

    INTEGER,                  INTENT( IN ) :: jg  ! domain/grid index
    TYPE(t_patch),            INTENT( IN ) :: p_patch    
    TYPE(datetime),  POINTER, INTENT( IN ) :: current_date

    TYPE(boundary_conditions), POINTER    :: bc
    INTEGER                :: jbc, ntimes, ntimes_prior, i
    TYPE(datetime)         :: znexttime
    LOGICAL                :: lread

    bc=>bc_lists(jg)

    DO jbc = 1, nbc
      IF ( bc%bc_list(jbc)%bc_ef%ef_type == EF_FILE ) THEN 
        lread = .FALSE.

        IF (lfirst(jg)) THEN
          ! in the special case that the data for the first model timestep is in the "previous" file,
          ! the routine must know about lfirst
          ! handling of first and next timestep is slightly different -- call special routine for first
          ! timestep (only once called per bc), so ef_get_next_timestep (called frequently during model run)
          ! hasn't to deal with lfirst
          CALL ef_get_first_timestep(current_date, jg, p_patch, bc%bc_list(jbc)%bc_ef)
        END IF
        IF (bc%bc_list(jbc)%bc_ef%ef_timeindex /= 0) THEN
          znexttime = bc%bc_list(jbc)%bc_ef%ef_times(bc%bc_list(jbc)%bc_ef%ef_timeindex)%datetime

          IF (lfirst(jg) .OR. ((current_date > znexttime) .OR. (current_date == znexttime))) THEN
            lread = .TRUE.
            CALL bc_in_geom(jg ,p_patch, bc, jbc)
            IF (bc%bc_list(jbc)%bc_ef%ef_timedef /= EF_CONSTANT) THEN
              IF (bc%bc_list(jbc)%bc_ef%ef_interpolate /= EF_NOINTER) THEN
                bc%bc_list(jbc)%bc_ef%ef_timeindex_prior = bc%bc_list(jbc)%bc_ef%ef_timeindex
                ntimes_prior = size(bc%bc_list(jbc)%bc_ef%ef_times)
                IF (ALLOCATED(bc%bc_list(jbc)%bc_ef%ef_times_prior)) THEN
                  DEALLOCATE(bc%bc_list(jbc)%bc_ef%ef_times_prior)
                END IF
                ALLOCATE(bc%bc_list(jbc)%bc_ef%ef_times_prior(ntimes_prior))
                bc%bc_list(jbc)%bc_ef%ef_times_prior = bc%bc_list(jbc)%bc_ef%ef_times
              ENDIF
              CALL ef_get_next_timestep(current_date, jg, p_patch, bc%bc_list(jbc)%bc_ef)
              ntimes = size(bc%bc_list(jbc)%bc_ef%ef_times)
            ENDIF
          ENDIF
        ENDIF
        !cms check deallocate for datetime..     

        bc%bc_list(jbc)%bc_ldefined = .true.  !! ignore read errors, because program will stop then anyway...

      END IF
    END DO

    !! ### mgs TEMPORARY CODE ###
    IF (number_of_errors > 0) CALL finish('bc_list_read', 'Abort run because errors were encountered.')
  
    lfirst(jg) = .FALSE.


  END SUBROUTINE bc_list_read

!!$  SUBROUTINE bc_vert_integration(jbc,kproma,jrow,bc_values,values)
!!$! distribute f. ex. number of molecules (number doesn't change in in vertical
!!$! column through integration)
!!$  USE mo_kind,      ONLY: dp
!!$  USE mo_physical_constants, ONLY: grav
!!$  USE mo_vphysc,    ONLY: vphysc
!!$  USE mo_control,   ONLY: nlev
!!$
!!$  INTEGER,               INTENT(IN)    :: jbc,kproma,jrow
!!$  REAL(dp), ALLOCATABLE, INTENT(IN)    :: bc_values(:,:,:)
!!$  REAL(dp), ALLOCATABLE, INTENT(INOUT) :: values(:,:)
!!$
!!$  INTEGER  :: jk, jl, jz, jincr, jzstart, jzstop, jkstart, jkstop
!!$  REAL(dp) :: zgi
!!$  REAL(dp) :: zmha(kproma,nlev+1)
!!$  REAL(dp) :: zfrac, zfdelta, zfdeltai
!!$
!!$! initialize with 0
!!$
!!$  values(:,:) = 0._dp
!!$
!!$  IF (bc_list(jbc)%bc_domain == BC_ALTITUDE) THEN
!!$
!!$! Inverse gravitational acceleration
!!$    zgi = 1._dp/grav
!!$! calculate height of full and half levels in m
!!$    zmha(1:kproma,:) =  vphysc%geohm1(1:kproma,:,jrow) * zgi
!!$    jincr   = -1
!!$! zvalueh (= half levels in height[m] (descending order) of input) has one element more
!!$! than zvalue (which means, it is dimensioned with ef_nzval + 1)
!!$    jzstart = bc_list(jbc)%bc_ef%ef_nzval
!!$    jzstop  = 1
!!$! zmha (= half levels in height[m] (descending order) at actual geolocation) has one element more
!!$! (which means, it is dimensioned with nlev + 1)
!!$    jkstart = bc_list(jbc)%bc_maxlev
!!$! as model has "no" height boundaries geohm1 has an "endless" value at index 1
!!$! ==> therefore stop at index 2 
!!$    jkstop  = bc_list(jbc)%bc_minlev 
!!$    IF (jkstop == 1) jkstop=2
!!$  ELSE
!!$    zmha(1:kproma,:) =  vphysc%aphm1(1:kproma,:,jrow)
!!$    jincr  = 1
!!$    jzstart = 2
!!$    jzstop  = bc_list(jbc)%bc_ef%ef_nzval + 1
!!$    jkstart = bc_list(jbc)%bc_minlev + 1
!!$    jkstop  = bc_list(jbc)%bc_maxlev
!!$  ENDIF
!!$
!!$! Loop over latitudes
!!$
!!$  DO jl=1,kproma
!!$
!!$! Loop over emission file levels        
!!$
!!$    DO jz = jzstart, jzstop, jincr
!!$! Calculate increment of file's half levels
!!$
!!$      zfdelta = bc_list(jbc)%bc_ef%ef_zvalueh(jz) - bc_list(jbc)%bc_ef%ef_zvalueh(jz-jincr)
!!$      zfdeltai = 1._dp / zfdelta
!!$
!!$! Loop over model levels
!!$
!!$      DO jk = jkstart, jkstop, jincr
!!$
!!$! fraction of model level overlapping with file level 
!!$
!!$        zfrac = MAX(0._dp, (MIN(bc_list(jbc)%bc_ef%ef_zvalueh(jz),zmha(jl,jk)) - &
!!$                          MAX(bc_list(jbc)%bc_ef%ef_zvalueh(jz-jincr),zmha(jl,jk-jincr))) * zfdeltai)
!!$
!!$! determine fraction of model level coinciding with file level
!!$
!!$        zfrac = MIN(zfrac, 1._dp)
!!$        values(jl,jk) = values(jl,jk) + zfrac * bc_values(jl,jz,jrow)
!!$
!!$      ENDDO
!!$    ENDDO
!!$  ENDDO
!!$  END SUBROUTINE bc_vert_integration
!!$
!!$
  SUBROUTINE bc_vert_weighted_interpolation(jg, jbc, kproma, jrow, paphm1, pzf, pzh, bc_values, values)
  USE mo_kind,                     ONLY: dp
  USE mo_run_config,               ONLY: nlev

  INTEGER,               INTENT(IN)    :: jg, jbc, kproma, jrow
  REAL(dp),              INTENT(IN)    :: pzf(:,:)    ! kbdim, nlev,   geometric height above sea level, full level  
  REAL(dp),              INTENT(IN)    :: pzh(:,:)    ! kbdim, nlev+1, geometric height above sea level, half level
  REAL(dp),              INTENT(IN)    :: paphm1(:,:) ! kbdim, nlev+1, half level pressure [Pa]
  REAL(dp), ALLOCATABLE, INTENT(IN)    :: bc_values(:,:,:)
  REAL(dp), ALLOCATABLE, INTENT(INOUT) :: values(:,:)

  INTEGER  :: jk, jl, jz, jincr, jzstart, jzstop, jkstart, jkstop
  REAL(dp) :: zmha(kproma,nlev+1)
  REAL(dp) :: zfrac, zmdelta, zmdeltai

  TYPE(boundary_conditions), POINTER    :: bc

  bc=>bc_lists(jg)

  ! initialize with 0
  values(:,:) = 0._dp

  IF (bc%bc_list(jbc)%bc_domain == BC_ALTITUDE) THEN
! height of half levels in m
    zmha(1:kproma,:) =  pzh(1:kproma,:) 
    jincr   = -1
! zvalueh (= half levels in height[m] (descending order) of input) has one element more
! than zvalue (which means, it is dimensioned with ef_nzval + 1)
    jzstart = bc%bc_list(jbc)%bc_ef%ef_nzval
    jzstop  = 1
! zmha (= half levels in height[m] (descending order) at actual geolocation) has one element more
! (which means, it is dimensioned with nlev + 1)
    jkstart = bc%bc_list(jbc)%bc_maxlev
! as model has "no" height boundaries geohm1 has an "endless" value at index 1
! ==> therefore stop at index 2 
    jkstop  = bc%bc_list(jbc)%bc_minlev 
    IF (jkstop == 1) jkstop=2
  ELSE
    zmha(1:kproma,:) =  paphm1(1:kproma,:) !pressure at interfaces
    jincr  = 1
    jzstart = 2
    jzstop  = bc%bc_list(jbc)%bc_ef%ef_nzval + 1
    jkstart = bc%bc_list(jbc)%bc_minlev + 1
    jkstop  = bc%bc_list(jbc)%bc_maxlev
  ENDIF

! Loop over latitudes

  DO jl=1,kproma

! Loop over model levels

    DO jk = jkstart, jkstop, jincr

! Calculate increment of model's half levels

      zmdelta = zmha(jl,jk) - zmha(jl,jk-jincr)
      zmdeltai = 1._dp / zmdelta

! Loop over emission file levels        

      DO jz = jzstart, jzstop, jincr

! fraction of file level overlapping with model level 

        zfrac = MAX(0._dp, (MIN(zmha(jl,jk),bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz)) - &
                          MAX(zmha(jl,jk-jincr),bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz-jincr))) * zmdeltai)

! determine fraction of file level coinciding with model level

        zfrac = MIN(zfrac, 1._dp)
        values(jl,jk) = values(jl,jk) + zfrac * bc_values(jl,jz,jrow)

      ENDDO
    ENDDO
  ENDDO
 
  END SUBROUTINE bc_vert_weighted_interpolation

  SUBROUTINE bc_vert_interpolation(jg, jbc, kproma, jrow, paphm1, pzf, pzh, bc_values, values)
  USE mo_kind,      ONLY: dp
  USE mo_run_config,               ONLY: nlev

  INTEGER,               INTENT(IN)    :: jg,jbc, kproma, jrow
  REAL(dp),              INTENT(IN)    :: pzf(:,:)    ! kbdim, nlev,   geometric height above sea level, full level  
  REAL(dp),              INTENT(IN)    :: pzh(:,:)    ! kbdim, nlev+1, geometric height above sea level, half level
  REAL(dp),              INTENT(IN)    :: paphm1(:,:) ! kbdim, nlev+1, half level pressure [Pa]
  REAL(dp), ALLOCATABLE, INTENT(IN)    :: bc_values(:,:,:)
  REAL(dp), ALLOCATABLE, INTENT(INOUT) :: values(:,:)

  INTEGER  :: jk, jl, jz, jincr, jzstart, jzstop, jkstart, jkstop
  REAL(dp) :: zmha(kproma,nlev+1)
  REAL(dp) :: denom_diff, nom_diff, vertintfac

  TYPE(boundary_conditions), POINTER    :: bc

  bc=>bc_lists(jg)

  values(:,:) = 0._dp

  IF (bc%bc_list(jbc)%bc_domain == BC_ALTITUDE) THEN
! height of half levels in m
    zmha(1:kproma,:) =  pzh(1:kproma,:) 
! zvalueh (= half levels in height[m] (descending order) of input) has one element more
! than zvalue (which means, it is dimensioned with ef_nzval + 1)
    jzstart = bc%bc_list(jbc)%bc_ef%ef_nzval
    jzstop = 1
! zmha (= half levels in height[m] (descending order) at actual geolocation) has one element more
! (which means, it is dimensioned with nlev + 1)
    jkstart = bc%bc_list(jbc)%bc_maxlev
! as model has "no" height boundaries geohm1 has an "endless" value at index 1
! ==> therefore stop at index 2
    jkstop  = bc%bc_list(jbc)%bc_minlev
    IF (jkstop == 1) jkstop=2

! Loop over latitudes

    DO jl=1,kproma

! Index for input levels

      jz = jzstart

! Don't extrapolate --> set upper and lower values to the values of upper and
! lower values found in input file!
! Loop over model levels

      DO jk = jkstart, jkstop, -1
  
        IF (zmha(jl,jk) .ge. bc%bc_list(jbc)%bc_ef%ef_zvalueh(jzstart)) THEN
           values(jl,jk) = bc_values(jl,jzstart,jrow)
        ELSE IF (zmha(jl,jk) .le. bc%bc_list(jbc)%bc_ef%ef_zvalueh(jzstop)) THEN
           values(jl,jk) = bc_values(jl,jzstop,jrow)
        ELSE
          denom_diff   = bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz) - bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz-1)
          nom_diff     = bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz) - zmha(jl,jk)

          IF (nom_diff .ge. 0.0_dp) THEN
            vertintfac   = nom_diff / denom_diff
            values(jl,jk) = bc_values(jl,jz,jrow) + &
                            vertintfac * &
                             (bc_values(jl,jz-1,jrow) &
                            - bc_values(jl,jz,jrow))
          ELSE
            jz = MAX(jz - 1, jzstop)
          ENDIF
        ENDIF
      ENDDO
    ENDDO
  ELSE
    zmha(1:kproma,:) =  paphm1(1:kproma,:)
    jzstart = 2
    jzstop  = bc%bc_list(jbc)%bc_ef%ef_nzval + 1
    jkstart = bc%bc_list(jbc)%bc_minlev + 1
    jkstop  = bc%bc_list(jbc)%bc_maxlev

! Loop over latitudes

    DO jl=1,kproma

! Index for input levels

      jz = jzstart

! Don't extrapolate --> set upper and lower values to the values of upper and
! lower values found in input file!
! Loop over model levels

      DO jk = jkstart, jkstop

        IF (zmha(jl,jk) .le. bc%bc_list(jbc)%bc_ef%ef_zvalueh(jzstart)) THEN
           values(jl,jk) = bc_values(jl,jzstart,jrow)
        ELSE IF (zmha(jl,jk) .ge. bc%bc_list(jbc)%bc_ef%ef_zvalueh(jzstop)) THEN
           values(jl,jk) = bc_values(jl,jzstop,jrow)
        ELSE
          denom_diff   = bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz) - bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz-1)
          nom_diff     = bc%bc_list(jbc)%bc_ef%ef_zvalueh(jz) - zmha(jl,jk)

          IF (nom_diff .ge. 0.0_dp) THEN
            vertintfac   = nom_diff / denom_diff
            values(jl,jk) = bc_values(jl,jz,jrow) + &
                            vertintfac * &
                             (bc_values(jl,jz-1,jrow) &
                            - bc_values(jl,jz,jrow))
          ELSE
            jz = MIN(jz + 1, jzstop)
          ENDIF
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  END SUBROUTINE bc_vert_interpolation
!>
!! interpolate boundary conditions vertically to the levels needed
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE p_bcast_bc (bc_struc, p_source, comm)
  USE mo_mpi, ONLY: p_bcast, p_comm_work   !, p_all_comm

  TYPE(bc_nml),      INTENT(INOUT) :: bc_struc
  INTEGER,           INTENT(in)    :: p_source
  INTEGER, OPTIONAL, INTENT(in)    :: comm

  INTEGER :: p_comm

  IF (PRESENT(comm)) THEN
     p_comm = comm
  ELSE
     p_comm = p_comm_work
  ENDIF

  CALL p_bcast(bc_struc%ef_type,    p_source, p_comm)
  CALL p_bcast(bc_struc%ef_template, p_source, p_comm)
  CALL p_bcast(bc_struc%ef_varname,     p_source, p_comm)
  CALL p_bcast(bc_struc%ef_geometry,     p_source, p_comm)
  CALL p_bcast(bc_struc%ef_timedef,      p_source, p_comm)
  CALL p_bcast(bc_struc%ef_timeoffset,   p_source, p_comm)
  CALL p_bcast(bc_struc%ef_timeindex,    p_source, p_comm)
  CALL p_bcast(bc_struc%ef_interpolate,  p_source, p_comm)
  CALL p_bcast(bc_struc%ef_value,        p_source, p_comm)
  CALL p_bcast(bc_struc%ef_factor,       p_source, p_comm)
  CALL p_bcast(bc_struc%ef_actual_unit,  p_source, p_comm)
  CALL p_bcast(bc_struc%bc_domain,       p_source, p_comm)
  CALL p_bcast(bc_struc%bc_mode,         p_source, p_comm)
  CALL p_bcast(bc_struc%bc_maxlev,       p_source, p_comm)
  CALL p_bcast(bc_struc%bc_minlev,       p_source, p_comm)

  END SUBROUTINE p_bcast_bc

  SUBROUTINE bc_printstat (bc, out)
  USE mo_io_units,                 ONLY: nerr
  USE mo_external_field_processor, ONLY: EF_INACTIVE, EF_VALUE, EF_FILE, EF_MODULE, &
                                         EF_UNDEFINED, EF_3D, EF_LONLAT, EF_LEV, EF_SINGLE, &
                                         EF_TIMERESOLVED, EF_IGNOREYEAR, EF_CONSTANT, &
                                         EF_LINEAR, EF_CUBIC
  USE mo_exception,                ONLY: message, message_text, em_info, em_param

  !! print value of all bc fields ... ###
  !! private routine: called from bc_define if lverbose=.true. (debugging help)

  TYPE(boundary_condition), INTENT(IN) :: bc
  INTEGER, INTENT(in), OPTIONAL        :: out ! choice of output unit. Default: error and log file

  INTEGER           :: iout
  CHARACTER(LEN=24) :: cc, cc2

  IF (PRESENT(out)) THEN
    iout = out 
  ELSE
    iout = nerr
  END IF

  CALL message('','')
  WRITE (message_text, '(a)') 'Boundary condition:'
  CALL message('',message_text,iout, level=em_info)
  SELECT CASE (bc%bc_mode)
    CASE (BC_REPLACE)
      cc = ' BC_REPLACE'
    CASE (BC_ADD)
      cc = ' BC_ADD'
    CASE (BC_RELAX)
      cc = ' BC_RELAX'
    CASE (BC_SPECIAL)
      cc = ' BC_SPECIAL'
  END SELECT
  WRITE (message_text, '(3a,i1,2a)') 'name: ',TRIM(bc%bc_name),', ndims: ',bc%bc_ndim,', mode: ',TRIM(cc)
  CALL message('',message_text,iout,level=em_param)

  SELECT CASE (bc%bc_domain)
    CASE (BC_EVERYWHERE)
      cc = ' BC_EVERYWHERE'
    CASE (BC_BOTTOM)
      cc = ' BC_BOTTOM'
    CASE (BC_TOP)
      cc = ' BC_TOP'
    CASE (BC_LEVEL)
      cc = ' BC_LEVEL'
    CASE (BC_ALTITUDE)
      cc = ' BC_ALTITUDE'
    CASE (BC_PRESSURE)
      cc = ' BC_PRESSURE'
  END SELECT
  WRITE (message_text, '(a,L1,3a,2i4)') 'ldefined: ',bc%bc_ldefined, ', domain: ',TRIM(cc),      &
                ' minlev/maxlev : ',bc%bc_minlev,bc%bc_maxlev
  CALL message('',message_text,iout,level=em_param)
  IF (bc%bc_mode == BC_RELAX) THEN
    WRITE (message_text, '(a,E11.4)') 'relaxtime:', bc%bc_relaxtime
    CALL message('',message_text,iout,level=em_param)
  ENDIF
  SELECT CASE (bc%bc_ef%ef_type)
    CASE (EF_INACTIVE) 
      cc = ' EF_INACTIVE'
    CASE (EF_VALUE)    
      cc = ' EF_VALUE'
    CASE (EF_FILE)     
      cc = ' EF_FILE'
    CASE (EF_MODULE)   
      cc = ' EF_MODULE'
  END SELECT
  WRITE (message_text, '(2a)') 'external field type:', cc
  CALL message('',message_text,iout,level=em_param)

  IF (bc%bc_ef%ef_type == EF_FILE) THEN
    WRITE (message_text, '(2a)') 'ef_template: ', TRIM(bc%bc_ef%ef_template)
    CALL message('',message_text,iout,level=em_param)
    WRITE (message_text, '(5a,e11.4)') 'ef_varname:  ', TRIM(bc%bc_ef%ef_varname),   &
                  ', units: ', TRIM(bc%bc_ef%ef_actual_unit),            &
                  ', conversion factor : ',bc%bc_ef%ef_factor
    CALL message('',message_text,iout,level=em_param)
    SELECT CASE (bc%bc_ef%ef_geometry)
      CASE (EF_UNDEFINED)
        cc = ' EF_UNDEFINED'
      CASE (EF_3D)
        cc = ' EF_3D'
      CASE (EF_LONLAT)
        cc = ' EF_LONLAT'
      CASE (EF_LEV)
        cc = ' EF_LEV'
      CASE (EF_SINGLE)
        cc = ' EF_SINGLE'
    END SELECT
    WRITE (message_text, '(2a)') 'ef_geometry: ', cc
    CALL message('',message_text,iout,level=em_param)
    SELECT CASE (bc%bc_ef%ef_timedef)
      CASE (EF_TIMERESOLVED)
        cc = ' EF_TIMERESOLVED'
      CASE (EF_IGNOREYEAR)
        cc = ' EF_IGNOREYEAR'
      CASE (EF_CONSTANT)
        cc = ' EF_CONSTANT'
    END SELECT
    SELECT CASE (bc%bc_ef%ef_interpolate)
      CASE (EF_NOINTER)
        cc2 = ' EF_NOINTER'
      CASE (EF_LINEAR)
        cc2 = ' EF_LINEAR'
      CASE (EF_CUBIC)
        cc2 = ' EF_CUBIC'
    END SELECT
    WRITE (message_text, '(3a,e11.4,a,i4,2a)') 'ef_timedef: ', TRIM(cc), ', ef_timeoffset: ',bc%bc_ef%ef_timeoffset,  &
                  ', ef_timeindex: ', bc%bc_ef%ef_timeindex, ', time interpolation: ', TRIM(cc2)
    CALL message('',message_text,iout,level=em_param)
  ELSE IF (bc%bc_ef%ef_type == EF_VALUE) THEN
    WRITE (message_text, '(a,e11.4)') 'constant BC value: ', bc%bc_ef%ef_value
    CALL message('',message_text,iout,level=em_param)
  END IF
  CALL message('','',level=em_param)

  END SUBROUTINE bc_printstat

!++mgs 2010-03-01
  SUBROUTINE bc_find (jg, name, ibc, ierr)

  USE mo_exception,           ONLY: finish, message, message_text, em_info, em_error
  USE mo_util_string,         ONLY: tolower

  
  INTEGER,          INTENT( IN ) :: jg  ! domain/grid index
  CHARACTER(len=*), INTENT(in)   :: name
  INTEGER, INTENT(out)           :: ibc   ! index in bc_list
  INTEGER, OPTIONAL, INTENT(out) :: ierr  ! error status

  INTEGER     :: i
  TYPE(boundary_conditions), POINTER :: bc

  bc=>bc_lists(jg)

  !-- Initialize and check validity of arguments
  ibc = 0
  IF (PRESENT(ierr)) THEN
    ierr = 0
  END IF
  IF (TRIM(name) == '') CALL finish('bc_find', 'Invalid (empty) name string!')

  DO i=1, nbc
    IF (TRIM(tolower(name)) == TRIM(tolower(bc%bc_list(i)%bc_name))) ibc = i
  END DO

  IF (ibc > 0) THEN
    WRITE(message_text,'(a,i4)') 'Located boundary condition for "'//TRIM(name)//'" as index ', ibc
    CALL message('bc_find', message_text, level=em_info)
  ELSE
    IF (PRESENT(ierr)) THEN
      ierr = 1
    ELSE
      CALL message('bc_find', 'Cannot find index for boundary condition '//TRIM(name),   &
                   level=em_error)
    END IF
  END IF

  END SUBROUTINE bc_find
!--mgs

!++mgs
  SUBROUTINE bc_query (jg, ibc, name, ef_type, ef_actual_unit)

  USE mo_exception,         ONLY: finish

  INTEGER, INTENT(in)                      :: jg    !< domain id
  INTEGER, INTENT(in)                      :: ibc   !< index in bc_list
  CHARACTER(len=*), INTENT(out), OPTIONAL  :: name
  INTEGER,          INTENT(out), OPTIONAL  :: ef_type
  CHARACTER(len=*), INTENT(out), OPTIONAL  :: ef_actual_unit

  TYPE(boundary_conditions), POINTER :: bc

  bc=>bc_lists(jg)

  !-- check validity of arguments
  IF (ibc < 0 .OR. ibc > nbc) CALL finish('bc_query', 'Invalid index to bc_list!')

  IF (PRESENT(name)) THEN
    name = bc%bc_list(ibc)%bc_name
  END IF
  IF (PRESENT(ef_type)) THEN
    ef_type = bc%bc_list(ibc)%bc_ef%ef_type
  END IF
  IF (PRESENT(ef_actual_unit)) THEN
    ef_actual_unit = bc%bc_list(ibc)%bc_ef%ef_actual_unit
  END IF

  END SUBROUTINE bc_query
!--mgs

!++mgs  2010-02-25
  SUBROUTINE bc_modify (jg, ibc, name, ef_type, bc_domain, bc_ndims, bc_vertint, ef_actual_unit)

  USE mo_exception,         ONLY: finish

  INTEGER,          INTENT(in)             :: jg  ! domain/grid index
  INTEGER,          INTENT(in)             :: ibc   ! index in bc_list
  CHARACTER(len=*), INTENT(in), OPTIONAL   :: name
  INTEGER,          INTENT(in), OPTIONAL   :: ef_type
  INTEGER,          INTENT(in), OPTIONAL   :: bc_domain
  INTEGER,          INTENT(in), OPTIONAL   :: bc_ndims
  INTEGER,          INTENT(in), OPTIONAL   :: bc_vertint
  CHARACTER(len=*), INTENT(in), OPTIONAL   :: ef_actual_unit

  TYPE(boundary_conditions), POINTER       :: bc

  !-- check validity of arguments
  IF (ibc < 0 .OR. ibc > nbc) CALL finish('bc_query', 'Invalid index to bc_list!')

  bc=>bc_lists(jg)

  IF (PRESENT(name)) THEN
    bc%bc_list(ibc)%bc_name = TRIM(name)
  END IF
  IF (PRESENT(ef_type)) THEN
    bc%bc_list(ibc)%bc_ef%ef_type = ef_type     ! add error checks (?)
  END IF
  IF (PRESENT(bc_domain)) THEN
    bc%bc_list(ibc)%bc_domain = bc_domain       ! add error checks (?)
  END IF
  IF (PRESENT(bc_ndims)) THEN
    bc%bc_list(ibc)%bc_ndim = bc_ndims         ! add error checks (?)
    ! may need to do more here to ensure consistency...
  END IF
  IF (PRESENT(bc_vertint)) THEN
    bc%bc_list(ibc)%bc_vertint = bc_vertint
  END IF
  IF (PRESENT(ef_actual_unit)) THEN
    bc%bc_list(ibc)%bc_ef%ef_actual_unit = ef_actual_unit
  END IF

  END SUBROUTINE bc_modify
!--mgs

!>
!! function to define a new boundary condition and return its index in the
!! boundary condition list
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  FUNCTION bc_define(jg, bc_name,bc_struc,bc_ndim,lverbose) RESULT(ibc)
  USE mo_external_field_processor, ONLY: EF_LEV, EF_INACTIVE, EF_VALUE, EF_FILE, EF_MODULE
  USE mo_exception,                ONLY: finish, message, message_text, em_error, em_warn
  USE mo_run_config,               ONLY: nlev
  USE mo_mpi,                      ONLY: p_pe, p_io

  INTEGER, INTENT(in)           :: jg ! grid/block index
  CHARACTER(LEN=*),  INTENT(in) :: bc_name
  TYPE(bc_nml),      INTENT(in) :: bc_struc
  INTEGER, INTENT(in)           :: bc_ndim
  LOGICAL, OPTIONAL, INTENT(in) :: lverbose

  CHARACTER(LEN=24)            :: cc
  INTEGER                      :: ibc
  TYPE(boundary_conditions), POINTER    :: bc
 
  bc=>bc_lists(jg)
 
  ibc = -1

  IF ( called_bc_lists_init(jg) ) THEN
    CALL finish(thismodule, "bc_define must be called before bc_lists_init")
  END IF

! not to be used in this way bc_name(CHARACTER(128)), message_text(CHARACTER(132))
! WRITE (message_text, '(a)') 'Boundary condition ' // TRIM(bc_name) // ' of type'
  SELECT CASE (bc_struc%ef_type)
    CASE (EF_INACTIVE) 
      cc = ' EF_INACTIVE'
    CASE (EF_VALUE)    
      cc = ' EF_VALUE'
    CASE (EF_FILE)     
      cc = ' EF_FILE'
    CASE (EF_MODULE)   
      cc = ' EF_MODULE'
    CASE DEFAULT       
      cc = ' INVALID'
      CALL message('bc_define', 'Invalid value of EF_TYPE for boundary condition ' &
                   // TRIM(bc_name) //'!', level=em_error)
      RETURN
  END SELECT
! not to be used in this way bc_name(CHARACTER(128)), message_text(CHARACTER(132))
! CALL message('bc_define', TRIM(message_text) // cc, level=em_info)
  IF (bc_struc%ef_type == EF_INACTIVE) RETURN  ! nothing to be done

  nbc = nbc + 1
  ! Error checks
  IF (nbc > MAXNBC) THEN
    WRITE (message_text, '(a,i4,a)') 'Too many boundary conditions (MAXNBC=', MAXNBC, ')'
    CALL finish('bc_define', message_text)
  END IF
  ibc = nbc
  ! bc_mode
  IF (bc_struc%bc_mode < BC_REPLACE .OR. bc_struc%bc_mode > BC_SPECIAL) THEN
    WRITE (message_text, '(a,i2)') 'Invalid value for bc_mode : ', bc_struc%bc_mode
    CALL message('bc_define', message_text, level=em_error)
  END IF
  ! bc_domain
  IF (bc_struc%bc_domain < BC_EVERYWHERE .OR. bc_struc%bc_domain > BC_PRESSURE) THEN
    WRITE (message_text, '(a,i2)') 'Invalid value for bc_domain : ', bc_struc%bc_domain
    CALL message('bc_define', message_text, level=em_error)
  END IF
  ! bc_relaxtime
  IF (bc_struc%bc_relaxtime < 0._dp) THEN
    WRITE (message_text, '(a,e11.4)') 'Invalid value for bc_relaxtime : ', bc_struc%bc_relaxtime
    CALL message('bc_define', message_text, level=em_error)
  END IF
  IF ((bc_struc%bc_mode == BC_RELAX) .AND. (bc_struc%bc_relaxtime == 0._dp)) &
    CALL message('bc_define', 'value for bc_relaxtime must be set if bc_mode=BC_RELAX!', level=em_error)

  ! settings for all ef_type values
  bc%bc_list(nbc)%bc_name              = bc_name
  bc%bc_list(nbc)%bc_ef%ef_type        = bc_struc%ef_type
  bc%bc_list(nbc)%bc_domain            = bc_struc%bc_domain
  bc%bc_list(nbc)%bc_minlev            = MAX(bc_struc%bc_minlev, 1)  ! i.e. also 1 if "undefined"
  bc%bc_list(nbc)%bc_maxlev            = MIN(bc_struc%bc_maxlev, nlev)
  IF (bc%bc_list(nbc)%bc_maxlev < 1) bc%bc_list(nbc)%bc_maxlev = nlev   ! any value < 1 could otherwise cause segfault
  bc%bc_list(nbc)%bc_mode              = bc_struc%bc_mode  
  bc%bc_list(nbc)%bc_relaxtime         = bc_struc%bc_relaxtime
  IF ( bc_struc%ef_nlev .EQ. -1 .AND. bc_ndim /= 2 ) THEN
    bc%bc_list(nbc)%bc_ef%ef_nzval       = nlev 
  ELSE
    bc%bc_list(nbc)%bc_ef%ef_nzval       = bc_struc%ef_nlev !cms nlev
  END IF
  ! ef_actual_unit has to be reported by ALL boundary conditions (also by modules!)
  bc%bc_list(nbc)%bc_ef%ef_actual_unit = bc_struc%ef_actual_unit

  SELECT CASE (bc_struc%ef_type)
    CASE (EF_VALUE)    
      bc%bc_list(nbc)%bc_ef%ef_value       = bc_struc%ef_value

! 2014/05/16 sschr: fix for bug #346
! the above statement sets the value of this boundary condition already at definition time
      bc%bc_list(nbc)%bc_ldefined = .true.

      ! note: scale factor always = 1.0 (=default) !
      IF (bc_struc%ef_factor /= 1.0_dp) CALL message ('bc_define',  &
          ' ef_factor is set though ef_type is EF_VALUE (the factor is ignored!!)', level=em_warn)

    CASE (EF_FILE)     
      bc%bc_list(nbc)%bc_ef%ef_template    = bc_struc%ef_template
      bc%bc_list(nbc)%bc_ef%ef_varname     = bc_struc%ef_varname
      bc%bc_list(nbc)%bc_ef%ef_geometry    = bc_struc%ef_geometry
      bc%bc_list(nbc)%bc_ef%ef_timedef     = bc_struc%ef_timedef
      bc%bc_list(nbc)%bc_ef%ef_timeoffset  = bc_struc%ef_timeoffset
      bc%bc_list(nbc)%bc_ef%ef_timeindex   = bc_struc%ef_timeindex
      bc%bc_list(nbc)%bc_ef%ef_interpolate = bc_struc%ef_interpolate
      bc%bc_list(nbc)%bc_ef%ef_factor      = bc_struc%ef_factor
      IF ( ( ((bc_struc%bc_domain == BC_BOTTOM) .OR. (bc_struc%bc_domain == BC_TOP)) &
           .AND. ((bc_struc%ef_geometry == EF_LEV) ) ) .OR. ( (bc_struc%bc_domain == BC_LEVEL) ) ) &
        CALL message ('bc_define', ' bc_domain and ef_geometry do not fit!', level=em_error)

    CASE (EF_MODULE)   
      ! nothing to be done here. Module must call bc_set to set BC value. Reserve memory in bc_list ==> bc_ndim must be set!
  END SELECT

  bc%bc_list(nbc)%bc_ndim = bc_ndim

  IF (PRESENT(lverbose)) THEN
    IF (lverbose .AND. p_pe .EQ. p_io  ) CALL bc_printstat(bc%bc_list(nbc))
  ENDIF

  END FUNCTION bc_define

!>
!! set boundary condition from ECHAM module (2D)
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE bc_set1d(jg,jbc,kproma,jrow,values)
  USE mo_external_field_processor, ONLY: EF_MODULE

  INTEGER,  INTENT(in) :: jg, jbc, kproma, jrow
  REAL(dp), INTENT(in) :: values(:)
  TYPE(boundary_conditions), POINTER :: bc
  
  bc=>bc_lists(jg)


  ! overwrite bc value only if ef_type=EF_MODULE
  IF (bc%bc_list(jbc)%bc_ef%ef_type /= EF_MODULE) RETURN

  ! check SHAPE of incoming array
  ! careful with nproma ...
  ! in the end do
  bc%bc_list(jbc)%bc_values_pointer%p_2d(1:kproma,jrow) = values(1:kproma)
  bc%bc_list(jbc)%bc_ldefined = .true.

  END SUBROUTINE bc_set1d

!>
!! set boundary condition in ECHAM module (3D)
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE bc_set2d(jg, jbc,kproma, jrow,values)
  USE mo_external_field_processor, ONLY: EF_MODULE

  INTEGER,  INTENT(in) :: jg, jbc, kproma, jrow
  REAL(dp), INTENT(in) :: values(:,:)

  TYPE(boundary_conditions), POINTER :: bc

  bc=>bc_lists(jg)

  ! overwrite bc value only if ef_type=EF_MODULE
  IF (bc%bc_list(jbc)%bc_ef%ef_type /= EF_MODULE) RETURN

  ! check SHAPE of incoming array
  ! careful with nproma ...
  ! in the end do
  bc%bc_list(jbc)%bc_values_pointer%p_3d(1:kproma,:,jrow) = values(1:kproma,:)
  bc%bc_list(jbc)%bc_ldefined = .true.

  END SUBROUTINE bc_set2d


!>
!! apply boundary condition to user field (1D - vertical column)
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE bc_apply1d(jg, jbc, kproma, ngpblks, jrow, current_date, values)
  USE mo_run_config,               ONLY: dtime !cms check that this is called each timestep (& nesting?)
  USE mtime,                       ONLY: datetime
  USE mo_exception,                ONLY: finish, message, EM_WARN
  USE mo_external_field_processor, ONLY: EF_VALUE, EF_FILE
  USE mo_parallel_config,          ONLY: nproma

  INTEGER,  INTENT(in)    :: jg, jbc, kproma, jrow, ngpblks
  TYPE(datetime), POINTER, INTENT( IN ) :: current_date
  REAL(dp), INTENT(inout) :: values(:)

  REAL(dp)             :: factor
  REAL(dp),ALLOCATABLE :: bc_values(:,:) 

  TYPE(boundary_conditions), POINTER :: bc

  TYPE(datetime), POINTER            :: mtime_hour

  bc =>  bc_lists(jg)


  IF ( .NOT. bc%bc_list(jbc)%bc_ldefined ) THEN
    CALL finish('bc_apply', TRIM(bc%bc_list(jbc)%bc_name)//' has no values defined yet!')
    !cms temporary ++
    !if ( jrow .eq. 1) then
    !call message('bc_apply', TRIM(bc%bc_list(jbc)%bc_name)//' has no values defined yet!', level=EM_WARN)
    !values=0._dp
    !end if
    !return
    !cms --
  END IF

  ALLOCATE(bc_values(1:nproma,1:ngpblks))
  IF (bc%bc_list(jbc)%bc_ef%ef_type == EF_VALUE) THEN
    bc_values = bc%bc_list(jbc)%bc_ef%ef_value
  ELSE
    IF ((bc%bc_list(jbc)%bc_ef%ef_interpolate /= EF_NOINTER) &
         .AND. (bc%bc_list(jbc)%bc_ef%ef_type == EF_FILE)) THEN

       bc_values = (1._dp - bc_timefac(jbc, bc, current_date)) * bc%bc_list(jbc)%bc_values_pointer%p_2d &
                   + bc_timefac(jbc, bc, current_date) * bc%bc_list(jbc)%bc_values_pointer_later%p_2d

    ELSE
      bc_values = bc%bc_list(jbc)%bc_values_pointer%p_2d
    ENDIF
  ENDIF

!!!!  - following de-activated because BC_BOTTOM or BC_TOP should be valid conditions here
!!   IF ( .NOT. (bc%bc_list(jbc)%bc_domain == BC_EVERYWHERE) ) &
!!     CALL finish('bc_apply', TRIM(bc%bc_list(jbc)%bc_name)//' bc_domain indicates 3d field, but 2d field passed!')
  SELECT CASE(bc%bc_list(jbc)%bc_mode)
  CASE (BC_REPLACE)
    values(1:kproma) = bc_values(1:kproma,jrow)
  CASE (BC_ADD)
    values(1:kproma) = values(1:kproma) + bc_values(1:kproma,jrow)
  CASE (BC_RELAX)
    factor = exp(-(dtime/bc%bc_list(jbc)%bc_relaxtime))
    values(1:kproma) = (1.0_dp - factor) * values(1:kproma) + factor * bc_values(1:kproma,jrow)
  CASE (BC_SPECIAL)
! user should pass a temporary field (not the real boundary condition array)
    values(1:kproma) = bc_values(1:kproma,jrow)
  END SELECT
  DEALLOCATE(bc_values)

  END SUBROUTINE bc_apply1d

!>
!! apply boundary condition to user field (2D)
!!
!! Description?
!!
!! @par Revision History
!! code implementation by S. Schroeder (2009-08-25)
!!
  SUBROUTINE bc_apply2d(jg, jbc, kproma,  ngpblks, jrow, current_date,   &
                         paphm1, pzf, pzh, values, minlev, maxlev)
  USE mo_run_config,               ONLY: dtime
  USE mtime,                       ONLY: datetime
  USE mo_exception,                ONLY: finish
  USE mo_run_config,               ONLY: nlev
  USE mo_parallel_config,          ONLY: nproma
  USE mo_external_field_processor, ONLY: EF_VALUE, EF_FILE
  
  INTEGER,  INTENT(in)    :: jg, jbc, kproma, ngpblks, jrow
  TYPE(datetime), POINTER, INTENT( IN ) :: current_date
  REAL(dp), INTENT(in) :: pzf(:,:)     ! kbdim, nlev,   geometric height above sea level, full level 
  REAL(dp), INTENT(in) :: pzh(:,:)     ! kbdim, nlev+1, geometric height above sea level, half level
  REAL(dp), INTENT(in) :: paphm1(:,:)  ! kbdim, nlev+1, half level pressure [Pa] 
  REAL(dp), INTENT(inout) :: values(:,:)
  INTEGER,  OPTIONAL,  INTENT(out) :: minlev, maxlev  ! return minlev and maxlev from structure for SPECIAL mode

  INTEGER               :: k, kmin, kmax
  REAL(dp)              :: factor
  REAL(dp), ALLOCATABLE :: bc_values(:,:,:) 
  REAL(dp), ALLOCATABLE :: ztmpvalues(:,:)

  TYPE(boundary_conditions), POINTER :: bc

  bc =>  bc_lists(jg)

  IF ( .NOT. bc%bc_list(jbc)%bc_ldefined ) &
    CALL finish('bc_apply', TRIM(bc%bc_list(jbc)%bc_name)//' has no values defined yet!')

  ALLOCATE(bc_values(1:nproma,bc%bc_list(jbc)%bc_ef%ef_nzval,1:ngpblks))
  IF (bc%bc_list(jbc)%bc_ef%ef_type == EF_VALUE) THEN
    bc_values = bc%bc_list(jbc)%bc_ef%ef_value
  ELSE
    IF ((bc%bc_list(jbc)%bc_ef%ef_interpolate /= EF_NOINTER) &
         .AND. (bc%bc_list(jbc)%bc_ef%ef_type == EF_FILE)) THEN
!!$      bc_values = (1._dp - bc_timefac(jbc)) * bc_list(jbc)%bc_values_pointer &
!!$                + bc_timefac(jbc) * bc_list(jbc)%bc_values_pointer_later
write(0,*) "rrrrrrrr7"
stop 2517
    ELSE
      bc_values = bc%bc_list(jbc)%bc_values_pointer%p_3d
    ENDIF
  ENDIF


  SELECT CASE(bc%bc_list(jbc)%bc_domain)
  CASE (BC_EVERYWHERE)
  ! if BC_EVERYWHERE is the domain of a field given in altitude/pressure levels,
  ! then BC_EVERYWHERE has already been converted (by bc_in_geom) to BC_ALTITUDE/BC_PRESSURE
  ! with bc_minlev/bc_maxlev=1/nlev
    kmin = 1
    kmax = nlev
  CASE (BC_BOTTOM)
    kmin = nlev
    kmax = nlev
  CASE (BC_TOP)
    kmin = 1
    kmax = 1
  CASE (BC_LEVEL)
    kmin =bc%bc_list(jbc)%bc_minlev
    kmax =bc%bc_list(jbc)%bc_maxlev
  CASE (BC_ALTITUDE, BC_PRESSURE)
    ALLOCATE(ztmpvalues(SIZE(values,1),SIZE(values,2)))
    SELECT CASE(bc%bc_list(jbc)%bc_vertint)
    CASE (BC_VERTICAL_NONE)
    CASE (BC_VERTICAL_INTERPOLATION)    
      CALL bc_vert_interpolation(jg,jbc,kproma,jrow,paphm1,pzf,pzh,bc_values,ztmpvalues)
    CASE (BC_VERTICAL_WEIGHTED_INTERPOLATION) 
      CALL bc_vert_weighted_interpolation(jg,jbc,kproma,jrow,paphm1,pzf,pzh,bc_values,ztmpvalues)
    CASE (BC_VERTICAL_INTEGRATION)
         write(0,*) "RRRRRRRRRRRRRRRRr"    
         STOP 555
!!$      CALL bc_vert_integration(jbc,kproma,jrow,bc_values,ztmpvalues)
    END SELECT
    ! not yet ready:
    ! bc_minlev/bc_maxlev have to be calculated into indices (by the routine that reads the application domain)
    ! at this point bc_minlev/bc_maxlev are always 1/nlev
    kmin =bc%bc_list(jbc)%bc_minlev
    kmax =bc%bc_list(jbc)%bc_maxlev
  END SELECT


  IF (bc%bc_list(jbc)%bc_ndim == 2) THEN
! now apply a 1d field to the 2d argument field
    SELECT CASE(bc%bc_list(jbc)%bc_mode)
    CASE (BC_REPLACE)
      DO k=kmin,kmax
        values(1:kproma,k) = bc_values(1:kproma,1,jrow)
      END DO
    CASE (BC_ADD)
      DO k=kmin,kmax
        values(1:kproma,k) = values(1:kproma,k) + bc_values(1:kproma,1,jrow)
      END DO
    CASE (BC_RELAX)
      factor = exp(-(dtime/bc%bc_list(jbc)%bc_relaxtime))
      DO k=kmin,kmax
        values(1:kproma,k) = (1.0_dp - factor) * values(1:kproma,k) + factor * bc_values(1:kproma,1,jrow)
      END DO
    CASE (BC_SPECIAL)
! user passed a temporary 2d field, but would get a 1d field back
      CALL finish('bc_apply', TRIM(bc%bc_list(jbc)%bc_name)//' a 3d field was passed, but bc_list contains a 2d field!')
    END SELECT
  ELSE
! now apply a 2d field to the 2d argument field
    SELECT CASE(bc%bc_list(jbc)%bc_mode)
    CASE (BC_REPLACE)
      IF (ALLOCATED(ztmpvalues)) THEN
        DO k=kmin,kmax
          values(1:kproma,k) = ztmpvalues(1:kproma,k)
        END DO
        DEALLOCATE(ztmpvalues)
      ELSE
        DO k=kmin,kmax
          values(1:kproma,k) = bc_values(1:kproma,k,jrow)
        END DO
      ENDIF
    CASE (BC_ADD)
      IF (ALLOCATED(ztmpvalues)) THEN 
        DO k=kmin,kmax
          values(1:kproma,k) = values(1:kproma,k) + ztmpvalues(1:kproma,k)
        END DO
        DEALLOCATE(ztmpvalues)
      ELSE
        DO k=kmin,kmax
          values(1:kproma,k) = values(1:kproma,k) + bc_values(1:kproma,k,jrow)
        END DO
      ENDIF
    CASE (BC_RELAX)
      factor = exp(-(dtime/bc%bc_list(jbc)%bc_relaxtime))
      IF (ALLOCATED(ztmpvalues)) THEN 
        DO k=kmin,kmax
          values(1:kproma,k) = (1.0_dp - factor) * values(1:kproma,k) + factor * ztmpvalues(1:kproma,k)
        END DO
        DEALLOCATE(ztmpvalues)
      ELSE 
        DO k=kmin,kmax
          values(1:kproma,k) = (1.0_dp - factor) * values(1:kproma,k) + factor * bc_values(1:kproma,k,jrow)
        END DO
      ENDIF  
    CASE (BC_SPECIAL)
      IF (ALLOCATED(ztmpvalues)) THEN
        DO k=kmin,kmax
! user should pass a temporary field (not the real boundary condition array)
          values(1:kproma,k) = ztmpvalues(1:kproma,k)
        END DO
        DEALLOCATE(ztmpvalues)
      ELSE
        DO k=kmin,kmax
! user should pass a temporary field (not the real boundary condition array)
          values(1:kproma,k) = bc_values(1:kproma,k,jrow)
        END DO
      ENDIF
    END SELECT
  ENDIF
  !! return kmin and kmax as minlev, maxlev if requested
  IF (PRESENT(minlev)) minlev = kmin
  IF (PRESENT(maxlev)) maxlev = kmax
  DEALLOCATE(bc_values)
  END SUBROUTINE bc_apply2d

END MODULE mo_boundary_condition

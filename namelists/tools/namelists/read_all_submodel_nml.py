import xml.etree.ElementTree as ET

import utils.util as u
import sys

def read_namelists_submodel(xml_filename, drivers_shared_dir ):
    # create module that reads all submodel namelists 
    indir=drivers_shared_dir.templates
    outdir=drivers_shared_dir.src   

    fname="mo_submodel_nml.f90"

    ifile=indir+"/"+fname
    ofile=outdir+"/"+fname

    fin=open(ifile, "r")   
    fout=open(ofile, "w")

    tree=ET.parse(xml_filename)
    root=tree.getroot()   


    while True:
       l=fin.readline()
       if not l: break
       lo=u.replace_line( read_nml_replace_var, l, root )
       print(lo)
       fout.write(lo+"\n")

def read_nml_replace_var( rvar, root, leading_blanks ):
    rvar_in=rvar
    if rvar == "use_all_namelist_readers":
       rvar=use_all_namelist_readers( root, leading_blanks )
    if rvar == "call_all_namelists":
       rvar=create_call_all_namelists( root, leading_blanks )
    if rvar == "disclaimer":
       rvar=u.disclaimer("read_all_submodel_nml.py") 
    if rvar == rvar_in:
       message="no rule for replacing "+rvar
       u.finish ("read_all_submodel_nml.py",message)
    return rvar

def create_call_all_namelists( root, leading_blanks ):
    rvar=""
    bb=""
    for namelist in root.findall('namelist'):
        nname= namelist.attrib['name']
        rvar=rvar+bb+"CALL read_"+nname+"_namelist( filename )\n"
        bb=" " * leading_blanks 
        print( namelist.attrib['name'])
    #sys.exit(5)
    return rvar

def use_all_namelist_readers( root, leading_blanks ):
    rvar=""
    bb=""
    for namelist in root.findall('namelist'):
        nname= namelist.attrib['name']
        rvar=rvar+bb+"USE mo_"+nname+"_nml, ONLY: "
        rvar=rvar+"read_"+nname+"_namelist\n"
        bb=" " * leading_blanks 
        print( namelist.attrib['name'])
    #sys.exit(5)
    return rvar

!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_ham_dust.f90
!!
!! \brief
!! mo_ham_dust contains the MPI BGC dust emission scheme.
!!
!! \author Martin Werner, MPI Biogeochemistry
!!
!! \responsible_coder
!! Martin Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Werner (MPI BGC) - original code (2002-10)
!!   -# P. Stier (MPI-Met) - fitted parameters for emissions size distribution
!!                           introduced scale factor for wind stress threshold (2003)
!!   -# M. Werner (MPI BGC) - changed scheme from 0.5x0.5 degree grid to default
!!                            Echam5-HAM grid (to save computational costs) (2004-10)
!!   -# M. Schultz (FZ Juelich) - merged all bgc_dust modules into one,
!!                                also added setdust routine and ham_dustctl namelist (2009-10-01)
!!   -# C. Siegenthaler (C2SM-ETHZ) - implementation of a regional tuning factor nduscale 
!!                                    Issue 433 (06-2015)
!!   -# S. Ferrachat (ETH Zurich) - Improved support for namelist settings (See #479)
!!                                - Soil type input file merging (#253)
!!   -# M. Salzmann (LIM) - adapted for ICON
!!
!! \limitations
!! None
!!
!! \details
!! None
!!
!! \bibliographic_references
!!   - Marticorena, B., and G. Bergametti (1995), Modeling the atmospheric dust cycle: 1. 
!!     Design of a soil-derived dust emission scheme, 
!!     J. Geophys. Res., 100(D8), 16,415?C16,430.
!!   - Marticorena, B., G. Bergametti, B. Aumont, Y. Callot, C. N'Doume, and M. Legrand (1997), 
!!     Modeling the atmospheric dust cycle 2. Simulation of Saharan dust sources, 
!!     J. Geophys. Res., 102(D4), 4387?C4404.
!!   - Tegen, I., S. P. Harrison, K. Kohfeld, I. C. Prentice, M. Coe, and M. Heimann (2002), 
!!     Impact of vegetation and preferential source areas on global dust aerosol: Results 
!!     from a model study, 
!!     J. Geophys. Res., 107(D21), 4576, doi:10.1029/2001JD000963. 
!!   - Cheng, T., Peng, Y., Feichter, J., and Tegen, I.: 
!!     An improvement on the dust emission scheme in the global aerosol-climate model ECHAM5-HAM, 
!!     Atmos. Chem. Phys., 8, 1105-1117, 2008. 
!!     http://www.atmos-chem-phys.net/8/1105/2008/acp-8-1105-2008.html
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

MODULE mo_ham_dust

  
  USE mo_kind, ONLY: dp
  USE mo_mpi,  ONLY: p_bcast, p_io, p_pe,  p_comm_work
  USE mo_ham,  ONLY: ndust !SF #479

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: setdust,                        &
            bgc_dust_initialize,            &
            bgc_dust_init_diag,             &
            bgc_dust_read_monthly,          &
            bgc_dust_calc_emis,             &
!!$            bgc_dust_trastat,               &
!!$            bgc_dust_cleanup
            comp_nduscale_reg 

  PUBLIC :: ndurough

  PUBLIC :: flux_6h, ntrace         ! emission mass flux 

  PUBLIC :: Dmin, Dmax, nbinit, Dstep

  INTEGER :: ibc_regint ! boundary condition indices for regional mask file (!csld #433) 

! *** dust parameter
  REAL(dp), PARAMETER     :: a_rnolds=1331.647_dp          ! Reynolds constant
  REAL(dp), PARAMETER     :: aeff=0.35_dp                  ! efficient fraction

  REAL(dp), PARAMETER     :: b_rnolds=0.38194_dp           ! Reynolds constant
  
  REAL(dp), PARAMETER     :: d_thrsld=0.00000231_dp        ! thresold value
  REAL(dp), PARAMETER     :: Dmin=0.00002_dp               ! minimum particules diameter (cm)
  REAL(dp), PARAMETER     :: Dmax=0.130_dp                 ! maximum particules diameter (cm)
  REAL(dp), PARAMETER     :: Dstep=0.0460517018598807_dp   ! diameter increment (cm)
      
  INTEGER, PARAMETER      :: ntrace=8                      ! number of tracers
  INTEGER, PARAMETER      :: nbin=24                       ! number of bins per tracer
  INTEGER, PARAMETER      :: nbinit=24                     ! number of bins per tracer
                                                           !alaak: needed for salsa interface
  INTEGER, PARAMETER      :: nclass=ntrace*nbin            ! number of particle classes
  INTEGER, PARAMETER      :: nats =17                      ! number of soil types
  INTEGER, PARAMETER      :: nmode=4
  INTEGER, PARAMETER      :: nspe = nmode*3+2
    
  REAL(dp), PARAMETER     :: roa=0.001227_dp               ! air density (g/cm-3)
  REAL(dp), PARAMETER     :: rop=2.65_dp                   ! particle density (g/cm-3)
  
  REAL(dp), PARAMETER     :: umin=21._dp                   ! minimum threshold friction windspeed (cm/s)
 
  REAL(dp), PARAMETER     :: vk=0.4_dp                     ! Von Karman constant: 0.4 (0.35 <--> 0.42 see Stull) 
  
  REAL(dp), PARAMETER     :: w0=0.99_dp                    ! threshold of relative soil humidity 
  
  REAL(dp), PARAMETER     :: x_rnolds=1.561228_dp          ! Reynolds constant
  REAL(dp), PARAMETER     :: xeff=10._dp                   ! efficient fraction
  
  REAL(dp), PARAMETER     :: ZZ=1000._dp                   ! wind measurment height (cm)
   
  REAL(dp), PARAMETER     :: Z0s=0.001_dp                  ! roughness length of surface without obstacles(cm)
                                                            ! (see: thesis of B.Marticorena, p.85) 
  ! Attention: parameters below listed are only for sensitivity tests
  !            and should not be changed in the control run
                                       !
  REAL(dp):: ndurough     = 1.0E-03_dp ! Surface roughness length (cm)
                                       !
                                       !    ndurough = 0 A monthly mean satellite derived (Prigent et al.,
                                       !                 JGR 2005) surface roughness length map is used
                                       !             > 0 The globally constant surface roughness length
                                       !                 ndurough (cm) us used.
                                       ! default 0.001cm
                                       !
  REAL(dp):: nduscale_reg(8) = 8.6E-01_dp ! Regional scale factor for threshold wind friction velocity !csld #433
                                      !  
                                      ! The indices correspond to the following regions
                                      !     1       All the other locations than the following regions
                                      !     2       North america
                                      !     3       South America 
                                      !     4       North Africa 
                                      !     5       South Africa 
                                      !     6       Middle East 
                                      !     7       Asia
                                      !     8       Australia 
  REAL(dp):: r_dust_lai   = 1.0E-10_dp ! Parameter for the threshold lai value (unitless)
                                       ! default 1.E-10
                                       !
  REAL(dp):: r_dust_umin  = 2.1E+01_dp ! Minimum threshold friction windspeed (cm/s)
                                       ! default 21. cm/s
                                       !
  REAL(dp):: r_dust_z0s   = 1.0E-03_dp ! z0s (cm)
                                       ! default 0.001 cm
                                       !
  REAL(dp):: r_dust_scz0  = 1.0E+00_dp ! Scale factor of satellite z0 (unitless)
                                       ! default 1.
                                       !
  REAL(dp):: r_dust_z0min = 1.0E-05_dp ! Parameter for minimum of z0 (cm)
                                       ! default 1.E-05 cm
                                       !
  REAL(dp):: r_dust_sf13  = 1.0E+00_dp ! duscale over Takelimakan desert (unitless)
  REAL(dp):: r_dust_sf14  = 1.0E+00_dp ! duscale over Loess (unitless)
  REAL(dp):: r_dust_sf15  = 1.0E+00_dp ! duscale over Gobi desert (unitless)
  REAL(dp):: r_dust_sf16  = 1.0E+00_dp ! duscale over other mixture soils (unitless)
  REAL(dp):: r_dust_sf17  = 1.0E+00_dp ! duscale over desert and sand land (unitless)
                                       ! default 1.
                                       !
  REAL(dp):: r_dust_af13  = 1.9E-06_dp ! Parameter for the alfa value over Takelimakan desert (unitless)
  REAL(dp):: r_dust_af14  = 1.9E-04_dp ! Parameter for the alfa value over Loess (unitless)
  REAL(dp):: r_dust_af15  = 3.9E-05_dp ! Parameter for the alfa value over Gobi desert  (unitless)
  REAL(dp):: r_dust_af16  = 3.1E-05_dp ! Parameter for the alfa value over other mixture soils (unitless)
  REAL(dp):: r_dust_af17  = 2.8E-06_dp ! Parameter for the alfa value over desert and sand land  (unitless)
                                       ! default values taken from Cheng et al.(2008)

  INTEGER :: k_dust_smst  = 1          ! Effect of soil moisture on threshold wind friction velocity
                                       !    0: on
                                       !    1: off
                                       ! default 1
                                       !
  INTEGER :: k_dust_easo  = 1          ! Including the new East-Asia soil type
                                       !    0: on, cheng's implementation, with a bug
                                       !    1: off
                                       !    2: on, bug-removed-version 0
                                       ! default 1
                                       !

     
! *** dust emssion scheme related variables
!cms not used  REAL(dp), ALLOCATABLE    :: biome(:,:)            ! biome distribution 

  REAL(dp)                 :: dustsum(4)            ! total dust flux (for different paricle sizes)

  !>>dod omp bugfix
  REAL(dp), ALLOCATABLE    :: flux_6h(:,:,:)        ! 6h flux (for tm3 use)
  !$OMP THREADPRIVATE (flux_6h)
  !<<dod

  REAL(dp), ALLOCATABLE    :: flux_ann(:,:)         ! mean annual flux 
  REAL(dp), ALLOCATABLE    :: flux_a1(:,:)          ! for statistics of small particles
  REAL(dp), ALLOCATABLE    :: flux_a2(:,:)          ! for statistics of small particles
  REAL(dp), ALLOCATABLE    :: flux_a10(:,:)         ! for statistics of small particles

!cms  not used INTEGER, ALLOCATABLE     :: idust(:,:)            ! dust potential source (1: source, 0: no source)
   
  REAL(dp), TARGET, ALLOCATABLE    :: k_fpar_eff(:,:)       ! KERNEL effective FPAR

  REAL(dp), ALLOCATABLE         :: mat_s1(:,:)    ! soil type #1 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s2(:,:)    ! soil type #2 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s3(:,:)    ! soil type #3 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s4(:,:)    ! soil type #4 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s6(:,:)    ! soil type #6 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s13(:,:)   ! soil type #13 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s14(:,:)   ! soil type #14 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s15(:,:)   ! soil type #15 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s16(:,:)   ! soil type #16 area (in relative percentage)
  REAL(dp), ALLOCATABLE, TARGET :: mat_s17(:,:)   ! soil type #17 area (in relative percentage)
  REAL(dp), ALLOCATABLE         :: mat_psrc(:,:)  ! preferential source area (in relative percentage)
  REAL(dp), ALLOCATABLE         :: mat_msg(:,:)   ! MSG Saharan pref. source area (in relative percent.) !BH #382
 
  REAL(dp), ALLOCATABLE    :: Z01(:,:)              ! surface rough length (see: Thesis of B.Marticorena)
  REAL(dp), ALLOCATABLE    :: Z02(:,:)              ! surface rough length (see: Thesis of B.Marticorena)
  REAL(dp), ALLOCATABLE, TARGET    :: nduscale_2d(:,:)      ! 2D nduscale !csld #433
  REAL(dp), POINTER    :: nduscale2dptr(:,:)      ! 2D nduscale !csld #433

  REAL(dp)                 :: srel(nats,nclass)     ! relative surface 
  REAL(dp)                 :: srelV(nats,nclass)
  REAL(dp)                 :: su_srelV(nats,nclass)
   
  REAL(dp)                 :: Uth(nclass)           ! threshold friction velocity

  !>>SF #253
  INTEGER, PARAMETER :: nsoil_types = 9

  TYPE mat_general
       REAL(dp), POINTER :: ptr(:,:)
  END TYPE mat_general

  TYPE(mat_general) :: mat_all(nsoil_types)

  CHARACTER(len=2),PARAMETER :: jtypes(nsoil_types) = (/ &
                                                ' 2',' 3',' 4',' 6','13','14','15','16','17' /) ! Soil types labels
  !<<SF #253

  ! -- diagnostic fields
  REAL(dp), POINTER        :: ustar_acrit(:,:)

  ! --- from former mo_bgc_dust_data:
  ! combimax    value   int.    number of different biome types
  INTEGER, PARAMETER :: combimax=29
  ! active vegetation types
  INTEGER :: active(combimax)
  ! soil specs
  REAL(dp) :: solspe(17,14)

  ! 'boundary conditions' from JSBACH 
  INTEGER, SAVE :: ibc_ssf     = -1,   &  ! index of surface snow fraction 'boundary condition' 
                   ibc_ws_l1   = -1,   &  ! index of soil water content [m] 'boundary condition' 
                   ibc_ws_fc_l1 = -1      ! index of field capacity [m] 'boundary condition'  


  CHARACTER(LEN=*), PARAMETER :: thismodule='mo_ham_dust'

  CONTAINS

  !--- set dust_data

  SUBROUTINE set_dust_data

  ! Description:
  ! ------------
  ! *mo_bgc_dust_data* holds some classification dependent biome and soil data 
  !
  ! Author:
  ! -------
  ! Martin Werner, MPI Biogeochemistry (MPI BGC), Jena, May 2003.
  ! Tiantao Cheng, MPI-M, Hamburg, 2006-2007.
  !      add East-Asia soil type in solspe (type 13-17)
  !
  !   The 28 different biome types output by BIOME4
  !-----------------------------------------------------------------
  !     1       Tropical evergreen broadleaf forest
  !     2       Tropical semi-evergreen broadleaf forest
  !     3       Tropical deciduous broadleaf forest and woodland
  !     4       Temperate deciduous broadleaf forest
  !     5       Temperate evergreen needleleaf forest
  !     6       Warm-temperate evergreen broadleaf and mixed forest
  !     7       Cool mixed forest
  !     8       Cool evergreen needleleaf forest
  !     9       Cool-temperate evergreen needleleaf and mixed forest
  !     10      Cold evergreen needleleaf forest
  !     11      Cold deciduous forest
  !     12      Tropical savanna
  !     13      Tropical xerophytic shrubland
  !     14      Temperate xerophytic shrubland
  !     15      Temperate sclerophyll woodland and shrubland
  !     16      Temperate deciduous broadleaf savanna
  !     17      Temperate evergreen needleleaf open woodland
  !     18      Cold parkland
  !     19      Tropical grassland
  !     20      Temperate grassland
  !     21      Desert
  !     22      Graminoid and forb tundra
  !     23      Low and high shrub tundra
  !     24      Erect dwarf-shrub tundra
  !     25      Prostrate dwarf-shrub tundra
  !     26      Cushion-forb tundra
  !     27      Barren
  !     28      Ice
  !    (29)     Water (this is implied)

  !
  ! active vegetation types:
  ! ------------------------
  ! 
  active = (/ &
       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
       1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, & 
       1, 1, 1, 0, 0 /)

!!mgs!!   commented out because shrubs is not used anywhere
!!mgs!!   ! Biomes including shrubs (active=1 only) 
!!mgs!!   ! ------------------------
!!mgs!!   ! 
!!mgs!!   REAL(dp) :: shrub(combimax) = (/ &       
!!mgs!!        0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
!!mgs!!        0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
!!mgs!!        1.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &       
!!mgs!!        0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, &
!!mgs!!        1.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

  !----------------------------------------------------------------------------
  ! solspe --> SOIL CARACTERISTICS:
  ! -------------------------------
  !
  !  ZOBLER texture classes |
  !
  ! SOLSPE: for 4 populations : values = 3*(Dmed sig p); ratio of fluxes; 
  !                                      residual moisture
  
  !   Populations: Coarse sand, medium/fine sand, Silt, Clay 
  !
  !     soil type 1 : Coarse     
  !     soil type 2 : Medium     
  !     soil type 3 : Fine     
  !     soil type 4 : Coarse Medium      
  !     soil type 5 : Coarse Fine 
  !     soil type 6 : Medium Fine 
  !     soil type 7 : Coarse_dp, Medium_dp, Fine 
  !     soil type 8 : Organic
  !     soil type 9 : Ice
  !     soil type 10 : Potential Lakes (additional)
  !     soil type 11 : Potential Lakes (clay)
  !     soil type 12 : Potential Lakes Australia
  !     soil type 13 : Taklimakan desert
  !     soil type 14 : Asian Loess
  !     soil type 15 : Asian Gobi
  !     soil type 16 : Asian other mixture
  !     soil type 17 : Asian desert
  !----------------------------------------------------------------------------

  solspe = &
       RESHAPE ( (/ &
       0.0707_dp, 2.0_dp, 0.43_dp, 0.0158_dp, 2.0_dp, 0.40_dp, 0.0015_dp, &!1 
       2.0_dp, 0.17_dp, 0.0002_dp, 2.0_dp, 0.00_dp, 2.1e-06_dp, 0.20_dp,  &  
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.37_dp, 0.0015_dp, &!2 
       2.0_dp, 0.33_dp, 0.0002_dp, 2.0_dp, 0.30_dp, 4.0e-06_dp, 0.25_dp,  &  
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.00_dp, 0.0015_dp, &!3 
       2.0_dp, 0.33_dp, 0.0002_dp, 2.0_dp, 0.67_dp, 1.0e-07_dp, 0.50_dp,  &   
       0.0707_dp, 2.0_dp, 0.10_dp, 0.0158_dp, 2.0_dp, 0.50_dp, 0.0015_dp, &!4 
       2.0_dp, 0.20_dp, 0.0002_dp, 2.0_dp, 0.20_dp, 2.7e-06_dp, 0.23_dp,  &   
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.50_dp, 0.0015_dp, &!5 
       2.0_dp, 0.12_dp, 0.0002_dp, 2.0_dp, 0.38_dp, 2.8e-06_dp, 0.25_dp,  &   
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.27_dp, 0.0015_dp, &!6 
       2.0_dp, 0.25_dp, 0.0002_dp, 2.0_dp, 0.48_dp, 1.0e-07_dp, 0.36_dp,  &   
       0.0707_dp, 2.0_dp, 0.23_dp, 0.0158_dp, 2.0_dp, 0.23_dp, 0.0015_dp, &!7 
       2.0_dp, 0.19_dp, 0.0002_dp, 2.0_dp, 0.35_dp, 2.5e-06_dp, 0.25_dp,  &   
       0.0707_dp, 2.0_dp, 0.25_dp, 0.0158_dp, 2.0_dp, 0.25_dp, 0.0015_dp, &!8 
       2.0_dp, 0.25_dp, 0.0002_dp, 2.0_dp, 0.25_dp, 0.0e-00_dp, 0.50_dp,  &   
       0.0707_dp, 2.0_dp, 0.25_dp, 0.0158_dp, 2.0_dp, 0.25_dp, 0.0015_dp, &!9 
       2.0_dp, 0.25_dp, 0.0002_dp, 2.0_dp, 0.25_dp, 0.0e-00_dp, 0.50_dp,  &   
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.00_dp, 0.0015_dp, &!10: 100% silt 
       2.0_dp, 1.00_dp, 0.0002_dp, 2.0_dp, 0.00_dp, 1.0e-05_dp, 0.25_dp,  &
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.00_dp, 0.0015_dp, &!11: 100% clay
       2.0_dp, 0.00_dp, 0.0002_dp, 2.0_dp, 1.00_dp, 1.0e-05_dp, 0.25_dp,  & 
       0.0707_dp, 2.0_dp, 0.00_dp, 0.0158_dp, 2.0_dp, 0.00_dp, 0.0027_dp, &!12: 100% silt 
       2.0_dp, 1.00_dp, 0.0002_dp, 2.0_dp, 0.00_dp, 1.0e-05_dp, 0.25_dp,  & 
       0.0442_dp, 1.5_dp, 0.03_dp, 0.0084_dp, 1.5_dp, 0.85_dp, 0.0015_dp, &!13: taklimakan
       2.0_dp, 0.11_dp, 0.0002_dp, 2.0_dp, 0.02_dp, 1.9e-06_dp, 0.12_dp,  & 
       0.0450_dp, 1.5_dp, 0.00_dp, 0.0070_dp, 1.5_dp, 0.33_dp, 0.0015_dp, &!14: loess 
       2.0_dp, 0.50_dp, 0.0002_dp, 2.0_dp, 0.17_dp, 1.9e-04_dp, 0.15_dp,  & 
       0.0457_dp, 1.8_dp, 0.31_dp, 0.0086_dp, 1.5_dp, 0.22_dp, 0.0015_dp, &!15: gobi 
       2.0_dp, 0.34_dp, 0.0002_dp, 2.0_dp, 0.12_dp, 3.9e-05_dp, 0.13_dp,  & 
       0.0293_dp, 1.8_dp, 0.39_dp, 0.0090_dp, 1.5_dp, 0.16_dp, 0.0015_dp, &!16: other mixture soils 
       2.0_dp, 0.35_dp, 0.0002_dp, 2.0_dp, 0.10_dp, 3.1e-05_dp, 0.13_dp,  & 
       0.0305_dp, 1.5_dp, 0.46_dp, 0.0101_dp, 1.5_dp, 0.41_dp, 0.0015_dp, &!17: desert and sand land
       2.0_dp, 0.10_dp, 0.0002_dp, 2.0_dp, 0.03_dp, 2.8e-06_dp, 0.12_dp   &
        /), (/ 17, 14 /), ORDER=(/2,1/))


  END SUBROUTINE set_dust_data
!==================================================================================================== 
!>>csld #433
  SUBROUTINE set_bc_nduscale_reg ( p_patch )
  
    ! Description:
    ! ------------
    ! *set_bc_nduscale_reg* define the boundary condition for reading the input file containing the different regions (#433)
    !
    ! Authors:
    ! --------
    ! C. Siegenthaler, C2SM-ETHZ, June 2015 : original source
    !
    ! Interface:
    ! ----------
    ! *set_bc_nduscale_reg* is called from *bgc_dust_initialize*.
    !
    ! Method:
    ! -------
    ! Reads regions defined in external netcdf file
    !
    ! The integer contained in the external netcdf file dust_regions.nc describes the following  8 regions
    !   (the integer correspond also to the index of the region in nduscale_reg)
    !-----------------------------------------------------------------
    !     1       All the other locations than the following regions
    !     2       North america
    !     3       South America 
    !     4       North Africa 
    !     5       South Africa 
    !     6       Middle East 
    !     7       Asia
    !     8       Australia 

    USE mo_boundary_condition,       ONLY: bc_nml, bc_define, p_bcast_bc, BC_BOTTOM,BC_REPLACE
    USE mo_external_field_processor, ONLY: EF_FILE, EF_LONLAT, EF_IGNOREYEAR, EF_NOINTER
    USE mo_parallel_config,          ONLY: nproma

    USE mo_model_domain,             ONLY: t_patch

    TYPE(t_patch), INTENT(IN)  :: p_patch

    TYPE(bc_nml) :: bc_regint            

    INTEGER :: nblks_c

    nblks_c=p_patch%nblks_c

    ! allocate the global 2D tuning array nduscale_2d
    IF (.NOT. ALLOCATED(nduscale_2d)) THEN
      ALLOCATE (nduscale_2d(nproma, nblks_c))  
    END IF
    ! Set defaults
    ibc_regint   = -1

    ! define the BC for regions
    bc_regint%bc_domain = BC_BOTTOM
    bc_regint%ef_type = EF_FILE
    bc_regint%ef_template = 'dust_regions.nc' 
    bc_regint%ef_varname = 'regions'
    bc_regint%ef_geometry = EF_LONLAT
    bc_regint%ef_timedef = EF_IGNOREYEAR
    bc_regint%ef_interpolate = EF_NOINTER 
    bc_regint%bc_mode = BC_REPLACE

    !cms is this really needed here?
    CALL p_bcast_bc (bc_regint,   p_io)
    
    ! assign index for BC
    ibc_regint   = bc_define( p_patch%id, 'regions for different nduscale', bc_regint, 2, .TRUE.)
    
     END SUBROUTINE set_bc_nduscale_reg
     

     SUBROUTINE comp_nduscale_reg(jg, p_patch, current_date)  
    ! Description:
    ! ------------
    ! *comp_nduscale_reg* computes a location-dependent tuning factor nduscale to resolve Issue #433
    !
    ! Authors:
    ! --------
    ! C. Siegenthaler, C2SM-ETHZ, June 2015 : original source
    !
    ! Interface:
    ! ----------
    ! *comp_nduscale_reg* is called from *bgc_dust_calc_emiss*.
    !
    ! Method:
    ! -------
    ! Get regions defined in external netcdf file through bc_apply, derive 2D nduscale by
    ! assigning the appropriate nduscale for each location

       USE mo_boundary_condition,  ONLY: bc_apply
       USE mo_parallel_config,     ONLY: nproma
       USE mtime,                  ONLY: datetime
       USE mo_model_domain,        ONLY: t_patch
       USE mo_impl_constants,      ONLY: min_rlcell_int, grf_bdywidth_c
       USE mo_loopindices,         ONLY: get_indices_c

       INTEGER,                 INTENT( IN ) :: jg
       TYPE(datetime), POINTER, INTENT( IN ) :: current_date       
       TYPE(t_patch),           INTENT( IN ) :: p_patch

       INTEGER        ::  jreg     
       INTEGER        ::  nreg     = 8   ! number of regions
       
       REAL(dp)       ::  zregint(nproma) 

       INTEGER   :: nblks_c

       INTEGER  :: i_nchdom, rl_start, rl_end
       INTEGER  :: jc, jcs, jce         !< cell in row index, start and end indices
         
       INTEGER  :: i_startblk, i_endblk, jb


       nblks_c = p_patch%nblks_c

       ! Inquire current grid level and the total number of grid cells (code snippet based on mo_interface_iconam echam)
       i_nchdom = MAX(1,p_patch%n_childdom)
       rl_start = grf_bdywidth_c+1
       rl_end   = min_rlcell_int
      
       i_startblk = p_patch%cells%start_blk(rl_start,1)
       i_endblk   = p_patch%cells%end_blk(rl_end,i_nchdom)

       DO jb = i_startblk,i_endblk
          CALL get_indices_c(p_patch, jb, i_startblk, i_endblk, jcs,jce, rl_start, rl_end)
        ! get the regions integer from the input file
          CALL bc_apply(jg, ibc_regint, nproma, nblks_c, jb, current_date, zregint)

          ! assign the right tuning factor for each location depending on in which region it is lying
          DO jreg=1,nreg
            nduscale_2d(jcs:jce,jb)=MERGE(nduscale_reg(jreg), nduscale_2d(jcs:jce,jb), INT(zregint(jcs:jce)) .EQ. jreg)
          END DO

        END DO
    
  END SUBROUTINE comp_nduscale_reg
!!<<csld #433
!====================================================================================================

  SUBROUTINE bgc_read_annual_fields( p_patch )

    ! Description:
    ! ------------
    ! *bgc_read_annual_fields* reads the prescribed annual field of preferential dust source area
    !
    ! Authors:
    ! --------
    ! M. Werner, MPI BGC, November 2002: original source
    ! P. Stier,  MPI Met, January  2004: replaced by fast NetCDF read-in
    ! M. Werner, MPI BGC, October  2004: changed emission cheme from 0.5x0.5 degree grid to 
    !                                    default Echam5-HAM grid (to save computational costs)
    !                                 => annual fields of different biome types are not read
    !                                    in this version any longer but may be added again in future releases
    !
    ! Interface:
    ! ----------
    ! *bgc_read_annual_fields* is called from *call_read_forcing* in file *call_submodels.f90*.
    !
    ! Method:
    ! -------
    ! Reads biome and soil fields specified in the three different namlists 
    ! *biome_fields.nml*, *soil_types.nml*, *pot_sources.nml*
    ! and stores them in the corresponding variables.
    !
    ! This routine is based on *xt_read_emiss_fields* by Philip Stier,  MPI-Met, HH.
    ! 
    USE mo_exception,          ONLY: message, message_text, em_info, em_param
    USE mo_util_string,        ONLY: separator

    USE mo_parallel_config,    ONLY: nproma
    USE mo_model_domain,       ONLY: t_patch

    USE mo_io_config,          ONLY: default_read_method
    USE mo_read_interface,     ONLY: openInputFile, closeFile, on_cells, &
    &                                t_stream_id, read_2D_time, read_2D

    IMPLICIT NONE

    TYPE(t_patch),INTENT(IN)  :: p_patch

    TYPE(t_stream_id)         :: stream_id
                          
    INTEGER        :: ierr
    INTEGER        :: i, j
    
    CHARACTER(512)   :: cfile                               ! filename

    !>>Kai Zhang, 2009-02
    REAL(dp)       :: frac_eastasia(nproma)            ! total fraction of east asia soil type (from tiantao) 
    !<<Kai Zhang, 2009-02

    REAL(dp), POINTER             :: zvar2d(:,:), zvar3d(:,:,:)


    INTEGER :: nblks_c

    nblks_c=p_patch%nblks_c

!cms not used    IF (.NOT. ALLOCATED(biome))    ALLOCATE (biome(nlon, ngl))
    IF (.NOT. ALLOCATED(mat_s1))   ALLOCATE (mat_s1(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s2))   ALLOCATE (mat_s2(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s3))   ALLOCATE (mat_s3(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s4))   ALLOCATE (mat_s4(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s6))   ALLOCATE (mat_s6(nproma, nblks_c))
!   read asian soil type data (T. Cheng)
    IF (.NOT. ALLOCATED(mat_s13))  ALLOCATE (mat_s13(nproma, nblks_c))    
    IF (.NOT. ALLOCATED(mat_s14))  ALLOCATE (mat_s14(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s15))  ALLOCATE (mat_s15(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s16))  ALLOCATE (mat_s16(nproma, nblks_c))
    IF (.NOT. ALLOCATED(mat_s17))  ALLOCATE (mat_s17(nproma, nblks_c))

    IF (.NOT. ALLOCATED(mat_psrc)) ALLOCATE (mat_psrc(nproma, nblks_c))

    !>>Bernd Heinold, 2014-03 #382
    IF (ndust == 5) THEN
       IF (.NOT. ALLOCATED(mat_msg))  ALLOCATE (mat_msg(nproma, nblks_c))
    ENDIF

    IF (.NOT. ALLOCATED(Z01)) ALLOCATE (Z01(nproma, nblks_c))
    IF (.NOT. ALLOCATED(Z02)) ALLOCATE (Z02(nproma, nblks_c))

    !SFNote: Both zin_degen_time and zin_all have a 'time' degenerate dimension to account for
    !        the new dust_preferential_sources.nc (#227) and the new merged soil types file (#253)
    !        in waiting for a proper handling by the boundary condition scheme (ToDo)

    !<<SF #253
    !--- Map soil type matrices onto mat_all
    DO i=1,nsoil_types
       NULLIFY(mat_all(i)%ptr)
    ENDDO
    mat_all(1)%ptr => mat_s2(:,:)
    mat_all(2)%ptr => mat_s3(:,:)
    mat_all(3)%ptr => mat_s4(:,:)
    mat_all(4)%ptr => mat_s6(:,:)
    mat_all(5)%ptr => mat_s13(:,:)
    mat_all(6)%ptr => mat_s14(:,:)
    mat_all(7)%ptr => mat_s15(:,:)
    mat_all(8)%ptr => mat_s16(:,:)
    mat_all(9)%ptr => mat_s17(:,:)
    !<<SF #253

    !--- Set biome type distribution:

    !cms not used biome(:,:)=21._dp      ! Biome type is set to dessert (= biome type 21) for all grid points

    !--- Set initial soil type distribution:

    mat_s1(:,:)=1._dp      ! Soil type is initialized to coarse texture (= soil type 1) for all grid points

    !--- Read other soil type distributions:
    
    CALL message('',separator)
    CALL message('bgc_read_annual_fields','Reading soil type distributions',level=em_info)

    cfile='soil_type_all.nc'
    CALL check_input_file_exist('bgc_read_annual_fields',cfile)

    stream_id=openInputFile(cfile, p_patch, default_read_method)

    DO i=1,nsoil_types
      WRITE(message_text,'(a,a,a,a)') 'reading soil type #',ADJUSTL(TRIM(jtypes(i))), &
                                             ' distribution from: ', TRIM(cfile)
      CALL message('',message_text,level=em_param)

      CALL read_2D_time(stream_id=stream_id, location=on_cells, variable_name="type"//ADJUSTL(TRIM(jtypes(i))), &
             &          return_pointer=zvar3d )
      CALL shape_check_fields(SHAPE(mat_all(i)%ptr(:,:)),SHAPE(zvar3d(:,:,1)),cfile,"type"//ADJUSTL(TRIM(jtypes(i))), &
                                  'bgc_read_annual_fields','mo_ham_dust')
      mat_all(i)%ptr(:,:) = zvar3d(:,:,1)
      DEALLOCATE(zvar3d)
    END DO

    CALL closeFile(stream_id)

    !--- Read preferential sources distribution:
    cfile='dust_preferential_sources.nc' !SF partial resolution of #227

    WRITE(message_text,'(a,a)') 'reading potential sources distribution ', TRIM(cfile)
    CALL message('',message_text,level=em_param)
    CALL check_input_file_exist('bgc_read_annual_fields',cfile)

    stream_id=openInputFile(cfile, p_patch, default_read_method)

    CALL read_2D_time(stream_id=stream_id, location=on_cells, variable_name="source", &
            &          return_pointer=zvar3d )
    CALL shape_check_fields(SHAPE(mat_psrc),SHAPE(zvar3d(:,:,1)),cfile,"source", &
                                 'bgc_read_annual_fields','mo_ham_dust')

    mat_psrc = zvar3d(:,:,1)
    DEALLOCATE(zvar3d)


    CALL closeFile(stream_id)

    !>>Bernd Heinold, 2014-03 #382
       !--- Read Saharan dust source activation frequency map from MSG-SEVIRI dust index
       !    (Schepanski et al., GRL 2007; RSE 2012):
    IF (ndust == 5) THEN

      cfile='dust_msg_pot_sources.nc'

      WRITE(message_text,'(a,a)') 'reading potential Saharan sources', TRIM(cfile)
      CALL message('',message_text,level=em_param)
      CALL check_input_file_exist('bgc_read_annual_fields',cfile)

      stream_id=openInputFile(cfile, p_patch, default_read_method)

      CALL read_2D(stream_id=stream_id, location=on_cells, variable_name="dsaf", &
            &       return_pointer=zvar2d )
      CALL shape_check_fields(SHAPE(mat_msg),SHAPE(zvar2d(:,:)),cfile,"dasf", &
                                 'bgc_read_annual_fields','mo_ham_dust')
      mat_msg=zvar2d 
      DEALLOCATE(zvar2d)
      CALL closeFile(stream_id)
    END IF
    !<<Bernd Heinold, 2014-03 #382

    CALL message('',separator)

    !>>Kai Zhang, 2009-02

    !if East Asia soil type is used, set original soil type fraction to zero in this region

    IF(k_dust_easo.eq.2) THEN

       DO j = 1,nblks_c !krow

       frac_eastasia(:) = mat_s13(:,j) + mat_s14(:,j) + mat_s15(:,j) + mat_s16(:,j) + mat_s17(:,j) 

       DO i = 1,nproma  !nproma 
          IF (frac_eastasia(i).gt.0) THEN
             mat_s1  (i,j) = frac_eastasia(i) 
             mat_s2  (i,j) = 0._dp
             mat_s3  (i,j) = 0._dp
             mat_s4  (i,j) = 0._dp
             mat_s6  (i,j) = 0._dp
             mat_psrc(i,j) = 0._dp
          END IF
       END DO 

       END DO 

    END IF 

    !<<Kai Zhang, 2009-02

    !>>Bernd Heinold, 2014-03 #382
     
    ! [ndust = 5]:
    ! Alternative approach for the Tegen et al. (2002) scheme using satellite observed dust-source-activation 
    ! frequencies to prescribe potential dust sources in the Sahara. The DSAF map is based on the IR
    ! dust index product of SEVIRI observations aboard the Meteosat Second Generation (MSG) satellite 
    ! (Schepanski et al., GRL 2007; RSE 2012).
    !
    ! Dust emission is calculated for grid cells, where at least 1 percent of the observations from March 2006 to
    ! February 2010 show dust source activations. The surface roughness in those areas is set to a constant 
    ! value of r_dust_z0s = 0.001 cm. 

    !--- Adapt soil type and roughness settings in the Saharan region

    IF (ndust == 5) THEN

       DO j = 1,nblks_c !krow
         DO i = 1,nproma  !nproma

           IF (mat_msg(i,j).gt.0) THEN
             mat_s1  (i,j) = 0._dp
             mat_s2  (i,j) = 0._dp
             mat_s3  (i,j) = 0._dp
             mat_s4  (i,j) = 0._dp
             mat_s6  (i,j) = 0._dp
             mat_psrc(i,j) = 0._dp

             IF (mat_msg(i,j).ge.0.01) THEN
               mat_psrc(i,j) = 1._dp
               Z01     (i,j) = r_dust_z0s
               Z02     (i,j) = r_dust_z0s
             END IF
           END IF

         END DO
       END DO

    END IF

    !<<Bernd Heinold, 2014-03 #382
    
    CALL set_bc_nduscale_reg ( p_patch )!csld #433

  END SUBROUTINE bgc_read_annual_fields


!====================================================================================================

!!mgs!! cleanup of interface
  SUBROUTINE bgc_dust_read_monthly(jg, p_patch, kmonth, ndurough)

    USE mo_model_domain,           ONLY: t_patch

    INTEGER,           INTENT( IN )  :: jg         !< domain/grid index 
    TYPE(t_patch),     INTENT( IN )  :: p_patch    
    INTEGER,           INTENT( IN )  :: kmonth
    REAL(dp),          INTENT( IN )  :: ndurough

  
    ! --- read effective monthly FPAR field
    CALL bgc_read_fpar_field(jg, p_patch, kmonth)

    ! --- set constant surface roughness if desired
    IF (ndurough>0.0_dp) CALL bgc_set_constant_surf_rough(ndurough)
 
  END SUBROUTINE bgc_dust_read_monthly

!====================================================================================================



  SUBROUTINE bgc_read_fpar_field(jg, p_patch, kmonth)

    ! Description:
    ! ------------
    ! *bgc_read_fpar_field* reads the effective FPAR field (NDVI satellite data or KERNEL output)
    !
    ! Authors:
    ! --------
    ! M. Werner, MPI BGC, November 2002: original source
    ! P. Stier,  MPI Met, January  2004: replaced by fast NetCDF read-in
    ! M. Werner, MPI BGC, October  2004: changed emission cheme from 0.5x0.5 degree grid to 
    !                                    default Echam5-HAM grid (to save computational costs)
    ! M. Salzmann, LIM,   July     2017: adapted for ICON
    !
    ! Interface:
    ! ----------
    ! *bgc_read_fpar_field* is called from *call_read_forcing* in file *call_submodels.f90*.
    !
    ! Method:
    ! -------
    ! Reads daily (or monthly) effective FPAR fields specified in the namlist 
    ! *fpar_eff.nml* and stores it in the variable *k_fpar_eff*.
    !

    USE mo_exception,              ONLY: finish, message, message_text, em_info, em_param
    USE mo_model_domain,           ONLY: t_patch
    USE mo_util_string,            ONLY: separator

    USE mo_parallel_config,        ONLY: nproma

    USE mo_io_config,              ONLY: default_read_method
    USE mo_read_interface,         ONLY: openInputFile, closeFile, on_cells, &
    &                                    t_stream_id, read_2D_time, read_2D

    IMPLICIT NONE

    INTEGER,           INTENT( IN )  :: jg         !< domain/grid index 
    TYPE(t_patch),     INTENT( IN )  :: p_patch   
    INTEGER,           INTENT( IN )  :: kmonth

    ! local vars 
    TYPE(t_stream_id)                :: stream_id
    INTEGER                          :: nblks_c
    CHARACTER(512)                   :: cfile  ! filename
    REAL(dp), POINTER                :: p3d(:,:,:)

    nblks_c=p_patch%nblks_c


    IF (.NOT. ALLOCATED(k_fpar_eff)) ALLOCATE (k_fpar_eff(nproma, nblks_c))

    !>>T. Cheng 2006-2007

    !read surface roughness length data 

    CALL message('',separator)
    CALL message('','')

    cfile='dust_potential_sources.nc' !csld / SF partial resolution of #227

    WRITE(message_text,'(a,a)') 'reading dust potential sources field ', TRIM(cfile)
    CALL message('bgc_read_fpar_field',message_text,level=em_info)

    CALL check_input_file_exist('bgc_read_fpar_field',cfile)

    stream_id=openInputFile(cfile, p_patch, default_read_method)

    CALL read_2D_time(stream_id=stream_id, location=on_cells, variable_name="pot_source", &
           &          return_pointer=p3d, start_timestep=kmonth, end_timestep=kmonth      ) 
    CALL shape_check_fields(SHAPE(k_fpar_eff),SHAPE(p3d(:,:,1)),cfile,"pot_source",       &
                                'bgc_read_fpar_field','mo_ham_dust')
    k_fpar_eff=p3d(:,:,1)
    DEALLOCATE(p3d)
    CALL closeFile(stream_id)


    !--- Read surface roughness length field:
    ! cms Earlier the same file was read twice here. For now, there is only one read statement.
    ! cms Also, for ndurough >0 (default), this read is probably not necessary

    cfile='surface_rough_12m.nc'

    WRITE(message_text,'(a,a)') 'reading surface roughness field', TRIM(cfile)
    CALL message('bgc_read_fpar_field',message_text,level=em_info)

    CALL check_input_file_exist('bgc_read_fpar_field',cfile)

    stream_id=openInputFile(cfile, p_patch, default_read_method)

    CALL read_2D_time(stream_id=stream_id, location=on_cells, variable_name="surfrough", &
           &          return_pointer=p3d, start_timestep=kmonth, end_timestep=kmonth      ) 
    CALL shape_check_fields(SHAPE(Z02),SHAPE(p3d(:,:,1)),cfile,"surfrough",              &
                                'bgc_read_fpar_field','mo_ham_dust')
    Z01=p3d(:,:,1)
    Z02=p3d(:,:,1)  ! cms as before, Z01 and Z02 are identical ...
    DEALLOCATE(p3d)
    CALL closeFile(stream_id)


    !<<T. Cheng 2006-2007

    !>>Kai Zhang, 2009-02

    Z01(:,:) = max (Z01(:,:), r_dust_z0min)
    Z02(:,:) = max (Z02(:,:), r_dust_z0min)
    
    Z01(:,:) = Z01(:,:) * r_dust_scz0
    Z02(:,:) = Z02(:,:) * r_dust_scz0

    !<<Kai Zhang, 2009-02

    CALL message('','')
    CALL message('',separator)

  END SUBROUTINE bgc_read_fpar_field
  

!====================================================================================================


  SUBROUTINE bgc_set_constant_surf_rough(roughness_length)

    ! Description:
    ! ------------
    ! *bgc_set_constant_surf_rough* overwrites the surface roughness length
    ! fields with a given constant surface roughness length (cm).
    !
    ! Authors:
    ! --------
    ! J. Kazil, MPI-M, November 2008
    !
    ! Interface:
    ! ----------
    ! *bgc_set_constant_surf_rough* is called from *call_read_forcing* in file *call_submodels.f90*.
    !
    ! Method:
    ! -------
    ! Fills the surface roughness fields Z01 and Z02 with a given constant.

    IMPLICIT NONE

    REAL(dp):: roughness_length
    
    Z01 = roughness_length
    Z02 = roughness_length
    
  END SUBROUTINE bgc_set_constant_surf_rough
  

!====================================================================================================


  SUBROUTINE setdust( p_patch )

    ! Description:
    ! ------------
    ! *setdust* gets pre-configured defaults and reads the ham_dustctl namelist for potential overwritting
    !
    ! author: m.schultz@fz-juelich.de
    USE mo_ham_dustctl_config,         ONLY: ham_dustctl
    USE mo_config_util,                ONLY: set_config
    USE mo_exception,           ONLY: finish, message, message_text, em_info, em_warn, em_param
    USE mo_submodel,            ONLY: print_value
    USE mo_util_string,         ONLY: separator
    USE mo_model_domain,        ONLY: t_patch 

    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    !--- Set defaults based on context !SF #479
    CALL get_dust_namelist_defaults( p_patch )
    
    !--- Take values from namelist:
    CALL message('',separator)
    CALL message('setdust', 'Importing variables from namelist ham_dustctl...', level=em_info)

    CALL set_config( ndurough, ham_dustctl % ndurough )
    CALL set_config( nduscale_reg , ham_dustctl % nduscale_reg )
    CALL set_config( r_dust_lai , ham_dustctl % r_dust_lai )
    CALL set_config( r_dust_umin , ham_dustctl % r_dust_umin )
    CALL set_config( r_dust_z0s , ham_dustctl % r_dust_z0s ) 
    CALL set_config( r_dust_scz0 , ham_dustctl % r_dust_scz0 )
    CALL set_config( r_dust_z0min , ham_dustctl % r_dust_z0min )
    CALL set_config( r_dust_sf13 , ham_dustctl % r_dust_sf13 )
    CALL set_config( r_dust_sf14 , ham_dustctl % r_dust_sf14 )
    CALL set_config( r_dust_sf15 , ham_dustctl % r_dust_sf15 )
    CALL set_config( r_dust_sf16 , ham_dustctl % r_dust_sf16 )
    CALL set_config( r_dust_sf17 , ham_dustctl % r_dust_sf17 )
    CALL set_config( r_dust_af13 , ham_dustctl % r_dust_af13 )
    CALL set_config( r_dust_af14 , ham_dustctl % r_dust_af14 )
    CALL set_config( r_dust_af15 , ham_dustctl % r_dust_af15 )
    CALL set_config( r_dust_af16 , ham_dustctl % r_dust_af16 )
    CALL set_config( r_dust_af17 , ham_dustctl % r_dust_af17 )
    CALL set_config( k_dust_smst , ham_dustctl % k_dust_smst )
    CALL set_config( k_dust_easo , ham_dustctl % k_dust_easo ) 



    !--- Report parameter settings
    CALL message('','',level=em_param)
    CALL message('setdust','Parameter settings for BGC dust emission model',level=em_info)
    CALL message('','',level=em_param)
    IF (ndurough==0.0_dp)  THEN
      CALL message('','Using satellite-derived surface roughness length map (ndurough = 0.)', level=em_param)
      CALL message('','(Prigent et al., JGR 2005)', level=em_param)
    ELSE
      CALL print_value(' Globally constant surface roughness length (ndurough)  = ', ndurough)
    ENDIF
    CALL message('', '', level=em_param)
!>>csld #433
    CALL message('', 'Regional threshold wind friction velocity:', level=em_param)
    CALL print_value('nduscale_reg(2) (North America) = ', nduscale_reg(2)) 
    CALL print_value('nduscale_reg(3) (South America) = ', nduscale_reg(3)) 
    CALL print_value('nduscale_reg(4) (North Africa)  = ', nduscale_reg(4)) 
    CALL print_value('nduscale_reg(5) (South Africa)  = ', nduscale_reg(5)) 
    CALL print_value('nduscale_reg(6) (Middle East)   = ', nduscale_reg(6))
    CALL print_value('nduscale_reg(7) (Asia)          = ', nduscale_reg(7)) 
    CALL print_value('nduscale_reg(8) (Australia)     = ', nduscale_reg(8))
    CALL print_value('nduscale_reg(1) (elsewhere)     = ', nduscale_reg(1)) 
!<<csld #433
    CALL message('', '', level=em_param)
    CALL print_value(' r_dust_lai      = ', r_dust_lai)
    CALL print_value(' r_dust_umin     = ', r_dust_umin)
    CALL print_value(' r_dust_z0s      = ', r_dust_z0s)
    CALL print_value(' r_dust_scz0     = ', r_dust_scz0)
    CALL print_value(' r_dust_z0min    = ', r_dust_z0min)
    CALL print_value(' r_dust_sf13     = ', r_dust_sf13)
    CALL print_value(' r_dust_sf14     = ', r_dust_sf14)
    CALL print_value(' r_dust_sf15     = ', r_dust_sf15)
    CALL print_value(' r_dust_sf16     = ', r_dust_sf16)
    CALL print_value(' r_dust_sf17     = ', r_dust_sf17)
    CALL print_value(' r_dust_af13     = ', r_dust_af13)
    CALL print_value(' r_dust_af14     = ', r_dust_af14)
    CALL print_value(' r_dust_af15     = ', r_dust_af15)
    CALL print_value(' r_dust_af16     = ', r_dust_af16)
    CALL print_value(' r_dust_af17     = ', r_dust_af17)
    CALL message('', '', level=em_param)
    CALL print_value(' k_dust_smst     = ', k_dust_smst)
    CALL print_value(' k_dust_easo     = ', k_dust_easo)

    CALL message('',separator)

  END SUBROUTINE setdust

!====================================================================================================

  SUBROUTINE get_dust_namelist_defaults( p_patch )

    ! Description:
    ! ------------
    ! *get_dust_namelist_defaults* applies context-specific defaults for the dust namelist parameters
    !
    ! Authors:
    ! --------
    ! Sylvaine Ferrachat (ETH Zurich), January 2016

    !cms USE mo_control, ONLY: nn, lnudge, lcouple
    USE mo_kind,                ONLY: dp
    USE mo_model_domain,        ONLY: t_patch
    USE mo_exception,           ONLY: finish, message, em_warn, em_info
    USE mo_util_string,         ONLY: separator

    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    REAL(dp)    :: rsltn   ! horizontal resolution

    IF ( SIZE ( p_patch ) .GT. 1 ) THEN
      CALL finish('mo_ham_dust','nesting not supported')
    END IF

    rsltn = p_patch(1)%geometry_info%mean_characteristic_length

    !--- Configuration for dust scheme: preconfigured settings
    SELECT CASE (ndust)
       CASE (2)
           !Tiantao's version with modified scale factor for threshold velocity
           ndurough        = 0.000_dp
           r_dust_lai      = 1.E-10_dp
           r_dust_umin     = 21._dp
           r_dust_z0s      = 0.001_dp
           r_dust_scz0     = 1._dp
           r_dust_z0min    = 1.E-05_dp
           k_dust_smst     = 0
           k_dust_easo     = 0
           nduscale_reg(:) = 0.68_dp
       CASE (3)
           !Stier et al. (2005)
           ndurough     = 0.001_dp
           r_dust_lai   = 1.E-10_dp
           r_dust_umin  = 21._dp
           r_dust_z0s   = 0.001_dp
           r_dust_scz0  = 1._dp
           r_dust_z0min = 1.E-05_dp
           k_dust_smst  = 1
           k_dust_easo  = 1
    
           ! Choose the parameter for the threshold wind friction velocity based on
           ! the horizontal resolution: This parameterization was obtained by tuning
           ! nduscale_reg at the horizontal resolutions T21 and T42 to reproduce the
           ! total annual dust emissions at T63 with nduscale_reg(:)= 0.86 in nudged year
           ! 2000 runs.
           !
           ! THIS PARAMETERIZATION WILL LIKELY PRODUCE ERRONEOUS RESULTS FOR
           ! HORIZONTAL RESOLUTIONS > T63, FOR WHICH nduscale_reg MUST BE RE-TUNED!
           !
           ! jan.kazil@noaa.gov 2009-02-26 21:34:11 -07:00
           !cms   nduscale_reg(:)= -7.93650E-05_dp*dble(nn)**2.0_dp + 0.0095238_dp*dble(nn) + 0.575_dp
    
          nduscale_reg(:)= 0.86_dp
          CALL message('',separator)
          CALL message('mo_ha_dust','nduscale_reg default has not been tuned',level=em_warn)
          CALL message('',separator)

       CASE (4, 5)
           ! Stier et al. (2005) + East Asia soil properties 
           !
           ! OR 
           !
           ! Stier et al. (2005) with Saharan dust source
           ! activation map + East Asia soil properties

           ndurough     = 0.001_dp
           r_dust_lai   = 1.E-1_dp
           r_dust_umin  = 21._dp
           r_dust_z0s   = 0.001_dp
           r_dust_scz0  = 1._dp
           r_dust_z0min = 1.E-05_dp
           r_dust_af14  = 1.E-06_dp
           r_dust_sf13  = 0.6
           k_dust_smst  = 1
           k_dust_easo  = 2

        !   SELECT CASE (nn)
        !       CASE (63)
        !           IF (lnudge) THEN
        !              nduscale_reg(:) = (/ &
        !                                0.95_dp, & ! All the other locations than the following regions
        !                                1.25_dp, & ! North America
        !                                1.25_dp, & ! South America
        !                                0.95_dp, & ! North Africa
        !                                0.95_dp, & ! South Africa
        !                                0.95_dp, & ! Middle East
        !                                1.25_dp, & ! Asia
        !                                0.95_dp  & ! Australia
        !                               /)
        !           ELSE
        ! cms this needs to be checked
!!$        IF ( rsltn .GE. 150e3_dp .AND.  rsltn .LE. 300e3_dp ) THEN
!!$                      nduscale_reg(:) = (/ &
!!$                                        1.05_dp, & ! All the other locations than the following regions
!!$                                        1.45_dp, & ! North America
!!$                                        1.45_dp, & ! South America
!!$                                        1.05_dp, & ! North Africa
!!$                                        1.05_dp, & ! South Africa
!!$                                        1.05_dp, & ! Middle East
!!$                                        1.45_dp, & ! Asia
!!$                                        1.05_dp  & ! Australia
!!$                                       /)
!!$        ELSE
        !           ENDIF
        !       CASE DEFAULT
!!&                   nduscale_reg(:)= 0.86_dp
                   nduscale_reg(:)= 0.9_dp !1.0_dp
        !   END SELECT
!!$        END IF

         CALL message('',separator)
         CALL message('mo_ha_dust','nduscale_reg default has not been properly tuned',level=em_warn)
         CALL message('',separator)

           !>>SF obsolete code. Kept as example for potential resurrection
           !IF (lcouple) THEN
           !   nduscale_reg(:) = 0.92_dp  !checked with T63L47, by Lorenzo Tomassini (ZMAW) on 2013-01
           !ENDIF
           !
           !IF (lnudge) THEN
           !   nduscale_reg(:)= -7.93650E-05_dp*dble(nn)**2.0_dp + 0.0095238_dp*dble(nn) + 0.575_dp
           !   IF(nn.ge.63) nduscale_reg(:)= 0.86_dp
           !ENDIF    
           !<<SF

    END SELECT

  END SUBROUTINE get_dust_namelist_defaults

!====================================================================================================


  SUBROUTINE bgc_dust_initialize( jg, p_patch, current_date )

    ! Description:
    ! ------------
    ! *bgc_dust_initialize* initializes several parameters and variables of the MPI BGC dust emission scheme.
    ! It also performs the wind independent calculations of threshold friction velocity and soil particle
    ! distribution. The effective soil fraction for dust deflation is determined, too.
    !
    ! Authors:
    ! --------
    ! M. Werner, MPI BGC, November 2002
    ! P. Stier, MPI MET,  June     2004: added scale factor for wind stress threshold
    ! M. Werner, MPI BGC, October  2004: changed emission cheme from 0.5x0.5 degree grid to 
    !                                    default Echam5-HAM grid (to save computational costs)
    !
    ! Interface:
    ! ----------
    ! *bgc_dust_initialize* is called from *call_read_forcing* in file *call_submodels.f90*.
    !
    ! This routine is based on the offline dust emission scheme by Ina Tegen, MPI BGC.
    ! 

    USE mo_exception,          ONLY: finish, message, EM_WARN, EM_INFO
    USE mo_math_constants,     ONLY: pi
    USE mo_physical_constants, ONLY: grav
    !cms USE mo_control,       ONLY: nlon, ngl
    !cms USE mo_decomposition, ONLY: lc => local_decomposition
    USE mo_parallel_config,    ONLY: nproma
    USE mo_model_domain,       ONLY: t_patch
    USE mtime,                 ONLY: datetime
  
    USE mo_boundary_condition,       ONLY: bc_find, bc_nml, bc_define, BC_REPLACE
    USE mo_external_field_processor, ONLY: EF_MODULE 

    IMPLICIT NONE

    INTEGER,                 INTENT( IN ) :: jg  ! domain/grid index
    TYPE(t_patch),           INTENT( IN ) :: p_patch
    TYPE(datetime), POINTER, INTENT( IN ) :: current_date    

    REAL(dp), PARAMETER :: gravi=grav*100._dp              ! gravity acceleration in cm/s2

    REAL(dp)            :: AAA, BB, CCC, DDD, EE, FF    ! tool variables
    
    REAL(dp)            :: dlastj(nclass)
    REAL(dp)            :: rdp

    INTEGER             :: i, j                         ! do indices
    INTEGER             :: kk                           ! do index
    INTEGER             :: nd, nsi, np
    INTEGER             :: ns                           ! do index

    REAL(dp)            :: rsize(nclass)                ! granulometric class size
    REAL(dp)            :: stotal                       ! total surface
    REAL(dp)            :: stotalV
    REAL(dp)            :: su                           ! surface 
    REAL(dp)            :: suV
    REAL(dp)            :: su_loc                       ! surface
    REAL(dp)            :: su_locV
    REAL(dp)            :: su_class(nclass)             ! surface occupied by each granulometric class
    REAL(dp)            :: su_classV(nclass)
    REAL(dp)            :: sum_srel(nats,nclass)        ! srel accumulated

    REAL(dp)            :: utest(nats)

    INTEGER             :: vgtp        

    REAL(dp)            :: xnV
    REAL(dp)            :: xk,xl,xm,xn                  ! tool variables

    INTEGER             :: nblks_c, ierr

    TYPE(bc_nml)        :: bc_ssf,      &     ! surface snow fraction 'boundary condition'
                           bc_ws_l1,    &     ! soil water content [m] 'boundary condition' 
                           bc_ws_fc_l1        ! soil field capacity [m] 'boundary condition' 

!---

    ! --- former mo_bgc_dust_data
    CALL set_dust_data

    ! --- initialisation now called from mo_ham_init

    CALL bgc_prestat                  ! read in dust emission statistics
    CALL bgc_read_annual_fields( p_patch )

    ! --- original code from here on
    nblks_c=p_patch%nblks_c
    IF (.NOT. ALLOCATED(flux_ann))  ALLOCATE (flux_ann(nproma, nblks_c))
    IF (.NOT. ALLOCATED(flux_a1))   ALLOCATE (flux_a1(nproma,  nblks_c))
    IF (.NOT. ALLOCATED(flux_a2))   ALLOCATE (flux_a2(nproma,  nblks_c))
    IF (.NOT. ALLOCATED(flux_a10))  ALLOCATE (flux_a10(nproma, nblks_c))
!cms not used    IF (.NOT. ALLOCATED(idust))     ALLOCATE (idust(nlon, ngl))

    flux_ann(:,:) = 0._dp
    flux_a1 (:,:) = 0._dp
    flux_a2 (:,:) = 0._dp
    flux_a10(:,:) = 0._dp
    rsize     (:) = 0._dp
    srel    (:,:) = 0._dp
    srelV   (:,:) = 0._dp
    sum_srel(:,:) = 0._dp
    su_srelV(:,:) = 0._dp
    Uth       (:) = 0._dp
    utest     (:) = 0._dp

    i = 0

    rdp = Dmin

    ! calculation of the threshold friction velocity Uth, mat-95 formula (6)-(7)

    DO WHILE(rdp.lE.Dmax + 1.E-5_dp)

       i = i + 1

       rsize(i) = rdp

       BB = a_rnolds * (rdp ** x_rnolds) + b_rnolds  !formula (5) in mat-95

       AAA = SQRT(rop * gravi * rdp / roa)           !formula after (4) in mat-95

       CCC = SQRT(1._dp + d_thrsld /(rdp ** 2.5_dp)) !formula after (4) in mat-95

       IF (BB.LT.10._dp) THEN                        !formula (6) in mat-95
          DDD=SQRT(1.928_dp * (BB ** 0.092_dp) - 1._dp)
          Uth(i) = 0.129_dp * AAA * CCC / DDD
       ELSE                                          !formula (7) in mat-95
          EE = -0.0617_dp * (BB - 10._dp) 
          FF = 1._dp -0.0858_dp * EXP(EE)
!gf          Uth(i) = 0.12_dp * AAA * CCC * FF       
          Uth(i) = 0.129_dp * AAA * CCC * FF      
       ENDIF

       rdp = rdp * EXP(Dstep)   

    END DO      

    ! calculation of the soil particle distribution and related surfaces

    DO ns = 1,nats                                      ! loop over all soil types
       
       rdp = Dmin                                       
       kk = 0
       stotal  = 0._dp
       stotalV = 0._dp
       su_class (:) = 0._dp
       su_classV(:) = 0._dp

       DO WHILE (rdp.LE.Dmax+1.E-5_dp)                  ! surface calculations

          kk = kk + 1
          su  = 0._dp
          suV = 0._dp
          
          DO i = 1, nmode
          
             nd  = ((i - 1) *3 ) + 1
             nsi = nd + 1
             np  = nd + 2

             IF (solspe(ns,nd).EQ.0._dp) THEN            
                su_loc = 0._dp
                su_locV= 0._dp
             ELSE
                xk = solspe(ns,np)/(sqrt(2._dp* pi)*log(solspe(ns,nsi)))
                xl = ((log(rdp)-log(solspe(ns,nd)))**2)/(2._dp*(log(solspe(ns,nsi)))**2)
                xm = xk * exp(-xl)   !mass size distribution, formula (29) in mat-95
                xn = rop*(2._dp/3._dp)*(rdp/2._dp)  !surface, formula (30) in mat-95 
                xnV= 1._dp !volume
                su_loc  = (xm*Dstep/xn)       
                su_locV = (xm*Dstep/xnV)     
             ENDIF !

             su = su + su_loc
             suV = suV + su_locV

          END DO !Nmode

          su_class (kk) = su
          su_classV(kk) = suV
          stotal  = stotal  + su
          stotalV = stotalV + suV

          dlastj(kk) = rdp*5000._dp
          rdp = rdp * exp(Dstep)

       END DO !rdp
           
       DO j = 1,nclass !formula (32) in mat-95 

          IF (stotal.eq.0._dp) THEN
             srel(ns,j) = 0._dp
             srelV(ns,j) = 0._dp
          ELSE
             srel (ns,j) = su_class (j)/stotal
             srelV(ns,j) = su_classV(j)/stotalV
             utest(ns)   = utest(ns)+srelV(ns,j)
             su_srelV(ns,j) = utest(ns) !sum of nclasses
          ENDIF

       END DO !j=1,nclass
         
    END DO !ns (soil type)


    !currently not used in this code 

!cms ++ not used
!    DO j = 1,ngl                       ! loop over all latitudes
!    DO i = 1,nlon                      ! loop over all longitudes
!       idust(i,j) = 0                  ! check if vegetation type allows dust deflation
!       vgtp = int(biome(i,j))          ! (idust=0: no deflation allowed; idust=1: deflation allowed)
!       IF (vgtp.GT.0) idust(i,j) = active(vgtp)
!    END DO !i
!    END DO !j
!cms --    

    !>>Kai Zhang, 2009-02

    !the factor of F (horizontal flux) / G (vertical flux)

    solspe(13,nmode*3+1) = r_dust_af13
    solspe(14,nmode*3+1) = r_dust_af14
    solspe(15,nmode*3+1) = r_dust_af15
    solspe(16,nmode*3+1) = r_dust_af16
    solspe(17,nmode*3+1) = r_dust_af17

    !<<Kai Zhang, 2009-02

!!mgs, 2010-02!!    CALL bgc_dust_init_diag
  !cms++ moved to here
     IF (.NOT. ALLOCATED(flux_6h)) ALLOCATE (flux_6h(nproma, ntrace, p_patch%nblks_c))
  !cms --

  !cms ++
    CALL bc_find(jg, 'surface snow fraction', ibc_ssf, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ssf%ef_type = EF_MODULE
      bc_ssf%bc_mode = BC_REPLACE
      bc_ssf%ef_actual_unit='m2 m-2'
      ibc_ssf = bc_define(jg, 'surface snow fraction', bc_ssf, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'ssf bc already defined - will use previous definition', level=EM_INFO )
    END IF
    CALL bc_find(jg, 'soil water content in first layer', ibc_ws_l1, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ws_l1%ef_type = EF_MODULE
      bc_ws_l1%bc_mode = BC_REPLACE
      bc_ws_l1%ef_actual_unit = 'm'
      ibc_ws_l1 = bc_define(jg, 'soil water content in first layer', bc_ws_l1, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'ws_l1 soil bc already defined - will use previous definition', level=EM_INFO )
    END IF
    CALL bc_find(jg, 'field capacity in first soil layer', ibc_ws_fc_l1, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ws_fc_l1%ef_type = EF_MODULE
      bc_ws_fc_l1%bc_mode = BC_REPLACE
      bc_ws_fc_l1%ef_actual_unit = 'm'
      ibc_ws_fc_l1 = bc_define(jg, 'field capacity in first soil layer', bc_ws_fc_l1, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'soil field capacity ws_fc_l1 bc already defined -  will use previous definition', level=EM_INFO )
    END IF
  !cms--
  END SUBROUTINE bgc_dust_initialize


!====================================================================================================

  SUBROUTINE bgc_dust_init_diag(emi_stream, table, shape2d)

  ! initialize dust emission diagnostics
  ! add field to emis stream (init_emi_stream in mo_emi_interface must have been called!)

    USE mo_linked_list,         ONLY: t_var_list
    USE mo_var_list,            ONLY: add_var
    USE mo_cf_convention,       ONLY: t_cf_var
    USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
       &                              ZA_SURFACE
    USE mo_grib2,               ONLY: t_grib2_var, grib2_var
    USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
      &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
      &                               GRID_UNSTRUCTURED, TSTEP_INSTANT,  &
      &                               TSTEP_ACCUM
    USE mo_io_config,           ONLY: lnetcdf_flt64_output
    USE mo_var_metadata,        ONLY: groups

    TYPE(t_var_list), POINTER  :: emi_stream
    INTEGER, INTENT(IN)        :: table       ! grib table
    INTEGER, INTENT(IN)        :: shape2d(2)  ! shape of 2-D output array (kproma, kblks)

    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
       datatype_flt = DATATYPE_FLT64
    ELSE
       datatype_flt = DATATYPE_FLT32
    END IF

    ! auxilliary variable ustar_acrit
    cf_desc    = t_cf_var( 'ustar_acrit', '%', 'time fraction with ustar > dust threshold', datatype_flt)
    grib2_desc = grib2_var(table, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( emi_stream, 'ustar_acrit',ustar_acrit,                                    &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                     )
 
!!$    IF ( .NOT. ALLOCATED(nduscale_2d)) THEN
!!$      ALLOCATE (nduscale_2d(shape2d(1), shape2d(2)))  
!!$    END IF
!!$    nduscale2dptr=>nduscale_2d
!!$
!!$    cf_desc    = t_cf_var( 'nduscale', '', 'dust tuning factor', datatype_flt)
!!$    grib2_desc = grib2_var(table, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( emi_stream, 'nduscale', nduscale2dptr,                                    &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
!!$                & lrestart=.FALSE.                                                          )



  END SUBROUTINE bgc_dust_init_diag

!====================================================================================================

  SUBROUTINE bgc_dust_calc_emis( jg, p_patch, kproma, kbdim, krow,  current_date,  plsmask,  pglac, psfcWind )

    ! Description:
    ! ------------
    ! *bgc_dust_calc_emis* calculates online global dust emission fields
    ! using the MPI BGC dust emissions scheme.
    !
    ! Authors:
    ! --------
    ! M. Werner, MPI BGC, October 2002
    ! P. Stier, MPI MET,  June    2004: added scale factor for wind stress threshold
    ! M. Werner, MPI BGC, October 2004: changed emission cheme from 0.5x0.5 degree grid to 
    !                                   default Echam5-HAM grid (to save computational costs)
    ! L. Kornblueh, MPI MET, July 2006: fix bug with respect to subscriptin in flux_6h
    !         
    !
    ! Interface:
    ! ----------
    ! The routine is called from *dust_emissions_bgc* in module *mo_bgc_dust_emis.f90*.
    !
    ! Method:
    ! -------
    ! The emissions are calculated every model time step using ECHAM 10m wind speeds.
    !
    ! This version of the emission scheme calculates dust emssissions for different bins, controlled by 
    ! dmin:   minimum particle diameter, 
    ! nbin:   number of binned size fractions, 
    ! ntrace: maximum number of bins, 
    ! dbmin(ntrace) & dbmax(ntrace): diameter limits of each bin
    !
    ! This routine is based on the offline dust emission scheme by Ina Tegen, MPI BGC.

    USE mo_physical_constants, ONLY: grav
    USE mo_run_config,         ONLY: dtime
!!$    USE mo_memory_g3b,    ONLY: slm, glac, vlt, wl, ws, wsmx, sn, snc, orostd
!!$    USE mo_physc2,        ONLY: cqsncr, cwlmax
!!$    USE mo_time_control,  ONLY: delta_time
!!$    USE mo_decomposition, ONLY: lc => local_decomposition
!!$    USE mo_vphysc,        ONLY: vphysc
!!$    USE mo_time_control,  ONLY: lstart,lresume !csld #433

    USE mo_boundary_condition, ONLY: bc_apply

    USE mo_subm_get_from_jsbach, ONLY: get_snow_cover_bc_from_jsbach,       &
                                       get_soil_moisture_from_jsbach,       &
                                       get_soil_field_capacity_from_jsbach

    USE mtime,             ONLY: datetime
    USE mo_model_domain,   ONLY: t_patch

    IMPLICIT NONE

    INTEGER,                 INTENT( IN )  :: jg, kproma, kbdim, krow
    REAL(dp),                INTENT( IN )  :: plsmask(kbdim),  &       ! land-sea mask (1. = land, 0. = sea/lakes)     
                                              pglac(kbdim),    &       ! fraction of land covered by glaciers
                                              psfcWind(kbdim)          ! 10m surface wind 

    TYPE(datetime), POINTER, INTENT( IN )  :: current_date
    TYPE(t_patch),           INTENT( IN )  :: p_patch      

    REAL(dp), PARAMETER          :: cd=1.00_dp*roa/(grav*100._dp) ! flux dimensioning parameter

    REAL(dp), PARAMETER          :: zepsec=1.E-12_dp    ! ECHAM parameter for snow cover calculation
    REAL(dp), PARAMETER          :: zsigfac=0.15_dp     ! ECHAM parameter for snow cover calculation
      
! Variables
    REAL(dp)           :: alpha

    REAL(dp)           :: dbmin(ntrace)              ! bin size limits
    REAL(dp)           :: dbmax(ntrace)              ! bin size limits
    REAL(dp)           :: dlast                      ! bin size limits
    REAL(dp)           :: rdp
    REAL(dp)           :: dpk(ntrace)
    INTEGER            :: dn(kproma)               ! number of dislocated particle classes
    INTEGER            :: dk(kproma,nclass)        ! dislocated particle classes
    
    REAL(dp)           :: du_snow(kproma)          ! ECHAM snow coverage interpolated to 0.5x0.5 grid resolution
    REAL(dp)           :: du_W1r(kproma)           ! ECHAM relative soil humidity interpolated to 0.5x0.5 grid resolution
    REAL(dp)           :: du_wind(kproma)          ! ECHAM wind field interpolated to 0.5x0.5 grid resolution

    INTEGER            :: dust_mask(kproma)        ! grid point mask for dust emissions (0: no emission)

!lk to reduce bank conflicts on SX-6 added 1 to kproma
    REAL(dp)           :: fluxtyp(kproma+1,nclass)
    REAL(dp)           :: fluxbin(kproma,ntrace)
    REAL(dp)           :: fdp1(nats), fdp2(nats)
    REAL(dp)           :: c_eff(kproma)             ! fraction efficiency
    
    REAL(dp)           :: fluxdiam1(kproma,nclass)  ! flux for soil type #1
    REAL(dp)           :: fluxdiam2(kproma,nclass)  ! flux for soil type #2
    REAL(dp)           :: fluxdiam3(kproma,nclass)  ! flux for soil type #3
    REAL(dp)           :: fluxdiam4(kproma,nclass)  ! flux for soil type #4
    REAL(dp)           :: fluxdiam6(kproma,nclass)  ! flux for soil type #6
    REAL(dp)           :: fluxdiam_pf(kproma,nclass)! flux for preferential sources soil type
    REAL(dp)           :: fluxdiam13(kproma,nclass) ! flux for soil type #13
    REAL(dp)           :: fluxdiam14(kproma,nclass) ! flux for soil type #14
    REAL(dp)           :: fluxdiam15(kproma,nclass) ! flux for soil type #15
    REAL(dp)           :: fluxdiam16(kproma,nclass) ! flux for soil type #16
    REAL(dp)           :: fluxdiam17(kproma,nclass) ! flux for soil type #17
    
    INTEGER            :: i, j                      ! loop index
    INTEGER            :: i_soil                    ! soil type
 
    REAL(dp)           :: AAA, BB, CCC, DDD, EE, FF !tool variables
    REAL(dp)           :: d1                        !distance between obstacles
    REAL(dp)           :: feff
    
    INTEGER            :: kk,kkk                    ! loop index
    INTEGER            :: kkmin
        
    INTEGER            :: n,nn                      ! loop index
    
    REAL(dp)           :: Ustar,Ustar_d(kproma)     ! threshold friction velocity for saltation
    REAL(dp)           :: uthp
    
    REAL(dp)           :: zw1r(kproma)             ! relative soil moisture
    REAL(dp)           :: zcvs(kproma)             ! snow cover fraction from JSBAC
    REAL(dp)           :: ws(kproma)               ! soil moisture (m) from JSBACH   
    REAL(dp)           :: wsmx(kproma)             ! max. soil moisture (m) from JSBACH

    !>>Kai Zhang, 2009-02
    REAL(dp)           :: mf13(kproma) 
    REAL(dp)           :: mf14(kproma) 
    REAL(dp)           :: mf15(kproma) 
    REAL(dp)           :: mf16(kproma) 
    REAL(dp)           :: mf17(kproma) 
    REAL(dp)           :: utsc(kproma) 
    REAL(dp)           :: tmpa, tmpb, tmpc         ! tmp vars 
    !<<Kai Zhang, 2009-02

!---

    !>>csld #433 computation of some quantities deriving from input files read with the BC scheme
    ! cms moved CALL comp_nduscale_reg(jg, kproma, kbdim, p_patch%nblks_c, current_date, krow)
    !<<csld


!cms moved to init    IF (.NOT. ALLOCATED(flux_6h)) ALLOCATE (flux_6h(nproma, ntrace, nblks_c))

    du_wind(:) = psfcWind(1:kproma) !cms vphysc%velo10m(1:kproma,krow)

    !>>SF #458 (replacing where statements)
    du_wind(:) = MERGE( &
                       0._dp, & ! set missing values to zero
                       du_wind(:), &
                       (du_wind(:) < 0._dp))
    !<<SF #458 (replacing where statements)


   ! get snow cover fraction *zcvs*, soil moisture (ws), and maximum soil moisture (wsmx) from JSBACH 
   ! and compute relative soil moisture *zw1r* (here: ratio *ws* to *wsmax*)

!cms++ temporary? (re-check mapping for jsbach variables) 
    CALL get_snow_cover_bc_from_jsbach( jg, ibc_ssf, kproma, krow )
    CALL get_soil_moisture_from_jsbach( jg, ibc_ws_l1 , kproma, krow )
    CALL get_soil_field_capacity_from_jsbach( jg, ibc_ws_fc_l1, kproma, krow )
!cms--

    CALL bc_apply(jg, ibc_ssf, kproma,  p_patch%nblks_c, krow , current_date, zcvs)
    CALL bc_apply(jg, ibc_ws_l1, kproma,  p_patch%nblks_c, krow , current_date, ws)
    CALL bc_apply(jg, ibc_ws_fc_l1, kproma,  p_patch%nblks_c, krow , current_date, wsmx)

    DO i=1,kproma
      IF ((plsmask(i).GT.0.5_dp).AND.(pglac(i).LT.0.5_dp)) THEN    ! land surface, but not a glacier

        ! cms: here, ws and wsmx are taken from the first layer of the soil model (which is differs
        ! from their original definition). Whether the  zw1r threshold of 0.9 for dust emsission is 
        ! still o.k. should be checked.
        ! As in the original mo_ham_dust, here the relative soil moisture is simply defined as the fraction 
        ! of soil moisture to maximum soil moisture (field capacity). 
        ! JSBACH, on the other hand uses (and probably always has used) 
        ! Eq. 3.3.2.9 of ECHAM3 for relative humidity (see function relative_humidity
        ! in mo_soil_process.f90)

        zw1r (i)= MIN(ws(i)/wsmx(i),1._dp)

      ELSEIF((plsmask(i).GT.0.5_dp).AND.(pglac(i).GT.0.5_dp)) THEN ! glacier on land surface
        zw1r(i)= 1._dp
        zcvs(i)= 1._dp
      ELSE                                                        ! ocean grid point
        zw1r(i)= -9.e9_dp
        zcvs(i)= -9.e9_dp      
      END IF
    END DO 


    du_W1r (:) = zw1r(:)
    du_snow(:) = zcvs(:)

    !>>SF #458 (replacing where statements)
    du_W1r(:) = MERGE( &
                       0._dp, & ! set missing values to zero
                       du_W1r(:), &
                       (du_W1r(:) < 0._dp))

    du_snow(:) = MERGE( &
                       0._dp, & ! set missing values to zero
                       du_snow(:), &
                       (du_snow(:) < 0._dp))
    !<<SF #458 (replacing where statements)

    ! changed calculation for c_eff (T. Cheng)

      d1 = 0._dp
    feff = 0._dp
     AAA = 0._dp
      BB = 0._dp
     CCC = 0._dp
     DDD = 0._dp
      EE = 0._dp
      FF = 0._dp


    !get east-asia soil type fraction 

    mf13(1:kproma) = mat_s13(1:kproma,krow) !taklimakan
    mf14(1:kproma) = mat_s14(1:kproma,krow) !loess
    mf15(1:kproma) = mat_s15(1:kproma,krow) !gobi
    mf16(1:kproma) = mat_s16(1:kproma,krow) !other mixture soils
    mf17(1:kproma) = mat_s17(1:kproma,krow) !desert and sand land 

    !scale fator for Ut over East Asia desert (for sensitivity test) 

    utsc(:) = 1._dp

    !>>SF #458 (replacing where statements)
    utsc(:) = MERGE( &
                    r_dust_sf13, & !after r1060 (exp A0109)
                    utsc(:), &
                    (mf13(:) > 0._dp))

    utsc(:) = MERGE( &
                    r_dust_sf14, & !after r1060 (exp A0109)
                    utsc(:), &
                    (mf14(:) > 0._dp))

    utsc(:) = MERGE( &
                    r_dust_sf15, & !after r1060 (exp A0109)
                    utsc(:), &
                    (mf15(:) > 0._dp))

    utsc(:) = MERGE( &
                    r_dust_sf16, & !after r1060 (exp A0109)
                    utsc(:), &
                    (mf16(:) > 0._dp))

    utsc(:) = MERGE( &
                    r_dust_sf17, & !after r1060 (exp A0109)
                    utsc(:), &
                    (mf17(:) > 0._dp))
    !<<SF #458 (replacing where statements)

    !zero soil fraction for type 13-17 when k_dust_easo=1

    IF (k_dust_easo.eq.1) THEN 
       mf13(:) = 0._dp
       mf14(:) = 0._dp
       mf15(:) = 0._dp
       mf16(:) = 0._dp
       mf17(:) = 0._dp
    END IF


    !calculate the efficient friction velocity ratio feff, formula (17) in mat-95 
    !if use uniform z0 = 0.001cm, feff will be zero. 

    DO i=1,kproma
    
       IF (Z01(i,krow) .EQ. 0._dp .OR. Z02(i,krow) .EQ. 0._dp) THEN

          feff = 0._dp 

       ELSE

         AAA = log(Z01(i,krow)/r_dust_z0s)
          BB = log(aeff*(xeff/r_dust_z0s)**0.8_dp)
         CCC = 1._dp-AAA/BB

         IF (d1.eq.0._dp) THEN
            FF = 1._dp
         ELSE
            DDD = log(Z02(i,krow)/Z01(i,krow))
             EE = log(aeff * (d1/Z01(i,krow))**0.8_dp)
             FF = 1._dp- DDD/EE
         END IF

         feff = FF*CCC

         if (feff.lt.0._dp) feff=0._dp
         if (feff.gt.1._dp) feff=1._dp 

       END IF 

       c_eff(i) = feff
    
    END DO !DO i=1,kproma

    nn=1     
    rdp  =Dmin  
    dlast=Dmin
    dpk  (:)=0._dp
    dbmin(:)=0._dp
    dbmax(:)=0._dp

    DO kk=1,nclass                              ! assign fluxes to bins
       IF (mod(kk,nbin).eq.0) THEN
          dbmax(nn)=  rdp*10000._dp*0.5_dp      ! calculate bin minimum/maximum radius in um
          dbmin(nn)=dlast*10000._dp*0.5_dp     
          dpk(nn)=sqrt(dbmax(nn)*dbmin(nn))
          nn=nn+1
          dlast=rdp
       ENDIF
       rdp = rdp * exp(Dstep)
    ENDDO !kk      

    fluxbin(:,:)     =0._dp
    flux_6h(:,:,krow)=0._dp
       
    dust_mask(:)=0
     

    DO i = 1,kproma

    IF (c_eff(i).GT.0._dp) THEN    

       Ustar = (vk * du_wind(i) *100._dp)/(log(ZZ/Z02(i,krow)))  ! U*(cm/s): formular (15) in mat-95 

       ! check critical wind speed (wind stress threshold)

       IF ((Ustar .GT. 0._dp) .AND. (Ustar .GE. r_dust_umin*nduscale_2d(i,krow)/c_eff(i))) THEN  

          IF (k_fpar_eff(i,krow).GT.r_dust_lai) THEN   ! check if the grid cell is a potential dust source
             dust_mask(i)=1                            ! set grid point as a potential dust source point
             Ustar_d(i)=Ustar                          ! store wind speed of dust grid
          ENDIF ! k_fpar_eff.gt.0.

          ustar_acrit(i,krow)=dtime

       ENDIF   ! Ustar

    ENDIF !IF (c_eff(i).GT.0._dp) THEN 

    ENDDO !DO i = 1,kproma
     

    fluxtyp(:,:)=0._dp
    fluxdiam1(:,:)=0._dp
    fluxdiam2(:,:)=0._dp
    fluxdiam3(:,:)=0._dp
    fluxdiam4(:,:)=0._dp
    fluxdiam6(:,:)=0._dp
    fluxdiam_pf(:,:)=0._dp
    fluxdiam13(:,:)=0._dp
    fluxdiam14(:,:)=0._dp
    fluxdiam15(:,:)=0._dp
    fluxdiam16(:,:)=0._dp
    fluxdiam17(:,:)=0._dp

    AAA = 0._dp
    BB  = 0._dp

    dn(:)=0


    DO kk=1,nclass


    DO i = 1,kproma 
   

    IF (c_eff(i).GT.0._dp) THEN 


    IF (dust_mask(i).EQ.1) THEN   

    !>>Kai Zhang, 2009-02

    !calculate the horizontal flux G(Dp), formula (28) in mat-95 

    IF (k_dust_smst.eq.0) THEN

       ! caculation of wet erosion threshold friction velocity (T. Cheng)
       ! replace ws with zmr 

       IF ((plsmask(i).GT.0.5_dp).AND.(pglac(i).LT.0.5_dp)) THEN
          BB = MIN(ws(i)/rop,1._dp) * 100._dp
       ELSE
          BB = 0._dp
       ENDIF
       
       DO j = 1, nats
       
          AAA = solspe(j,nspe)*100._dp
       
          IF ( BB .LE. AAA ) THEN
             uthp = Uth(kk)
          ELSE
             uthp = Uth(kk)*SQRT(1._dp+1.21_dp*(BB-AAA)**0.68_dp) 
          ENDIF
          uthp = uthp * nduscale_2d(i,krow)

           ! Marticorena:
           !
           !fdp1(j) = (1._dp+(uthp/(c_eff(i) * Ustar_d(i))))**2
           !fdp2(j) = (1._dp-(uthp/(c_eff(i) * Ustar_d(i))))
 
           !>>Kai Zhang, 2009-02
           fdp1(j) = (1._dp+(uthp*utsc(i)/(c_eff(i) * Ustar_d(i))))**2
           fdp2(j) = (1._dp-(uthp*utsc(i)/(c_eff(i) * Ustar_d(i))))
           !<<Kai Zhang, 2009-02

           ! Shao:
           ! 
           ! fdp1 = (1.-(Uthp/(c_eff(i) * Ustar_d(i)))**2)
           ! fdp2 = 1.
       
       ENDDO 


    ELSE 

       ! do not consider effect of soil moisture on threshold velocity 

       uthp = Uth(kk) * nduscale_2d(i,krow)
       
       DO j = 1, nats

         ! Marticorena:

           !>>Kai Zhang, 2009-02

           fdp1(j) = (1._dp+(uthp*utsc(i)/(c_eff(i) * Ustar_d(i))))**2
           fdp2(j) = (1._dp-(uthp*utsc(i)/(c_eff(i) * Ustar_d(i))))

           !<<Kai Zhang, 2009-02

         ! Shao:

         ! fdp1 = (1.-(Uthp/(c_eff(i) * Ustar_d(i)))**2)
         ! fdp2 = 1.

       ENDDO 


    ENDIF !IF (k_dust_smst.eq.0) THEN

    !<<Kai Zhang, 2009-02


    !calculate horizontal flux distribution as function of Dp
    !formular (33) in mat-95, formula (3) in tegen-2002

    tmpa = cd * Ustar_d(i)**3

    i_soil=1

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1) 
      fluxdiam1(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF 

    i_soil=2

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam2(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF

    i_soil=3

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam3(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF

    i_soil=4

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam4(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF

    i_soil=6

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam6(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF

    i_soil=10

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam_pf(i,kk)= srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha 
    ENDIF

    !add asian dust source emission (cheng)

    i_soil=13

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam13(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha
    ENDIF

    i_soil=14

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam14(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha
    ENDIF

    i_soil=15

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam15(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha
    ENDIF

    i_soil=16

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam16(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha
    ENDIF

    i_soil=17

    IF (fdp2(i_soil).gt.0._dp) THEN
      alpha= solspe(i_soil,nmode*3+1)
      fluxdiam17(i,kk) = srel(i_soil,kk) * fdp1(i_soil) * fdp2(i_soil) * tmpa *alpha
    ENDIF


    IF (kk.eq.1) THEN 
       
       tmpb = 1._dp-mat_psrc(i,krow)
       tmpc = (mat_s1(i,krow)-mat_s2(i,krow)-mat_s3(i,krow)-mat_s4(i,krow)-mat_s6(i,krow) & 
             - mf13(i)-mf14(i)-mf15(i)-mf16(i)-mf17(i))

       fluxtyp(i,kk) = fluxtyp(i,kk) &
         + fluxdiam1 (i,kk)*tmpb*tmpc &
         + fluxdiam2 (i,kk)*tmpb*mat_s2 (i,krow)                                      &
         + fluxdiam3 (i,kk)*tmpb*mat_s3 (i,krow)                                      &
         + fluxdiam4 (i,kk)*tmpb*mat_s4 (i,krow)                                      &
         + fluxdiam6 (i,kk)*tmpb*mat_s6 (i,krow)                                      &
         + fluxdiam13(i,kk)*tmpb*mf13(i)                                      &
         + fluxdiam14(i,kk)*tmpb*mf14(i)                                      &
         + fluxdiam15(i,kk)*tmpb*mf15(i)                                      &
         + fluxdiam16(i,kk)*tmpb*mf16(i)                                      &
         + fluxdiam17(i,kk)*tmpb*mf17(i)                                      &
         + fluxdiam_pf(i,kk)*mat_psrc(i,krow)

    ELSE

       dn(i)=dn(i)+1           ! increase number of dislocated particle classes
       dk(i,dn(i))=kk          ! store dislocated particle class

    ENDIF !IF (kk.eq.1) THEN 


    ENDIF !IF (dust_mask(i).EQ.1) THEN   

    ENDIF !IF (c_eff(i).GT.0._dp) THEN

    ENDDO !DO i = 1,kproma 

    ENDDO !DO kk=1,nclass

    !calculate horizontal flux distribution

    kkmin=1

    DO i = 1,kproma

    tmpa = 1._dp-mat_psrc(i,krow)

    tmpb = mat_s1(i,krow)-mat_s2(i,krow) &
          -mat_s3(i,krow)-mat_s4(i,krow)-mat_s6(i,krow) &
          -mf13(i)-mf14(i)-mf15(i)- mf16(i)-mf17(i)

    IF (dust_mask(i).EQ.1) THEN

       DO n=1,dn(i)                 ! loop over dislocated dust particle classes

       DO kkk=1,dk(i,n)             ! scaling with relative contribution of dust size  fraction


          i_soil=1
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*tmpb * fluxdiam1(i,dk(i,n))*tmpc

          i_soil=2
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mat_s2(i,krow) * fluxdiam2(i,dk(i,n))*tmpc

          i_soil=3
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mat_s3(i,krow) * fluxdiam3(i,dk(i,n))*tmpc

          i_soil=4
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mat_s4(i,krow) * fluxdiam4(i,dk(i,n))*tmpc

          i_soil=6
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mat_s6(i,krow) * fluxdiam6(i,dk(i,n))*tmpc

          IF(du_wind(i).gt.10._dp) THEN
             i_soil=11 ! flux from preferential source at high wind speeds
          ELSE
             i_soil=10 ! flux from preferential source at low wind speeds
          ENDIF

          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + mat_psrc(i,krow) * fluxdiam_pf(i,dk(i,n))*tmpc


          i_soil=13
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mf13(i) * fluxdiam13(i,dk(i,n))*tmpc

          i_soil=14
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mf14(i) * fluxdiam14(i,dk(i,n))*tmpc 

          i_soil=15
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mf15(i) * fluxdiam15(i,dk(i,n))*tmpc

          i_soil=16
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mf16(i) * fluxdiam16(i,dk(i,n))*tmpc

          i_soil=17
          tmpc = srelV(i_soil,kkk)/((su_srelV(i_soil,dk(i,n))-su_srelV(i_soil,kkmin)))
          fluxtyp(i,kkk) = fluxtyp(i,kkk) + tmpa*mf17(i) * fluxdiam17(i,dk(i,n))*tmpc


       ENDDO ! kkk
       ENDDO ! n

    ENDIF ! dust_mask.eq.1

    ENDDO ! i


    !calculate the vertical dust particle flux based on White (1979), formula (2) in tegen-2002

    DO nn=1,ntrace
       DO i = 1,kproma
          IF (dust_mask(i).EQ.1) THEN

!%%%%%%%%%%%%
!CDIR NOVETOR
!%%%%%%%%%%%%

          fluxbin(i,nn) = fluxbin(i,nn)+SUM(fluxtyp(i,(nn-1)*nbin+1:nn*nbin))

          ! mask out dust fluxes, where soil moisture threshold reached

          IF (du_W1r(i).gt.w0) fluxbin(i,nn)=0._dp             

          ! fluxbin: g/cm2/sec; flux_6h: g/m2/sec

          flux_6h(i,nn,krow)=fluxbin(i,nn)*10000._dp*(1._dp-du_snow(i))*k_fpar_eff(i,krow)

          ENDIF ! dust_mask.eq.1
       ENDDO ! i
    ENDDO ! nn

    DO nn=1,ntrace
       DO i=1,kproma
          tmpa = flux_6h(i,nn,krow)*dtime
          flux_ann(i,krow)=flux_ann(i,krow)+tmpa                          ! flux_ann: g/m2/year
          IF (dpk(nn).le.10._dp) flux_a10(i,krow)=flux_a10(i,krow)+tmpa   ! flux_a10: g/m2/year
          IF (dpk(nn).le. 2._dp) flux_a2(i,krow) = flux_a2(i,krow)+tmpa   ! flux_a2:  g/m2/year
          IF (dpk(nn).le. 1._dp) flux_a1(i,krow) = flux_a1(i,krow)+tmpa   ! flux_a1:  g/m2/year
       ENDDO ! i                           
    ENDDO ! nn

  END SUBROUTINE bgc_dust_calc_emis


!====================================================================================================

  SUBROUTINE bgc_prestat

    ! Description:
    ! ------------
    ! *bgc_prestat* reads total global dust emission numbers from the text file "dust.diagnostics.dat"
    ! The purpose is to correctly continue monitoring total dust emissions after a (re)start of the model
    !
    ! Authors:
    ! --------
    ! M. Werner, MPI BGC, January 2003
    !
    ! Interface:
    ! ----------
    ! *bgc_prestat* is called from *call_submodels.f90*.
    !
    ! Method:
    ! -------
    ! Reads diagnostic dust sums from text file "dust.diagnostics.dat".
    ! If the text file does not exist, the dust sums are reset to zero (=initial start).

    USE mo_io_units,          ONLY: find_next_free_unit  

    IMPLICIT NONE

    INTEGER                           :: iunit   ! fortran unit for input file
    LOGICAL                           :: lex     ! flag to test if file exists
    INTEGER                           :: kyear, kmonth, kday, khour, kminute, ksecond

    INQUIRE(file='./dust.diagnostics.dat', exist=lex)  ! read dust diagnostic file
    IF (lex) THEN
     IF (p_pe .EQ. p_io) THEN
      iunit = find_next_free_unit(10,99)
      OPEN(iunit,file='./dust.diagnostics.dat',status='old')      
20    READ(iunit,*,END=25) kyear, kmonth, kday, khour, kminute, ksecond, dustsum(1), dustsum(2), dustsum(3), dustsum(4) 
      GOTO 20
25    CLOSE(iunit)
      dustsum(:) = dustsum(:)*1.e12_dp              ! convert back from Mt/yr to g/yr
     ENDIF
    ELSE
     dustsum(:) = 0._dp                             ! if diagnostic file does not exist: initialize dust diagnostics
    ENDIF     

    !--- Broadcast field to all CPUs:

    CALL p_bcast(dustsum, p_io, p_comm_work) 

  END SUBROUTINE bgc_prestat

  
!!$!====================================================================================================
!!$
!!$
!!$  SUBROUTINE bgc_dust_trastat(kyear, kmonth, kday, khour, kminute, ksecond)
!!$
!!$    ! Description:
!!$    ! ------------
!!$    ! *bgc_trastat* writes total global dust emission numbers to the text file "dust.diagnostics.dat"
!!$    ! The purpose is to correctly continue monitoring total dust emissions after a (re)start of the model
!!$    !
!!$    ! Authors:
!!$    ! --------
!!$    ! M. Werner, MPI BGC, January 2003
!!$    ! M. Werner, MPI BGC, October 2004: changed emission cheme from 0.5x0.5 degree grid to 
!!$    !                                    default Echam5-HAM grid (to save computational costs)
!!$    !
!!$    ! Interface:
!!$    ! ----------
!!$    ! *bgc_dust_trastat* is called from *call_submodels.f90*.
!!$    !
!!$    ! Method:
!!$    ! -------
!!$    ! Writes diagnostic dust sums to text file "dust.diagnostics.dat".
!!$    ! If the text file does not exist, it is newly created (=cold start), 
!!$    ! otherwise the output is appended (=restart).
!!$
!!$    USE mo_math_constants,    ONLY: pi
!!$    USE mo_control,           ONLY: nlon, ngl
!!$    USE mo_io_units,          ONLY: nout
!!$    USE mo_filename,          ONLY: find_next_free_unit
!!$    USE mo_decomposition,     ONLY: gl_dc=> global_decomposition
!!$    USE mo_transpose,         ONLY: gather_gp
!!$
!!$
!!$    IMPLICIT NONE
!!$
!!$    REAL(dp), POINTER :: zflux_ann(:,:)
!!$    REAL(dp), POINTER :: zflux_a1(:,:)
!!$    REAL(dp), POINTER :: zflux_a2(:,:)
!!$    REAL(dp), POINTER :: zflux_a10(:,:)
!!$    REAL(dp), POINTER :: zk_fpar_eff(:,:)
!!$
!!$    INTEGER                           :: iunit   ! fortran unit for input file
!!$    INTEGER, INTENT(IN)               :: kyear, kmonth, kday, khour, kminute, ksecond
!!$
!!$    REAL(dp)           :: dustsumt, dustsum10, dustsum2, dustsum1  ! dummy values for diagnostics
!!$
!!$    REAL(dp)           :: latitude    
!!$    REAL(dp)           :: S
!!$    REAL(dp)           :: surf(ngl)                  ! grid surface area
!!$    REAL(dp)           :: tool
!!$        
!!$    INTEGER        :: x, y                       ! longitude / latitude index
!!$
!!$    REAL(dp)             :: resol
!!$    REAL(dp), PARAMETER  :: radius = 6371000._dp        ! radius of Earth
!!$
!!$
!!$    resol = 180._dp/ngl
!!$
!!$    dustsumt = dustsum(1)                        ! initialize dustsums with values from last (re)start
!!$    dustsum10 = dustsum(2)                       ! initialize dustsums with values from last (re)start
!!$    dustsum2 = dustsum(3)
!!$    dustsum1 = dustsum(4)
!!$
!!$    DO y = 1,ngl                                 ! calculate grid surface area for different latitudes
!!$      latitude =(90._dp + resol * (0.5_dp - REAL(y,dp)) )
!!$      S = radius * radius * pi / (180._dp/resol)
!!$      tool = COS(latitude * pi/180._dp)
!!$      surf(y) = S * abs(tool) * 2._dp*pi/(360._dp/resol)
!!$    ENDDO
!!$
!!$    IF (p_pe == p_io) THEN
!!$       ALLOCATE (zflux_ann(nlon,ngl))
!!$       ALLOCATE (zflux_a10(nlon,ngl))
!!$       ALLOCATE (zflux_a2(nlon,ngl))
!!$       ALLOCATE (zflux_a1(nlon,ngl))
!!$       ALLOCATE (zk_fpar_eff(nlon,ngl))
!!$    END IF
!!$
!!$    CALL gather_gp (zflux_ann, flux_ann, gl_dc)
!!$    CALL gather_gp (zflux_a10, flux_a10, gl_dc)
!!$    CALL gather_gp (zflux_a2, flux_a2, gl_dc)
!!$    CALL gather_gp (zflux_a1, flux_a1, gl_dc)
!!$    CALL gather_gp (zk_fpar_eff, k_fpar_eff, gl_dc)
!!$
!!$    IF (p_parallel_io) THEN                      ! write out diagnostics to standard output   
!!$      DO y = 1,ngl
!!$        DO x = 1,nlon
!!$          IF (zk_fpar_eff(x,y).GT.1.E-10_dp) THEN        ! if the area is a potential dust source: sum up for diagnostic output
!!$            dustsumt=dustsumt+zflux_ann(x,y)*surf(y)   ! total grams of dust per year
!!$            dustsum10=dustsum10+zflux_a10(x,y)*surf(y) ! small dust particles < 10um
!!$            dustsum2=dustsum2+zflux_a2(x,y)*surf(y)    ! small dust particles < 2um
!!$            dustsum1=dustsum1+zflux_a1(x,y)*surf(y)    ! small dust particles < 1um
!!$          ENDIF !! (k_fpar_eff.gt.1.e-10)
!!$        END DO !! x
!!$      END DO !! y
!!$
!!$      DEALLOCATE (zflux_ann)
!!$      DEALLOCATE (zflux_a10)
!!$      DEALLOCATE (zflux_a2)
!!$      DEALLOCATE (zflux_a1)
!!$      DEALLOCATE (zk_fpar_eff)
!!$
!!$     WRITE(nout,'(4(a,e10.4))')                                 &
!!$                   ' DUST (Mt/yr): total: ',dustsumt*1.e-12_dp,    &
!!$                   '     < 10um: ',dustsum10*1.e-12_dp,            &
!!$                   '     < 2um: ',dustsum2*1.e-12_dp,              &
!!$                   '     < 1um: ',dustsum1*1.e-12_dp
!!$     iunit = find_next_free_unit(55,99)
!!$     OPEN(iunit,file='dust.diagnostics.dat',status='unknown',position='append')
!!$     WRITE(iunit,'(i0,i0,i0,i0,i0,i0,e25.15,e25.15,e25.15,e25.15)') kyear, kmonth, kday, khour, kminute, ksecond, &
!!$                    dustsumt*1.e-12_dp, dustsum10*1.e-12_dp, dustsum2*1.e-12_dp, dustsum1*1.e-12_dp
!!$     CLOSE(iunit)
!!$
!!$    ENDIF
!!$
!!$  END SUBROUTINE bgc_dust_trastat
!!$
!!$  
!!$!====================================================================================================
!!$
!!$
!!$  SUBROUTINE bgc_dust_cleanup
!!$
!!$    ! Description:
!!$    ! ------------
!!$    ! *bgc_dust_cleanup* deallocates memory needed for the dust scheme.
!!$    !
!!$    ! Authors:
!!$    ! --------
!!$    ! M. Werner, MPI BGC, November 2004
!!$    !
!!$    ! Interface:
!!$    ! ----------
!!$    ! *bgc_dust_cleanup* is called from *call_free_submodel_memory*
!!$    
!!$    IMPLICIT NONE
!!$
!!$!cms not used    IF (ALLOCATED(biome))       DEALLOCATE (biome)
!!$    IF (ALLOCATED(mat_s1))      DEALLOCATE (mat_s1)
!!$    IF (ALLOCATED(mat_s2))      DEALLOCATE (mat_s2)
!!$    IF (ALLOCATED(mat_s3))      DEALLOCATE (mat_s3)
!!$    IF (ALLOCATED(mat_s4))      DEALLOCATE (mat_s4)
!!$    IF (ALLOCATED(mat_s6))      DEALLOCATE (mat_s6)
!!$    IF (ALLOCATED(mat_psrc))    DEALLOCATE (mat_psrc)
!!$    IF (ALLOCATED(mat_s13))     DEALLOCATE (mat_s13)
!!$    IF (ALLOCATED(mat_s14))     DEALLOCATE (mat_s14)
!!$    IF (ALLOCATED(mat_s15))     DEALLOCATE (mat_s15)
!!$    IF (ALLOCATED(mat_s16))     DEALLOCATE (mat_s16)
!!$    IF (ALLOCATED(mat_s17))     DEALLOCATE (mat_s17)
!!$    IF (ALLOCATED(k_fpar_eff))  DEALLOCATE (k_fpar_eff)
!!$    IF (ALLOCATED(Z01))         DEALLOCATE (Z01)
!!$    IF (ALLOCATED(Z02))         DEALLOCATE (Z02) 
!!$    IF (ALLOCATED(flux_ann))    DEALLOCATE (flux_ann)
!!$    IF (ALLOCATED(flux_a1))     DEALLOCATE (flux_a1)
!!$    IF (ALLOCATED(flux_a2))     DEALLOCATE (flux_a2)
!!$    IF (ALLOCATED(flux_a10))    DEALLOCATE (flux_a10)
!!$    IF (ALLOCATED(idust))       DEALLOCATE (idust)
!!$    IF (ALLOCATED(flux_6h))     DEALLOCATE (flux_6h)
!!$    IF (ALLOCATED(nduscale_2d)) DEALLOCATE (nduscale_2d) !csld #433
!!$
!!$  END SUBROUTINE bgc_dust_cleanup

!====================================================================================================
!-------------------------------------------------------------------------
! 
!> SUBROUTINE size_check_zerofields -- checks the shape of the shape of the 
!! fields read into the icon program (origin: mo_bc_aeropt_kinne, J.S. Rast)

  SUBROUTINE shape_check_fields(kdim_icon,kdim_file,cfname,cvarname,croutine_name,cmodule_name)
    USE mo_exception,             ONLY: finish
    INTEGER,INTENT(in)            :: kdim_icon(:), kdim_file(:)
    CHARACTER(LEN=*), INTENT(in)  :: cfname, cvarname, croutine_name, cmodule_name
    INTEGER                       :: idim
    CHARACTER(LEN=2)              :: cidim 
    CHARACTER(LEN=32)             :: cidim_len_file, cidim_len_icon
    IF (SIZE(kdim_icon) /= SIZE(kdim_file)) THEN
      CALL finish(TRIM(ADJUSTL(croutine_name))//' of '//TRIM(ADJUSTL(cmodule_name )), &
                  'variable '//TRIM(ADJUSTL(cvarname))//' has wrong number of dimensions in file ' &
                  //TRIM(ADJUSTL(cfname)))
    END IF
    DO idim=1,SIZE(kdim_icon)
      IF (kdim_icon(idim) /= kdim_file(idim)) THEN
        WRITE(cidim,'(i2)') idim
        WRITE(cidim_len_icon,'(i32)') kdim_icon(idim)
        WRITE(cidim_len_file,'(i32)') kdim_file(idim)
        CALL finish(TRIM(ADJUSTL(croutine_name))//' of '//TRIM(ADJUSTL(cmodule_name )), &
                  'variable '//TRIM(ADJUSTL(cvarname))//' has wrong length in dimension ' &
                  //TRIM(ADJUSTL(cidim))//' length in icon model: '//TRIM(ADJUSTL(cidim_len_icon)) &
                  //' but length in file '//TRIM(ADJUSTL(cfname))//' is '//TRIM(ADJUSTL(cidim_len_file)))
      END IF
    END DO
  END SUBROUTINE shape_check_fields

!====================================================================================================

  SUBROUTINE check_input_file_exist(caller, cfile)
    USE mo_mpi,       ONLY: my_process_is_mpi_workroot
    USE mo_exception, ONLY: finish
    CHARACTER(LEN=*), INTENT(IN) :: caller, cfile
    LOGICAL :: lex       
    IF ( my_process_is_mpi_workroot() ) THEN
       INQUIRE (file=TRIM(cfile), exist=lex)
       IF ( .NOT. lex ) THEN
          CALL finish('bgc_read_annual_fields','missing input file '//TRIM(cfile))
       END IF
    END IF
  END SUBROUTINE check_input_file_exist

END MODULE mo_ham_dust

!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_activ

  USE mo_kind,                ONLY: dp
  USE mo_fortran_tools,       ONLY: t_ptr_2d3d
  USE mo_linked_list,         ONLY: t_var_list

  USE mo_exception,           ONLY: message, finish

  USE mo_impl_constants,      ONLY: SUCCESS, MAX_CHAR_LENGTH,  & 
    &                               VINTP_METHOD_PRES,         &
    &                               VINTP_METHOD_LIN,          &
    &                               VINTP_METHOD_LIN_NLEVP1

  USE mo_linked_list,         ONLY: t_var_list

  USE mo_var_list,            ONLY: default_var_list_settings, &
    &                               add_var, add_ref,          &
    &                               new_var_list,              &
    &                               delete_var_list
  USE mo_var_metadata,        ONLY: create_vert_interp_metadata, vintp_types, &
                                    groups
  USE mo_cf_convention,       ONLY: t_cf_var
  USE mo_grib2,               ONLY: t_grib2_var, grib2_var
  USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
    &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
    &                               GRID_UNSTRUCTURED,                 &
    &                               TSTEP_INSTANT, TSTEP_AVG,          &
    &                               TSTEP_ACCUM, cdiDefMissval
  USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
    &                               ZA_HYBRID, ZA_HYBRID_HALF,         &
    &                               ZA_SURFACE
  USE mo_parallel_config,     ONLY: nproma
  USE mo_io_config,           ONLY: lnetcdf_flt64_output

  IMPLICIT NONE

  PUBLIC activ_initialize
  PUBLIC activ_updraft
  PUBLIC activ_lin_leaitch
  PUBLIC construct_activ_stream

  PRIVATE

  INTEGER ::  idt_cdnc, idt_icnc, nfrzmod

!  TYPE (t_stream), PUBLIC, POINTER :: activ

  INTEGER,         PUBLIC          :: nw ! actual number of updraft velocity (w) bins 
                                         ! (can be 1 if characteristic updraft is used)

  TYPE t_activ_field
    REAL(dp),         POINTER :: swat(:,:,:)
    REAL(dp),         POINTER :: w_large(:,:,:)
    REAL(dp),         POINTER :: w_turb(:,:,:)
    REAL(dp),         POINTER :: w_cape(:,:)
    REAL(dp),         POINTER :: w_sigma(:,:,:)
    REAL(dp),         POINTER :: reffl(:,:,:)
    REAL(dp),         POINTER :: reffi(:,:,:)
    REAL(dp),         POINTER :: na(:,:,:)
    REAL(dp),         POINTER :: qnuc(:,:,:)
    REAL(dp),         POINTER :: qaut(:,:,:)
    REAL(dp),         POINTER :: qacc(:,:,:)
    REAL(dp),         POINTER :: qfre(:,:,:)
    REAL(dp),         POINTER :: qeva(:,:,:)
    REAL(dp),         POINTER :: qmel(:,:,:)
    REAL(dp),         POINTER :: cdnc_acc(:,:,:)
    REAL(dp),         POINTER :: cdnc(:,:,:)
    REAL(dp),         POINTER :: icnc_acc(:,:,:)
    REAL(dp),         POINTER :: icnc(:,:,:)
    REAL(dp),         POINTER :: icnc_instant(:,:,:) ! Ice crystal number concentration (ICNC), actual instantaneous value [1/m3]
    REAL(dp),         POINTER :: cdnc_instant(:,:,:) 
    REAL(dp),         POINTER :: lwc_acc(:,:,:)
    REAL(dp),         POINTER :: iwc_acc(:,:,:)
    REAL(dp),         POINTER :: cloud_time(:,:,:)
    REAL(dp),         POINTER :: cliwc_time(:,:,:)
    REAL(dp),         POINTER :: cdnc_burden_acc(:,:)
    REAL(dp),         POINTER :: cdnc_burden(:,:)
    REAL(dp),         POINTER :: icnc_burden_acc(:,:)
    REAL(dp),         POINTER :: icnc_burden(:,:)
    REAL(dp),         POINTER :: burden_time(:,:)
    REAL(dp),         POINTER :: burdic_time(:,:)
    REAL(dp),         POINTER :: reffl_acc(:,:,:)
    REAL(dp),         POINTER :: reffi_acc(:,:,:)
    REAL(dp),         POINTER :: cloud_cover_duplic(:,:,:)
    REAL(dp),         POINTER :: sice(:,:,:)
    REAL(dp),         POINTER :: reffl_ct(:,:)
    REAL(dp),         POINTER :: reffl_time(:,:)
    REAL(dp),         POINTER :: cdnc_ct(:,:)
    REAL(dp),         POINTER :: reffi_tovs(:,:)
    REAL(dp),         POINTER :: reffi_time(:,:)
    REAL(dp),         POINTER :: iwp_tovs(:,:)

    TYPE(t_ptr_2d3d),  ALLOCATABLE :: w(:)
    TYPE(t_ptr_2d3d),  ALLOCATABLE :: w_pdf(:)
    TYPE(t_ptr_2d3d),  ALLOCATABLE :: swat_max_strat(:)
    TYPE(t_ptr_2d3d),  ALLOCATABLE :: swat_max_conv(:)

    REAL(dp), POINTER :: w_4d(:,:,:,:)
    REAL(dp), POINTER :: w_pdf_4d(:,:,:,:)
    REAL(dp), POINTER :: swat_max_strat_4d(:,:,:,:) 
    REAL(dp), POINTER :: swat_max_conv_4d(:,:,:,:) 

!!$!cmsd++
!!$   REAL(dp),         POINTER :: icnc_tm1(:,:,:), &
!!$                                dicnc1in(:,:,:), &
!!$                                dicnc2cor(:,:,:), &
!!$                                dicnc3cor(:,:,:), &
!!$                                dicnc4detr(:,:,:), &
!!$                                dicnc5nucl(:,:,:), &
!!$                                icnc_dum(:,:,:), &
!!$                                dicnc6fre(:,:,:), &
!!$                                dicnc7lim(:,:,:), &
!!$                                dicnc8mlt(:,:,:), &
!!$                                dicnc9sed(:,:,:), &
!!$                                dicnc10cor(:,:,:), &
!!$                                dicnc11upd(:,:,:), &
!!$                                dicnc12hom(:,:,:), &
!!$                                dicnc13het(:,:,:), &
!!$                                dicnc14cr(:,:,:),  &
!!$                                paclc(:,:,:),      &
!!$                                zxtec(:,:,:),      &
!!$                                zrid(:,:,:)     
!!$                                  
!!$                 
!!$!cmsd--

  END TYPE t_activ_field


  TYPE(t_activ_field), ALLOCATABLE, TARGET :: activ_field(:)  !< shape: (n_dom)

  TYPE(t_var_list), ALLOCATABLE :: activ_field_list(:)  !< shape: (n_dom) 

  REAL(dp)            :: w_min = 0.0_dp       ! minimum characteristic w for activation [m s-1]
  REAL(dp), PARAMETER :: w_sigma_min = 0.1_dp ! minimum value of w standard deviation [m s-1]

  CHARACTER(LEN=*), PARAMETER :: thismodule='mo_activ'


  PUBLIC :: idt_cdnc, idt_icnc, nfrzmod
  PUBLIC :: activ_field

  !--- Subroutines:

CONTAINS

  SUBROUTINE activ_updraft(jg, kproma,   kbdim,  klev,    krow, &
                           ptkem1,   pwcape, pvervel, prho,     &
                           pw,       pwpdf                      )

    ! *activ_updraft* calculates the updraft vertical velocity
    !                 as sum of large scale and turbulent velocities
    !
    ! Author:
    ! -------
    ! Philip Stier, University of Oxford                 2008
    !
    ! References:
    ! -----------
    ! Lohmann et al., ACP, (2008)
    !

    USE mo_physical_constants, ONLY: grav
    !>>SF #345
    USE mo_cloud_utils,        ONLY: fact_tke
    USE mo_set_subm_phys,     ONLY: ncd_activ
    !<<SF #345
    USE mo_set_subm_phys, ONLY: nactivpdf !ZK

    IMPLICIT NONE

    INTEGER,  INTENT(in)  :: jg, kproma, kbdim, klev, krow

    REAL(dp), INTENT(out) :: pw(kbdim,klev,nw)        ! stratiform updraft velocity bins, large-scale+TKE (>0.0) [m s-1]
    REAL(dp), INTENT(out) :: pwpdf(kbdim,klev,nw)     ! stratiform updraft velocity PDF

    REAL(dp), INTENT(in)  :: prho(kbdim,klev),      & ! air density
                             ptkem1(kbdim,klev),    & ! turbulent kinetic energy
                             pvervel(kbdim,klev),   & ! large scale vertical velocity [Pa s-1]
                             pwcape(kbdim)            ! CAPE contribution to convective vertical velocity [m s-1]

    REAL(dp) :: zwlarge(kbdim, klev), & ! large-scale vertical velocity [m s-1]
                zwturb(kbdim, klev)     ! TKE-derived vertical velocity or st. dev. thereof [m s-1]

    !--- Large scale vertical velocity in SI units:

    zwlarge(1:kproma,:)      = -1._dp* pvervel(1:kproma,:)/(grav*prho(1:kproma,:))
    activ_field(jg)%w_large(1:kproma,:,krow) = zwlarge(1:kproma,:)

    !--- Turbulent vertical velocity:

    activ_field(jg)%w_turb(1:kproma,:,krow)  = fact_tke*SQRT(ptkem1(1:kproma,:))

    !>>SF #345: correction for the TKE prefactor, in case of Lin & Leaitch scheme only
    IF (ncd_activ == 1) THEN ! Lin & Leaitch scheme
       activ_field(jg)%w_turb(1:kproma,:,krow)  = 1.33_dp*SQRT(ptkem1(1:kproma,:))
    ENDIF
    !<<SF #345

    !--- Convective updraft velocity from CAPE:

    activ_field(jg)%w_cape(1:kproma,krow)  = pwcape(1:kproma) !SF although this is no longer used as a contribution to the
                                                              ! convective updraft velocity, this is just kept here
                                                              ! for recording it into the activ stream

    !--- Total stratiform updraft velocity:

    IF (nactivpdf == 0) THEN
       !--- Turbulent vertical velocity:
       pw(1:kproma,:,1) = MAX(w_min,activ_field(jg)%w_large(1:kproma,:,krow)+activ_field(jg)%w_turb(1:kproma,:,krow))
       activ_field(jg)%w(1)%p_3d(1:kproma,:,krow) = pw(1:kproma,:,1)
       ! Only one "bin", with probability of 1. The actual value doesn't
       ! matter so long as it's finite, since it cancels out of the CDNC
       ! calculation.
       pwpdf(1:kproma,:,1) = 1.0_dp
    ELSE
       CALL aero_activ_updraft_sigma(jg, kproma,   kbdim,   klev,    krow, &
                                     ptkem1,  zwturb                   )

       CALL aero_activ_updraft_pdf(jg, kproma,  kbdim,  klev,  krow, &
                                   zwlarge, zwturb, pw,    pwpdf   )
    END IF

  END SUBROUTINE activ_updraft

  SUBROUTINE aero_activ_updraft_sigma(jg, kproma,   kbdim,   klev,    krow, &
                                      ptkem1,   pwsigma                 )

    ! *aero_activ_updraft_sigma* calculates the standard deviation of the pdf of uf 
    !                            updraft vertical velocity
    !
    ! Author:
    ! -------
    ! Philip Stier, University of Oxford                 2013
    !
    ! References:
    ! -----------
    ! West et al., ACP, 2013. 
    !

    IMPLICIT NONE

    INTEGER,  INTENT(in)  :: jg, kproma, kbdim, klev, krow

    REAL(dp), INTENT(in)  :: ptkem1(kbdim,klev)  ! turbulent kinetic energy

    REAL(dp), INTENT(out) :: pwsigma(kbdim,klev) ! st. dev. of vertical velocity

    !--- Large scale vertical velocity in SI units:

    pwsigma(1:kproma,:)      = MAX(w_sigma_min, ((2.0_dp/3.0_dp)*ptkem1(1:kproma,:))**0.5_dp) ! m/s
    activ_field(jg)%w_sigma(1:kproma,:,krow) = pwsigma(1:kproma,:)

  END SUBROUTINE aero_activ_updraft_sigma

  SUBROUTINE aero_activ_updraft_pdf(jg, kproma,  kbdim,   klev, krow, &
                                    pwlarge, pwsigma, pw,   pwpdf )

    ! *aero_activ_updraft_* calculates Gaussian pdf of  
    !                       updraft vertical velocity
    !
    ! Author:
    ! -------
    ! Philip Stier, University of Oxford                 2013
    !
    ! References:
    ! -----------
    ! West et al., ACP, 2013. 
    !

    USE mo_math_constants, ONLY: pi
    USE mo_set_subm_phys,  ONLY: nactivpdf

    IMPLICIT NONE

    INTEGER,  INTENT(in)  :: jg, kproma, kbdim, klev, krow

    REAL(dp), INTENT(in)  :: pwlarge(kbdim,klev), & ! large-scale vertical velocity [m s-1]
                             pwsigma(kbdim,klev)    ! st. dev. of vertical velocity [m s-1]

    REAL(dp), INTENT(out) :: pw(kbdim,klev,nw),   & ! vertical velocity bins [m s-1]
                             pwpdf(kbdim,klev,nw)   ! vettical velocity PDF [s m-1]

    INTEGER               :: jl, jk, jw

    REAL(dp)              :: zw_width(kbdim,klev), &
                             zw_min(kbdim,klev), &
                             zw_max(kbdim,klev)

    zw_min(1:kproma,:)   = 0.0_dp
    zw_max(1:kproma,:)   = 4.0_dp*pwsigma(1:kproma,:)
    zw_width(1:kproma,:) = (zw_max(1:kproma,:) - zw_min(1:kproma,:)) / DBLE(nw)

    DO jw=1, nw
      pw(1:kproma,:,jw) = zw_min(1:kproma,:) + (DBLE(jw) - 0.5_dp) * zw_width(1:kproma,:)

      pwpdf(1:kproma,:,jw) = (1.0_dp / ((2.0_dp*pi)**0.5_dp))                         &
                             * (1.0_dp / pwsigma(1:kproma,:))                         &
                             * EXP( -((pw(1:kproma,:,jw) - pwlarge(1:kproma,:))**2_dp &
                                    / (2.0_dp*pwsigma(1:kproma,:)**2.0_dp)) )
    END DO

    IF (nactivpdf < 0) THEN
       DO jw=1, nw
          activ_field(jg)%w(jw)%p_3d(1:kproma,:,krow) = pw(1:kproma,:,jw)
          activ_field(jg)%w_pdf(jw)%p_3d(1:kproma,:,krow) = pwpdf(1:kproma,:,jw)
       END DO
    END IF

  END SUBROUTINE aero_activ_updraft_pdf

!-------------------------------------------

  SUBROUTINE activ_lin_leaitch(jg, kproma,  kbdim,    klev,     krow, &
                               pw, pcdncact                       )

    ! *activ_lin_leaitch* calculates the number of activated aerosol 
    !                     particles from the aerosol number concentration
    !SF now independent of HAM, since HAM-specific calculation are computed in mo_ham_activ
    !
    ! Author:
    ! -------
    ! Philip Stier, MPI-MET                       2004
    !
    ! Method:
    ! -------
    ! The parameterisation follows the simple empirical relations of 
    ! Lin and Leaitch (1997).
    ! Updraft velocity is parameterized following Lohmann et al. (1999).
    !

    USE mo_kind,       ONLY: dp
    USE mo_conv,       ONLY: conv_field !na_cv, cdncact_cv

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: jg, kproma, kbdim, klev, krow

    REAL(dp), INTENT(IN)  :: pw(kbdim,klev)  ! stratiform updraft velocity, large-scale+TKE (>0.0) [m s-1]
    REAL(dp), INTENT(out) :: pcdncact(kbdim,klev)  ! number of activated particles

    REAL(dp), PARAMETER :: c2=2.3E-10_dp, & ! [m4 s-1]
                           c3=1.27_dp       ! [1]

    INTEGER  :: jl, jk
    REAL(dp) :: zNmax, zeps

    zeps=EPSILON(1.0_dp)

    pcdncact(:,:) = 0._dp
    conv_field(jg)%cdncact_cv(:,:,krow) = 0._dp

    !--- Aerosol activation:

    DO jk=1, klev
       DO jl=1, kproma

          !--- Stratiform clouds:

          ! Activation occurs only in occurrence of supersaturation

          !>>SF note: 
          !     The previous temperature restriction (temp > homogeneous freezing temp)
          !     has been removed because it was preventing to diagnose the number of
          !     dust and BC particules in soluble modes where temp < hom. freezing.
          !     The rationale behind is that diagnosing this allows further
          !     devel to implement concurrent homogeneous vs heterogenous freezing processes
          !     (which is not yet part of this version, though).
          !
          !IMPORTANT: 
          !     This temperature condition removal is completely transparent for the sanity 
          !     of the current code, since relevant temperature ranges are now safely checked
          !     directly in cloud_cdnc_icnc
          !<<SF

          IF(pw(jl,jk)>zeps .AND. activ_field(jg)%na(jl,jk,krow)>zeps) THEN

             !--- Maximum number of activated particles [m-3]:

             zNmax=(activ_field(jg)%na(jl,jk,krow)*pw(jl,jk))/(pw(jl,jk)+c2*activ_field(jg)%na(jl,jk,krow))

             ! Average number of activated particles [m-3]:
             ! zNmax need to be converted to [cm-3] and the
             ! result to be converted back to [m-3].

             pcdncact(jl,jk)=0.1E6_dp*(1.0E-6_dp*zNmax)**c3

          END IF

          !--- Convective clouds:

          IF(pw(jl,jk)>zeps .AND. conv_field(jg)%na_cv(jl,jk,krow)>zeps) THEN

             zNmax=(conv_field(jg)%na_cv(jl,jk,krow)*pw(jl,jk))/(pw(jl,jk)+c2*conv_field(jg)%na_cv(jl,jk,krow))
             conv_field(jg)%cdncact_cv(jl,jk,krow)=0.1E6_dp*(1.0E-6_dp*zNmax)**c3

          ENDIF

       END DO
    END DO

  END SUBROUTINE activ_lin_leaitch

 SUBROUTINE activ_initialize ( p_patch )

    USE mo_exception,          ONLY: finish, message, em_param
    USE mo_submodel,           ONLY: print_value, lham, lhammoz, lccnclim
    USE mo_echam_cloud_config, ONLY : echam_cloud_config ! ccsaut, ccraut
    USE mo_set_subm_phys,      ONLY : nauto, &          !++mgs
                                      ncd_activ, nactivpdf, nic_cirrus, lcdnc_progn, &
                                      cdnc_min_fixed
!cms needed:  nactivpdf, nic_cirrus, lcdnc_progn, cdnc_min_fixed
    USE mo_submodel_tracer,   ONLY: get_tracer

    USE mo_model_domain,       ONLY: t_patch

    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    CHARACTER(len=24)      :: csubmname

    INTEGER :: nlev, ndomain, jg
    REAL(dp)    :: rsltn   ! horizontal resolution
    
    ! pointers to variables in echam_cloud_config
    REAL(dp), POINTER :: ccsaut, ccraut

    ndomain=SIZE(p_patch)

    IF ( ndomain /= 1 ) THEN
      CALL finish(thismodule, "Nesting currently not supported")
    END IF
    ! set domain index to one
    jg=1

    nlev  = p_patch(jg)%nlev
    rsltn = p_patch(jg)%geometry_info%mean_characteristic_length

    ccsaut   => echam_cloud_config% ccsaut
    ccraut   => echam_cloud_config% ccraut

    !--- Set number of updraft bins: 

    SELECT CASE(ABS(nactivpdf))
      CASE(0)
        nw = 1
      CASE(1)
        nw = 20
      CASE DEFAULT
        nw = ABS(nactivpdf)
    END SELECT

!!$    !
!!$    !-- overwrite values for coupled CDNC/ICNC cloud scheme
!!$    !
!!$    IF (lcdnc_progn)  THEN
!!$      IF (nlev == 31) THEN
!!$         IF ( rsltn .GE. 150e3_dp .AND.  rsltn .LE. 300e3_dp ) THEN !cms needs to be checked, formerly T63
!!$            SELECT CASE (ncd_activ)
!!$               CASE(1) ! LL activtion
!!$                  !SF: updated on 2015.02.25 (David Neubauer / Katty Huang, pure atm run, HAM-M7, LL activation)
!!$                  ccsaut = 1200._dp
!!$                  ccraut = 3.5_dp
!!$               CASE(2) !AR&G activation
!!$                  SELECT CASE(cdnc_min_fixed)
!!$                     CASE(10)
!!$                        !SF: updated on 2017.02.14 (David Neubauer, pure atm run, HAM-M7)
!!$                        ccsaut = 900._dp
!!$                        ccraut = 2.8_dp
!!$                     CASE(40)
!!$                        !SF: updated on 2017.02.14 (David Neubauer, pure atm run, HAM-M7)
!!$                        ccsaut = 900._dp
!!$                        ccraut = 10.6_dp
!!$                  END SELECT
!!$            END SELECT
!!$         ENDIF
!!$      ENDIF
!!$
!!$      IF (nlev == 47) THEN
!!$         IF ( rsltn .GE. 150e3_dp .AND.  rsltn .LE. 300e3_dp ) THEN !cms needs to be checked, formerly T63
!!$            SELECT CASE (ncd_activ)
!!$               CASE(1) ! LL activtion
!!$                  !SF: updated on 2015.02.19 (David Neubauer, pure atm run, HAM-M7, LL activation)
!!$                  ccsaut = 800._dp
!!$                  ccraut = 5._dp
!!$               CASE(2) ! AR&G activtion
!!$                  SELECT CASE(cdnc_min_fixed)
!!$                     CASE(10)
!!$                        !SF: updated on 2017.02.14 (David Neubauer, pure atm run, HAM-M7)
!!$                        ccsaut = 900._dp
!!$                        ccraut = 2.8_dp
!!$                     CASE(40)
!!$                        !SF: updated on 2017.02.14 (David Neubauer, pure atm run, HAM-M7)
!!$                        ccsaut = 900._dp
!!$                        ccraut = 10.6_dp
!!$                  END SELECT
!!$            END SELECT
!!$         ENDIF
!!$      ENDIF
!!$    ENDIF

!>>SF
    !-- Define the cdnc and icnc tracer index to point to the correct tracer:
    CALL get_tracer('CDNC',idx=idt_cdnc)
    CALL get_tracer('ICNC',idx=idt_icnc)
!<<SF

    !
    !-- Write out new parameters
    !
    IF (ncd_activ>0 .OR. nic_cirrus>0) THEN

      csubmname = 'UNKNOWN'
      IF (lham) csubmname = 'HAM'
      IF (lhammoz) csubmname = 'HAMMOZ'
      IF (lccnclim) csubmname = 'CCNCLIM'

      CALL message('','')
      CALL message('','----------------------------------------------------------')
      CALL message('activ_initialize','Parameter settings for the ECHAM-'//TRIM(csubmname)  &
                   //' cloud microphysics scheme')
      CALL message('','---')
      CALL print_value('              ncd_activ                       = ', ncd_activ)
      CALL print_value('              nic_cirrus                       = ', nic_cirrus)
      CALL message('', ' => Parameter adjustments in mo_activ:', level=em_param)
      CALL print_value('              ccsaut =', ccsaut)
      CALL print_value('              ccraut =', ccraut)
      CALL message('','---')
      CALL message('','----------------------------------------------------------')

    ENDIF
  END SUBROUTINE activ_initialize

  SUBROUTINE construct_activ_stream( p_patch ) 

    ! *construct_stream_activ* allocates output streams
    !                          for the activation schemes
    !
    ! Author:
    ! -------
    ! Philip Stier, MPI-MET                       2004
    !
    USE mo_model_domain,            ONLY: t_patch  

!!$  USE mo_filename,       ONLY: trac_filetype

    IMPLICIT NONE

    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch 

    CHARACTER(len=MAX_CHAR_LENGTH) :: listname
    INTEGER :: ndomain, jg, ist, nblks, nlev

    CALL message(TRIM(thismodule),'Construction of ACTIV state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( activ_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of activ_field failed')

    ALLOCATE( activ_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of activ_field_list failed')



    DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      WRITE(listname,'(a,i2.2)') 'activ_state_diag_of_domain',jg
      CALL new_activ_field_list( jg, nproma, nlev, nblks,                                  &
                                 &  TRIM(listname), activ_field_list(jg), activ_field(jg)  )

    END DO

    CALL message(TRIM(thismodule),'Construction of ACTIV state finished.')

  END SUBROUTINE construct_activ_stream

  !--------------------------------------------------------------------
  !>
  SUBROUTINE new_activ_field_list( k_jg, kproma, klev, kblks,       &
                                 & listname, field_list, field    ) 

    USE mo_set_subm_phys,      ONLY : nactivpdf, ncd_activ, nic_cirrus

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev, kblks     !< dimension sizes

    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),    INTENT(INOUT) :: field_list
    TYPE(t_activ_field), INTENT(INOUT) :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    CHARACTER(len=10) :: cbin
    INTEGER :: jw

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)


    IF (nactivpdf <= 0) THEN
      ! These are used either if not using a PDF, or if per-bin
      ! diagnostics are requested.
      ALLOCATE(field%w(nw))
      ALLOCATE(field%w_pdf(nw))
      ALLOCATE(field%swat_max_strat(nw))
      ALLOCATE(field%swat_max_conv(nw))
    END IF


    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )
    CALL default_var_list_settings( field_list,                &
                                  & lrestart=.TRUE.  )    !filetype=trac_filetype

    !--- 1) Cloud Properties:
    cf_desc    = t_cf_var('SWAT', '% [0-1]', 'ICON supersaturation over water', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'swat', field%swat,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    IF (ncd_activ==2) THEN

       IF (nactivpdf == 0) THEN
          cf_desc    = t_cf_var('SWAT_MAX_STRAT', '% [0-1]', 'maximum supersaturation stratiform', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( field_list, 'swat_max_strat', field%swat_max_strat_4d,                 &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                      & ldims=(/kproma,klev,kblks,1/),                                         &
                      &  lcontainer=.TRUE.,  lrestart=.FALSE.                                  )

          cf_desc    = t_cf_var('SWAT_MAX_STRAT', '% [0-1]', 'maximum supersaturation stratiform', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_ref( field_list, 'swat_max_strat', 'swat_max_strat', field%swat_max_strat(1)%p_3d,            &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                      & vert_interp=create_vert_interp_metadata(                               &
                      &             vert_intp_type=vintp_types("P","Z","I"),                   &
                      &             vert_intp_method=VINTP_METHOD_LIN,                         &
                      &             l_loglin=.FALSE.,                                          &
                      &             l_extrapol=.FALSE.),                                       &
                      &  lrestart=.TRUE.,                                                      &
                      &  lrestart_cont=.TRUE.                                                  )

          cf_desc    = t_cf_var('SWAT_MAX_CONVECT', '% [0-1]', 'maximum supersaturation convective', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( field_list, 'swat_max_convect', field%swat_max_conv_4d,                &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                      & ldims=(/kproma,klev,kblks,1/),                                         &
                      & lcontainer=.TRUE.,  lrestart=.FALSE.                                   )

          cf_desc    = t_cf_var('SWAT_MAX_CONV', '% [0-1]', 'maximum supersaturation convective', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_ref( field_list, 'swat_max_convect', 'swat_max_conv', field%swat_max_conv(1)%p_3d,   &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                      & vert_interp=create_vert_interp_metadata(                               &
                      &             vert_intp_type=vintp_types("P","Z","I"),                   &
                      &             vert_intp_method=VINTP_METHOD_LIN,                         &
                      &             l_loglin=.FALSE.,                                          &
                      &             l_extrapol=.FALSE.),                                       &
                      &  lrestart=.TRUE.,                                                      &
                      &  lrestart_cont=.TRUE.                                                  )   

       ELSE IF (nactivpdf < 0) THEN

          cf_desc    = t_cf_var('SWAT_MAX_STRATIF', '% [0-1]', 'maximum supersaturation stratiform', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( field_list, 'swat_max_stratif', field%swat_max_strat_4d,               &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                      & ldims=(/kproma,klev,kblks,nw/),                                        &
                      &  lcontainer=.TRUE.,  lrestart=.FALSE.                                  )


          cf_desc    = t_cf_var('SWAT_MAX_CONVECT', '% [0-1]', 'maximum supersaturation convective', datatype_flt)
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( field_list, 'swat_max_convect', field%swat_max_conv_4d,                &
                      & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                      & ldims=(/kproma,klev,kblks,nw/),                                        &
                      &  lcontainer=.TRUE.,  lrestart=.FALSE.                                  )

          DO jw=1,nw
             WRITE (cbin, "(I2.2)") jw
             cf_desc    = t_cf_var('SWAT_MAX_STRAT_'//TRIM(cbin), '% [0-1]',                       &
                             'maximum supersaturation stratiform, vertical velocity bin '//TRIM(cbin), datatype_flt)
             grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
             CALL add_ref( field_list, 'swat_max_stratif', 'swat_max_strat_'//TRIM(cbin), field%swat_max_strat(jw)%p_3d,  &
                         & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                         & vert_interp=create_vert_interp_metadata(                               &
                         &             vert_intp_type=vintp_types("P","Z","I"),                   &
                         &             vert_intp_method=VINTP_METHOD_LIN,                         &
                         &             l_loglin=.FALSE.,                                          &
                         &             l_extrapol=.FALSE.),                                       &
                         &  lrestart=.TRUE.,                                                      &
                         &  lrestart_cont=.TRUE.                                                  )


             cf_desc    = t_cf_var('SWAT_MAX_CONV_'//TRIM(cbin), '% [0-1]',                       &
                             'maximum supersaturation convective, vertical velocity bin '//TRIM(cbin), datatype_flt)
             grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
             CALL add_ref( field_list, 'SWAT_MAX_CONVECT', 'SWAT_MAX_CONV_'//TRIM(cbin), field%swat_max_conv(jw)%p_3d,    &
                         & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                         & vert_interp=create_vert_interp_metadata(                               &
                         &             vert_intp_type=vintp_types("P","Z","I"),                   &
                         &             vert_intp_method=VINTP_METHOD_LIN,                         &
                         &             l_loglin=.FALSE.,                                          &
                         &             l_extrapol=.FALSE.),                                       &
                         &  lrestart=.TRUE.,                                                      &
                         &  lrestart_cont=.TRUE.                                                  )
          END DO
       END IF
    END IF

    IF (nactivpdf == 0) THEN

      cf_desc    = t_cf_var('Wvel', 'm s-1', 'total vertical velocity for activation', datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( field_list, 'wvel', field%w_4d,                                        &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                  & ldims=(/kproma,klev,kblks,1/),                                         &
                  &  lcontainer=.TRUE.,  lrestart=.FALSE.                                  )

      cf_desc    = t_cf_var('W_actv', 'm s-1', 'total vertical velocity for activation', datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_ref( field_list, 'wvel', 'w_actv', field%w(1)%p_3d,                           &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                  & vert_interp=create_vert_interp_metadata(                               &
                  &             vert_intp_type=vintp_types("P","Z","I"),                   &
                  &             vert_intp_method=VINTP_METHOD_LIN,                         &
                  &             l_loglin=.FALSE.,                                          &
                  &             l_extrapol=.FALSE.),                                       &
                  &  lrestart=.TRUE.,                                                      &
                  &  lrestart_cont=.TRUE.                                                  )
     ELSE IF (nactivpdf < 0) THEN

       cf_desc    = t_cf_var('Wvel', 'm s-1', 'total vertical velocity for activation', datatype_flt)
       grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
       CALL add_var( field_list, 'wvel', field%w_4d,                                        &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,                &
                   & ldims=(/kproma,klev,kblks,nw/),                                        &
                   &  lcontainer=.TRUE., lrestart=.FALSE.                                   )

         cf_desc    = t_cf_var('W_PDF', 'm s-1', 'Vertical velocity PDF in each bin for activation', &
                       &         datatype_flt)
         grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
         CALL add_var( field_list, 'w_pdf', field%w_pdf_4d,                                 &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,              &
                     & ldims=(/kproma,klev,kblks,nw/),                                      &
                     & lcontainer=.TRUE., lrestart=.FALSE.                                  )


       DO jw=1, nw
         WRITE (cbin, "(I2.2)") jw

         cf_desc    = t_cf_var('W_'//TRIM(cbin), 'm s-1', 'total vertical velocity bin '//TRIM(cbin)//' for activation', &
                       &         datatype_flt)
         grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
         CALL add_ref( field_list, 'Wvel', 'W_'//TRIM(cbin), field%w(jw)%p_3d,                &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                     & vert_interp=create_vert_interp_metadata(                               &
                     &             vert_intp_type=vintp_types("P","Z","I"),                   &
                     &             vert_intp_method=VINTP_METHOD_LIN,                         &
                     &             l_loglin=.FALSE.,                                          &
                     &             l_extrapol=.FALSE.),                                       &
                     &  lrestart=.TRUE.,                                                      &
                     &  lrestart_cont=.TRUE.                                                  )
   
         cf_desc    = t_cf_var('W_PDF_'//TRIM(cbin), 'm s-1', 'Vertical velocity PDF in bin '//TRIM(cbin)//' for activation', &
                       &         datatype_flt)
         grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
         CALL add_ref( field_list, 'W_PDF', 'W_PDF_'//TRIM(cbin), field%w_pdf(jw)%p_3d,       &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                     & vert_interp=create_vert_interp_metadata(                               &
                     &             vert_intp_type=vintp_types("P","Z","I"),                   &
                     &             vert_intp_method=VINTP_METHOD_LIN,                         &
                     &             l_loglin=.FALSE.,                                          &
                     &             l_extrapol=.FALSE.),                                       &
                     &  lrestart=.TRUE.,                                                      &
                     &  lrestart_cont=.TRUE.                                                  )
       END DO
    END IF

    cf_desc    = t_cf_var('W_LARGE', 'm s-1', 'large scale vertical velocity', &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'w_large', field%w_large,                                  &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    IF (nactivpdf == 0) THEN
      cf_desc    = t_cf_var('W_TURB', 'm s-1', 'turbulent vertical velocity', &
                    &         datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( field_list, 'w_turb', field%w_turb,                                    &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                  & vert_interp=create_vert_interp_metadata(                               &
                  &             vert_intp_type=vintp_types("P","Z","I"),                   &
                  &             vert_intp_method=VINTP_METHOD_LIN,                         &
                  &             l_loglin=.FALSE.,                                          &
                  &             l_extrapol=.FALSE.),                                       &
                  &  lrestart=.TRUE.,                                                      &
                  &  lrestart_cont=.TRUE.                                                  )
    ELSE
      cf_desc    = t_cf_var('W_SIGMA', 'm s-1', 'sub-grid st. dev. of vertical velocity',  &
                    &         datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( field_list, 'w_sigma', field%w_sigma,                                  &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                  & vert_interp=create_vert_interp_metadata(                               &
                  &             vert_intp_type=vintp_types("P","Z","I"),                   &
                  &             vert_intp_method=VINTP_METHOD_LIN,                         &
                  &             l_loglin=.FALSE.,                                          &
                  &             l_extrapol=.FALSE.),                                       &
                  &  lrestart=.TRUE.,                                                      &
                  &  lrestart_cont=.TRUE.                                                  )
    END IF

    cf_desc    = t_cf_var('W_CAPE', 'm s-1', 'convective updraft velocity from CAPE',     &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'w_cape', field%w_cape,                                     &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )


    cf_desc    = t_cf_var('REFFL', 'um', 'cloud drop effective radius',                   &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'reffl', field%reffl,                                      &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    IF (nic_cirrus>0) THEN
      cf_desc    = t_cf_var('REFFI', 'um', 'ice crystal effective radius',                 &
                    &         datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( field_list, 'reffi', field%reffi,                                      &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                  & vert_interp=create_vert_interp_metadata(                               &
                  &             vert_intp_type=vintp_types("P","Z","I"),                   &
                  &             vert_intp_method=VINTP_METHOD_LIN,                         &
                  &             l_loglin=.FALSE.,                                          &
                  &             l_extrapol=.FALSE.),                                       &
                  &  lrestart=.TRUE.,                                                      &
                  &  lrestart_cont=.TRUE.                                                  )
    END IF

    cf_desc    = t_cf_var('NA', 'm-3', 'aerosol number for activation',                   &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'na', field%na,                                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

   ! accumulated
    cf_desc    = t_cf_var('QNUC', 'm-3 s-1', 'CD nucleation rate',                      &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'qnuc', field%qnuc,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('QAUT', 'm-3 s-1', 'CD autoconversion rate',                   &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'qaut', field%qaut,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('QACC', 'm-3 s-1', 'CD accretion rate',                        &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'qacc', field%qacc,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('QFRE', 'm-3 s-1', 'CD freezing rate',                         &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'qfre', field%qfre,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

!>>dod deleted QEVA, not used anywhere
!    cf_desc    = t_cf_var('QEVA', 'm-3 s-1', 'CD evaporation rate',                         &
!                  &         datatype_flt)
!    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!    CALL add_var( field_list, 'qeva', field%qeva,                                        &
!                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!                & vert_interp=create_vert_interp_metadata(                               &
!                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!                &             l_loglin=.FALSE.,                                          &
!                &             l_extrapol=.FALSE.),                                       &
!                &  lrestart=.TRUE.,                                                      &
!                &  lrestart_cont=.TRUE.,                                                 )

    cf_desc    = t_cf_var('QMEL', 'm-3 s-1', 'CD source rate from melting ice',          &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'qmel', field%qmel,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('CDNC_ACC', 'm-3', 'CDNC occurence acc.+ cloud weighted',      &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_acc', field%cdnc_acc,                                &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('CDNC_actv', 'm-3', 'CDNC_actv',  datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_actv', field%cdnc,                                   &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )



    cf_desc    = t_cf_var('CDNC_BURDEN_ACC', 'm-2', 'CDNC burden occurence accumulated', &
                       &   datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_burden_acc', field%cdnc_burden_acc,                   &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('cdnc_burden', 'm-2', 'CDNC burden', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_burden', field%cdnc_burden,                           &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('BURDEN_TIME', '1', 'acc. cdnc burden occ.time fraction', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'burden_time', field%burden_time,                           &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )


    cf_desc    = t_cf_var('LWC_ACC', 'kg m-3', 'liq wat cont acc.+ cloud weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'lwc_acc', field%lwc_acc,                                  &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('CLOUD_TIME', '1', 'acc. cloud occurence time fraction', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cloud_time', field%cloud_time,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('REFFL_ACC', 'um', 'cloud drop effective radius weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'reffl_acc', field%reffl_acc,                              &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('REFFL_CT', 'um', 'cloud top effective radius weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'reffl_ct', field%reffl_ct,                                 &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('REFFL_TIME', '1', 'cloud top effective radius occ. time', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'reffl_time', field%reffl_time,                               &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                &  lrestart=.TRUE.,                                                         &
                &  lrestart_cont=.TRUE.                                                     )

    cf_desc    = t_cf_var('CDNC_CT', 'cm-3', 'cloud top cloud droplet number conc.', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_ct', field%cdnc_ct,                                   &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('IWC_ACC', 'kg m-3', 'ice wat cont acc.+ cloud weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'iwc_acc', field%iwc_acc,                                  &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('CLIWC_TIME', '1', 'acc. cloud occurence time fraction', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cliwc_time', field%cliwc_time,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    ! instantaneous
    cf_desc    = t_cf_var('CLOUD_COVER_DUPLIC', '1', 'cloud cover duplicate for record at t+1',  &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cloud_cover_duplic', field%cloud_cover_duplic,            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('CDNC_instantaneous', 'm-3', 'CDNC instantaneous',             &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'cdnc_instantaneous', field%cdnc_instant,                  &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

  IF (nic_cirrus>0) THEN

    cf_desc    = t_cf_var('ICNC_instantaneous', 'm-3', 'ICNC instantaneous',             &
                  &         datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'icnc_instantaneous', field%icnc_instant,                  &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    ! accumulated 
    cf_desc    = t_cf_var('ICNC_ACC', 'm-3', 'ICNC occurence acc.+ cloud weighted', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'icnc_acc', field%icnc_acc,                                &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    
    cf_desc    = t_cf_var('ICNC_actv', 'm-3', 'ICNC_actv', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'icnc_actv', field%icnc,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )
 

    cf_desc    = t_cf_var('ICNC_BURDEN_ACC', 'm-2', 'ICNC burden occurence accumulated', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'icnc_burden_acc', field%icnc_burden_acc,                   &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('ICNC_BURDEN', 'm-2', 'ICNC burden', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'icnc_burden', field%icnc_burden,                           &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )
  
    cf_desc    = t_cf_var('BURDIC_TIME', '1', 'acc. icnc burden occ.time fraction', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'burdic_time', field%burdic_time,                           &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('REFFI_ACC', 'um', 'ice crystal effective radius weighted', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'reffi_acc', field%reffi_acc,                              &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )
 
 
    cf_desc    = t_cf_var('REFFI_TOVS', 'um', 'semi-transparent cirrus effectiv radius', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'reffi_tovs', field%reffi_tovs,                             &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

  
    cf_desc    = t_cf_var('REFFI_TIME', '1', 'accumulted semi-transp. cirrus time', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'reffi_time', field%reffi_time,                             &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )

    cf_desc    = t_cf_var('IWP_TOVS', 'kg m-2', 'IWP sampled a la TOVS', datatype_flt )
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL )
    CALL add_var( field_list, 'iwp_tovs', field%iwp_tovs,                                 &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d, &
                &  lrestart=.TRUE.,                                                       &
                &  lrestart_cont=.TRUE.                                                   )  

    ! instantaneous

    cf_desc    = t_cf_var('SICE', '% [0-1]', 'ICON supersaturation over ice', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'sice', field%sice,                                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )   


  END IF

!!$!cmsd++
!!$    cf_desc    = t_cf_var('icnc_tm1', 'x', 'icnc_tm1',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'icnc_tm1', field%icnc_tm1,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$    cf_desc    = t_cf_var('dicnc1in', 'x', 'dicnc1in',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc1in', field%dicnc1in,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$   cf_desc    = t_cf_var('dicnc2cor', 'x', 'dicnc2cor',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc2cor', field%dicnc2cor,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$   cf_desc    = t_cf_var('dicnc3cor', 'x', 'dicnc3cor',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc3cor', field%dicnc3cor,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$   cf_desc    = t_cf_var('dicnc4detr', 'x', 'dicnc4detr',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc4detr', field%dicnc4detr,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$   cf_desc    = t_cf_var('dicnc5nucl', 'x', 'dicnc5nucl',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc5nucl', field%dicnc5nucl,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$   cf_desc    = t_cf_var('icnc_dum', 'x', 'icnc_dum',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'icnc_dum', field%icnc_dum,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$  cf_desc    = t_cf_var('dicnc6fre', 'x', 'dicnc6fre',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc6fre', field%dicnc6fre,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$  cf_desc    = t_cf_var('dicnc7lim', 'x', 'dicnc7lim',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc7lim', field%dicnc7lim,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$  cf_desc    = t_cf_var('dicnc8mlt', 'x', 'dicnc8mlt',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc8mlt', field%dicnc8mlt,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$  cf_desc    = t_cf_var('dicnc9sed', 'x', 'dicnc9sed',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc9sed', field%dicnc9sed,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$ cf_desc    = t_cf_var('dicnc10cor', 'x', 'dicnc10cor',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc10cor', field%dicnc10cor,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$ cf_desc    = t_cf_var('dicnc11upd', 'x', 'dicnc11upd',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc11upd', field%dicnc11upd,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$
!!$
!!$ cf_desc    = t_cf_var('dicnc12hom', 'x', 'dicnc12hom',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc12hom', field%dicnc12hom,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$ cf_desc    = t_cf_var('dicnc13het', 'x', 'dicnc13het',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc13het', field%dicnc13het,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$  cf_desc    = t_cf_var('dicnc14cr', 'x', 'dicnc14cr',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'dicnc14cr', field%dicnc14cr,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$  cf_desc    = t_cf_var('zrid', 'x', 'zrid',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'zrid', field%zrid,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$
!!$  cf_desc    = t_cf_var('paclc', 'x', 'paclc',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'paclc', field%paclc,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$  cf_desc    = t_cf_var('zxtec', 'x', 'zxtec',                   &
!!$                  &         datatype_flt)
!!$    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!$    CALL add_var( field_list, 'zxtec', field%zxtec,                                      &
!!$                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
!!$                & vert_interp=create_vert_interp_metadata(                               &
!!$                &             vert_intp_type=vintp_types("P","Z","I"),                   &
!!$                &             vert_intp_method=VINTP_METHOD_LIN,                         &
!!$                &             l_loglin=.FALSE.,                                          &
!!$                &             l_extrapol=.FALSE.),                                       &
!!$                &  lrestart=.TRUE.,                                                      &
!!$                &  lrestart_cont=.TRUE.                                                  )
!!$
!!$!cmsd--
  END SUBROUTINE new_activ_field_list

END MODULE mo_activ

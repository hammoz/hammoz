!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
###(disclaimer)###
MODULE mo_submodels_nml
  
  ###(use_all_namelist_readers)###

  IMPLICIT NONE 
  
  PRIVATE
 
  PUBLIC :: read_submodels_namelist

CONTAINS 
SUBROUTINE read_submodels_namelist( filename )

  CHARACTER(LEN=*),INTENT(IN) :: filename

  ###(call_all_namelists)###

END SUBROUTINE read_submodels_namelist
END MODULE mo_submodels_nml

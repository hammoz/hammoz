!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
! avoid circular dependence mo_species, mo_tracdef (now that tracers inherit definitions from species)  
MODULE mo_submodel_tracdef_constants

  IMPLICIT NONE

  PRIVATE

  !
  ! Individual settings for each tracer
  !
  INTEGER, PARAMETER :: ln =  24  ! length of name (char) components  
  INTEGER, PARAMETER :: ls =  32  ! length of short name
  INTEGER, PARAMETER :: ll = 256  ! length of longname and standardname 
  INTEGER, PARAMETER :: lf =   8  ! length of flag character string
  INTEGER, PARAMETER :: nf =  10  ! number of user defined flags
  INTEGER, PARAMETER :: ns =  20  ! max number of submodels

  !
  ! Limits
  ! 
  INTEGER, PARAMETER :: jptrac = 500  ! maximum number of tracers allowed 
  
  !
  ! Constants (argument values for new_tracer routine in mo_tracer)
  !
  !
  ! error return values
  !
  INTEGER, PARAMETER :: OK         = 0
  INTEGER, PARAMETER :: NAME_USED  = 2
  INTEGER, PARAMETER :: NAME_MISS  = 3
  INTEGER, PARAMETER :: TABLE_FULL = 4
  !
  ! general flags
  !
  INTEGER, PARAMETER :: OFF        = 0
  INTEGER, PARAMETER :: ON         = 1
  !
  ! initialisation flag  (ninit)
  !
  INTEGER, PARAMETER :: CONSTANT = 1
  INTEGER, PARAMETER :: RESTART  = 2
  INTEGER, PARAMETER :: INITIAL  = 4
  INTEGER, PARAMETER :: PERIODIC = 8
  ! 
  ! Tracer type
  ! 
  INTEGER, PARAMETER :: ITRNONE    = 0   ! No ECHAM tracer
  INTEGER, PARAMETER :: ITRPRESC   = 1   ! Prescribed tracer, read from file
  INTEGER, PARAMETER :: ITRDIAG    = 2   ! Diagnostic tracer, no transport
  INTEGER, PARAMETER :: ITRPROG    = 3   ! Prognostic tracer, transport by ECHAM
  !
  ! soluble flag  (nsoluble)
  !
  INTEGER, PARAMETER :: INSOLUBLE = 0 ! insoluble
  INTEGER, PARAMETER :: SOLUBLE   = 1 ! soluble
  !
  ! phase indicator  (nphase)
  !
  INTEGER, PARAMETER :: GAS            = 1  ! gas
  INTEGER, PARAMETER :: AEROSOL        = 2  ! aerosol (for species definition)
  INTEGER, PARAMETER :: GAS_OR_AEROSOL = 3  ! gas or aerosol (for species definition)
  INTEGER, PARAMETER :: AEROSOLMASS    = 2  ! aerosol mass
  INTEGER, PARAMETER :: AEROSOLNUMBER  = -2 ! particle number concentration
  INTEGER, PARAMETER :: UNDEFINED      = 0  ! other tracers (e.g. CDNC)

 
  PUBLIC :: ln, ls, ll, lf, nf, ns 
  
  PUBLIC :: jptrac

  PUBLIC :: OK, NAME_USED, NAME_MISS, TABLE_FULL
  PUBLIC :: OFF, ON
  PUBLIC :: CONSTANT, RESTART, INITIAL, PERIODIC
  PUBLIC :: ITRNONE, ITRPRESC, ITRDIAG, ITRPROG
  PUBLIC :: INSOLUBLE, SOLUBLE
  PUBLIC :: GAS, AEROSOL, GAS_OR_AEROSOL, AEROSOLMASS, AEROSOLNUMBER, UNDEFINED 


END MODULE mo_submodel_tracdef_constants

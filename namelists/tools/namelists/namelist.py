import os, sys
import glob

import utils.drivers as dri
import utils.util as u

import namelists.nml_parse as p
import namelists.read_all_submodel_nml as rsn


def namelist_file( xml_include_dir ):
    xml_filename=xml_include_dir+"/namelists.xml"
    return(xml_filename)

def prepare_namelist_io( xml_include_dir,driver_dir  ):
    xml_filename=namelist_file(xml_include_dir)

    # create a list of drivers that contains paths to template and src 
    #     directories
    dir_list=dri.create_drivers_list( driver_dir )

    # create modules for reading individual namelists 
    for dir in dir_list:
         indir=dir.templates
         outdir=dir.src
         
         p.nml_parse_and_replace(xml_filename, indir, outdir)
    ##base_dir=u.new_dirs("namelists.py", "base", "..")

    # create config.f90 files
    ##prepare_config( xml_include_dir, base_dir )

    # create read_hammoz_namelists.f90 (one routine for reading all hammoz namelists)
    drivers_shared_dir=u.new_dirs("namelists.py", "drivers_shared_dir", "./")
    rsn.read_namelists_submodel(xml_filename, drivers_shared_dir)  
    
def prepare_config( xml_include_dir, base_dir ):    
    xml_filename=namelist_file(xml_include_dir) 
    indir=base_dir.templates
    outdir=base_dir.src

    p.nml_parse_and_replace(xml_filename, indir, outdir)

            

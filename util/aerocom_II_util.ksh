remap_aerocom_II(){


spec=OC

set -A yys 1850  2000

for yy in ${yys[*]}; do

  irpath=emissions_inventories/aerocom_II
  fname_in=emiss_aerocom_${spec}_monthly_${yy}_${hres_in}.nc
  fname_out=emiss_aerocom_${spec}_monthly_${yy}_${grid_name_out}.nc
  ifilen=$irpath/${hres_in}/${yy}/$fname_in
  ofilen=$irpath/${grid_name_out}/${yy}/$fname_out
  cdo_remap_con $ifilen $ofilen


  spec=DMS
  fname_in=emiss_aerocom_${spec}_monthly_CLIM_${hres_in}.nc
  fname_out=emiss_aerocom_${spec}_monthly_CLIM_${grid_name_out}.nc
  cdo_remap_con $irpath/${hres_in}/$fname_in $irpath/${grid_name_out}/$fname_out

done

}

remap_gfas(){

yys=2000 
yye=2016

set -A specs bc c2h6s oc so2

for spec in ${specs[*]}; do
  yy=$yys
  while [[ $yy -le $yye ]]; do
     irpath=emissions_inventories/gfas
     fname_in=emiss_GFAS_${spec}_wildfire_${yy}_${hres_in}.nc
     fname_out=emiss_GFAS_${spec}_wildfire_${yy}_${grid_name_out}.nc
  
     ifilen=$irpath/${hres_in}/daily/${yy}/$fname_in
     ofilen=$irpath/${grid_name_out}/daily/${yy}/$fname_out
     cdo_remap_con $ifilen $ofilen
     (( yy = yy+1 ))
  done
done

}

!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_set_subm_phys

  USE mo_exception,            ONLY: message, message_text, em_error, em_warn, em_info, em_param
  USE mo_util_string,          ONLY: separator
  USE mo_subm_phyctl_config,   ONLY: subm_phyctl
  USE mo_config_util,          ONLY: set_config 
  USE mo_submodel,             ONLY: lccnclim, print_value 

  IMPLICIT NONE

  PRIVATE

  LOGICAL :: lconvmassfix   ! false for switching off aerosol mass fixer in conv

  LOGICAL :: lcdnc_progn    ! true for prognostic cloud droplet activation

  INTEGER :: ncd_activ      ! type of cloud droplet activation scheme (see physctl.inc)

  INTEGER :: nactivpdf      ! sub-grid scale PDF of updraft velocities (see physctl.inc) !ZK

  INTEGER :: nic_cirrus     ! type of cirrus scheme (see physctl.inc)

  INTEGER :: nauto          ! autoconversion scheme    (1,2)

  LOGICAL :: lsecprod       ! switch to take into account secondary ice production (see cloud_cdnc_icnc.f90) #251
                            ! lsecprod = .FALSE. (default, no secondary ice production)
                            !          = .TRUE.  (secondary ice prod)

  LOGICAL :: lorocirrus     ! switch to take into account gravity waves updraft velocity in 
                            ! ice crystal number concentration calculation (--> orographic cirrus clouds)
                            ! lorocirrus = .FALSE. (default, no orographic cirrus clouds)
                            !            = .TRUE.  (orographic cirrus clouds on)

  LOGICAL :: ldyn_cdnc_min  ! switch to turn on the dynamical setting of the minimum cloud droplet number concentration
                            ! ldyn_cdnc_min = .FALSE. (default, fixed minimum CDNC)
                            !                 .TRUE.  (dynamical min CDNC)

  INTEGER :: cdnc_min_fixed ! fixed value for min CDNC in cm-3 (used when ldyn_cdnc_min is FALSE)
                            ! Warning! So far only values of 40 or 10 are accepted.

  ! public subroutine
  PUBLIC :: set_subm_phys

  ! public variables
  PUBLIC :: lconvmassfix, lcdnc_progn, ncd_activ,  nactivpdf, nic_cirrus, nauto, lsecprod, lorocirrus, &
            ldyn_cdnc_min, cdnc_min_fixed

CONTAINS
  SUBROUTINE set_subm_phys

    ! Import namelist variables:

    CALL message('',separator)
    CALL message('mo_set_subm_phys', 'Importing variables from namelist subm_phyctl ...', level=em_info)

    CALL set_config( lconvmassfix, subm_phyctl % lconvmassfix )
    CALL set_config( lcdnc_progn, subm_phyctl % lcdnc_progn )
    CALL set_config( lcdnc_progn, subm_phyctl % lcdnc_progn )
    CALL set_config( ncd_activ, subm_phyctl % ncd_activ )
    CALL set_config( nactivpdf, subm_phyctl % nactivpdf )
    CALL set_config( nic_cirrus, subm_phyctl % nic_cirrus )
    CALL set_config( lsecprod, subm_phyctl % lsecprod )
    CALL set_config( lorocirrus, subm_phyctl % lorocirrus )
    CALL set_config( ldyn_cdnc_min, subm_phyctl % ldyn_cdnc_min )
    CALL set_config( cdnc_min_fixed, subm_phyctl % cdnc_min_fixed )
    CALL set_config( nauto, subm_phyctl % nauto )
  
    CALL message('','---')
    WRITE(message_text,'(a,a,1x,l1)') 'Prognostic eq. for cloud droplet- and ice crystal-number', &
                                      ' concentration (lcdnc_progn)', lcdnc_progn
    CALL message('',message_text, level=em_param)

    IF (.NOT. lcdnc_progn) THEN
      ncd_activ  = 0
      nic_cirrus = 0
      nauto      = 0       !SF added for security only
      lsecprod    = .FALSE. !SF added for security only (#251)
      lorocirrus = .FALSE. !GF added for security only
      ldyn_cdnc_min = .FALSE. !SF security
      CALL message('set_subm_phys', 'ncd_activ, nic_cirrus and nauto set to 0 because lcdnc_progn=.FALSE.',  &
                   level=em_warn)
      CALL message('set_subm_phys', 'lsecprod set to false because lcdnc_progn=.FALSE.', level=em_warn)
      IF (lccnclim) THEN
         CALL message('set_subm_phys', 'lcdnc_progn must be .TRUE. when lccnclim is active!', level=em_error)
      ENDIF
    ELSE !SF lcdnc_progn=true
      IF (ncd_activ <= 0) THEN
        CALL message('set_subm_phys', 'ncd_activ must be >0 for lcdnc_progn=.TRUE.!', level=em_error)
      ELSE !SF ncd_activ > 0
        IF (nauto <= 0) THEN
          CALL message('set_subm_phys', 'nauto must be >0 for ncd_activ>0!', level=em_error)
        ENDIF
        IF (nic_cirrus <= 0) THEN
          CALL message('set_subm_phys', 'nic_cirrus must be >0 for ncd_activ>0!', level=em_error)
        END IF
      END IF
    END IF

  !gf-security check
    IF (lorocirrus .AND. nic_cirrus /= 2) THEN
       CALL message('set_subm_phys', 'lorocirrus=.TRUE. only possible with nic_cirrus=2', level=em_error)
    END IF
  !gf-security check

      CALL message('','---')
      CALL print_value('Activation scheme (ncd_activ)                    = ', ncd_activ)
      SELECT CASE(ncd_activ)
        CASE(0)
         CALL message('','                            -> no activation',level=em_param)
        CASE(1)
         CALL message('','                            -> Lohmann et al. (1999) + Lin and Leaitch (1997)',level=em_param)
        CASE(2)
         CALL message('','                            -> Lohmann et al. (1999) + Abdul-Razzak and Ghan (2000)',level=em_param)
        CASE DEFAULT
          WRITE (message_text,*) '   ncd_activ = ',ncd_activ,' not supported.'
          CALL message('set_subm_phys',message_text, level=em_error)
      END SELECT


  !>>ZK
      CALL message('','---')
      CALL print_value('Sub-grid updraft PDF (nactivpdf)                 = ', nactivpdf)
      SELECT CASE(nactivpdf)
        CASE(0)
         CALL message('','                            -> Mean updraft from TKE scheme without PDF',level=em_param)
        CASE(1)
         CALL message('','                            -> Coupling of updraft PDF with 20 bins to TKE scheme (West et al., 2013)', &
                      level=em_param)
        CASE(-1)
         CALL message('','                            -> Coupling of updraft PDF with 20 bins to ' &
                         // 'TKE scheme (West et al., 2013) with per-bin diagnostics',             &
                      level=em_param)
        CASE DEFAULT
         IF (nactivpdf > 1) THEN
          WRITE (message_text,*) '                            -> Coupling of updraft PDF with ', &
                                 nactivpdf,' bins to TKE scheme (West et al., 2013)'
         ELSE
          WRITE (message_text,*) '                            -> Coupling of updraft PDF with ', &
                                 -nactivpdf,' bins to TKE scheme (West et al., 2013) with per-bin diagnostics'
         END IF
         CALL message('', message_text, level=em_param)
      END SELECT

      IF (nactivpdf /= 0 .AND. ncd_activ /= 2) THEN
         CALL message('set_subm_phys', 'nactivpdf/=0 only possible with ncd_activ=2', level=em_error)
      END IF
  !<<ZK

      CALL message('','---')
      CALL print_value('Cirrus scheme (nic_cirrus)                    = ', nic_cirrus)
      SELECT CASE(nic_cirrus)
        CASE(0)
         CALL message('','                            -> no cirrus scheme',level=em_param)
        CASE(1)
         CALL message('','                            -> Lohmann, JAS 2002',level=em_param)
        CASE(2)
         CALL message('','                            -> Kaercher & Lohmann, JGR 2002',level=em_param)
        CASE DEFAULT
          WRITE (message_text,*) '   nic_cirrus = ',nic_cirrus,' not supported.'
          CALL message('set_subm_phys',message_text, level=em_error)
      END SELECT

      CALL message('','---')
      CALL print_value('Autoconversion (nauto)                    = ', nauto)
      SELECT CASE(nauto)
        CASE (0)
          CALL message('set_subm_phys','nauto=0. Are you sure?',level=em_warn)
        CASE (1)
          CALL message('','                            -> Beheng (1994) - ECHAM5 Standard',level=em_param)
        CASE (2)
          CALL message('','                            -> Khairoutdinov and Kogan (2000)',level=em_param)
        CASE DEFAULT
          WRITE (message_text,*) '   nauto = ',nauto,' not supported.'
          CALL message('set_subm_phys',message_text, level=em_error)
      END SELECT

  !>>gf
      CALL message('','---')
      CALL print_value('Orographic cirrus cloud (lorocirrus)                    = ', lorocirrus)
  !<<gf

  !>>SF #251
      CALL message('','---')
      CALL print_value('Secondary ice production (lsecprod)                    = ', lsecprod)
  !<<SF

  !>>SF #475
      CALL message('','---')
      CALL print_value('Dynamical minimum CDNC (ldyn_cdnc_min)                 = ', ldyn_cdnc_min)
  !<<SF

  !>>SF #489
      CALL message('','---')
      SELECT CASE(cdnc_min_fixed)
         CASE(10, 40)
           CALL print_value('Fixed minimum CDNC (cdnc_min_fixed) = ', cdnc_min_fixed)
         CASE DEFAULT
           WRITE (message_text,*) '   cdnc_min_fixed = ',cdnc_min_fixed,' not supported.'
           CALL message('set_subm_phys',message_text, level=em_error)
      END SELECT
  !<<SF
      CALL message('','---')
 
  END SUBROUTINE set_subm_phys
END MODULE mo_set_subm_phys

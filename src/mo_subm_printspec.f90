!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_subm_printspec

   USE mo_exception,        ONLY: message, message_text, em_info, em_param
   USE mo_submodel_tracdef, ONLY: ntrac, trlist
   USE mo_util_string,      ONLY: separator
   USE mo_species,          ONLY: nspec, speclist,                      &
                                  itrdiag,  itrnone, itrpresc, itrprog, &
                                  spec_ntrac, spec_idt
                                
   USE mo_submodel_tracdef_constants, ONLY: GAS, AEROSOL, GAS_OR_AEROSOL 

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: printspec

CONTAINS

 SUBROUTINE printspec
  !! print species definitions
  !! and fill spec_idt matrix

    INTEGER               :: jt, ispec, i
    CHARACTER(len=11)    :: cstring

    CALL message('', 'Species definitions:', level=em_info) 
    CALL message('','',level=em_param)
    WRITE(message_text,'(a)') 'Id shortname         phase       submodel         '//&
                              'mol.wght unit         itrtype idt longname'
    CALL message('', message_text, level=em_param)
    DO jt = 1, nspec

      SELECT CASE(speclist(jt)%nphase)
      CASE (GAS)
        cstring = 'gas        '
      CASE (AEROSOL)
        cstring = 'aerosol    '
      CASE (GAS_OR_AEROSOL)
        cstring = 'gas|aerosol'
      END SELECT
      
!SF Note: in the line below, all string fields are meant to be printed with their original declared length
!         (which should make sense!), except for the shortname property (otherwise these lines are
!         extremely long and unreadable). Therefore it may be that the shortname property is truncated in the
!         printed lines (> 18 characters...)
      WRITE(message_text,'(i0,t4,a,t22,2(a,1x),f7.3,3x,a,1x,i3,1x,i3,2x,a)') &
                               jt,TRIM(speclist(jt)%shortname), cstring, speclist(jt)%tsubmname, &
                               speclist(jt)%moleweight, speclist(jt)%units, speclist(jt)%itrtype, &
                               speclist(jt)%idt, speclist(jt)%longname

      CALL message('', message_text, level=em_param)

    END DO

    CALL message('','',level=em_param)
    WRITE(message_text,'(a)') 'Notes:'
    CALL message('', message_text, level=em_param)
    WRITE(message_text,'(a)') '1. itrtype meaning: '
    CALL message('', message_text, level=em_param)
    WRITE(message_text,'(a,i0,a)') ' --> ',itrnone,' = No ECHAM tracer, '
    CALL message('', message_text, level=em_param)
    WRITE(message_text,'(a,i0,a)') ' --> ',itrpresc,' = Prescribed tracer, read from file'
    CALL message('', message_text, level=em_param)
    WRITE(message_text,'(a,i0,a)') ' --> ',itrdiag,' = Diagnostic tracer, no transport'
    CALL message('', message_text, level=em_param)
    WRITE(message_text,'(a,i0,a)') ' --> ',itrprog,' = Prognostic tracer, transport by ECHAM'
    CALL message('', message_text, level=em_param)
    CALL message('','',level=em_param)
    WRITE(message_text,'(a)') '2. idt is the tracer index for gas-phase tracers only'
    CALL message('', message_text, level=em_param)

    !! fill spec_idt matrix
    DO jt = 1, ntrac
      ispec = trlist%ti(jt)%spid 
      IF (ispec > 0) THEN
        spec_ntrac(ispec) = spec_ntrac(ispec) + 1
        spec_idt(ispec, spec_ntrac(ispec)) = jt
      END IF
    END DO

    CALL message('','',level=em_param)
    CALL message('', 'Species-tracer matrix:', level=em_param)
    CALL message('', 'Species index : Tracer index/indices', level=em_param)
    !>>dod removed use of list-directed I/O to variable
    message_text=''
    DO jt = 1, nspec
       WRITE(message_text, '(i0,t15,a1)') jt,':'
       DO i=1,spec_ntrac(jt)
          WRITE(cstring,'(i0,1x)') spec_idt(jt,i)
          WRITE(message_text,'(a,1x,a)') TRIM(message_text), TRIM(cstring)
       END DO
      CALL message('', message_text, level=em_param)
    END DO
    !<<dod
    CALL message('','',level=em_param)
    CALL message('',separator)

  END SUBROUTINE printspec
END MODULE mo_subm_printspec

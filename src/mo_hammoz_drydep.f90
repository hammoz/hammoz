!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_hammoz_drydep.f90
!!
!! \brief
!! Module to interface ECHAM submodules with dry deposition module(s).
!!
!! \author M. Schultz (FZ Juelich)
!! \author Grazia Frontoso (C2SM)
!!
!! \responsible_coder
!! M. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Schultz (FZ Juelich) - original code (2009-10-26)
!!   -# M. Schultz (FZ Juelich) - improved diag routines (2010-04-16)
!!   -# Grazia Frontoso (C2SM)  - usage of the input variables defined over land, water, ice to account
!!                                for the non-linearity in the drydep calculations for gridboxes
!!                                containing both water and sea ice (2012-02-01) 
!!
!! \limitations
!! All diag_lists must be defined in order to avoid problems with
!! get_diag_pointer in the actual sedi_interface routine. Lists can be empty.
!!
!! \details
!! Currently there is only one unified interactive drydep scheme for
!! aerosols (HAM) and gas-phase species (MOZ).
!! This module initializes the scheme based on the namelist parameters
!! in submodeldiagctl and creates a stream for variable pointers and 
!! diagnostic quantities used in the dry deposition scheme. It also
!! provides a generic interface to the actual dry deposition routine(s).
!!
!! \bibliographic_references
!! None
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mo_hammoz_drydep

  USE mo_kind,             ONLY: dp
  USE mo_submodel_diag,    ONLY: t_diag_list
  USE mo_linked_list,      ONLY: t_var_list

  IMPLICIT NONE

  PRIVATE

  ! public variables  (see declaration below)

  ! subprograms
  PUBLIC                       :: init_drydep_stream, &
                                  drydep_interface, drydep_init
  ! drydep_stream
  INTEGER, PARAMETER           :: ndrydepvars=2
  CHARACTER(LEN=32)            :: drydepvars(1:ndrydepvars)= &
                                (/'ddep             ', &   ! total drydep flux
                                  'vddep            ' /)   ! deposition velocity


  TYPE t_drydep_field
    ! variable pointers and diagnostic lists
    !!  TYPE (t_diag_list), PUBLIC   :: ddepinst     ! inst. dry deposition flux     ### needed ??? ###
    TYPE (t_diag_list), PUBLIC   :: ddep         ! dry deposition flux
    TYPE (t_diag_list), PUBLIC   :: vddep        ! dry deposition velocity
  END TYPE t_drydep_field

  TYPE(t_drydep_field), ALLOCATABLE, TARGET :: drydep_field(:)  !< shape: (n_dom)
  TYPE(t_var_list), POINTER :: drydep_field_list(:)  !< shape: (n_dom)
  TYPE(t_var_list), POINTER :: drydep_field_list1

  INTEGER :: idt_ddep_detail

  ! 'boundary conditions' from JSBACH 
  INTEGER, SAVE :: ibc_ssf          = -1,  &  ! index of surface snow fraction 'boundary condition' 
                   ibc_ws_l1        = -1,  &  ! index of soil water content [m] 'boundary condition' 
                   ibc_ws_fc_l1     = -1,  &  ! index of field capacity [m] 'boundary condition'  
                   ibc_fract_forest = -1,  &  ! index of forest fraction 'boundary condition'  
                   ibc_wfract_srf   = -1,  &  ! index of surface wet fraction 'boundary condition'  
                   ibc_vgrat        = -1,  &  ! index of vegetation fraction boundary condition
                   ibc_q_snowmelt   = -1      ! index of heating by melting of snow  boundary condition


  CHARACTER(LEN=*), PARAMETER :: thismodule='mo_hammoz_drydep'

  CONTAINS

!>>gf #244
  SUBROUTINE drydep_init( p_patch )

    USE mo_exception,           ONLY: finish
    USE mo_hammoz_drydep_lg,    ONLY: drydep_lg_init
    USE mo_submodel_tracdef,    ONLY: ntrac, trlist
    USE mo_model_domain,        ONLY: t_patch

    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    INTEGER :: ndomain

    ndomain=SIZE( p_patch )
    IF ( ndomain /= 1 ) THEN
     CALL finish(thismodule, 'Nesting currently not supported')
    END IF  

    !--- initialize the interactive drydep scheme (ndrydep==2)
    IF ( ANY(trlist%ti(1:ntrac)%ndrydep == 2) ) THEN
      CALL drydep_lg_init( p_patch(1)%id )
    END IF

    CALL init_drydep_bc_from_jsbach( p_patch(1)%id )

  END SUBROUTINE drydep_init
!<<gf

  SUBROUTINE init_drydep_stream( p_patch )

    USE mo_string_utls,         ONLY: st1_in_st2_proof
    USE mo_util_string,         ONLY: tolower
    USE mo_exception,           ONLY: finish, message
    USE mo_ham_m7_trac,         ONLY: ham_get_class_flag
    USE mo_submodel_tracer,     ONLY: validate_traclist
    USE mo_submodel_tracdef,    ONLY: ln, ntrac, trlist, GAS, AEROSOL
    USE mo_species,             ONLY: nspec, speclist 
    USE mo_ham,                 ONLY: nclass
    USE mo_submodel_streams,    ONLY: drydep_lpost, drydep_tinterval, drydepnam,  &
                                      drydep_gastrac, drydep_keytype, drydep_ldetail, &    ! ++mgs 20140519
                                      drydep_trac_detail
    USE mo_submodel_diag,       ONLY: BYTRACER, BYSPECIES, BYNUMMODE, BYMODE !SF #299 added BYMODE
    USE mo_hammoz_drydep_lg,    ONLY: init_drydep_lg_stream
    USE mo_submodel,            ONLY: lham !SF, see #228
    USE mo_parallel_config,     ONLY: nproma
    USE mo_model_domain,        ONLY: t_patch
    USE mo_impl_constants,      ONLY: MAX_CHAR_LENGTH, SUCCESS

    ! argument(s)
    TYPE(t_patch),INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    INTEGER, PARAMETER             :: ndefault = 2
    CHARACTER(LEN=32)              :: defnam(1:ndefault)   = &   ! default diagnostics
                                (/ 'ddep             ', &        ! total drydep flux
                                   'vddep            ' /)        ! dry deposition velocity
    CHARACTER(len=ln)              :: defaultgas(5)        = &   ! default gas-phase tracers for diagnostics
                                (/ 'SO2     ',               &
                                   'H2SO4   ',               &
                                   'HNO3    ',               &
                                   'O3      ',               &
                                   'NO2     '           /)
    LOGICAL                        :: tracflag(ntrac), specflag(nspec), modflag(MAX(nclass,1))
    CHARACTER(LEN=ln)              :: tracname(ntrac), specname(nspec), modname(MAX(nclass,1)), &
                                      modnumname(MAX(nclass,1)) !SF #299
!!$    TYPE (t_stream), POINTER       :: sdrydep
    INTEGER                        :: ierr, jt, ist

    INTEGER                        :: jg, ndomain, nblks, nlev


    CHARACTER(LEN=MAX_CHAR_LENGTH)  :: listname

    CHARACTER(LEN=*), PARAMETER    :: thismodule='mo_hammoz_drydep'

    !++mgs: default values and namelist read are done in init_submodel_streams !

    !-- handle ALL, DETAIL and DEFAULT options for drydep output variables
    !-- Note: ALL and DETAIL are identical for drydep output
    IF (TRIM(tolower(drydepnam(1))) == 'detail')  drydepnam(1:ndrydepvars) = drydepvars(:)
    IF (TRIM(tolower(drydepnam(1))) == 'all')     drydepnam(1:ndrydepvars) = drydepvars(:)
    IF (TRIM(tolower(drydepnam(1))) == 'default') drydepnam(1:ndefault) = defnam(:)

    !-- check that all diagnostic names from namelist are valid
    IF (.NOT. st1_in_st2_proof( drydepnam, drydepvars, ierr=ierr) ) THEN
      IF (ierr > 0) CALL finish ( 'ini_drydep_stream', 'variable '// &
                                  drydepnam(ierr)//' does not exist in drydep stream' )
    END IF

    !-- find out which gas-phase tracers shall be included in diagnostics
    CALL validate_traclist(drydep_gastrac, defaultgas, nphase=GAS,              &
                           ldrydep=.true.)                   !>>dod SOA: removed ltran <<dod

    !-- define the flags and names for the diagnostic lists. We need one set of flags and
    !   names for each key_type (BYTRACER, BYSPECIES, BYMODE)
    !   gas-phase tracers will always be defined BYTRACER, for aerosol tracers one of the
    !   following lists will be empty.
    !   Note: vddep uses BYTRACER or BYMODE, ddep uses BYTRACER or BYSPECIES
!!++mgs 2015-02-10 : split loop so that gas-phase tracers are always output last
    tracflag(:) = .FALSE.
    DO jt = 1,ntrac
      tracname(jt) = trlist%ti(jt)%fullname
      IF (trlist%ti(jt)%nphase /= GAS) THEN
        IF (drydep_keytype == BYTRACER .AND. nclass > 0) THEN
          tracflag(jt) = trlist%ti(jt)%ndrydep > 0
        END IF
      END IF
    END DO
    specflag(:) = .FALSE.
    DO jt = 1,nspec
      specname(jt) = speclist(jt)%shortname
      IF (drydep_keytype == BYSPECIES .AND.                     &
          IAND(speclist(jt)%nphase, AEROSOL) /= 0 .AND.         &   !>>dod SOA removed check of trtype <<dod
          nclass > 0) THEN
        specflag(jt) = speclist(jt)%ldrydep
      END IF
    END DO
    DO jt = 1,ntrac
      tracname(jt) = trlist%ti(jt)%fullname
      IF (trlist%ti(jt)%nphase == GAS) THEN
        tracflag(jt) = st1_in_st2_proof(trlist%ti(jt)%fullname, drydep_gastrac)
      END IF
    END DO


!--mgs
    modflag(:) = .FALSE.
    modname(:) = ''
    !SF #228, adding a condition to check that HAM is active:
    !SF #299, adding a condition to check if BYMODE is relevant:
    IF (lham .AND. nclass > 0 .AND. (drydep_keytype == BYMODE)) THEN
       CALL ham_get_class_flag(nclass, modflag, modname, modnumname, ldrydep=.true.)
    END IF

    ! since nesting may be supported in the future, let's just handle this properly
    CALL message(TRIM(thismodule),'Construction of HAM drydep state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( drydep_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of drydep_field failed')

    ALLOCATE( drydep_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of drydep_field_list failed')

    DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      drydep_field_list1 => drydep_field_list(jg)

      WRITE(listname,'(a,i2.2)') 'drydep_state_diag_of_domain',jg
      CALL new_drydep_field_list( jg,        nproma,     nlev,        nblks,               &
                               &  nclass,    ntrac,      nspec,                            &
                               &  tracflag,  specflag,   modflag,                          &
                               &  tracname,  specname,   modname,     modnumname,          &
                               &  TRIM(listname),                                          &
                               &  drydep_field_list1, drydep_field(jg)                     )

    END DO

    CALL message(TRIM(thismodule),'Construction of HAM drydep state finished.')


    !++mgs 20140519 : detailed diagnostics
    IF (drydep_ldetail) CALL init_drydep_lg_stream(p_patch, drydep_field_list, drydep_trac_detail, idt_ddep_detail)

  END SUBROUTINE init_drydep_stream

  SUBROUTINE new_drydep_field_list( k_jg,       kproma,     klev,       kblks,        &
                                 &  kclass,     ktrac,      kspec,                    &
                                 &  tracflag,   specflag,   modflag,                  &
                                 &  tracname,   specname,   modname,     modnumname,  &                      
                                 &  listname,                                         &
                                 &  field_list, field                                 ) 

    USE mo_kind,                ONLY: dp
    USE mo_exception,           ONLY: message, finish

    USE mo_impl_constants,      ONLY: SUCCESS, MAX_CHAR_LENGTH,  & 
      &                               VINTP_METHOD_PRES,         &
      &                               VINTP_METHOD_LIN,          &
      &                               VINTP_METHOD_LIN_NLEVP1

    USE mo_linked_list,         ONLY: t_var_list

    USE mo_var_list,            ONLY: default_var_list_settings, &
      &                               add_var, add_ref,          &
      &                               new_var_list,              &
      &                               delete_var_list
    USE mo_var_metadata,        ONLY: create_vert_interp_metadata, vintp_types, &
                                      groups
    USE mo_cf_convention,       ONLY: t_cf_var
    USE mo_grib2,               ONLY: t_grib2_var, grib2_var
    USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
      &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
      &                               GRID_UNSTRUCTURED,                 &
      &                               TSTEP_INSTANT, TSTEP_AVG,          &
      &                               cdiDefMissval
    USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
      &                               ZA_HYBRID, ZA_HYBRID_HALF,         &
      &                               ZA_SURFACE
    USE mo_io_config,           ONLY: lnetcdf_flt64_output

    USE mo_fortran_tools,       ONLY: t_ptr_2d3d
   
    USE mo_submodel_diag,       ONLY: new_diag_list, new_diag,  &
                                      BYTRACER, BYSPECIES, BYNUMMODE, BYMODE !SF #299 added BYMODE
    USE mo_submodel_streams,    ONLY: drydep_lpost, drydep_tinterval, drydepnam,  &
                                      drydep_gastrac, drydep_keytype, drydep_ldetail, &    ! ++mgs 20140519
                                      drydep_trac_detail

    USE mo_submodel_tracdef,    ONLY: ln
    USE mo_string_utls,         ONLY: st1_in_st2_proof

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev,  kblks     !< dimension sizes
    INTEGER, INTENT(IN) :: kclass, ktrac, kspec

    LOGICAL, INTENT(IN)            :: tracflag(ktrac), specflag(kspec), modflag(MAX(kclass,1))
    CHARACTER(LEN=ln), INTENT(IN)  :: tracname(ktrac), specname(kspec), modname(MAX(kclass,1)), &
                                      modnumname(MAX(kclass,1)) !SF #299

    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),  POINTER,   INTENT(INOUT) :: field_list
    TYPE(t_drydep_field), INTENT(INOUT)         :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: jt

    LOGICAL :: lpost

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)

    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )

    CALL default_var_list_settings( field_list,  loutput= drydep_lpost, &
                                  & lrestart=.FALSE.                   )
    !-- instantaneous diagnostic quantities
    !-- drydep velocities
    !-- these are used in the calculation and must always be present in the stream 
    !-- drydep_gastrac only controls output
    ! cms check if these should be in restart files. As far as I can see in echam-hammoz they are not.
    lpost = st1_in_st2_proof( 'vddep', drydepnam) .AND. drydep_lpost
    CALL new_diag_list ( field%vddep, field_list, diagname='vddep', tsubmname='',    &
                       & longname='dry deposition velocity', units='m s-1',          &
                       & ndims=2, nmaxkey=(/ktrac, 0, kclass, kclass, 0 /),          &
                       & table=199, lpost=lpost                                      )
    CALL new_diag(field%vddep, ktrac, tracflag, tracname, BYTRACER, shape2d=shape2d )
    IF (ANY(modflag)) THEN !SF #299 added mode mass diags and fix for mode number name
      CALL new_diag( field%vddep, kclass, modflag, modname, BYMODE,  shape2d=shape2d )
      CALL new_diag( field%vddep, kclass, modflag, modnumname, BYNUMMODE,  shape2d=shape2d )
    END IF

    !-- averaged diagnostic quantities
    !-- total drydep flux
    lpost = st1_in_st2_proof( 'ddep', drydepnam) .AND. drydep_lpost
    CALL new_diag_list ( field%ddep, field_list, diagname='ddep', tsubmname='',      &
                       & longname='accumulated dry deposition flux',                 &
                       & units='kg m-2 s-1', ndims=2,                                &
                       & nmaxkey=(/ktrac, kspec, kclass, kclass, 0 /), lpost=lpost,  &
                       & table=199                                                   )
    ! add diagnostic elements only when output is activated
    IF (lpost) THEN
      CALL new_diag(field%ddep, ktrac, tracflag, tracname, BYTRACER,  shape2d=shape2d )
      CALL new_diag(field%ddep, kspec, specflag, specname, BYSPECIES,  shape2d=shape2d )
      IF (ANY(modflag)) THEN !SF #299 added mode mass diags and fix for mode number name
        CALL new_diag(field%ddep, kclass, modflag, modname, BYMODE,  shape2d=shape2d )
        CALL new_diag(field%ddep, kclass, modflag, modnumname, BYNUMMODE,  shape2d=shape2d )
      END IF
    END IF


  END SUBROUTINE new_drydep_field_list

  SUBROUTINE init_drydep_bc_from_jsbach( jg )

    USE mo_exception,                ONLY: message, finish, EM_WARN, EM_INFO
    USE mo_boundary_condition,       ONLY: bc_find, bc_nml, bc_define, BC_REPLACE, BC_BOTTOM
    USE mo_external_field_processor, ONLY: EF_FILE, EF_MODULE, EF_LINEAR,  EF_LONLAT, EF_IGNOREYEAR

    INTEGER, INTENT( IN ) :: jg ! domain/grid index

    ! local variables
    TYPE(bc_nml)        :: bc_ssf,            &   ! surface snow fraction 'boundary condition'
                           bc_ws_l1,          &   ! soil water content [m] 'boundary condition' 
                           bc_ws_fc_l1,       &   ! soil field capacity [m] 'boundary condition' 
                           bc_fract_forest,   &   ! forest fraction 'boundary condition' 
                           bc_wfract_srf,     &   ! surface wet fraction 'boundary condition' 
                           bc_vgrat,          &   ! vegetation fraction boundary condition
                           bc_q_snowmelt          ! heating by melting of snow 'boundary condition' 

    INTEGER :: ierr

    CALL bc_find(jg, 'surface snow fraction', ibc_ssf, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ssf%ef_type = EF_MODULE
      bc_ssf%bc_mode = BC_REPLACE
      bc_ssf%ef_actual_unit='m2 m-2'
      ibc_ssf = bc_define(jg, 'surface snow fraction', bc_ssf, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'ssf bc already defined - will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'soil water content in first layer', ibc_ws_l1, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ws_l1%ef_type = EF_MODULE
      bc_ws_l1%bc_mode = BC_REPLACE
      bc_ws_l1%ef_actual_unit = 'm'
      ibc_ws_l1 = bc_define(jg, 'soil water content in first layer', bc_ws_l1, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'ws_l1 soil bc already defined - will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'field capacity in first soil layer', ibc_ws_fc_l1, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_ws_fc_l1%ef_type = EF_MODULE
      bc_ws_fc_l1%bc_mode = BC_REPLACE
      bc_ws_fc_l1%ef_actual_unit = 'm'
      ibc_ws_fc_l1 = bc_define(jg, 'field capacity in first soil layer', bc_ws_fc_l1, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'soil field capacity ws_fc_l1 bc already defined -  will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'forest fraction', ibc_fract_forest, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_fract_forest%ef_type = EF_MODULE
      bc_fract_forest%bc_mode = BC_REPLACE
      bc_fract_forest%ef_actual_unit = ''
      ibc_fract_forest = bc_define(jg, 'forest fraction', bc_fract_forest, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'forest fraction bc already defined -  will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'surface wet fraction', ibc_wfract_srf, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_wfract_srf%ef_type = EF_MODULE
      bc_wfract_srf%bc_mode = BC_REPLACE
      bc_wfract_srf%ef_actual_unit = ''
      ibc_wfract_srf = bc_define(jg, 'surface wet fraction', bc_wfract_srf, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'surface wet fraction bc already defined -  will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'vegetation ratio', ibc_vgrat, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_vgrat%ef_varname = 'VGRATCLIM'
      bc_vgrat%bc_domain = BC_BOTTOM
      bc_vgrat%bc_mode = BC_REPLACE
      bc_vgrat%ef_type = EF_FILE
      bc_vgrat%ef_template = 'VGRATCLIM.nc'
      bc_vgrat%ef_geometry = EF_LONLAT
      bc_vgrat%ef_timedef = EF_IGNOREYEAR
      bc_vgrat%ef_factor = 1._dp
      bc_vgrat%ef_interpolate =  EF_LINEAR 
      bc_vgrat%ef_actual_unit = 'fractional'  
      ibc_vgrat    = bc_define(jg, 'vegetation ratio',  bc_vgrat, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'vegetation ratio already defined -  will use previous definition', level=EM_INFO )
    END IF

    CALL bc_find(jg, 'heating by snow melting', ibc_q_snowmelt, ierr=ierr)
    IF (ierr /= 0) THEN
      bc_q_snowmelt%ef_type = EF_MODULE
      bc_q_snowmelt%bc_mode = BC_REPLACE
      bc_q_snowmelt%ef_actual_unit = 'W m-2'
      ibc_q_snowmelt  = bc_define(jg, 'heating by snow melting',  bc_q_snowmelt, 2, .TRUE.)
    ELSE
      CALL message(thismodule,'heating by snow melting -  will use previous definition', level=EM_INFO )
    END IF

  END SUBROUTINE init_drydep_bc_from_jsbach 
  !! ---------------------------------------------------------------------------------------
  !! drydep_interface: generic interface routine to dry deposition

  SUBROUTINE drydep_interface( jg, p_patch, kproma, kbdim, ktrac, klev, klevp1, krow,       &
                             &  ksfc_type, idx_wtr, idx_ice, idx_lnd,                       &
                             &  current_date,    pdtime,         pkap,                      &
                             &  plland,          ptsm1,                                     &
                             &  psfcWind,                                                   &
                             &  pdens,           pthetav_b,                                 & 
                             &  pzf,             pzh,            pdz,                       &  
                             &  pum1,            pvm1,           ptm1,                      & 
                             &  pqm1,            pqsat_b,                                   & 
                             &  pfrc,                                                       &
                             &  ptsfc_tile,      pri_tile,       pz0m_tile,                 &
                             &  pcdn_tile,       pcfnc_tile,     pcfm_tile,                 &       
                             &  prsns_tile,      pthetavsurf_tile,                          &
                             &  pxtm1,                                                      &
                             &  pxt_emis                                                    )   

    USE mo_exception,            ONLY: finish, message
    USE mtime,                   ONLY: datetime
    USE mo_model_domain,         ONLY: t_patch
    USE mo_submodel_tracdef,     ONLY: trlist 
    USE mo_submodel,             ONLY: lham 

    USE mo_subm_get_from_jsbach, ONLY: get_snow_cover_bc_from_jsbach,          &
                                     & get_soil_moisture_from_jsbach,          &
                                     & get_soil_field_capacity_from_jsbach,    & 
                                     & get_forest_fraction_from_jsbach,        &
                                     & get_surface_wet_fraction_from_jsbach,   &
                                     & get_wsn_critical_from_jsbach,           &
                                     & get_heating_by_melting_snow_from_jsbach 


    USE mo_boundary_condition,   ONLY: bc_apply
    USE mo_ham_drydep,           ONLY: ham_vdaer, ham_vd_presc
    USE mo_hammoz_drydep_lg,     ONLY: drydep_lg_calcra, drydep_lg_vdbl
    USE mo_submodel_diag,        ONLY: get_diag_pointer
    USE mo_physical_constants,   ONLY: grav

    IMPLICIT NONE

    INTEGER,  INTENT( IN ) ::                      &
                        & jg,                      &  !< domain id
                        & kproma,                  &  !< geographic block number of locations
                        & kbdim,                   &  !< geographic block maximum number of locations
                        & ktrac,                   &  !< number of tracers
                        & klev,                    &  !< number of levels
                        & klevp1,                  &  !< number of levels + 1
                        & krow,                    &  !< geographic block number
                        & ksfc_type,               &  !< number of surface types 
                        & idx_wtr,                 &  !< index for water
                        & idx_ice,                 &  !< index for ice  
                        & idx_lnd                     !< index for land
 
    REAL(dp), INTENT ( IN ) :: pdtime  ,& !< time step
                               pkap       !< von Karman constant

    TYPE(datetime), INTENT( IN ), POINTER    :: current_date
    TYPE(t_patch),  INTENT( IN )             :: p_patch 

    LOGICAL,  INTENT( IN )  :: plland(kbdim)              !< land mask (logical)
                             
                              

    REAL(dp), INTENT( IN )  ::                                   &
                        & ptsm1         ( kbdim )               ,&!< surface temperature
                        & psfcWind      ( kbdim )               ,&!< 10m windspeed
                        & pdens         ( kbdim )               ,&!< air density in the lowest model layer
                        & pthetav_b     ( kbdim )               ,&!< virtual potential temperature bottom layer
                        & pzf           ( kbdim,klev  )         ,&!< geometric height above sea level, full level   
                        & pzh           ( kbdim, klevp1 )       ,&!< geometric height above sea level, half level
                        & pdz           ( kbdim, klev )         ,&!< 
                        & ptm1          ( kbdim, klev )         ,&!< temperature at step t-dt                 
                        & pum1          ( kbdim, klev )         ,&!< u-wind at step t-dt
                        & pvm1          ( kbdim, klev )         ,&!< v-wind at step t-dt
                        & pqm1          ( kbdim, klev )         ,&!< specific humidity at step t-dt
                        & pqsat_b       ( kbdim )               ,&!< specific humidity at saturation bottom layer   (at step t-dt)
                        & pfrc          ( kbdim, ksfc_type)     ,&!< area fraction of each surface type   (land/water/ice)
                        & ptsfc_tile    ( kbdim,ksfc_type )     ,&!< surface temperature
                        & pri_tile      ( kbdim, ksfc_type )    ,&!< Richardson number
                        & pz0m_tile     ( kbdim, ksfc_type )    ,&!< roughness length             
                        & pcdn_tile     ( kbdim, ksfc_type )    ,&!< neutral drag coefficient
                        & pcfnc_tile     ( kbdim, ksfc_type )   ,&!< cdn*sqrt  (du2) 
                        & pcfm_tile      ( kbdim, ksfc_type )   ,&!< stability function times cfnc     
                        & prsns_tile     ( kbdim, ksfc_type )   ,&!< shortwave net flux at surface on tiles 
                        & pthetavsurf_tile ( kbdim, ksfc_type ) ,&!< surface virtual potential temperature    
                        & pxtm1          ( kbdim, klev, ktrac )   !< tracer mass mixing ratio (or number)


    REAL(dp), INTENT( INOUT ) :: pxt_emis(kbdim, ktrac)      !< tracer tendency due to surface emission
                                                             !< and dry deposition. "zxtems" in ECHAM6
    ! local variables

    REAL(dp)           :: zghf   ( kbdim, klev )      !< geopotential height above ground, full level

    REAL(dp)           :: zcvs          ( kbdim )    !< snow cover fraction from JSBACH
    REAL(dp)           :: zws           ( kbdim )    !< soil moisture (m) from JSBACH   
    REAL(dp)           :: zwsmx         ( kbdim )    !< max. soil moisture (m) from JSBACH
    REAL(dp)           :: zfract_forest ( kbdim )    !< forest fraction from JSBACH
    REAL(dp)           :: zwfract_srft  ( kbdim )    !< surface wet fraction from JSBACH

    REAL(dp)           :: zvgrat         ( kbdim )   !< vegetation ratio (from file)

    REAL(dp) :: zvd(kbdim,ktrac), zvdstom(kbdim,ktrac)      ! dry deposition velocity and stomatal ..
    REAL(dp) :: zdrydepflux(kbdim)                          ! dry deposition mass flux
    REAL(dp) :: zsncri                                      ! m water equivalent critical snow depth

    REAL(dp):: zrahwat(kbdim),   zrahice(kbdim),   zrahveg(kbdim),   zrahslsn(kbdim),   & ! aerodynamic resistances
               zustveg(kbdim),   zustslsn(kbdim),                                       & ! friction velocities ustar
               zustarl(kbdim),   zustarw(kbdim),   zustari(kbdim),                      &
               zrh(kbdim),       zcvbs(kbdim),                                          &  ! relative humidity, bare soil fraction 
               zq_snowmelt(kbdim)                                                          ! heating by melting of snow

    REAL(dp)           :: zvgratl         ( kbdim )   
    REAL(dp), PARAMETER  :: zephum=5.e-2_dp

    REAL(dp)  :: pepdu2

    INTEGER :: jl, jt, ierr

    !--- Pointer to diagnostic field
    REAL(dp), POINTER :: fld2d(:,:) 

    !--- 0) Initialisations: -------------------------------------------------------------

    zvd(1:kproma,:)       = 0._dp
    zvdstom(1:kproma,:)   = 0._dp
    zdrydepflux(1:kproma) = 0._dp

    pepdu2=1._dp ! 'security parameter' (should better be taken from mo_turbulence_diag.f90) 

    ! geopotential height above ground
    zghf (:,1:klev)        = pzf(:,1:klev)  -SPREAD(pzh(:,klevp1),2,klev  )

    !--- 1) get boundary conditions from jsbach

    !cms++ temporary? 
    CALL get_snow_cover_bc_from_jsbach           ( jg, ibc_ssf,          kproma, krow )
    CALL get_soil_moisture_from_jsbach           ( jg, ibc_ws_l1 ,       kproma, krow )
    CALL get_soil_field_capacity_from_jsbach     ( jg, ibc_ws_fc_l1,     kproma, krow )
    CALL get_forest_fraction_from_jsbach         ( jg, ibc_fract_forest, kproma, krow )
    CALL get_surface_wet_fraction_from_jsbach    ( jg, ibc_wfract_srf,   kproma, krow )
    CALL get_heating_by_melting_snow_from_jsbach ( jg, ibc_q_snowmelt,   kproma, krow )
    !cms --

    CALL bc_apply(jg, ibc_ssf,          kproma,  p_patch%nblks_c, krow, current_date, zcvs )
    CALL bc_apply(jg, ibc_ws_l1,        kproma,  p_patch%nblks_c, krow, current_date, zws )
    CALL bc_apply(jg, ibc_ws_fc_l1,     kproma,  p_patch%nblks_c, krow, current_date, zwsmx )
    CALL bc_apply(jg, ibc_fract_forest, kproma,  p_patch%nblks_c, krow, current_date, zfract_forest )
    CALL bc_apply(jg, ibc_wfract_srf,   kproma,  p_patch%nblks_c, krow, current_date, zwfract_srft )
    CALL bc_apply(jg, ibc_vgrat,        kproma,  p_patch%nblks_c, krow, current_date, zvgrat)
    CALL bc_apply(jg, ibc_q_snowmelt,   kproma,  p_patch%nblks_c, krow, current_date, zq_snowmelt)

    !--- 2) Calculate relative humidity and other parameters needed below
    zrh(1:kproma)=MIN(1._dp,MAX(zephum,pqm1(1:kproma,klev)/pqsat_b(1:kproma)))

    DO jl=1,kproma
      !--- Calculate bare soil fraction:
      !    It is calculated as residual term over land.
      IF (plland(jl)) THEN
        zvgratl(jl)=zvgrat(jl)                                           ! vegetation fraction
        zcvbs(jl)=(1._dp-zcvs(jl))*(1._dp-zvgrat(jl))*(1._dp-zwfract_srft(jl))  ! bare soil fraction
      ELSE
        zvgratl(jl)=0._dp
        zcvbs(jl)=0._dp
      ENDIF
    ENDDO


    !--- 3) Calculate dry deposition velocities ------------------------------------------
    ! This is done sequentially for 
    !    2.1: prescribed velocities
    !    2.2: gas-phase and aerosol tracers using the Ganzeveld scheme
    ! Each element of trlist has a ndrydep flag which determines the scheme to be used.
    ! Ndrydep==0 means no dry deposition for this tracer.
    ! Each of the routines called in 1.1, 1.2 contain their own tracer loop and
    ! modify zdv only for the tracers they are responsible for.

    IF ( ANY(trlist%ti(:)%ndrydep>2) ) THEN
      CALL finish('drydep_interface', 'Dry deposition with ndrydep > 2 not implemented')
    END IF

    !--- 3.1) Prescibed dry deposition velocities:


    IF( ANY(trlist%ti(:)%ndrydep==1) ) THEN
         zsncri=get_wsn_critical_from_jsbach()

      CALL ham_vd_presc( kproma,        kbdim,          klev,    krow,    plland,         & 
                       & pdtime,        pdz,                                              & 
                       & zcvs,         zfract_forest,                                     & 
                       & pfrc(:,idx_ice),     ptsfc_tile(:, idx_ice),                     & 
                       & zwfract_srft,  ptsm1,         zws,      zwsmx,   pdens,          & 
                       & zq_snowmelt,   zsncri,                                           & !in
                       & zvd                                                              ) !out

    END IF

  !--- 3.2) Explicitly calculated dry deposition velocities (Ganzeveld scheme):

  IF ( ANY(trlist%ti(:)%ndrydep==2) ) THEN
    !--- Calculate the aerodynamic resistance:
    CALL drydep_lg_calcra ( jg,  p_patch, kproma,   kbdim,    klev,    krow,               &
                           ksfc_type, idx_wtr, idx_ice, idx_lnd,                           & 
                           current_date,                                                   &
                           pfrc,                                                           &
                           pepdu2, pkap, pum1, pvm1, zghf,                                 &
                           pri_tile,   pz0m_tile,                                          &
                           pcdn_tile,  pcfnc_tile, pcfm_tile,                              &
                           pthetavsurf_tile,                                               &
                           pthetav_b,                                                      & !in
                           zrahwat,  zrahice, zrahveg,  zrahslsn,                          & 
                           zustarl, zustarw, zustari, zustveg, zustslsn                    ) !out



    !--- Calculate the dry deposition velocity for gas-phase species:
    CALL drydep_lg_vdbl (jg, p_patch,                                           &
                         kproma,   kbdim,     klev,    krow,  plland,           &
                         current_date,                                          &
                         prsns_tile(:,idx_lnd),                                 &
                         ptsm1,     pum1,    pvm1,    zrh,                      &
                         pfrc(:,idx_lnd), pfrc(:,idx_wtr),                      &
                         pfrc(:,idx_ice),  zcvbs,   zcvs,                       &
                         zwfract_srft,  zvgratl,    zrahwat, zrahice, zrahveg,  &
                         zrahslsn, zws,       zwsmx,                            &
                         zustarw,  zustari,   zustveg, zustslsn,                &
                         zvd,      zvdstom,   idt_ddep_detail                   )


    !--- Calculate the dry deposition velocity for aerosols:
    !### argument ordering should be similar to drydep_lg_vdbl
    IF (lham) THEN
       CALL ham_vdaer  (jg, kproma,   kbdim,            klev,              krow,     plland,          zvgratl,  &
                        zcvs,     zwfract_srft,     pfrc(:,idx_ice),   zcvbs,    pfrc(:,idx_wtr),     pum1,     &  
                        pvm1,     zustarl,          zustarw,           zustari,  zustveg,             zustslsn, &
                        psfcWind,                                                                               &
                        pz0m_tile(:,idx_wtr),       pz0m_tile(:,idx_ice),                                       &
                        zrahwat,  zrahice,          zrahveg,           zrahslsn,                                &
                        ptsm1,    zrh,              pxtm1,             zvd                                      )
    END IF
  END IF


  ! initilaize diagnostics to zero (icon accumulation)
  DO jt=1, ktrac  
    CALL get_diag_pointer(drydep_field(jg)%ddep, fld2d, jt, ierr=ierr)
    IF (ierr == 0) fld2d(1:kproma,krow)=0._dp
  END DO

  !--- prevent overflow and store dry deposition velocities for diagnostics !cms why?
  DO jt=1, ktrac
    DO jl=1, kproma
      ! Security check, limit the deposition velocity according to 2x CFL
     zvd(jl,jt) = MIN(zvd(jl,jt) , 2._dp*pdz(jl,klev)/pdtime)  
    END DO


    CALL get_diag_pointer(drydep_field(jg)%vddep, fld2d, jt, ierr=ierr)
    IF (ierr == 0) fld2d(1:kproma,krow) = zvd(1:kproma, jt)
!### also store zvdstom (only ozone ??)


  !--- 3) Change emissions tendencies due to dry deposition: --------------
  !--- and store dry depositon mass flux in diagnostics

    IF (trlist%ti(jt)%ndrydep > 0) THEN
      !--- Calculate the tracer flux to the surface that is 
      !    equivalent to the deposition velocity:
      zdrydepflux(1:kproma)=pxtm1(1:kproma,klev,jt)*pdens(1:kproma)*zvd(1:kproma,jt)
      !--- Reduce emission flux:
      pxt_emis(1:kproma,jt)=pxt_emis(1:kproma,jt)-zdrydepflux(1:kproma)
    ELSE ! trlist%ti(jt)%ndrydep <= 0
      zdrydepflux(1:kproma)=0._dp
    END IF
    ! get diagnostics pointer
    CALL get_diag_pointer(drydep_field(jg)%ddep, fld2d, jt, ierr=ierr)
    IF (ierr == 0) fld2d(1:kproma,krow)=fld2d(1:kproma,krow)+zdrydepflux(1:kproma)
!### add stomatal deposition flux ... ###
    ! special diagnostics for MOZ
    !! CALL moz_drydep_diag()  ....    ### to be done in mo_moz_diag
  END DO


  END SUBROUTINE drydep_interface

END MODULE mo_hammoz_drydep

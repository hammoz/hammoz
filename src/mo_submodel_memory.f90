!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_submodel_memory

  USE mo_submodel_tracdef,     ONLY: trlist, ntrac

  USE mo_linked_list,          ONLY: t_var_list
  USE mo_advection_config,     ONLY: t_advection_config
  USE mo_fortran_tools,        ONLY: t_ptr_2d3d

  USE mo_add_tracer_ref,       ONLY: add_tracer_ref

  USE mo_cf_convention,        ONLY: t_cf_var
  USE mo_cdi_constants,        ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL,              &
    &                                ZA_HYBRID
  USE mo_cdi,                  ONLY: DATATYPE_FLT32, DATATYPE_FLT64,                 &
                                     DATATYPE_PACK16, GRID_UNSTRUCTURED
  USE mo_io_config,            ONLY: lnetcdf_flt64_output
  USE mo_grib2,                ONLY: t_grib2_var, grib2_var
  USE mo_var_metadata,         ONLY: groups
  USE mo_tracer_metadata,      ONLY: create_tracer_metadata
  USE mo_util_string,          ONLY: tolower

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: add_submodel_tracers

CONTAINS 
SUBROUTINE add_submodel_tracers( this_list, vname_prefix, advconf, &
                             & tracer_ptr, ldims, tlev_source, tlev_suffix )

  TYPE(t_var_list), INTENT(INOUT)                          :: this_list    
  CHARACTER(len=*), INTENT(IN)                             :: vname_prefix
  TYPE(t_advection_config), INTENT(INOUT)                  :: advconf
  TYPE(t_ptr_2d3d), DIMENSION(:), INTENT(INOUT)            :: tracer_ptr
  INTEGER, DIMENSION(3),  INTENT(IN)                       :: ldims
  INTEGER,  INTENT(IN), OPTIONAL                           :: tlev_source  
  CHARACTER(len=4), INTENT(IN), OPTIONAL                   :: tlev_suffix

! local
  INTEGER :: datatype_flt
  
  CHARACTER(len=128) :: standard_name 
  CHARACTER(len=128) :: long_name 
  CHARACTER(len=128) :: units  
  CHARACTER(len=256) :: short_name    

  TYPE(t_cf_var)    :: cf_desc
  TYPE(t_grib2_var) :: grib2_desc

  INTEGER :: ibits, i, dummy_idx 

! save dummy (dummy_)idx in trinfo ?!

  IF ( lnetcdf_flt64_output ) THEN
    datatype_flt = DATATYPE_FLT64
  ELSE
    datatype_flt = DATATYPE_FLT32
  ENDIF

  ibits = DATATYPE_PACK16 !size of var in bits

  DO i=1, ntrac
    standard_name = trlist%ti(i)% fullname 
    long_name = trlist%ti(i)% fullname 
    short_name = trlist%ti(i)% fullname 

    units  = trlist%ti(i)% units
    cf_desc    = t_cf_var(TRIM(standard_name), TRIM(units),  &
      &          TRIM(long_name), datatype_flt, TRIM(short_name)) 
    grib2_desc = grib2_var(255,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
   
    IF ( PRESENT (tlev_source ) .AND. PRESENT (tlev_suffix ) ) THEN
          CALL add_tracer_ref( this_list, tolower(vname_prefix)//'tracer',           &
            &                  vname_prefix//tolower(TRIM(long_name))//tlev_suffix,  &
            &                  dummy_idx,                                            &
            &                  tracer_ptr(:),                                        &
            &                  cf_desc, grib2_desc,                                  &
            &                  advconf,                                              &
            &                  ldims=ldims,                                          &
            &                  tracer_info=create_tracer_metadata(lis_tracer=.TRUE., &
            &                              name =  vname_prefix//tolower(TRIM(long_name))//tlev_suffix ),  & 
            &                  tlev_source=tlev_source                               )




    ELSE

          CALL add_tracer_ref( this_list, vname_prefix//'tracer',                    &
            &                  tolower(vname_prefix)//TRIM(long_name),               &
            &                  dummy_idx,                                            &
            &                  tracer_ptr(:),                                        &
            &                  cf_desc, grib2_desc,                                  &
            &                  advconf,                                              &
            &                  ldims=ldims,                                          &
            &                  lrestart=.TRUE.,                                      &
            &                  tracer_info=create_tracer_metadata(lis_tracer=.TRUE., &
            &                              name = TRIM(long_name) )                  ) 

    END IF

    ! ICON tracer index (dummy_idx is an intent(out) in add_tracer_ref) 
    !cms see if this will be needed (better do without) trlist%ti(i)%itrc = dummy_idx

  END DO


END SUBROUTINE add_submodel_tracers
END MODULE mo_submodel_memory

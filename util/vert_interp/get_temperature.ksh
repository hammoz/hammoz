#!/bin/ksh

ipath=/projekt2/climate/MACC

year_start=2003
year_end=2009

yy=$year_start

while [ $yy -le $year_end ]; do
 mm=1
 unset ofiles_m
 ofile_y=TempY_${yy}.nc
 set -A ofiles_y ${ofiles_y[*]} $ofile_y 
 
 while [ $mm -le 12 ]; do
  rm -f temp[1-3].nc 
  typeset -Z2 mm
  date=${yy}${mm}

  ifile=$ipath/ml128.${date}.nc
  if [ ! -e $ifile ]; then
   echo ERROR: $ifile NOT FOUND
   exit 5
  fi

  ofile_m=tempX${date}.nc

  set -A ofiles_m ${ofiles_m[*]} $ofile_m 
  cdo timmean -selvar,t,q,lnsp,z $ifile temp1.nc
  cdo sp2gp temp1.nc temp2.nc 
  cdo remapbil,t63grid temp2.nc temp3.nc
  cdo remapeta,vct.txt temp3.nc $ofile_m
  echo $ifile
  (( mm = $mm +1 ))
  done
  echo AAA ${ofiles_m[*]}
  cdo cat ${ofiles_m[*]} $ofile_y
(( yy = $yy + 1 ))
done
rm -f tempX??????.nc
cdo cat ${ofiles_y[*]} temp_all.nc
cdo ymonmean temp_all.nc TmaccAvg_${year_start}-${year_end}.nc

exit

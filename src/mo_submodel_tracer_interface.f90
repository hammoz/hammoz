!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_submodel_tracer_interface

  USE mo_exception,                   ONLY: message,finish
  USE mo_kind,                        ONLY: wp

  USE mo_linked_list,                 ONLY: t_var_list
  USE mo_advection_config,            ONLY: t_advection_config
  USE mo_fortran_tools,               ONLY: t_ptr_2d3d

  USE mo_submodel_memory,             ONLY: add_submodel_tracers

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: submodel_tracer_interface
  ! for now just supply this wrapper for when the interface is called from icoham
  !  (this will probably become obsolete)
  PUBLIC :: submodel_tracer_interface_hs

  ! a subroutine for adding submodel tracer names to the echam phy tracers in mo_echam_phy_memory 
  PUBLIC :: add_submodel_tracer_names
CONTAINS
SUBROUTINE submodel_tracer_interface( this_list, vname_prefix, advconf, tracer_ptr,           &
                                      & ldims, tlev_source, tlev_suffix )

  TYPE(t_var_list), INTENT(INOUT)                          :: this_list    
  CHARACTER(len=*), INTENT(IN)                             :: vname_prefix
  TYPE(t_advection_config), INTENT(INOUT)                  :: advconf
  TYPE(t_ptr_2d3d), DIMENSION(:), INTENT(INOUT)            :: tracer_ptr
  INTEGER, DIMENSION(3),  INTENT(IN)                       :: ldims
  INTEGER, INTENT(IN), OPTIONAL                            :: tlev_source 
  CHARACTER(len=4), INTENT(IN), OPTIONAL                   :: tlev_suffix
  

  IF ( PRESENT( tlev_source ) .AND. PRESENT( tlev_suffix )) THEN
    CALL add_submodel_tracers( this_list, vname_prefix, advconf, &
                             & tracer_ptr, ldims, tlev_source=tlev_source, tlev_suffix=tlev_suffix  )
  ELSE
    CALL add_submodel_tracers( this_list, vname_prefix, advconf, &
                             & tracer_ptr, ldims  )
  END IF

END SUBROUTINE submodel_tracer_interface

SUBROUTINE submodel_tracer_interface_hs(this_list, vname_prefix, ktracer, advconf, &
                                      & field_hs, ldims )

  USE mo_icoham_dyn_types, ONLY: t_hydro_atm_prog

  TYPE(t_var_list), INTENT(INOUT)                  :: this_list    
  CHARACTER(len=*), INTENT(IN)                     :: vname_prefix
  INTEGER, INTENT(IN)                              :: ktracer
  TYPE(t_advection_config), INTENT(INOUT)          :: advconf
  TYPE(t_hydro_atm_prog),INTENT(INOUT)             :: field_hs
  INTEGER, DIMENSION(3),  INTENT(IN)               :: ldims
 

  !local 
  TYPE(t_ptr_2d3d), DIMENSION(ktracer) :: ptr_2d3d
  INTEGER :: i

  DO i=1,ktracer
    ptr_2d3d(i)%p_3d => field_hs%tracer_ptr(i)%p
  END DO

  CALL submodel_tracer_interface(this_list, vname_prefix, advconf, ptr_2d3d, ldims ) 
    
END SUBROUTINE submodel_tracer_interface_hs

SUBROUTINE add_submodel_tracer_names( ktracer, ctracer )
! called from mo_echam_phy_memory 
  USE mo_impl_constants,        ONLY: MAX_CHAR_LENGTH
  USE mo_submodel_tracdef,      ONLY: trlist

  INTEGER, INTENT(IN )            :: ktracer
  CHARACTER(len=MAX_CHAR_LENGTH)  :: ctracer(ktracer) 

! local
  INTEGER :: i_start, i, ii

  i_start = ktracer-trlist%ntrac+1
  ii = 1
  DO i = i_start, ktracer
     ctracer(i) = trlist % ti(ii) % fullname
     ii = ii + 1
  END DO

END SUBROUTINE add_submodel_tracer_names
END MODULE mo_submodel_tracer_interface

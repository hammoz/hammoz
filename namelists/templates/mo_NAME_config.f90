!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
###(disclaimer)###

MODULE mo_###(nml_name)###_config

  USE mo_kind,         ONLY: dp
  USE mo_hammoz_config,       ONLY: nmaxclass
  USE mo_config_util,         ONLY: set_to_missing

  ###(nml_use_modules)###

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: t_###(nml_name)###
  PUBLIC :: ###(nml_name)###
  PUBLIC :: init_defaults_###(nml_name)###

  ###(public_nml_config)###

  TYPE t_###(nml_name)###
    ###(nml_variable_declaration)### 
  END TYPE t_###(nml_name)###

  TYPE (t_###(nml_name)###), TARGET :: ###(nml_name)###
  

CONTAINS

   SUBROUTINE init_defaults_###(nml_name)###
      ###(nml_init_defaults)###
   END SUBROUTINE init_defaults_###(nml_name)###

   ###(nml_get_conf)###

   ###(nml_set_config)###

END MODULE mo_###(nml_name)###_config 

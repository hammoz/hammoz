!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_hammoz_emi_fire.f90
!!
!! \brief
!! Module for handling of fire emissions
!!
!! \author M. Schultz (FZ Juelich)
!!
!! \responsible_coder
!! M. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Schultz (FZ Juelich) - original code (2010-02)
!!   -# G. Frontoso (C2SM-ETHZ) - Fire emissions injection accounts for PBL height (2012-07)
!!                                (M. Val Martin, 2010), see #161 on redmine
!!   -# A. Veira (MPI-M) - Modification of vertical emission distribution:
!!                         Constant mass mixing ratio throughout PBL instead of simple
!!                         pro-rata distribution to all PBL model layers (see #310)
!! \limitations
!! None
!!
!! \details
!! - vertical distribution of fire emissions
!! - planned extension: derive emission factors based on MCE and read only
!!   CO and CO2 data from file.
!!
!! \bibliographic_references
!! None
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mo_hammoz_emi_fire

  USE mo_kind,       ONLY: dp

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: distribute_emi_fire

  CONTAINS

  SUBROUTINE distribute_emi_fire(kproma, kbdim, klev, klevp1, krow, ihpbl, pzh, pmconv,  zbc2d, zbc3d)

  INTEGER, INTENT(in)     :: kproma
  INTEGER, INTENT(in)     :: kbdim
  INTEGER, INTENT(in)     :: klev
  INTEGER, INTENT(in)     :: klevp1
  INTEGER, INTENT(in)     :: krow
  INTEGER, INTENT(in)     :: ihpbl(kbdim)             ! PBL height index

  REAL(dp), INTENT(in)    :: pzh(kbdim, klevp1)       ! geometric height above sea level, half level

  REAL(dp), INTENT(in)    :: pmconv(kbdim,klev)       ! either air mass or dry air mass [kg/m2] 

  REAL(dp), INTENT(in)    :: zbc2d(kbdim)
  REAL(dp), INTENT(inout) :: zbc3d(kbdim, klev)

! Local variables

  INTEGER   :: jl,jk,ilower,iupper
  INTEGER   :: lev_blw_pbl
  REAL(dp)  :: zfac_k,zbc2d_abv1(kbdim),zbc2d_abv2(kbdim),zbc2d_blw(kbdim)
  REAL(dp)  :: zdepth, p_pblh
  REAL(dp)  :: m_pbl

  ilower = klev       ! lower level


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Amount of fire emissions below and above top of PBL

  zbc2d_abv1(1:kproma)=0.17_dp*zbc2d(1:kproma)
  zbc2d_abv2(1:kproma)=0.08_dp*zbc2d(1:kproma)
  zbc2d_blw(1:kproma)=0.75_dp*zbc2d(1:kproma)


! Vertical profile

  DO jl=1,kproma
     iupper=ihpbl(jl)   ! upper level - top of the PBL
     zdepth=pzh(jl, iupper) 

! Check if the depth of the PBL is higher than 4km
! Emit only within the PBL

      IF (zdepth .GT. 4000._dp) THEN
         zbc2d_abv1(jl)=0._dp
         zbc2d_abv2(jl)=0._dp
         zbc2d_blw(jl)=zbc2d(jl)
      ENDIF

     lev_blw_pbl=ilower-ihpbl(jl)+1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Distribute emission in PBL with constant mass mixing ratio (!AV #310)

     m_pbl=SUM(pmconv(jl,iupper:ilower)) ! air mass in PBL [kg/m^2]
     zfac_k=0._dp

     DO jk=ilower, iupper, -1
         zfac_k = pmconv(jl,jk)/m_pbl 
         zbc3d(jl,jk) = zfac_k * zbc2d_blw(jl)
     END DO


! Distribute emissions in the FT
     zbc3d(jl,iupper-1)     = zbc2d_abv1(jl)
     zbc3d(jl,iupper-2)     = zbc2d_abv2(jl)
  ENDDO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Old way of fire injections

!  zbc3d(1:kproma,klev)   = 0.33 * zbc2d(1:kproma)
!  zbc3d(1:kproma,klev-1) = 0.26 * zbc2d(1:kproma)
!  zbc3d(1:kproma,klev-2) = 0.19 * zbc2d(1:kproma)
!  zbc3d(1:kproma,klev-3) = 0.15 * zbc2d(1:kproma)
!  zbc3d(1:kproma,klev-4) = 0.07 * zbc2d(1:kproma)

  ! note: zbc2d will be reset to zero in calling routine.

  END SUBROUTINE distribute_emi_fire

END MODULE mo_hammoz_emi_fire

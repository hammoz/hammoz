!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_hammoz_emi_ocean.f90
!!
!! \brief
!! Module for interactive ocean emisisons (DMS, NH3)
!!
!! \author M. Schultz (FZ Juelich)
!!
!! \responsible_coder
!! M. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Schultz (FZ Juelich) - original code (2010-03-01)
!!
!! \limitations
!! None
!!
!! \details
!! Based on code by Silvia Kloster and Philip Stier
!!
!! \bibliographic_references
!! None
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mo_hammoz_emi_ocean

  USE mo_kind,             ONLY: dp

  IMPLICIT NONE

  PRIVATE

  ! public variables  (see declaration below)

  ! subprograms
  PUBLIC            :: init_emi_ocean
  PUBLIC            :: init_emi_ocean_stream
  PUBLIC            :: oceani_emissions

  ! boundary condition index for DMS ocean concentrations
  INTEGER           :: ibc_dms_sea

  ! boundary condition indices for emission mass fluxes
  INTEGER           :: ibc_emi_dms, ibc_emi_nh3   ! ibc_emi_co2

  ! diagnostic pointers
  REAL(dp), POINTER :: vp_dms(:,:), vp_nh3(:,:), vp_co2(:,:)


  CONTAINS

  !! ---------------------------------------------------------------------------
  !! initialize interactive ocean emissions for DMS, NH3 (and CO2 ?)
  !! init_emi_ocean is only called when an OCEANI sector is defined in the emi_matrix
  !! Note: should actually check if ef_type is defined as EF_MODULE to allow for file alternative!

  SUBROUTINE init_emi_ocean( jg )

  USE mo_exception,            ONLY: message, message_text, em_info
  USE mo_boundary_condition,   ONLY: bc_find, bc_define, bc_nml, BC_REPLACE, BC_BOTTOM
  USE mo_external_field_processor, ONLY: EF_FILE, EF_LONLAT, EF_IGNOREYEAR

  IMPLICIT NONE

  INTEGER, INTENT ( IN )       :: jg ! grid/domain index

  CHARACTER(LEN=64)            :: bc_name
  TYPE(bc_nml)                 :: bc_struc
  INTEGER                      :: ierr

  ! -- Initialisation
  ibc_emi_dms = -1
  ibc_emi_nh3 = -1
  ibc_dms_sea = -1

  !-- locate boundary condition for emission mass flux of DMS and NH3
  !   bc_define was called from init_emissions in mo_emi_interface
  bc_name = 'OCEANI emissions of DMS'
  CALL bc_find(jg, bc_name, ibc_emi_dms, ierr)

  bc_name = 'OCEANI emissions of NH3'
  CALL bc_find(jg, bc_name, ibc_emi_nh3, ierr)

!!  bc_name = 'OCEANI emissions of CO2'
!!  CALL bc_find(bc_name, ibc_emi_co2)

  !-- define boundary condition for ocean DMS concentrations
  IF (ibc_emi_dms > 0) THEN
    bc_struc%ef_type      = EF_FILE
    bc_struc%ef_template  = 'conc_aerocom_DMS_sea.nc'
    bc_struc%ef_varname   = 'DMS_sea'
    bc_struc%ef_geometry  = EF_LONLAT
    bc_struc%ef_timedef   = EF_IGNOREYEAR
    bc_struc%bc_mode      = BC_REPLACE
    bc_struc%bc_domain    = BC_BOTTOM
    ibc_dms_sea = bc_define(jg, 'DMS seawater concentrations', bc_struc, 2, .TRUE.)
  END IF

  END SUBROUTINE init_emi_ocean


  !! ---------------------------------------------------------------------------
  !! subroutine to define additional diagnostics for the emi diagnostic stream
  SUBROUTINE init_emi_ocean_stream(nsectors, emi_field_list, shape2d, ldiagdetail)

    ! add field to emis stream (init_emi_stream in mo_emi_interface must have been called!)
    ! in principle one could use ldiagdetail to turn off detailed diagnostics for this sector ...

    USE mo_linked_list,         ONLY: t_var_list
    USE mo_var_list,            ONLY: add_var
    USE mo_cf_convention,       ONLY: t_cf_var
    USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
       &                              ZA_SURFACE
    USE mo_grib2,               ONLY: t_grib2_var, grib2_var
    USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
      &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
      &                               GRID_UNSTRUCTURED, TSTEP_INSTANT
    USE mo_io_config,           ONLY: lnetcdf_flt64_output
    USE mo_var_metadata,        ONLY: groups

    INTEGER,          INTENT( IN )               :: nsectors
    TYPE(t_var_list), INTENT( INOUT ), POINTER   :: emi_field_list
    INTEGER,          INTENT( IN )               :: shape2d(2)
    LOGICAL,          INTENT( IN )               :: ldiagdetail(nsectors)

    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
       datatype_flt = DATATYPE_FLT64
    ELSE
       datatype_flt = DATATYPE_FLT32
    END IF

    ! -- Diagnostics for DMS (and NH3) emissions (piston velocity)
    IF (ibc_emi_dms > 0) THEN
      cf_desc    = t_cf_var( 'vp_DMS', 'm s-1', 'piston velocity for DMS emissions', datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( emi_field_list, 'vp_dms', vp_dms,                                         &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                  & lrestart=.FALSE.                                                         )
    END IF

    IF (ibc_emi_nh3 > 0) THEN
      cf_desc    = t_cf_var( 'vp_NH3', 'm s-1', 'piston velocity for NH3 emissions', datatype_flt)
      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_var( emi_field_list, 'vp_nh3', vp_nh3,                                         &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
                  & lrestart=.FALSE.                                                          )
    END IF


!!    IF (ibc_emi_co2 > 0) THEN
!!      cf_desc    = t_cf_var( 'vp_CO2', 'm s-1', 'piston velocity for CO2 emissions', datatype_flt)
!!      grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
!!      CALL add_var( emi_field_list, 'vp_co2', vp_co2,                                         &
!!                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,   &
!!                  & lrestart=.FALSE.                                                          )
!!    END IF


  END SUBROUTINE init_emi_ocean_stream

  !! ---------------------------------------------------------------------------
  !! subroutine to handle interactive ocean emissions
  !! call piston velocity and call dms_emisisons routine and store result in boundary condition
  SUBROUTINE oceani_emissions( jg, kproma, kbdim, kblks, krow, npist, &
                             & current_date,                          &
                             & plsmask, pseaice, psfcWind, ptsw       )

  USE mo_boundary_condition,   ONLY: bc_set
  USE mtime,                   ONLY: datetime
  ! arguments

  INTEGER,  INTENT( IN )  :: jg                     ,& !< domain/grid index
                           & kproma                 ,& !< geographic block number of locations
                           & kbdim                  ,& !< geographic block number of locations
                           & kblks                  ,& !< geographic block maximum number of locations
                           & krow                   ,& !< geographic block number
                           & npist                     !< choice of method for computing piston velocity

  REAL(dp), INTENT( IN )  :: plsmask(kbdim)         ,& !< land-sea mask (1. = land, 0. = sea/lakes)     
                           & pseaice(kbdim)         ,& !< fraction of ocean covered by sea ice
                           & psfcWind(kbdim)        ,& !< 10m wind speed
                           & ptsw(kbdim)               !< water temperature

 TYPE(datetime), INTENT( IN ), POINTER  :: current_date ! the current date

  ! local variables
  REAL(dp)          :: zmassf1(kbdim)

  ! compute piston velocity
  CALL piston_velocity( kproma, kbdim, krow, npist, &
                      & plsmask, psfcWind, ptsw )
 
  ! calculate DMS emissions
  IF (ibc_emi_dms > 0) THEN
    CALL dms_emissions(jg, kproma, kbdim, kblks, krow, current_date, plsmask, pseaice, zmassf1) 
    CALL bc_set(jg, ibc_emi_dms, kproma, krow, zmassf1(1:kproma))
  END IF

  END SUBROUTINE oceani_emissions

  !! ---------------------------------------------------------------------------
  ! DMS (NH3 and CO2) emissions: first compute piston velocity, then emission mass flux

  SUBROUTINE piston_velocity( kproma, kbdim, krow, npist,  &
                            & plsmask, psfcWind, ptsw )

  ! *piston_velocity* calculation of the piston velocity
  !                   for the tracers DMS, CO2 and NH3
  !
  ! Authors:
  ! --------
  ! S. Kloster MPI-MET 28/10/02
  !

  ! USE mo_memory_g3b,         ONLY: slm
  ! USE mo_vphysc,             ONLY: vphysc
  USE mo_physical_constants, ONLY: tmelt

  IMPLICIT NONE

  INTEGER,  INTENT( IN )  :: kproma                 ,& !< geographic block number of locations
                           & kbdim                  ,& !< geographic block number of locations
                           & krow                   ,& !< geographic block number
                           & npist                     !< choice of method for computing piston velocity
 
  REAL(dp), INTENT( IN )  :: plsmask(kbdim)         ,& !< land-sea mask (1. = land, 0. = sea/lakes)     
                           & psfcWind(kbdim)        ,& !< 10m wind speed
                           & ptsw(kbdim)                !< water temperature


  !--- Local Variables:
  INTEGER                :: jl
  REAL(dp)               :: zsst, zzspeed
  REAL(dp)               :: zschmidt_dms, zschmidt_co2, zschmidt_nh3
  REAL(dp)               :: zkw
  REAL(dp)               :: zvp_dms(kbdim), zvp_nh3(kbdim)
  LOGICAL                :: loocean(kbdim)

  loocean(1:kproma)  = ( plsmask(1:kproma) < 1.e-2_dp )
  zvp_dms(:) = 0._dp
  zvp_nh3(:) = 0._dp

  DO jl=1,kproma

    !--- 1.) Schmidt number

    zsst=ptsw( jl )-tmelt

    !--- 1.1.)  DMS Schmidt number after Andreae

    !--- Limit SST to valid range as the polynomial gets negative
    !    for T > 35 C:

    zsst=MIN( 35.0_dp , zsst )

    zschmidt_dms=3652.047271_dp-246.99_dp*zsst+8.536397_dp*zsst*zsst     &
                 -0.124397_dp*zsst*zsst*zsst

    !--- 1.2.) CO2 Schmidt number after Jaehne et al. (1987b)
!      zschmidt_co2=2073.1     -125.62*zsst+3.6276  *zsst*zsst     &
!                   -0.043219*zsst*zsst*zsst

    !--- 1.2.) NH3 Schmidt number
    zschmidt_nh3=2073.1     -125.62*zsst+3.6276  *zsst*zsst     &
                 -0.043219*zsst*zsst*zsst

    !--- 2.)  piston velocity in [cm/hr]

    IF (loocean(jl)) THEN   ! ocean
      zzspeed=psfcWind( jl )

      IF (npist == 1) THEN ! Calculate piston velocity (Liss&Merlivat, 1986)
        IF (zzspeed.GT.3.6_dp.AND.zzspeed.LE.13._dp) THEN
          zkw=2.85_dp*zzspeed-9.65_dp
          zvp_dms(jl)=zkw*(zschmidt_dms/600._dp)**(-0.5_dp)
!         zvp_co2(jl)=zkw*(zschmidt_co2/600.)**(-0.5)
          zvp_nh3(jl)=zkw*(zschmidt_nh3/600.)**(-0.5)
        ELSE IF(zzspeed.LE.3.6_dp) THEN
          zkw=0.17_dp*zzspeed
          zvp_dms(jl)=zkw*(zschmidt_dms/600._dp)**(-2._dp/3._dp)
!         zvp_co2(jl)=zkw*(zschmidt_co2/600.)**(-2./3.)
          zvp_nh3(jl)=zkw*(zschmidt_nh3/600.)**(-2./3.)
        ELSE
          zkw=5.9_dp*zzspeed-49.3_dp
          zvp_dms(jl)=zkw*(zschmidt_dms/600._dp)**(-0.5_dp)
!         zvp_co2(jl)=zkw*(zschmidt_co2/600.)**(-0.5)
          zvp_nh3(jl)=zkw*(zschmidt_nh3/600.)**(-0.5)
        END IF

      ELSE IF (npist == 2) THEN ! Calculate piston velocity (Wanninkhof, 1992)
        zkw=0.31_dp*zzspeed*zzspeed
        zvp_dms(jl)=zkw*(zschmidt_dms/660._dp)**(-0.5_dp)
!       zvp_co2(jl)=zkw*(zschmidt_co2/660.)**(-0.5)
        zvp_nh3(jl)=zkw*(zschmidt_nh3/660.)**(-0.5)

      ELSE IF (npist == 3) THEN  ! Calculate piston velocity (Nightingale, 2000)
        zkw=(0.222_dp*zzspeed*zzspeed+0.333_dp*zzspeed)
        zvp_dms(jl)=zkw*(zschmidt_dms/660._dp)**(-0.5_dp)
!       zvp_co2(jl)=zkw*(zschmidt_co2/660.)**(-0.5)
        zvp_nh3(jl)=zkw*(zschmidt_nh3/660.)**(-0.5)
      END IF
    ELSE                    ! land
      zkw=0._dp
    END IF
  END DO

  !-- store piston velocity as diagnostics
  IF (ibc_emi_dms > 0)  vp_dms(1:kproma, krow) = zvp_dms(1:kproma)
  IF (ibc_emi_nh3 > 0)  vp_nh3(1:kproma, krow) = zvp_nh3(1:kproma)

  END SUBROUTINE piston_velocity


  SUBROUTINE dms_emissions(jg, kproma, kbdim, kblks, krow, current_date, plsmask, pseaice, pmassf)

  ! *dms_emissions* calculated DMS emission fluxes from the
  !                 precalculated piston velocities
  !
  !    Authors:
  !    --------
  !    Johann Feichter, MPI-Met
  !    Philip Stier,    MPI-Met        06/2002
  !    Silvia Kloster,  MPI-Met        10/2002

  USE mo_boundary_condition, ONLY: bc_apply
  USE mo_ham,                ONLY: mw_dms
  USE mtime,                 ONLY: datetime

  IMPLICIT NONE

  INTEGER, INTENT( IN )    :: jg                   ,&   !< domain/grid index
                           &  kproma               ,&   !< geographic block number of locations
                           &  kbdim                ,&   !< geographic block number of locations
                           &  kblks                ,&   !< geographic block maximum number of locations
                           &  krow                      !< geographic block number

  REAL(dp), INTENT( IN )  :: plsmask( kbdim )      ,&   !< land-sea mask (1. = land, 0. = sea/lakes)     
                           & pseaice( kbdim )           !< fraction of ocean covered by sea ice

  TYPE(datetime), INTENT( IN ), POINTER    :: current_date

  REAL(dp), INTENT( OUT ) :: pmassf(kbdim)               !< emission mass flux

  !--- Local Variables:

  REAL(dp)               :: zdmscon(kbdim)
  REAL(dp)               :: zseaice(kbdim)
  LOGICAL                :: loocean(kbdim)            ! ocean mask

  !--- Initialisation
  loocean(1:kproma)  = ( plsmask(1:kproma ) < 1.e-2_dp )
  zseaice(1:kproma)  = pseaice(1:kproma )
  pmassf(:) = 0._dp

  !--- Get seawater DMS concentration from boundary condition
  CALL bc_apply(jg, ibc_dms_sea, kproma, kblks, krow, current_date, zdmscon)

  !--- Mask out land points !SF #458 (replacement of WHERE statements)
  zdmscon(1:kproma) = MERGE(zdmscon(1:kproma), 0._dp, loocean(1:kproma))

  !--- Calculate DMS emissions
  !    conversion of piston velocity from cm/hr to m/s and nmol/L to mmr
  pmassf(1:kproma) = (1._dp-zseaice(1:kproma))                    &
                     * zdmscon(1:kproma) * vp_dms(1:kproma,krow)  &
                     * mw_dms*1.e-11_dp/3600._dp
!!mgs(S)!!                     * 32.064e-11_dp/3600._dp

  END SUBROUTINE dms_emissions

END MODULE mo_hammoz_emi_ocean

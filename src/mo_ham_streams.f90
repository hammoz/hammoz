!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename 
!! mo_ham_streams.f90
!!
!! \brief
!! Generalized definition of diagnostic output for the aerosol model. Only one
!! diagnostic stream (ham) left. Other output in wetdep, drydep or sedi streams
!!
!! \author Martin G. Schultz (FZ Juelich)
!!
!! \responsible_coder
!! Martin G. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# Martin G. Schultz (FZ Juelich) - original code (2009-09-30)
!!   -# T. Bergman (FMI) - nmod->nclass to facilitate new aerosol models (2013-02-05)
!!
!! \limitations
!! None
!!
!! \details
!! This code is based on mo_aero_mem by P. Stier, D. O'Donnell and others. Generalized 
!! by Martin Schultz.
!! *NOTE* All d_emi and emi stuff removed and re-implemented in mo_hammoz_emissions.f90
!!
!! \bibliographic_references
!! None 
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

MODULE mo_ham_streams

  USE mo_kind,                ONLY: dp
  USE mo_exception,           ONLY: message, finish

  USE mo_impl_constants,      ONLY: SUCCESS, MAX_CHAR_LENGTH,  & 
    &                               VINTP_METHOD_PRES,         &
    &                               VINTP_METHOD_LIN,          &
    &                               VINTP_METHOD_LIN_NLEVP1

  USE mo_linked_list,         ONLY: t_var_list

  USE mo_var_list,            ONLY: default_var_list_settings, &
    &                               add_var, add_ref,          &
    &                               new_var_list,              &
    &                               delete_var_list
  USE mo_var_metadata,        ONLY: create_vert_interp_metadata, vintp_types, &
                                    groups
  USE mo_cf_convention,       ONLY: t_cf_var
  USE mo_grib2,               ONLY: t_grib2_var, grib2_var
  USE mo_cdi,                 ONLY: DATATYPE_PACK16, DATATYPE_PACK24,  &
    &                               DATATYPE_FLT32,  DATATYPE_FLT64,   &
    &                               GRID_UNSTRUCTURED,                 &
    &                               TSTEP_INSTANT, TSTEP_AVG,          &
    &                               TSTEP_ACCUM, cdiDefMissval
  USE mo_cdi_constants,       ONLY: GRID_UNSTRUCTURED_CELL, GRID_CELL, &
    &                               ZA_HYBRID, ZA_HYBRID_HALF,         &
    &                               ZA_SURFACE
  USE mo_parallel_config,     ONLY: nproma
  USE mo_io_config,           ONLY: lnetcdf_flt64_output

  USE mo_fortran_tools,       ONLY: t_ptr_2d3d
  USE mo_ham_rad_data,        ONLY: Nwv_tot
  USE mo_ham,                 ONLY: nmaxclass, sizeclass
  USE mo_ham_ccnctl,          ONLY: nsat

  IMPLICIT NONE

  PRIVATE

  !--- Service routines

  PUBLIC :: new_stream_ham       ! construct the ham streams
  PUBLIC :: new_stream_ham_rad   ! construct the ham_rad stream

  !>
  !! Derived data type: t_ham_field
  !!
  !! This structure contains HAM variables
  !!
  !! All components are arrays of one of the following shapes:
  !! <ol>
  !! <li> (nproma,           nblks_phy)
  !! <li> (nproma, nlev_phy, nblks_phy)
  !! <li> (nproma, nlev_phy, nblks_phy, ntracers)
  !! </ol>
  
TYPE t_ham_field
  !-- pointers for diagnostic quantities
  TYPE (t_ptr_2d3d), ALLOCATABLE :: rdry(:)          ! used in cloud_cdnc_icnc
  TYPE (t_ptr_2d3d), ALLOCATABLE :: rwet(:)          ! used in cuasc and mo_drydep
  TYPE (t_ptr_2d3d), ALLOCATABLE :: densaer(:)       ! used in mo_drydep

  REAL(dp), POINTER :: rdry4d(:,:,:,:)
  REAL(dp), POINTER :: rwet4d(:,:,:,:)
  REAL(dp), POINTER :: densaer4d(:,:,:,:)


  !-- some more ...
  REAL(dp), POINTER :: relhum(:,:,:)             ! ### replace by ECHAM variable ??
  REAL(dp), POINTER :: ipr(:,:,:)
  REAL(dp), POINTER :: daylength(:,:)

  !-- chemical production and loss terms
  REAL(dp), POINTER :: d_prod_ms4as(:,:)
  REAL(dp), POINTER :: d_prod_ms4cs(:,:)
   
  REAL(dp), POINTER :: d_prod_so2_dms_oh(:,:)
  REAL(dp), POINTER :: d_prod_so4_dms_oh(:,:)
  REAL(dp), POINTER :: d_prod_so2_dms_no3(:,:)
  REAL(dp), POINTER :: d_prod_so4_so2_oh(:,:)

  REAL(dp), POINTER :: d_prod_h2so4(:,:,:)
  REAL(dp), POINTER :: d_prod_so4_liq(:,:,:)

  REAL(dp), POINTER :: d_cond_so4(:,:)

  REAL(dp), POINTER :: d_nuc_so4(:,:)

!>>SF
  !-- diagnostics for cloud activation
  REAL(dp), POINTER :: nbcsol_diag(:,:,:)
  REAL(dp), POINTER :: nbcinsol_diag(:,:,:)
  REAL(dp), POINTER :: nbcsol_acc(:,:,:)
  REAL(dp), POINTER :: nbcsol_ait(:,:,:)
  REAL(dp), POINTER :: nduinsol_diag(:,:,:)
  REAL(dp), POINTER :: nbcsol_strat(:,:,:)
  REAL(dp), POINTER :: nbcsol_cv(:,:,:)
  REAL(dp), POINTER :: ndusol_strat(:,:,:)
  REAL(dp), POINTER :: ndusol_cv(:,:,:)
  REAL(dp), POINTER :: nbcinsol(:,:,:)
  REAL(dp), POINTER :: nduinsolai(:,:,:)
  REAL(dp), POINTER :: nduinsolci(:,:,:)
  REAL(dp), POINTER :: naerinsol(:,:,:)

  TYPE (t_ptr_2d3d), ALLOCATABLE :: nact_strat(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: nact_conv(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: frac(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: a(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: b(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: sc(:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: rc_strat(:,:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: rc_conv(:,:)

  REAL(dp), POINTER :: nact_strat4d(:,:,:,:)
  REAL(dp), POINTER :: nact_conv4d(:,:,:,:)
  REAL(dp), POINTER :: frac4d(:,:,:,:)
  REAL(dp), POINTER :: a4d(:,:,:,:)
  REAL(dp), POINTER :: b4d(:,:,:,:)
  REAL(dp), POINTER :: sc4d(:,:,:,:)
  REAL(dp), POINTER :: rc_strat4d(:,:,:,:)
  REAL(dp), POINTER :: rc_conv4d(:,:,:,:)

!<<SF

  !-- mo_ham_ccn diagnostics
  TYPE (t_ptr_2d3d) :: ccn_2d(nsat)
  TYPE (t_ptr_2d3d) :: ccn_burden(nsat)
  TYPE (t_ptr_2d3d) :: ccn_3d(nsat)

  REAL(dp), POINTER :: ccn_2d_3d(:,:,:)      ! for container  
  REAL(dp), POINTER :: ccn_burden_3d(:,:,:)  ! for container  
  REAL(dp), POINTER :: ccn_3d_4d(:,:,:,:)    ! for container  


  TYPE (t_ptr_2d3d) :: cn_2d
  TYPE (t_ptr_2d3d) :: cn_burden
  TYPE (t_ptr_2d3d) :: cn_3d

END TYPE t_ham_field

TYPE t_ham_rad_field
  !-- ham radiation diagnostics (former mo_aero_rad_mem)
  TYPE (t_ptr_2d3d) :: tau_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: abs_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: sigma_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: omega_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: asym_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: nr_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: ni_mode(nmaxclass,Nwv_tot)

  REAL(dp), POINTER :: tau_mode4d(:,:,:,:) ! for container
  REAL(dp), POINTER :: abs_mode4d(:,:,:,:)
  REAL(dp), POINTER :: sigma_mode4d(:,:,:,:)
  REAL(dp), POINTER :: omega_mode4d(:,:,:,:)
  REAL(dp), POINTER :: asym_mode4d(:,:,:,:)
  REAL(dp), POINTER :: nr_mode4d(:,:,:,:)
  REAL(dp), POINTER :: ni_mode4d(:,:,:,:)

  TYPE (t_ptr_2d3d) :: sigma_2d_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: omega_2d_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: asym_2d_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: nr_2d_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: ni_2d_mode(nmaxclass,Nwv_tot)
  TYPE (t_ptr_2d3d) :: tau_2d(Nwv_tot)
  TYPE (t_ptr_2d3d) :: abs_2d(Nwv_tot)
  
  REAL(dp), POINTER :: sigma_2d_mode3d(:,:,:)  ! for container
  REAL(dp), POINTER :: omega_2d_mode3d(:,:,:)
  REAL(dp), POINTER :: asym_2d_mode3d(:,:,:)
  REAL(dp), POINTER :: nr_2d_mode3d(:,:,:)
  REAL(dp), POINTER :: ni_2d_mode3d(:,:,:)
  REAL(dp), POINTER :: tau_2d3d(:,:,:)
  REAL(dp), POINTER :: abs_2d3d(:,:,:)
 
  TYPE (t_ptr_2d3d), ALLOCATABLE :: tau_comp(:,:)
  TYPE (t_ptr_2d3d), ALLOCATABLE :: abs_comp(:,:)

  REAL(dp), POINTER ::  tau_comp3d(:,:,:) ! for container
  REAL(dp), POINTER ::  abs_comp3d(:,:,:) ! for container 

  TYPE (t_ptr_2d3d) :: ang

 
END TYPE t_ham_rad_field



     CHARACTER(len=*), PARAMETER :: thismodule = 'mo_ham_streams'

!----------------------------------------------------------------------------------------------------------------
  !!--------------------------------------------------------------------------
  !!                          STATE VARIABLES 
  !!--------------------------------------------------------------------------
  !! The variable names have the prefix "ham_" in order to emphasize that they
  !! are defined for and used in HAM.

  TYPE(t_ham_field),     ALLOCATABLE, TARGET :: ham_field(:)      !< shape: (n_dom)
  TYPE(t_ham_rad_field), ALLOCATABLE, TARGET :: ham_rad_field(:)  !< shape: (n_dom)

  !!--------------------------------------------------------------------------
  !!                          VARIABLE LISTS
  !!--------------------------------------------------------------------------
  TYPE(t_var_list), ALLOCATABLE :: ham_field_list(:)      !< shape: (n_dom)
  TYPE(t_var_list), ALLOCATABLE :: ham_rad_field_list(:)  !< shape: (n_dom) 

  PUBLIC :: ham_field
  PUBLIC :: ham_rad_field

  CONTAINS

!
!! brief: define diagnostics for the HAM stream
!

  SUBROUTINE new_stream_ham (p_patch, nclass)

    USE mo_model_domain,            ONLY: t_patch  

!!$    USE mo_filename,      ONLY: trac_filetype

    USE mo_activ,         ONLY: nw


    IMPLICIT NONE

    !-- parameters
    INTEGER,          INTENT(IN)      :: nclass         ! number of aerosol modes (or bins)
    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    CHARACTER(len=MAX_CHAR_LENGTH) :: listname
    INTEGER :: ndomain, jg, ist, nblks, nlev

    CALL message(TRIM(thismodule),'Construction of HAM state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( ham_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of ham_field failed')

    ALLOCATE( ham_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of ham_field_list failed')

    DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      WRITE(listname,'(a,i2.2)') 'ham_state_diag_of_domain',jg
      CALL new_ham_field_list( jg, nproma, nlev, nblks, nclass, nw,                   &
                               &  TRIM(listname), ham_field_list(jg), ham_field(jg)  )

    END DO

    CALL message(TRIM(thismodule),'Construction of HAM state finished.')


  END SUBROUTINE new_stream_ham
  !--------------------------------------------------------------------


  !--------------------------------------------------------------------
  !>
  SUBROUTINE new_ham_field_list( k_jg, kproma, klev, kblks, kclass,  kw, &
                                & listname, field_list, field    ) 

    
    USE mo_set_subm_phys, ONLY: ncd_activ, nactivpdf
    USE mo_ham,           ONLY: sizeclass, nccndiag
    USE mo_ham_ccnctl,    ONLY: zsat
    USE mo_util_string,   ONLY: tolower

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev, kblks     !< dimension sizes
    INTEGER, INTENT(IN) :: kclass
    INTEGER, INTENT(IN) :: kw  ! number of w-PDF bins

    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),  INTENT(INOUT) :: field_list
    TYPE(t_ham_field), INTENT(INOUT) :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: jt, jclass, jw, jsat, nclass_act, n_cw
    CHARACTER(len=10) :: cbin, csat

    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)

    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )

    CALL default_var_list_settings( field_list,                &
                                  & lrestart=.TRUE.  )

     !--- 4.1) Auxiliary fields for dust emissions:
    cf_desc    = t_cf_var('daylength', '1', 'relative daylength', datatype_flt)
    grib2_desc = grib2_var(255, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'daylength', field%daylength,       &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d )


    !--- 4.3) Hydrological parameters:
    CALL default_var_list_settings( field_list,                &
                                  & loutput=.TRUE.  )

    cf_desc    = t_cf_var('relhum', '%', 'ambient relative humidity', datatype_flt)
    grib2_desc = grib2_var(0, 1, 1, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'relhum', field%relhum,                                    &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE., l_pd_limit=.TRUE.,                     &
                &             lower_limit=0._dp ) )


    !--- 4.4) Aerosol Properties:
   CALL default_var_list_settings( field_list,                &
                                   & loutput=.TRUE.  )

    CALL add_var( field_list, 'rdry', field%rdry4d,                             &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('dry number median radius', 'm',                    &
                 &          'dry number median radius hammoz',                  &
                 &          datatype_flt),                                      &
                 & grib2_var(0,20,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
                 & ldims = (/kproma,klev,kblks,kclass/),                        &
                 & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         )

    CALL add_var( field_list, 'rwet', field%rwet4d,                             &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('wet number median radius', 'm',                    &
                 &          'wet number median radius (hammoz)',                &
                 &          datatype_flt),                                      &
                 & grib2_var(0,20,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
                 & ldims = (/kproma,klev,kblks,kclass/),                        &
                 & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         )     

    CALL add_var( field_list, 'densaer', field%densaer4d,                       &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('wet density', 'kg m-3',                            &
                 &          'wet density (hammoz)',                             &
                 &          datatype_flt),                                      &
                 & grib2_var(0,20,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
                 & ldims = (/kproma,klev,kblks,kclass/),                        &
                 & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         )     


    ALLOCATE( field%rdry(kclass) )
    ALLOCATE( field%rwet(kclass) )
    ALLOCATE( field%densaer(kclass) )

    DO jt = 1, kclass
      cf_desc    = t_cf_var('rdry_'//TRIM(sizeclass(jt)%shortname), 'm',  &
                      & 'dry number median radius - '//TRIM(sizeclass(jt)%shortname), datatype_flt)
      grib2_desc = grib2_var(0, 20, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)


      CALL add_ref( field_list, 'rdry',                                           &
                  & 'rdry_'//TRIM(sizeclass(jt)%shortname), field%rdry(jt)%p_3d,  &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                  & ldims=shape3d,                                                &
                  & vert_interp=create_vert_interp_metadata(                      &
                  &   vert_intp_type=vintp_types("P","Z","I"),                    &
                  &   vert_intp_method=VINTP_METHOD_LIN,                          &
                  &   l_extrapol=.FALSE.)                                         )

      cf_desc    = t_cf_var('rwet_'//TRIM(sizeclass(jt)%shortname), 'm',  &
                      & 'wet number median radius - '//TRIM(sizeclass(jt)%shortname), datatype_flt)
      grib2_desc = grib2_var(0, 20, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)

      CALL add_ref( field_list, 'rwet',                                           &
                  & 'rwet_'//TRIM(sizeclass(jt)%shortname), field%rwet(jt)%p_3d,  &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                  & ldims=shape3d,                                                &
                  & vert_interp=create_vert_interp_metadata(                      &
                  &   vert_intp_type=vintp_types("P","Z","I"),                    &
                  &   vert_intp_method=VINTP_METHOD_LIN,                          &
                  &   l_extrapol=.FALSE.)                                         )


      cf_desc    = t_cf_var('densaer_'//TRIM(sizeclass(jt)%shortname), 'm',  &
                      & 'wet density - '//TRIM(sizeclass(jt)%shortname), datatype_flt)
      grib2_desc = grib2_var(0, 20, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
      CALL add_ref( field_list, 'densaer',                                           &
                  & 'densaer_'//TRIM(sizeclass(jt)%shortname), field%densaer(jt)%p_3d,  &
                  & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,          &
                  & ldims=shape3d,                                                   &
                  & vert_interp=create_vert_interp_metadata(                         &
                  &   vert_intp_type=vintp_types("P","Z","I"),                       &
                  &   vert_intp_method=VINTP_METHOD_LIN,                             &
                  &   l_extrapol=.FALSE.)                                            )


    END DO

    !--- Ionisation rate (non accumulated)
    cf_desc    = t_cf_var('ipr', 'm-3 s-1', 'ion pair production rate', datatype_flt)
    grib2_desc = grib2_var(255, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'ipr', field%ipr,                                          &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.                                       ) )


    !--- Accumulated diagnostics elements
    !--- 2.2) Mass diagnostics:
    cf_desc    = t_cf_var( 'D_PROD_MS4AS', 'kg(SO4) m-2 s-1', 'sulfate production liquid phase acc', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_ms4as', field%d_prod_ms4as,                                 &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_PROD_MS4CS', 'kg(SO4) m-2 s-1', 'sulfate production liquid phase coarse', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_ms4cs', field%d_prod_ms4cs,                                 &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_PROD_SO2_DMS_OH', 'kg(SO2) m-2 s-1', 'sulfur production gas phase via DMS+OH', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_so2_dms_oh', field%d_prod_so2_dms_oh,                       &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_PROD_SO4_DMS_OH', 'kg(SO4) m-2 s-1', 'sulfate production gas phase via DMS+OH', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_so4_dms_oh', field%d_prod_so4_dms_oh,                       &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )

    cf_desc    = t_cf_var( 'D_PROD_SO2_DMS_NO3', 'kg(SO2) m-2 s-1', 'sulfur production gas phase via DMS+NO3', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_so2_dms_no3', field%d_prod_so2_dms_no3,                     &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_PROD_SO4_SO2_OH', 'kg(SO4) m-2 s-1', 'sulfate production gas phase via SO2+OH', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_so4_so2_oh', field%d_prod_so4_so2_oh,                       &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_COND_SO4', 'kg(SO4) m-2 s-1', 'condensation of sulfate on aerosol', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_cond_so4', field%d_cond_so4,                                     &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


    cf_desc    = t_cf_var( 'D_NUC_SO4', 'kg(SO4) m-2 s-1', 'nucleation of sulfate', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_nuc_so4', field%d_nuc_so4,                                       &
                & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc, ldims=shape2d,         &
                & lrestart=.TRUE., lrestart_cont=.TRUE.                                           )


   !--- 3D fields:
    cf_desc    = t_cf_var('D_PROD_SO4_LIQ', 'kg(SO4) m-3 s-1', 'sulfate production liquid phase', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_so4_liq', field%d_prod_so4_liq,                    &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('D_PROD_H2SO4', 'kg(SO4) m-3 s-1', 'sulfate production gas phase', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'd_prod_h2so4', field%d_prod_h2so4,                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

!>>SF moved this aerosol-dependent stuff out of the activ stream 
!     (as for other quantities above, this is in fact still HAM-dep more than simply aerosol-dep)

    cf_desc    = t_cf_var('NBCSOL_DIAG', 'm-3', 'Number of internally mixed BC particles', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcsol_diag', field%nbcsol_diag,                          &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NBCINSOL_DIAG', 'm-3', 'Number of externally mixed BC particles', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcinsol_diag', field%nbcinsol_diag,                      &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NBCSOL_ACC', 'm-3', 'Number of internally mixed accu mode BC', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcsol_acc', field%nbcsol_acc,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NBCSOL_AIT', 'm-3', 'Number internally mixed Aitken mode BC', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcsol_ait', field%nbcsol_ait,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('NDUINSOL_DIAG', 'm-3', 'Number of externally mixed dust particles', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nduinsol_diag', field%nduinsol_diag,                      &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    ! not accumulated:
    cf_desc    = t_cf_var('NBCSOL_STRAT', 'm-3', 'int mixed BC aerosols from strat. activ. part.', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcsol_strat', field%nbcsol_strat,                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NDUSOL_STRAT', 'm-3', 'int mixed dust aerosols from strat. activ. part.', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'ndusol_strat', field%ndusol_strat,                        &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NBCINSOL', 'm-3', 'ext mixed BC aerosols for frz in stream_ham', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nbcinsol', field%nbcinsol,                                &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )

    cf_desc    = t_cf_var('NDUINSOLAI', 'm-3', 'ext mixed accu. dust AP for frz in stream_ham', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nduinsolai', field%nduinsolai,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NDUINSOLCI', 'm-3', 'ext mixed coarse dust AP for frz in stream_ham', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'nduinsolci', field%nduinsolci,                            &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    cf_desc    = t_cf_var('NAERINSOL', 'm-3', 'total number of insoluble AP for frz in stream_ham', datatype_flt)
    grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
    CALL add_var( field_list, 'naerinsol', field%naerinsol,                              &
                & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc, ldims=shape3d, &
                & vert_interp=create_vert_interp_metadata(                               &
                &             vert_intp_type=vintp_types("P","Z","I"),                   &
                &             vert_intp_method=VINTP_METHOD_LIN,                         &
                &             l_loglin=.FALSE.,                                          &
                &             l_extrapol=.FALSE.),                                       &
                &  lrestart=.TRUE.,                                                      &
                &  lrestart_cont=.TRUE.                                                  )


    ! size of 4-d containers
    nclass_act=0
    DO jclass=1, kclass
      IF (sizeclass(jclass)%lactivation) THEN
        nclass_act=nclass_act+1
      END IF
    END DO


    CALL add_var( field_list, 'nact_strat', field%nact_strat4d,                 &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('NACT_STRAT', 'm-3',                                &
                 &          'number of activated particles stratiform',         &
                 &          datatype_flt),                                      &
                 & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                 & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                 & lcontainer=.TRUE., lrestart=.FALSE.                          )     

    CALL add_var( field_list, 'nact_conv', field%nact_conv4d,                   &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('NACT_CONV', 'm-3',                                 &
                 &          'number of activated particles convective',         &
                 &          datatype_flt),                                      &
                 & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                 & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                 & lcontainer=.TRUE., lrestart=.FALSE.                          )     


    CALL add_var( field_list, 'frac', field%frac4d,                             &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('FRAC', '%',                                        &
                 &          'fraction of activated particles',                  &
                 &          datatype_flt),                                      &
                 & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                 & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                 & lcontainer=.TRUE., lrestart=.FALSE.                          )    

    IF (ncd_activ == 2 .OR. nccndiag > 0) THEN
      CALL add_var( field_list, 'acurv', field%a4d,                               &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                   & t_cf_var('Acurv', 'm',                                       &
                   &          'curvature parameter',                              &
                   &          datatype_flt),                                      &
                   & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                   & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                   & lcontainer=.TRUE., lrestart=.FALSE.                          )    

      CALL add_var( field_list, 'bhyg', field%b4d,                                &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                   & t_cf_var('Bhyg', 'm3',                                       &
                   &          'hygroscopicity parameter',                         &
                   &          datatype_flt),                                      &
                   & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                   & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                   & lcontainer=.TRUE., lrestart=.FALSE.                          )    

    END IF
    IF ( ncd_activ == 2) THEN
      CALL add_var( field_list, 'scrit', field%sc4d,                              &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                   & t_cf_var('SC', '1',                                          &
                   &          'critical supersaturation',                         &
                   &          datatype_flt),                                      &
                   & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                   & ldims = (/kproma,klev,kblks,nclass_act/),                    &
                   & lcontainer=.TRUE., lrestart=.FALSE.                          )  

      IF (nactivpdf == 0) THEN
        n_cw=nclass_act
      ELSE IF (nactivpdf < 0) THEN
        n_cw=nclass_act*kw
      END IF

      CALL add_var( field_list, 'rc_strat', field%rc_strat4d,                     &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                   & t_cf_var('RC_STRAT', 'm',                                    &
                   &          'critical radius stratiform',                       &
                   &          datatype_flt),                                      &
                   & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                   & ldims = (/kproma,klev,kblks,n_cw/),                          &
                   & lcontainer=.TRUE., lrestart=.FALSE.                          )
  
      CALL add_var( field_list, 'rc_conv', field%rc_conv4d,                       &
                   & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                   & t_cf_var('RC_CONV', 'm',                                     &
                   &          'critical radius convective',                       &
                   &          datatype_flt),                                      &
                   & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), &
                   & ldims = (/kproma,klev,kblks,n_cw/),                          &
                   & lcontainer=.TRUE., lrestart=.FALSE.                          )  
    END IF

    ALLOCATE( field%nact_strat (kclass) )
    ALLOCATE( field%nact_conv  (kclass) ) 
    ALLOCATE( field%frac       (kclass) ) 
    IF (ncd_activ == 2 .OR. nccndiag > 0) THEN
      ALLOCATE( field%a          (kclass) ) 
      ALLOCATE( field%b          (kclass) ) 
    END IF
    IF ( ncd_activ == 2) THEN
      ALLOCATE( field%sc         (kclass) ) 
      ALLOCATE( field%rc_strat   (kclass, kw) ) 
      ALLOCATE( field%rc_conv    (kclass, kw) ) 
    END IF


    DO jclass=1, kclass
      IF (sizeclass(jclass)%lactivation) THEN ! lactivation
        cf_desc    = t_cf_var('NACT_STRAT_'//TRIM(sizeclass(jclass)%shortname), 'm-3',             &
                        & 'number of activated particles stratiform - '//TRIM(sizeclass(jclass)%shortname), &
                        &  datatype_flt                                                                 )
        grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
        CALL add_ref( field_list, 'nact_strat',                                     &
                    & 'nact_strat_'//TRIM(tolower(sizeclass(jclass)%shortname)),    &
                    &  field%nact_strat(jclass)%p_3d,                               &
                    & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                    & ldims=shape3d,                                                &
                    & vert_interp=create_vert_interp_metadata(                      &
                    &   vert_intp_type=vintp_types("P","Z","I"),                    &
                    &   vert_intp_method=VINTP_METHOD_LIN,                          &
                    &   l_extrapol=.FALSE.),                                        &
                    &   lrestart=.TRUE.                                             )


        cf_desc    = t_cf_var('NACT_CONV_'//TRIM(sizeclass(jclass)%shortname), 'm-3',                       &
                        & 'number of activated particles convective - '//TRIM(sizeclass(jclass)%shortname), &
                        &  datatype_flt                                                                     )
        grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
        CALL add_ref( field_list, 'nact_conv',                                      &
                    & 'nact_conv_'//TRIM(tolower(sizeclass(jclass)%shortname)),     &
                    &  field%nact_conv(jclass)%p_3d,                                &
                    & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                    & ldims=shape3d,                                                &
                    & vert_interp=create_vert_interp_metadata(                      &
                    &   vert_intp_type=vintp_types("P","Z","I"),                    &
                    &   vert_intp_method=VINTP_METHOD_LIN,                          &
                    &   l_extrapol=.FALSE.),                                        &
                    &   lrestart=.TRUE.                                             )


        cf_desc    = t_cf_var('FRAC_'//TRIM(sizeclass(jclass)%shortname), '%',                           &
                        & 'fraction of activated particles - '//TRIM(sizeclass(jclass)%shortname),       &
                        &  datatype_flt                                                                  )
        grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
        CALL add_ref( field_list, 'frac',                                           &
                    & 'frac_'//TRIM(tolower(sizeclass(jclass)%shortname)),          &
                    &  field%frac(jclass)%p_3d,                                     &
                    & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                    & ldims=shape3d,                                                &
                    & vert_interp=create_vert_interp_metadata(                      &
                    &   vert_intp_type=vintp_types("P","Z","I"),                    &
                    &   vert_intp_method=VINTP_METHOD_LIN,                          &
                    &   l_extrapol=.FALSE.),                                        &
                    &   lrestart=.TRUE.                                             )

          IF (ncd_activ == 2 .OR. nccndiag > 0) THEN

            cf_desc    = t_cf_var('Acurv_'//TRIM(sizeclass(jclass)%shortname), 'm',              &
                            & 'curvature parameter - '//TRIM(sizeclass(jclass)%shortname),       &
                            &  datatype_flt                                                      )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'acurv',                                          &
                        & 'acurv_'//TRIM(tolower(sizeclass(jclass)%shortname)),         &
                        &  field%a(jclass)%p_3d,                                        &
                        & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                        & ldims=shape3d,                                                &
                        & vert_interp=create_vert_interp_metadata(                      &
                        &   vert_intp_type=vintp_types("P","Z","I"),                    &
                        &   vert_intp_method=VINTP_METHOD_LIN,                          &
                        &   l_extrapol=.FALSE.),                                        &
                        &   lrestart=.TRUE.                                             )

            cf_desc    = t_cf_var('Bhyg_'//TRIM(sizeclass(jclass)%shortname), 'm3',                &
                            & 'hygroscopicity parameter - '//TRIM(sizeclass(jclass)%shortname),    &
                            &  datatype_flt                                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'bhyg',                                           &
                        & 'bhyg_'//TRIM(tolower(sizeclass(jclass)%shortname)),          &
                        &  field%b(jclass)%p_3d,                                        &
                        & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                        & ldims=shape3d,                                                &
                        & vert_interp=create_vert_interp_metadata(                      &
                        &   vert_intp_type=vintp_types("P","Z","I"),                    &
                        &   vert_intp_method=VINTP_METHOD_LIN,                          &
                        &   l_extrapol=.FALSE.),                                        &
                        &   lrestart=.TRUE.                                             )

         END IF

         IF (ncd_activ == 2) THEN ! ncd_activ
  
            cf_desc    = t_cf_var('SCrit_'//TRIM(sizeclass(jclass)%shortname), '1',                &
                            & 'critcal supersaturation - '//TRIM(sizeclass(jclass)%shortname),     &
                            &  datatype_flt                                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'scrit',                                          &
                        & 'scrit_'//TRIM(tolower(sizeclass(jclass)%shortname)),         &
                        &  field%sc(jclass)%p_3d,                                       &
                        & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                        & ldims=shape3d,                                                &
                        & vert_interp=create_vert_interp_metadata(                      &
                        &   vert_intp_type=vintp_types("P","Z","I"),                    &
                        &   vert_intp_method=VINTP_METHOD_LIN,                          &
                        &   l_extrapol=.FALSE.),                                        &
                        &   lrestart=.TRUE.                                             )


             IF (nactivpdf == 0) THEN

               cf_desc    = t_cf_var('RC_STRAT_'//TRIM(sizeclass(jclass)%shortname), 'm',             &
                               & 'critcal radius stratiform - '//TRIM(sizeclass(jclass)%shortname),   &
                               &  datatype_flt                                                        )
               grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
               CALL add_ref( field_list, 'rc_strat',                                       &
                           & 'rc_strat_'//TRIM(tolower(sizeclass(jclass)%shortname)),      &
                           &  field%rc_strat(jclass,1)%p_3d,                               &
                           & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                           & ldims=shape3d,                                                &
                           & vert_interp=create_vert_interp_metadata(                      &
                           &   vert_intp_type=vintp_types("P","Z","I"),                    &
                           &   vert_intp_method=VINTP_METHOD_LIN,                          &
                           &   l_extrapol=.FALSE.),                                        &
                           &   lrestart=.TRUE.                                             )

               cf_desc    = t_cf_var('RC_CONV_'//TRIM(sizeclass(jclass)%shortname), 'm',              &
                               & 'critcal radius convective - '//TRIM(sizeclass(jclass)%shortname),   &
                               &  datatype_flt                                                        )
               grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
               CALL add_ref( field_list, 'rc_conv',                                          &
                           & 'rc_conv_'//TRIM(tolower(sizeclass(jclass)%shortname)),         &
                           &  field%rc_conv(jclass,1)%p_3d,                                  &
                           & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,         &
                           & ldims=shape3d,                                                  &
                           & vert_interp=create_vert_interp_metadata(                        &
                           &   vert_intp_type=vintp_types("P","Z","I"),                      &
                           &   vert_intp_method=VINTP_METHOD_LIN,                            &
                           &   l_extrapol=.FALSE.),                                          &
                           &   lrestart=.TRUE.                                               )


             ELSE IF (nactivpdf < 0) THEN
                DO jw=1,kw
                   WRITE (cbin, "(I2.2)") jw

                   cf_desc    = t_cf_var('RC_STRAT_'//TRIM(sizeclass(jclass)%shortname)//'_'//TRIM(cbin), 'm',     &
                                   & 'critcal radius stratiform '//TRIM(sizeclass(jclass)%shortname)//             &
                                   & ' vertical velocity bin'//TRIM(sizeclass(jclass)%shortname),                  &
                                   &  datatype_flt                                                                 )
                   grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                   CALL add_ref( field_list, 'rc_strat',                                          &
                               & 'rc_strat_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//TRIM(tolower(cbin)), &
                               &  field%rc_strat(jclass,jw)%p_3d,                                 &
                               & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,          &
                               & ldims=shape3d,                                                   &
                               & vert_interp=create_vert_interp_metadata(                         &
                               &   vert_intp_type=vintp_types("P","Z","I"),                       &
                               &   vert_intp_method=VINTP_METHOD_LIN,                             &
                               &   l_extrapol=.FALSE.),                                           &
                               &   lrestart=.TRUE.                                                )

                   cf_desc    = t_cf_var('RC_CONV_'//TRIM(sizeclass(jclass)%shortname)//'_'//TRIM(cbin), 'm',      &
                                   & 'critcal radius convective '//TRIM(sizeclass(jclass)%shortname)//             &
                                   & ' vertical velocity bin'//TRIM(sizeclass(jclass)%shortname),                  &
                                   &  datatype_flt                                                                 ) 
                   grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                   CALL add_ref( field_list, 'rc_conv',                                           &
                               & 'rc_conv_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//TRIM(tolower(cbin)),  &
                               &  field%rc_conv(jclass,jw)%p_3d,                                  &
                               & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,          &
                               & ldims=shape3d,                                                   &
                               & vert_interp=create_vert_interp_metadata(                         &
                               &   vert_intp_type=vintp_types("P","Z","I"),                       &
                               &   vert_intp_method=VINTP_METHOD_LIN,                             &
                               &   l_extrapol=.FALSE.),                                           &
                               &   lrestart=.TRUE.                                                )


                END DO
             END IF
  
          END IF ! ncd_activ

      END IF ! lactivation

    END DO

    IF (nccndiag > 0) THEN

      ! Main CCN and CN diagnostics at surface if nccndiag in {1,3,5};
      ! or 3D if nccndiag in {2,4,6})

      SELECT CASE (nccndiag)
         CASE (1,3,5)
         CALL add_var( field_list, 'ccn_2d', field%ccn_2d_3d,                            &
                       & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                             &
                       & t_cf_var('Cloud Condensation Nuclei at S', 'm-3',               &
                       &          'Cloud Condensation Nuclei at S',                      &
                       &          datatype_flt),                                         &
                       & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
                       & ldims = (/kproma,kblks,nsat/),                           &
                       & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         )     
         CASE (2,4,6)
          CALL add_var( field_list, 'ccn_3d', field%ccn_3d_4d,                           &
                       & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                              &
                       & t_cf_var('Cloud Condensation Nuclei at S', 'm-3',               &
                       &          'Cloud Condensation Nuclei at S',                      &
                       &          datatype_flt),                                         &
                       & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
                       & ldims = (/kproma,kblks,nsat/),                           &
                       & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         )              
      END SELECT 
         

      ! CCN diagnostics for each supersaturation
      ! (surface if nccndiag in {1,3,5}; 3D if in {2,4,6})
      DO jsat=1,nsat

        WRITE(csat,'(F7.3)') zsat(jsat)*100.0

        SELECT CASE (nccndiag)
           CASE (1,3,5)
             ! 2-D
            cf_desc    = t_cf_var('CCN_'//TRIM(ADJUSTL(csat)), 'm-3',              &
                            & 'Cloud Condensation Nuclei at S='//csat//'%',        &
                            &  datatype_flt                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'ccn_2d',                                         &
                        & 'ccn_'//TRIM(ADJUSTL(tolower(csat))),                         &
                        & field%ccn_2d(jsat)%p_2d,                                      &
                        & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,      &
                        & ldims=shape2d,                                                &       
                        &   lrestart=.TRUE.                                             )


           CASE (2,4,6)
             ! 3-D
            cf_desc    = t_cf_var('CCN_'//TRIM(ADJUSTL(csat)), 'm-3',              &
                            & 'Cloud Condensation Nuclei at S='//csat//'%',        &
                            &  datatype_flt                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'ccn_3d',                                         &
                        & 'ccn_'//TRIM(ADJUSTL(tolower(csat))),                         &
                        & field%ccn_3d(jsat)%p_3d,                                      &
                        & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                        & ldims=shape3d,                                                &
                        & vert_interp=create_vert_interp_metadata(                      &
                        &             vert_intp_type=vintp_types("P","Z","I"),          &
                        &             vert_intp_method=VINTP_METHOD_LIN,                &
                        &             l_loglin=.FALSE.,                                 &
                        &             l_extrapol=.FALSE.),                              &              
                        &   lrestart=.TRUE.                                             )
        END SELECT

      END DO ! jsat

      ! CN diagnostics if nccndiag in {3,4,5,6}
      ! (surface if in {3,5}; 3D if in {4,6})
      SELECT CASE (nccndiag)
         CASE (3,5)
            ! 2-D
            cf_desc    = t_cf_var('CN', 'm-3',                                     &
                            & 'Condensation Nuclei',                               &
                            &  datatype_flt                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_var( field_list, 'cn',  field%cn_2d%p_2d,                          &
                        & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,      &
                        & ldims=shape2d,                                                &   
                        &   lrestart=.TRUE.                                             )
 
         CASE (4,6)
            ! 3-D
            cf_desc    = t_cf_var('CN', 'm-3',                                     &
                            & 'Condensation Nuclei',                               &
                            &  datatype_flt                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_var( field_list, 'cn',  field%cn_3d%p_3d,                          &
                        & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                        & ldims=shape3d,                                                &
                        & vert_interp=create_vert_interp_metadata(                      &
                        &   vert_intp_type=vintp_types("P","Z","I"),                    &
                        &   vert_intp_method=VINTP_METHOD_LIN,                          &
                        &   l_extrapol=.FALSE.),                                        &              
                        &   lrestart=.TRUE.                                             )
      END SELECT

      ! CCN and CN column burden diagnostics if nccndiag in {5,6}
      SELECT CASE (nccndiag)
         CASE (5,6)
           CALL add_var( field_list, 'ccn_burden', field%ccn_burden_3d,        &
             & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                             &
             & t_cf_var('Cloud Condensation Nuclei burden at S', 'm-2',        &
             &          'Cloud Condensation Nuclei burden at S',               &
             &          datatype_flt),                                         &
             & grib2_var(199,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),    &
             & ldims = (/kproma,kblks,nsat/),                           &
             & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.         ) 

            
            DO jsat=1,nsat
               WRITE(csat,'(F7.3)') zsat(jsat)*100.0
               cf_desc    = t_cf_var('CCN_BURDEN_'//TRIM(ADJUSTL(csat)), 'm-2',              &
                               & 'Cloud Condensation Nuclei burden at S='//csat//'%',        &
                               &  datatype_flt                                        )
               grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
               CALL add_ref( field_list, 'ccn_burden',                                     &
                           & 'ccn_burden_'//TRIM(ADJUSTL(tolower(csat))),                  &
                           & field%ccn_burden(jsat)%p_2d,                                  &
                           & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,      &
                           & ldims=shape2d,                                                &                  
                           &   lrestart=.TRUE.                                             )
            END DO
   
            cf_desc    = t_cf_var('CN_BURDEN', 'm-2',                                   &
                            & 'Condensation Nuclei Burden',                             &
                            &  datatype_flt                                        )
            grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_var( field_list, 'cn_burden',  field%cn_burden%p_2d,               &
                        & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,      &
                        & ldims=shape2d,                                                &                
                        &   lrestart=.TRUE.                                             )

      END SELECT

    END IF ! nccndiag
    !-- end entries for mo_ham_ccn

  END SUBROUTINE new_ham_field_list

!--------------------------------------------------------------------------------------------------------------------

  SUBROUTINE new_stream_ham_rad ( p_patch )

  ! *new_stream_ham_rad* constructs diagnostics stream rad
  !                      and defines stream elemenst for
  !                      HAM aerosol optical properties
  !
  ! Author:
  ! -------
  ! Philip Stier, MPI-Met, Hamburg          05/2003
  !
  ! Interface:
  ! ----------
  ! *new_stream_ham_rad* is called from *init_subm_memory*
  !                             in *mo_submodel_interface*

    USE mo_model_domain,            ONLY: t_patch 
    USE mo_ham,                     ONLY: nclass

    TYPE(t_patch), INTENT(IN), DIMENSION(:)  :: p_patch

    ! local variables
    CHARACTER(len=MAX_CHAR_LENGTH) :: listname
    INTEGER :: ndomain, jg, ist, nblks, nlev

    CALL message(TRIM(thismodule),'Construction of HAM_RAD state started.')

    ndomain = SIZE(p_patch)

    ALLOCATE( ham_rad_field(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      & 'allocation of ham_rad_field failed')

    ALLOCATE( ham_rad_field_list(ndomain), STAT=ist)
    IF (ist/=SUCCESS) CALL finish(TRIM(thismodule), &
      &'allocation of ham_rad_field_list failed')

    DO jg = 1,ndomain

      nblks = p_patch(jg)%nblks_c
      nlev  = p_patch(jg)%nlev

      WRITE(listname,'(a,i2.2)') 'ham_rad_state_diag_of_domain',jg
      CALL new_ham_rad_field_list ( jg, nproma, nlev, nblks, nclass,                   &
                                &  TRIM(listname), ham_rad_field_list(jg), ham_rad_field(jg)  )

    END DO

    CALL message(TRIM(thismodule),'Construction of HAM_RAD state finished.')

  END SUBROUTINE new_stream_ham_rad


  !--------------------------------------------------------------------
  !>
  SUBROUTINE new_ham_rad_field_list( k_jg, kproma, klev, kblks, kclass,  &
                                & listname, field_list, field    ) 

!!$    USE mo_filename,      ONLY: trac_filetype
    USE mo_species,       ONLY: speclist
    USE mo_ham,           ONLY: nrad
    USE mo_ham_rad_data,  ONLY: lambda, nradang
    USE mo_ham_rad_data,  ONLY: Nwv_tot, nraddiagwv
    USE mo_ham,           ONLY: subm_naerospec, subm_aerospec
    USE mo_exception,     ONLY: finish
    USE mo_ham,           ONLY: nclass, nraddiag, sizeclass
    USE mo_util_string,   ONLY: tolower

    INTEGER, INTENT(IN) :: k_jg !> patch ID
    INTEGER, INTENT(IN) :: kproma, klev, kblks     !< dimension sizes
    INTEGER, INTENT(IN) :: kclass
    
    CHARACTER(len=*),  INTENT(IN) :: listname

    TYPE(t_var_list),  INTENT(INOUT) :: field_list
    TYPE(t_ham_rad_field), INTENT(INOUT) :: field

    ! local variables
    INTEGER :: ibits, iextbits
    INTEGER :: datatype_flt
    INTEGER :: shape2d(2), shape3d(3)
    
    TYPE(t_cf_var)    :: cf_desc
    TYPE(t_grib2_var) :: grib2_desc

    INTEGER :: jclass, jn, jwv, Nwv_diag, Nwv_diag0, Nwv_diag_wv1, &
                                Nwv_diag_wv1_0, Nwv_diag_nsub

    CHARACTER(len=30) :: cwv, cwv2
    CHARACTER(len=18) :: comp_str


    ibits = DATATYPE_PACK16
    iextbits = DATATYPE_PACK24

    IF ( lnetcdf_flt64_output ) THEN
      datatype_flt = DATATYPE_FLT64
    ELSE
      datatype_flt = DATATYPE_FLT32
    END IF

    shape2d  = (/kproma,       kblks/)
    shape3d  = (/kproma, klev, kblks/)

    !--- Create new variable list:
    CALL new_var_list( field_list, TRIM(listname), patch_id=k_jg )
    CALL default_var_list_settings( field_list,                &
                                  & lrestart=.TRUE.  )

    !---allocate arrays for per-compound diagnostics (only diagnosed wvs are actually allocated as stream

    ALLOCATE(field%tau_comp(subm_naerospec,Nwv_tot))
    ALLOCATE(field%abs_comp(subm_naerospec,Nwv_tot))

    ! count elements for containers 
    Nwv_diag=0
    Nwv_diag0=0
    Nwv_diag_wv1=0
    Nwv_diag_wv1_0=0
    Nwv_diag_nsub=0
    DO jwv=1, Nwv_tot
      IF (nraddiagwv(jwv)>0) THEN! nraddiagwv>0
        Nwv_diag0=Nwv_diag0+1
        DO jclass=1, nclass  
          IF( nrad(jclass)>0 ) THEN ! nrad  
            Nwv_diag=Nwv_diag+1
            IF (nraddiagwv(jwv)>1) THEN
              Nwv_diag_wv1=Nwv_diag_wv1+1
            END IF
          END IF ! nrad
        END DO  ! nclass
        IF (nraddiagwv(jwv)>1) THEN
          DO jn=1, subm_naerospec
            Nwv_diag_nsub=Nwv_diag_nsub+1 
          END DO
          Nwv_diag_wv1_0=Nwv_diag_wv1_0+1
        END IF 
      END IF ! nraddiagwv>0
    END DO ! jwv


    ! containers
    CALL add_var( field_list, 'tau_mode', field%tau_mode4d,                     &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('Optical thickness', '1',                           &
                 &          'Optical thickness',                                &
                 &          datatype_flt),                                      &
                 & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & ! cms check for code
                 & ldims = (/kproma,klev,kblks,Nwv_diag/),                      &
                 & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )


   CALL add_var( field_list, 'abs_mode', field%abs_mode4d,                      &
                 & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                 & t_cf_var('Absorption Optical thickness', '1',                &
                 &          'Absorption Optical thickness',                     &
                 &          datatype_flt),                                      &
                 & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                 & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                 & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )


    IF (nraddiag>0) THEN
      CALL add_var( field_list, 'sigma_2d_mode', field%sigma_2d_mode3d,            &
                    & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                    & t_cf_var('SIGMA_2D_MODE', 'm-2',                             &
                    &          'Extinction cross section per particle 2D',         &
                    &          datatype_flt),                                      &
                    & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                    & ldims = (/kproma,kblks,Nwv_diag_wv1/),                       &
                    & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'omega_2d_mode', field%omega_2d_mode3d,            &
                    & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                    & t_cf_var('OMEGA_2D_MODE', '1',                               &
                    &          'Single scattering albedo 2D',                      &
                    &          datatype_flt),                                      &
                    & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                    & ldims = (/kproma,kblks,Nwv_diag_wv1/),                       &
                    & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'asym_2d_mode', field%asym_2d_mode3d,              &
                    & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                    & t_cf_var('ASYM_2D_MODE', '1',                                &
                    &          'Asymmetry Factor 2D',                              &
                    &          datatype_flt),                                      &
                    & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                    & ldims = (/kproma,kblks,Nwv_diag_wv1/),                       &
                    & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

     CALL add_var( field_list, 'nr_2d_mode', field%nr_2d_mode3d,                   &
                    & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                    & t_cf_var('NR_2D_MODE', '1',                                  &
                    &          'Refractive Index - real part 2D',                  &
                    &          datatype_flt),                                      &
                    & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                    & ldims = (/kproma,kblks,Nwv_diag_wv1/),                       &
                    & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )


     CALL add_var( field_list, 'ni_2d_mode', field%ni_2d_mode3d,                   &
                    & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                    & t_cf_var('NI_2D_MODE', '1',                                  &
                    &          'Refractive Index - imaginary part 2D',             &
                    &          datatype_flt),                                      &
                    & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                    & ldims = (/kproma,kblks,Nwv_diag_wv1/),                       &
                    & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )
    END IF

    IF (nraddiag==2) THEN
      CALL add_var( field_list, 'sigma_mode', field%sigma_mode4d,                   &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                     & t_cf_var('SIGMA_MODE', 'cm+2 part-1',                        &
                     &          'Extinction cross section per particle',            &
                     &          datatype_flt),                                      &
                     & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                     & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                     & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'omega_mode', field%omega_mode4d,                   &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                     & t_cf_var('OMEGA_MODE', 'cm+2 part-1',                        &
                     &          'Single scattering albedo',                         &
                     &          datatype_flt),                                      &
                     & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                     & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                     & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'asym_mode', field%asym_mode4d,                     &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                     & t_cf_var('ASYM_MODE', '1',                                   &
                     &          'Asymmetry Factor',                                 &
                     &          datatype_flt),                                      &
                     & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                     & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                     & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'nr_mode', field%nr_mode4d,                         &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                     & t_cf_var('NR_MODE', '1',                                     &
                     &          'Refractive Index - real part',                     &
                     &          datatype_flt),                                      &
                     & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                     & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                     & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

      CALL add_var( field_list, 'ni_mode', field%ni_mode4d,                         &
                     & GRID_UNSTRUCTURED_CELL, ZA_HYBRID,                           &
                     & t_cf_var('NI_MODE', '1',                                     &
                     &          'Refractive Index - imaginary part',                &
                     &          datatype_flt),                                      &
                     & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                     & ldims = (/kproma,klev,kblks,Nwv_diag_wv1/),                  &
                     & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

    END IF


    CALL add_var( field_list, 'tau_2d', field%tau_2d3d,                          &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                  & t_cf_var('TAU_2D', '1',                                      &
                  &          'Optical thickness - total',                        &
                  &          datatype_flt),                                      &
                  & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                  & ldims = (/kproma,kblks,Nwv_diag0/),                           &
                  & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )


    CALL add_var( field_list, 'tau_comp', field%tau_comp3d,                        &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                            &
                  & t_cf_var('TAU_COMP', '1',                                      &
                  &          'Optical thickness by compound',                      &
                  &          datatype_flt),                                        &
                  & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),   & 
                  & ldims = (/kproma,kblks,Nwv_diag_nsub/),                        &
                  & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.          )
     

   CALL add_var( field_list, 'abs_comp', field%abs_comp3d,                         &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                            &
                  & t_cf_var('ABS_COMP', '1',                                      &
                  &          'Absorption optical thickness by compound',           &
                  &          datatype_flt),                                        &
                  & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL),   & 
                  & ldims = (/kproma,kblks,Nwv_diag_nsub/),                        &
                  & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.          )


   CALL add_var( field_list, 'abs_2d', field%abs_2d3d,                           &
                  & GRID_UNSTRUCTURED_CELL, ZA_SURFACE,                          &
                  & t_cf_var('ABS_2D', '1',                                      &
                  &          'Absorption optical thickness - total',             &
                  &          datatype_flt),                                      &
                  & grib2_var(200,255,255, ibits, GRID_UNSTRUCTURED, GRID_CELL), & 
                  & ldims = (/kproma,kblks,Nwv_diag_wv1_0/),                     &
                  & lcontainer=.TRUE., lrestart=.FALSE., loutput=.FALSE.        )

     
    ! end containers

    DO jwv=1, Nwv_tot

       IF (nraddiagwv(jwv)>0) THEN

          WRITE(cwv,'(I6)') INT(lambda(jwv)*1.E9_dp)
          cwv=TRIM(ADJUSTL(cwv))//'nm'
          !--- 2.1) Diagnostics for each mode:

          DO jclass=1, nclass

            IF( nrad(jclass)>0 ) THEN ! nrad

              cf_desc    = t_cf_var('TAU_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',        &
                              & 'Optical thickness '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,        &
                              &  datatype_flt                                                             )
              grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
              CALL add_ref( field_list, 'tau_mode',                                       &
                          & 'tau_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,   &
                          & field%tau_mode(jclass,jwv)%p_3d,                              &
                          & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                          & ldims=shape3d,                                                &
                          & vert_interp=create_vert_interp_metadata(                      &
                          &             vert_intp_type=vintp_types("P","Z","I"),          &
                          &             vert_intp_method=VINTP_METHOD_LIN,                &
                          &             l_loglin=.FALSE.,                                 &
                          &             l_extrapol=.FALSE.),                              &                  
                          &   lrestart=.TRUE.                                             )


              IF (nraddiagwv(jwv)>1) THEN

                cf_desc    = t_cf_var('ABS_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',            &
                                & 'Absorption Optical thickness '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,  &
                                &  datatype_flt                                                             )
                grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                CALL add_ref( field_list, 'abs_mode',                                       &
                            & 'abs_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,     &
                            & field%abs_mode(jclass,jwv)%p_3d,                              &
                            & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                            & ldims=shape3d,                                                &
                            & vert_interp=create_vert_interp_metadata(                      &
                            &             vert_intp_type=vintp_types("P","Z","I"),          &
                            &             vert_intp_method=VINTP_METHOD_LIN,                &
                            &             l_loglin=.FALSE.,                                 &
                            &             l_extrapol=.FALSE.),                              &        
                            &   lrestart=.TRUE.                                             )


                IF (nraddiag>0) THEN


                  cf_desc    = t_cf_var('SIGMA_2D_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, 'm-2',                 &
                                  & 'Extinction cross section per particle 2D '//TRIM(sizeclass(jclass)%shortname)//' '//cwv, &
                                  &  datatype_flt                                                                             )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'sigma_2d_mode',                                     &
                               & 'sigma_2d_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,  &
                               & field%sigma_2d_mode(jclass,jwv)%p_2d,                           &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &              
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )


                  cf_desc    = t_cf_var('OMEGA_2D_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',         &
                                  & 'Single scattering albedo 2D '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,    &
                                  &  datatype_flt                                                                   )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'omega_2d_mode',                                     &
                               & 'omega_2d_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,  &
                               & field%omega_2d_mode(jclass,jwv)%p_2d,                           &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &    
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )

                  cf_desc    = t_cf_var('ASYM_2D_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',    &
                                  & 'Asymmetry Factor 2D '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,      &
                                  &  datatype_flt                                                             )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'asym_2d_mode',                                      &
                               & 'asym_2d_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,   &
                               & field%asym_2d_mode(jclass,jwv)%p_2d,                            &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &               
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.        )


                  cf_desc    = t_cf_var('NR_2D_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',                 &
                                  & 'Refractive Index - real part 2D '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,     &
                                  &  datatype_flt                                                                       )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'nr_2d_mode',                                        &
                               & 'nr_2d_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,     &
                               & field%nr_2d_mode(jclass,jwv)%p_2d,                              &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )

                  cf_desc    = t_cf_var('NI_2D_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',                 &
                                  & 'Refractive Index - imaginary part 2D '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,     &
                                  &  datatype_flt                                                                       )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'ni_2d_mode',                                        &
                               & 'ni_2d_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,     &
                               & field%ni_2d_mode(jclass,jwv)%p_2d,                              &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &       
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )

                END IF

                IF (nraddiag==2) THEN


                  cf_desc    = t_cf_var('SIGMA_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',                    &
                                  & 'Extinction cross section per particle '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,  &
                                  &  datatype_flt                                                                           )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'sigma_mode',                                     &
                              & 'sigma_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,   &
                              & field%sigma_mode(jclass,jwv)%p_3d,                            &
                              & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                              & ldims=shape3d,                                                &
                              & vert_interp=create_vert_interp_metadata(                      &
                              &             vert_intp_type=vintp_types("P","Z","I"),          &
                              &             vert_intp_method=VINTP_METHOD_LIN,                &
                              &             l_loglin=.FALSE.,                                 &
                              &             l_extrapol=.FALSE.),                              &       
                              &   lrestart=.TRUE.                                             )

                  cf_desc    = t_cf_var('OMEGA_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',       &
                                  & 'Single scattering albedo '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,  &
                                  &  datatype_flt                                                              )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'omega_mode',                                     &
                              & 'omega_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,   &
                              & field%omega_mode(jclass,jwv)%p_3d,                            &
                              & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                              & ldims=shape3d,                                                &
                              & vert_interp=create_vert_interp_metadata(                      &
                              &             vert_intp_type=vintp_types("P","Z","I"),          &
                              &             vert_intp_method=VINTP_METHOD_LIN,                &
                              &             l_loglin=.FALSE.,                                 &
                              &             l_extrapol=.FALSE.),                              &      
                              &   lrestart=.TRUE.                                             )


                  cf_desc    = t_cf_var('ASYM_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',        &
                                  & 'Asymmetry Factor '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,          &
                                  &  datatype_flt                                                              )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'asym_mode',                                      &
                              & 'asym_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,    &
                              & field%asym_mode(jclass,jwv)%p_3d,                             &
                              & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                              & ldims=shape3d,                                                &
                              & vert_interp=create_vert_interp_metadata(                      &
                              &             vert_intp_type=vintp_types("P","Z","I"),          &
                              &             vert_intp_method=VINTP_METHOD_LIN,                &
                              &             l_loglin=.FALSE.,                                 &
                              &             l_extrapol=.FALSE.),                              &      
                              &   lrestart=.TRUE.                                             )

                  cf_desc    = t_cf_var('NR_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',                      &
                                  & 'Refractive Index - real part '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,          &
                                  &  datatype_flt                                                                          )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'nr_mode',                                        &
                              & 'nr_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,    &
                              & field%nr_mode(jclass,jwv)%p_3d,                               &
                              & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                              & ldims=shape3d,                                                &
                              & vert_interp=create_vert_interp_metadata(                      &
                              &             vert_intp_type=vintp_types("P","Z","I"),          &
                              &             vert_intp_method=VINTP_METHOD_LIN,                &
                              &             l_loglin=.FALSE.,                                 &
                              &             l_extrapol=.FALSE.),                              &                 
                              &   lrestart=.TRUE.                                             )



                  cf_desc    = t_cf_var('NI_MODE_'//TRIM(sizeclass(jclass)%shortname)//'_'//cwv, '1',                      &
                                  & 'Refractive Index - imaginary part '//TRIM(sizeclass(jclass)%shortname)//' '//cwv,     &
                                  &  datatype_flt                                                                          )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'ni_mode',                                        &
                              & 'ni_mode_'//TRIM(tolower(sizeclass(jclass)%shortname))//'_'//cwv,  &
                              & field%ni_mode(jclass,jwv)%p_3d,                               &
                              & GRID_UNSTRUCTURED_CELL, ZA_HYBRID, cf_desc, grib2_desc,       &
                              & ldims=shape3d,                                                &
                              & vert_interp=create_vert_interp_metadata(                      &
                              &             vert_intp_type=vintp_types("P","Z","I"),          &
                              &             vert_intp_method=VINTP_METHOD_LIN,                &
                              &             l_loglin=.FALSE.,                                 &
                              &             l_extrapol=.FALSE.),                              &           
                              &   lrestart=.TRUE.                                             )

                END IF ! nraddiag

              END IF !nraddiagwv>1

            END IF ! nrad

          END DO !nclass

          !--- 2.3) 2D fields:

          IF (nraddiagwv(jwv)>0) THEN

            cf_desc    = t_cf_var('TAU_2D_'//cwv, '1',                    &
                            & 'Optical thickness - total '//cwv,          &
                            &  datatype_flt                              )
            grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
            CALL add_ref( field_list, 'tau_2d',                                            &
                         & 'tau_2d_'//cwv,                                                 &
                         & field%tau_2d(jwv)%p_2d,                                         &
                         & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                         & ldims=shape2d,                                                  &                 
                         &   lrestart=.TRUE., lrestart_cont=.TRUE.                         )

          END IF

          IF (nraddiagwv(jwv)>1) THEN

             !--- Diagnostics for each compound:

             DO jn=1, subm_naerospec
               comp_str = TRIM(ADJUSTL(speclist(subm_aerospec(jn))%shortname))

                  cf_desc    = t_cf_var('TAU_COMP_'//TRIM(ADJUSTL(comp_str))//'_'//cwv, '1',     &
                                  & 'Optical thickness '//TRIM(ADJUSTL(comp_str))//' '//cwv,     &
                                  &  datatype_flt                                                )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'tau_comp',                                          &
                               & 'tau_comp_'//TRIM(ADJUSTL(tolower(comp_str)))//'_'//cwv,        &
                               & field%tau_comp(jn,jwv)%p_2d,                                    &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &   
                               &   lrestart=.TRUE., lrestart_cont=.TRUE.                         )

                  cf_desc    = t_cf_var('ABS_COMP_'//TRIM(ADJUSTL(comp_str))//'_'//cwv, '1',            &
                                  & 'Absorption optical thickness '//TRIM(ADJUSTL(comp_str))//' '//cwv, &
                                  &  datatype_flt                                                      )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'abs_comp',                                          &
                               & 'abs_comp_'//TRIM(ADJUSTL(tolower(comp_str)))//'_'//cwv,        &
                               & field%abs_comp(jn,jwv)%p_2d,                                    &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &        
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )
             END DO

           !--- 2.3) 2D total fields:

                 cf_desc    = t_cf_var('ABS_2D'//'_'//cwv, '1',                    &
                                  & 'Absorption optical thickness - total '//cwv,  &
                                  &  datatype_flt                                  )
                  grib2_desc = grib2_var(200, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
                  CALL add_ref( field_list, 'abs_2d',                                            &
                               & 'abs_2d'//'_'//cwv,                                             &
                               & field%abs_2d(jwv)%p_2d,                                         &
                               & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,        &
                               & ldims=shape2d,                                                  &           
                               &   lrestart=.TRUE.,  lrestart_cont=.TRUE.                        )

          END IF !nraddiagwv>1


       END IF !nraddiagwv>0
    END DO !jwv


    IF (nradang(1)/=0 .AND. nradang(2)/=0) THEN

       IF (nraddiagwv(nradang(1))>0 .AND. nraddiagwv(nradang(2))>0 ) THEN

          WRITE(cwv,'(I6)') INT(lambda(nradang(1))*1.E9_dp)
          cwv=TRIM(ADJUSTL(cwv))//'nm'

          WRITE(cwv2,'(I6)') INT(lambda(nradang(2))*1.E9_dp)
          cwv2=TRIM(ADJUSTL(cwv2))//'nm'


          cf_desc    = t_cf_var('ANG_'//TRIM(ADJUSTL(cwv))//'_'//TRIM(ADJUSTL(cwv2)), '1',                    &
                          & 'Angstroem parameter between '//TRIM(ADJUSTL(cwv))//' and '//TRIM(ADJUSTL(cwv2)), &
                          &  datatype_flt                                                                     )
          grib2_desc = grib2_var(199, 255, 255, ibits, GRID_UNSTRUCTURED, GRID_CELL)
          CALL add_var( field_list, 'ang_'//TRIM(ADJUSTL(cwv))//'_'//TRIM(ADJUSTL(cwv2)),  field%ang%p_2d,    &
                      & GRID_UNSTRUCTURED_CELL, ZA_SURFACE, cf_desc, grib2_desc,                              &
                      & ldims=shape2d,                                                                        &        
                      &   lrestart=.TRUE.                                                                     )

       ELSE
        CALL finish('construct_stream_ham_rad:','Angstroem parameter between '//TRIM(ADJUSTL(cwv))  &
                    //' and '//TRIM(ADJUSTL(cwv2))//' requested but inconsistent nraddiagwv')
       END IF

    END IF


  END SUBROUTINE new_ham_rad_field_list


END MODULE mo_ham_streams

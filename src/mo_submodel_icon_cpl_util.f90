!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_submodel_icon_cpl_util

  USE mo_exception,          ONLY: message, message_text

  USE mo_submodel_tracdef,   ONLY: ntrac   ! submodel (e.g. HAMMOZ) number of tacers 
  USE mo_run_config,         ONLY: ntracer, & ! from ICON
                                   io3, ich4, in2o, ico2 
   

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: update_icon_ntracer

CONTAINS
SUBROUTINE update_icon_ntracer 

  CHARACTER(LEN=*), PARAMETER :: this_routine = "update_icon_ntracer" 

  ! add number of submodel (HAMMOZ) tracers to ICON ntracer 
  ntracer = ntracer + ntrac

  io3 = io3 + ntrac
  ich4 = ich4 + ntrac 
  in2o = in2o + ntrac 
  ico2 = ico2 + ntrac 

  WRITE(message_text,'(a,i3,a,i3)') 'Attention: HAMMOZ tracers active, '//&
                               'ntracer is increased by ',ntrac, &
                               ' to ',ntracer
  CALL message(TRIM(this_routine),message_text)

END SUBROUTINE update_icon_ntracer
END MODULE mo_submodel_icon_cpl_util

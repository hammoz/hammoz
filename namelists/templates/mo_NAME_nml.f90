!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
###(disclaimer)###

MODULE mo_###(nml_name)###_nml 


  USE mo_###(nml_name)###_config

  USE mo_kind,                ONLY: wp, dp
  USE mo_io_units,            ONLY: nnml
  USE mo_namelist,            ONLY: position_nml, POSITIONED, open_nml, close_nml
  USE mo_master_control,      ONLY: use_restart_namelists
  USE mo_restart_namelist,    ONLY: open_tmpfile, store_and_close_namelist, &
                                  & open_and_restore_namelist, close_tmpfile
  USE mo_mpi,                 ONLY: my_process_is_stdio
  USE mo_nml_annotate,        ONLY: temp_defaults, temp_settings

  ###(nml_use_modules)###

  IMPLICIT NONE
  PRIVATE
  PUBLIC :: read_###(nml_name)###_namelist

  ###(nml_variable_declaration)### 
 
  ###(nml_namelist_declaration)###

CONTAINS
  SUBROUTINE read_###(nml_name)###_namelist( filename )

   CHARACTER(LEN=*),INTENT(IN) :: filename
   INTEGER  :: ist, funit, iunit

   CALL init_defaults_###(nml_name)###

   ###(call_nml_get_conf)###

   !-------------------------------------------------------------------
   ! If this is a resumed integration, overwrite the defaults above
   ! by values used in the previous integration.
   !
   IF (use_restart_namelists()) THEN
     funit = open_and_restore_namelist('###(nml_name)###_nml')
     READ(funit,NML=###(nml_name)###_nml)
     CALL close_tmpfile(funit)
   END IF

   !-------------------------------------------------------------------------
   ! Read user's (new) specifications. (Done so far by all MPI processes)
   !
   CALL open_nml(TRIM(filename))
   CALL position_nml('###(nml_name)###_nml',STATUS=ist)
   IF (my_process_is_stdio()) THEN
     iunit = temp_defaults()
     WRITE(iunit, ###(nml_name)###_nml)      ! write defaults to temporary text file
   END IF
   SELECT CASE (ist)
   CASE (POSITIONED)
     READ (nnml, ###(nml_name)###_nml)       ! overwrite default settings
     IF (my_process_is_stdio()) THEN
       iunit = temp_settings()
       WRITE(iunit, ###(nml_name)###_nml)    ! write settings to temporary text file
     END IF
   END SELECT
   CALL close_nml
   !------------------------------------------------------------
   ! Sanity check
   !

   !-----------------------------------------------------
   ! Store the namelist for restart
   !
   IF(my_process_is_stdio())  THEN
     funit = open_tmpfile()
     WRITE(funit,NML=###(nml_name)###_nml)
     CALL store_and_close_namelist(funit, '###(nml_name)###_nml')
   ENDIF

   !-----------------------------------------------------
   ! Fill configuration state
   !
   ###(call_nml_set_config)###

  END SUBROUTINE read_###(nml_name)###_namelist
END MODULE mo_###(nml_name)###_nml 

!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
! avoid circular dependence of  mo_submodel_streams, mo_submdiagctl_nml
MODULE mo_submodel_streams_constants

 USE mtime,               ONLY: MAX_TIMEDELTA_STR_LEN
 
 IMPLICIT NONE

 PRIVATE

 PUBLIC :: nmaxstreamvars 
 PUBLIC :: default_output
 PUBLIC :: default_output_interval
 

 INTEGER, PARAMETER   :: nmaxstreamvars = 60   ! maximum number of different diagnostics in stream

 LOGICAL :: default_output = .TRUE.   ! logical to allow easy disabling of submodel output

 CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN) :: default_output_interval = 'P1D'

 

END MODULE mo_submodel_streams_constants

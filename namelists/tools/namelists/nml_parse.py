import os, sys
import xml.etree.ElementTree as ET
 
from utils import util

def nml_parse_and_replace( xml_filename, indir, outdir ):
    tree=ET.parse(xml_filename)
    root=tree.getroot()
    for namelist in root.findall('namelist'):
        print( namelist.attrib['name'])
        for f90_in_filename in os.listdir(indir):
            if f90_in_filename.endswith("NAME_nml.f90") or f90_in_filename.endswith("_config.f90"):
                nml_replace(f90_in_filename, namelist, indir, outdir)
            

def nml_replace( f90_in_filename, namelist, indir, outdir ):
    nml_name=namelist.attrib['name']
    f90_out_filename=f90_in_filename.replace("NAME",nml_name)  
    ifile=indir+"/"+f90_in_filename
    ofile=outdir+"/"+f90_out_filename
    fin=open(ifile, "r") 
    fout=open(ofile, "w")
    while True:
       l=fin.readline()
       if not l: break
       lo=util.replace_line( nml_replace_var, l, namelist )
       print(lo)
       fout.write(lo+"\n")
    print( f90_out_filename )
    fin.close()
    fout.close()
        

def nml_replace_var( rvar, namelist, leading_blanks ):
    rvar_in=rvar
    if rvar == "nml_name":
        rvar=namelist.attrib['name']
    if rvar == "nml_variable_declaration":
        rvar=create_nml_variable_declaration( namelist, leading_blanks )
    if rvar == "nml_namelist_declaration":
        rvar=create_nml_declaration( namelist )
    if rvar == "nml_set_config":
        rvar=create_nml_set_config( namelist, leading_blanks )
    if rvar == "call_nml_set_config":
        rvar=create_call_nml_set_config( namelist, leading_blanks )
    if rvar == "public_nml_config":
        rvar=create_public_nml_config( namelist, leading_blanks )
    if rvar == "nml_init_defaults":
        rvar=create_nml_init_defaults( namelist, leading_blanks )
    if rvar == "nml_get_conf":
        rvar=create_nml_get_conf( namelist, leading_blanks )
    if rvar == "nml_use_modules":
        rvar=create_nml_use_modules( namelist, leading_blanks )
    if rvar == "call_nml_get_conf":
        rvar=create_call_nml_get_conf( namelist, leading_blanks )
    if rvar == "disclaimer":
        rvar=util.disclaimer("nml_parse.py") 
    if rvar == rvar_in:
        message="no rule for replacing "+rvar
        util.finish ("parse.py",message)
    return rvar

def set_fldlen_varname():
    fldlen=20 
    return fldlen

def set_fldlen_vartype():
    fldlen=30 
    return fldlen

def create_nml_variable_declaration( namelist, leading_blanks ):
    bb="" 
    rvar=""
    fldlen_v=set_fldlen_varname()
    fldlen_t=set_fldlen_vartype()
    for var in namelist.findall("variable"):
        vname=var.attrib['name'].ljust(fldlen_v)
        vtype=var.find("type").text.ljust(fldlen_t)
        #vdefault=" (default: "+var.find("default").text+")"
        if var.find("dimension") is not None:
           vtype=vtype.rstrip()+", DIMENSION("+var.find("dimension").text+")"
           vtype=vtype.ljust(fldlen_t)
        rvar=rvar+bb+vtype+" :: "+vname
        bb=" " * leading_blanks 
        if var.find("long_description") is not None:
            dum=var.find("long_description").text
            vldescr=os.linesep.join([s for s in dum.splitlines() if s])
            rvar=rvar+vldescr.splitlines()[0].lstrip()
            for l in vldescr.splitlines()[1:]:
                 rvar=rvar+"\n"+' '* (fldlen_v+fldlen_t+4)+bb+l.lstrip()
            rvar=rvar+"\n"
    return rvar

def create_nml_declaration( namelist ):
    number_of_vars_per_line=4
    bb="" 
    lname="/"+namelist.attrib['name']+"_nml"+"/"
    rvar="NAMELIST".rjust(2)+" " * 2+lname+" "
    lindent=len(rvar)+2
    vars=namelist.findall("variable")
    nvar=len( namelist.findall("variable"))
    ivar=0
    for var in vars:
        #print ( var.attrib['name'] )
        rvar=rvar+var.attrib['name']
        ivar=ivar+1
        if ivar < nvar:
            rvar=rvar+", "
            if ivar%number_of_vars_per_line == 0:
                rvar=rvar+" & \n"+lindent*' ';
    return rvar

def set_conf_subr_name(nmlname, vname):
     return "set_conf_"+nmlname+"_"+vname

def get_conf_func_name(nmlname, vname):
     return "get_conf_"+nmlname+"_"+vname

def create_nml_init_defaults( namelist, leading_blanks  ):
     rvar="\n"
     bb=" " * leading_blanks 
     fldlen_v=set_fldlen_varname()
     nmlname=namelist.attrib['name']
     for var in namelist.findall("variable"):
         vname=var.attrib['name'].ljust(fldlen_v)
         vnamet=nmlname+" % "+vname.strip()
         if var.find("default") is None and var.find("defaultBlock") is None:
             rvar=rvar+bb+"CALL set_to_missing( "+vnamet+" )\n"
         if var.find("default") is not None:
             default=var.find("default").text
             rvar=rvar+bb+vnamet+" = "+default+"\n"
         if var.find("defaultBlock") is not None:
             rvar=rvar+"\n" 
             dum=var.find("defaultBlock").text
             dblock=os.linesep.join([s for s in dum.splitlines() if s])
             for l in dblock.splitlines()[:]:
                 if len(l.lstrip()) > 1:
                     x=bb+nmlname+" % "+l.lstrip()
                     rvar=rvar+x+"\n"
                     print ( x )
                 rvar=rvar+"\n" 
         if var.find("default") is not None and var.find("defaultBlock") is not None:
              print ( "ERROR: specify either default or defaultBlock but not both - "+vname )
              sys.exit(5)  
     return rvar

def create_nml_get_conf( namelist, leading_blanks  ):
    bb=""  
    rvar=""
    nmlname=namelist.attrib['name']
    func="FUNCTION"
    efunc="END "+func
    for var in namelist.findall("variable"):
        vname=var.attrib['name']
        vtype=var.find("type").text
        fname=get_conf_func_name(nmlname, vname)
        if var.find("dimension") is None:
            rvar=rvar+bb+vtype+" "+func+" "+fname+"() RESULT ( "+vname+" )\n"
            rvar=rvar+bb+2*" "+vname+" = "+nmlname+" % "+vname+"\n"
        else:
            dim=var.find("dimension").text
            rvar=rvar+bb+func+" "+fname+"()\n"
            rvar=rvar+bb+" "+vtype+", DIMENSION( "+dim+") :: "+fname+"\n"
            rvar=rvar+bb+" "+fname+" = "+nmlname+" % "+vname+"\n"
        
        bb=" " * leading_blanks 
        
        rvar=rvar+bb+efunc+" "+fname+"\n\n"
    return rvar

def create_call_nml_get_conf( namelist, leading_blanks  ):
     rvar="" 
     bb=""  
     nmlname=namelist.attrib['name']
     for var in namelist.findall("variable"):
         vname=var.attrib['name']
         fname=get_conf_func_name(nmlname, vname)
         rvar=rvar+bb+vname+" = "+fname+"( )\n" 
         bb=" " * leading_blanks 
     return rvar

def create_nml_set_config( namelist, leading_blanks  ):
     bb=""  
     rvar=""
     nmlname=namelist.attrib['name']
     pref="SUBROUTINE "
     suff="END "+pref
     intent="INTENT(IN)"
     for var in namelist.findall("variable"):
         vname=var.attrib['name']
         
         vtype=var.find("type").text
         if var.find("dimension") is not None:
            vtype=vtype.rstrip()+", DIMENSION("+var.find("dimension").text+")"
         rvar=rvar+bb+pref+set_conf_subr_name(nmlname,vname)
         bb=" " * leading_blanks 
         rvar=rvar+"( "+vname+" )\n"
         rvar=rvar+bb+2*" "+vtype+", "+intent+" :: "+vname+"\n"
         rvar=rvar+bb+2*" "+nmlname+" % "+vname+" = "+vname+"\n"
         rvar=rvar+bb+suff+"\n\n"
     return rvar

def create_call_nml_set_config( namelist, leading_blanks  ):
     bb=""  
     rvar=""
     nmlname=namelist.attrib['name']
     pref="CALL set_conf_"+nmlname
     for var in namelist.findall("variable"):
         vname=var.attrib['name']
         rvar=rvar+bb+pref+"_"+vname
         rvar=rvar+" ( "+vname+" )\n"
         bb=" " * leading_blanks 
     return rvar

def create_public_nml_config( namelist, leading_blanks  ):
     pref="PUBLIC :: "
     bb=""  
     rvar=""
     nmlname=namelist.attrib['name']  
     for var in namelist.findall("variable"):
         vname=var.attrib['name']
         rvar=rvar+bb+pref+set_conf_subr_name(nmlname,vname)+"\n"
         bb=" " * leading_blanks
     rvar=rvar+"\n\n" 
     for var in namelist.findall("variable"):
         vname=var.attrib['name']
         rvar=rvar+bb+pref+get_conf_func_name(nmlname,vname)+"\n"
     return rvar

def create_nml_use_modules( namelist, leading_blanks  ):
    use="USE "
    only=" ONLY: "
    bb=""  
    rvar=""
    fldlen_t=set_fldlen_vartype()
    for module in namelist.findall("use_module"):
        dum=module.attrib['name']+","
        rvart=bb+use+dum.ljust(fldlen_t)+only
        rvar=rvar+rvart
        lindent=len(rvart)
        ivar=0
        nvar=len(module.findall("variable"))
        for var in module.findall("variable"):
            ivar=ivar+1
            if ivar < nvar:
                dum=var.text+","
                rvar=rvar+dum.ljust(fldlen_t)
                rvar=rvar+" & \n"+lindent*' '
            else:
                rvar=rvar+var.text
        bb=" " * leading_blanks
        rvar=rvar+"\n\n" 
    return rvar

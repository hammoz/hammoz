!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz, MPI fuer Meteorologie, FZJ 
!>
!! @par Copyright
!! This code is subject to the MPI-M-Software - License - Agreement in it's most recent form.
!! Please see URL http://www.mpimet.mpg.de/en/science/models/model-distribution.html and the
!! file COPYING in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the headers of the routines.
!!
!! @brief: mo_submodel_streams: manages generic diagnostic output from submodels
!!
!! @remarks
!! This module reads the submdiagctl namelist and collects the information necessary for
!! initialisatin of the following streams:
!!   - vphysc : containing physical ECHAM variables that are not part of the standard 
!!              ECHAM output
!!   - wetdep : containing variable pointers for the wetdep calculation and additional
!!              diagnostic output
!!   - drydep : containing variable pointers for the drydep calculation and additional
!!              diagnostic output
!!   - sedi:    containing variable pointers for the sedimentation calculation and
!!              additional diagnostic output
!!
!! @author
!! Martin Schultz, FZ Juelich     (1009-10-02)
!!
!!


MODULE mo_submodel_streams

  USE mo_submodel_tracdef,            ONLY: jptrac, ln
  USE mtime,                          ONLY: MAX_TIMEDELTA_STR_LEN
  USE mo_submodel_streams_constants,  ONLY: nmaxstreamvars 

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: init_submodel_streams

  PUBLIC :: nmaxstreamvars
  PUBLIC :: vphysc_lpost, vphysc_tinterval, vphyscnam
  PUBLIC :: wetdep_lpost, wetdep_tinterval, wetdepnam, wetdep_gastrac, wetdep_keytype
  PUBLIC :: drydep_lpost, drydep_tinterval, drydepnam, drydep_gastrac, drydep_keytype, drydep_ldetail, drydep_trac_detail   !++mgs20140519
  PUBLIC :: sedi_lpost, sedi_tinterval, sedinam, sedi_keytype
  PUBLIC :: emi_lpost, emi_lpost_sector, emi_tinterval, eminam, emi_gastrac, emi_keytype

  ! Module variables

  ! Note: the variables below are declared in this module rather than in the mo_<process>_interface
  ! routines to de-couple generic ECHAM code from submodel code. The mo_<process>_interface
  ! modules contain references to specific submodels while mo_submodel_streams is supposed
  ! to be generic.
  ! vphysc
  LOGICAL,                              SAVE    :: vphysc_lpost
  CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN), SAVE    :: vphysc_tinterval
  CHARACTER(LEN=32),                    SAVE    :: vphyscnam(nmaxstreamvars)
  ! wetdep
  LOGICAL,                              SAVE    :: wetdep_lpost
  CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN), SAVE    :: wetdep_tinterval
  CHARACTER(LEN=32),                    SAVE    :: wetdepnam(nmaxstreamvars)
  CHARACTER(LEN=ln),                    SAVE    :: wetdep_gastrac(jptrac)
  INTEGER,                              SAVE    :: wetdep_keytype
  ! drydep
  LOGICAL,                              SAVE    :: drydep_lpost
  LOGICAL,                              SAVE    :: drydep_ldetail     ! ++mgs 20140519
  CHARACTER(LEN=ln),                    SAVE    :: drydep_trac_detail
  CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN), SAVE    :: drydep_tinterval
  CHARACTER(LEN=32),                    SAVE    :: drydepnam(nmaxstreamvars)
  CHARACTER(LEN=ln),                    SAVE    :: drydep_gastrac(jptrac)
  INTEGER,                              SAVE    :: drydep_keytype
  ! sedimentation
  LOGICAL,                              SAVE    :: sedi_lpost
  CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN), SAVE    :: sedi_tinterval
  CHARACTER(LEN=32),                    SAVE    :: sedinam(nmaxstreamvars)
  INTEGER,                              SAVE    :: sedi_keytype
  ! emissions
  LOGICAL,                              SAVE    :: emi_lpost
  LOGICAL,                              SAVE    :: emi_lpost_sector
  CHARACTER(LEN=MAX_TIMEDELTA_STR_LEN), SAVE    :: emi_tinterval
  CHARACTER(LEN=32),                    SAVE    :: eminam(nmaxstreamvars)    ! probably obsolete
  CHARACTER(LEN=ln),                    SAVE    :: emi_gastrac(jptrac)
  INTEGER,                              SAVE    :: emi_keytype



  CONTAINS

  SUBROUTINE init_submodel_streams

  !! imports submdiagctl namelist and initializes various generic submodel diagnostics
    USE mo_exception,                 ONLY: finish, message, em_warn, em_info
    USE mo_util_string,               ONLY: separator

    USE mo_submdiagctl_config,        ONLY: submdiagctl
    USE mo_config_util,               ONLY: set_config

    USE mo_submodel,                  ONLY: ldrydep, lemissions, lwetdep

    eminam(:)=''
    drydepnam(:)=''
    sedinam(:)=''
    eminam(:)=''
    wetdepnam(:)=''
    vphyscnam(:)=''

    CALL message('',separator)
    CALL message('submodel_streams', 'Importing variables from namelist submdiagctl...', level=em_info)

    !--- vphysc 
    CALL set_config( vphysc_lpost, submdiagctl % vphysc_lpost )
    CALL set_config( vphysc_tinterval, submdiagctl % vphysc_tinterval ) 
    CALL set_config( vphyscnam, submdiagctl % vphyscnam )

    !--- wetdep
    CALL set_config( wetdep_lpost, submdiagctl % wetdep_lpost )
    CALL set_config( wetdep_tinterval, submdiagctl % wetdep_tinterval ) 
    CALL set_config( wetdepnam, submdiagctl % wetdepnam )
    CALL set_config( wetdep_gastrac,  submdiagctl % wetdep_gastrac )
    CALL set_config( wetdep_keytype, submdiagctl % wetdep_keytype )

    !--- drydep
    CALL set_config( drydep_lpost, submdiagctl % drydep_lpost )
    CALL set_config( drydep_tinterval, submdiagctl % drydep_tinterval ) 
    CALL set_config( drydepnam, submdiagctl % drydepnam )
    CALL set_config( drydep_gastrac,  submdiagctl % drydep_gastrac )
    CALL set_config( drydep_keytype, submdiagctl % drydep_keytype )
    CALL set_config( drydep_ldetail, submdiagctl % drydep_ldetail )
    CALL set_config( drydep_trac_detail, submdiagctl % drydep_trac_detail )

    !--- sedimentation
    CALL set_config( sedi_lpost, submdiagctl % sedi_lpost )
    CALL set_config( sedi_tinterval, submdiagctl % sedi_tinterval ) 
    CALL set_config( sedinam, submdiagctl % sedinam )
    CALL set_config( sedi_keytype, submdiagctl % sedi_keytype )

    !--- emissions
    CALL set_config( emi_lpost, submdiagctl % emi_lpost )
    CALL set_config( emi_lpost_sector, submdiagctl % emi_lpost_sector )
    CALL set_config( emi_tinterval, submdiagctl % emi_tinterval ) 
    CALL set_config( eminam, submdiagctl % eminam )
    CALL set_config( emi_gastrac,  submdiagctl % emi_gastrac )
    CALL set_config( emi_keytype, submdiagctl % emi_keytype )


    !--- turn off lpost flags if submodel process flags are FALSE
    IF (drydep_lpost .AND. .NOT. ldrydep) THEN
      drydep_lpost = .FALSE.
      CALL message('init_submodel_streams', 'drydep_lpost turned FALSE because ldrydep=FALSE!', &
                   level=em_warn)
    END IF
    IF (emi_lpost .AND. .NOT. lemissions) THEN
      emi_lpost = .FALSE.
      CALL message('init_submodel_streams', 'emi_lpost turned FALSE because lemissions=FALSE!', &
                   level=em_warn)
    END IF
    IF (emi_lpost_sector .AND. .NOT. lemissions) THEN
      emi_lpost_sector = .FALSE.
      CALL message('init_submodel_streams', 'emi_lpost_sector turned FALSE because lemissions=FALSE!', &
                   level=em_warn)
    END IF
    IF (wetdep_lpost .AND. .NOT. lwetdep) THEN
      wetdep_lpost = .FALSE.
      CALL message('init_submodel_streams', 'wetdep_lpost turned FALSE because lwetdep=FALSE!', &
                   level=em_warn)
    END IF

    ! Initialisation of streams has to be called from mo_submodel_interface to avoid
    ! dependencies on individual submodels in this module.

  END SUBROUTINE init_submodel_streams

END MODULE mo_submodel_streams

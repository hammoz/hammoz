!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!>
!! Description:  Contains the data structures
!!  to store the HAMMOZ EMISSION fields
!!
!! @author Marc Salzmann, LIM
!!
!! @par Revision History
!! initial version by Marc Salzmann (2017-06-05)
!
!! @par Copyright and License
!!
MODULE mo_hammoz_emission_types

 USE mo_emi_matrix,          ONLY: maxsectors
 USE mo_submodel_diag,       ONLY: t_diag_list

 IMPLICIT NONE

 PRIVATE

 PUBLIC :: t_hammoz_emissions

 TYPE t_hammoz_emissions
   TYPE (t_diag_list) :: emi_diag                      ! emissions mass flux diagnostics (total)
   TYPE (t_diag_list) :: emi_diag_detail(maxsectors)   ! diagnostics per sector
 END TYPE t_hammoz_emissions

END MODULE mo_hammoz_emission_types


remap_accmip_interpolated(){

yys=1999
yye=2000
exp0=historic # other experiments are linked
set -A specs BC OC SO2 C2H6S
set -A source_types anthropogenic aircraft biomassburning ships
irpath=emissions_inventories/accmip_interpolated

yy=$yys
while [ $yy -le $yye ]; do # year loop
  for source_type in ${source_types[*]}; do # source type loop
    for spec in ${specs[*]}; do # species loop
      fname_in=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${hres_in}.nc
      ifilen=$irpath/${hres_in}/${yy}/$fname_in
      if [ -e $idir/$ifilen ]; then
	 fname_out=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 ofilen=$irpath/${grid_name_out}/${yy}/$fname_out
	 cdo_remap_con $ifilen $ofilen
	 expl="RCP85"
	 fname_link=accmip_interpolated_emissions_${expl}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 ln_file $irpath/${grid_name_out}/${yy} $fname_out $fname_link
      #else
      #  echo "NO FILE " $idir/$ifilen
      fi
    done # species loop
  done # source type loop
  (( yy = $yy + 1 ))
done # year loop


yys=2001
yye=2015
exp0=RCP85 # other experiments are linked
set -A specs BC OC SO2 C2H6S
set -A source_types anthropogenic aircraft biomassburning ships
irpath=emissions_inventories/accmip_interpolated

yy=$yys
while [ $yy -le $yye ]; do # year loop
  for source_type in ${source_types[*]}; do # source type loop
    for spec in ${specs[*]}; do # species loop
      fname_in=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${hres_in}.nc
      ifilen=$irpath/${hres_in}/${yy}/$fname_in
      if [ -e $idir/$ifilen ]; then
	 fname_out=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 ofilen=$irpath/${grid_name_out}/${yy}/$fname_out
	 cdo_remap_con $ifilen $ofilen

	 #expl="RCP45"
	 #fname_link=accmip_interpolated_emissions_${expl}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 #ln_file $irpath/${grid_name_out}/${yy} $fname_out $fname_link
      #else
      #  echo "NO FILE " $idir/$ifilen
      fi
    done # species loop
  done # source type loop
  (( yy = $yy + 1 ))
done # year loop


yys=1850
yye=1850
exp0=historic # other experiments are linked
set -A specs BC OC SO2 C2H6S
set -A source_types anthropogenic aircraft  ships
irpath=emissions_inventories/accmip_interpolated

yy=$yys
while [ $yy -le $yye ]; do # year loop
  for source_type in ${source_types[*]}; do # source type loop
    for spec in ${specs[*]}; do # species loop
      fname_in=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${hres_in}.nc
      ifilen=$irpath/${hres_in}/${yy}/$fname_in
      if [ -e $idir/$ifilen ]; then
	 fname_out=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 ofilen=$irpath/${grid_name_out}/${yy}/$fname_out
	 cdo_remap_con $ifilen $ofilen
      fi
    done # species loop
  done # source type loop
  (( yy = $yy + 1 ))
done # year loop


yys=1900
yye=1900
exp0=historic # other experiments are linked
set -A specs BC OC SO2 C2H6S
set -A source_types biomassburning 
irpath=emissions_inventories/accmip_interpolated

yy=$yys
while [ $yy -le $yye ]; do # year loop
  for source_type in ${source_types[*]}; do # source type loop
    for spec in ${specs[*]}; do # species loop
      fname_in=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${hres_in}.nc
      ifilen=$irpath/${hres_in}/${yy}/$fname_in
      if [ -e $idir/$ifilen ]; then
	 fname_out=accmip_interpolated_emissions_${exp0}_${spec}_${source_type}_${yy}_${grid_name_out}.nc
	 ofilen=$irpath/${grid_name_out}/${yy}/$fname_out
	 cdo_remap_con $ifilen $ofilen
      fi
    done # species loop
  done # source type loop
  (( yy = $yy + 1 ))
done # year loop


}

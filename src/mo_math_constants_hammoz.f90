!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_math_constants_hammoz

  USE mo_kind,      ONLY:  wp

  IMPLICIT NONE

  PUBLIC

  !pi/6
  REAL(wp), PARAMETER :: pi_6      =  0.523598775598298873077107230546583814032862_wp 

END MODULE mo_math_constants_hammoz

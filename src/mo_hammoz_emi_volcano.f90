!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>
!! \filename
!! mo_hammoz_emi_volcano.f90
!!
!! \brief
!! handles emissions from continuously degassing and explosive volcanoes
!!
!! \author M. Schultz (FZ Juelich)
!!
!! \responsible_coder
!! M. Schultz, m.schultz@fz-juelich.de
!!
!! \revision_history
!!   -# M. Schultz (FZ Juelich) - original code (2010-02-25)
!!   -# M. Salzmann (LIM) - adapted for ICON (2017-07-28)
!!
!! \limitations
!! input files have to be the reformatted ASCII files
!! (reformatted from original to include header lines)
!!
!! \details
!! Input data: AEROCOM climatology of SO2 emissions
!! Emissions are provided with lon and lat coordinates and a min and max altitude
!! for each cell. They are mapped onto the current ECHAM grid and stored as
!! a 3-dimensional boundary condition.
!! This code was originally developed by Philip Stier.
!! It has been almost entirely rewritten in order to allow for a commented
!! ascii file and to make use of the boundary condition concept.
!! Major conceptual change: emissions are now distributed onto the ECHAM grid once
!! at the model startup (formerly, the altitude->model level conversion was done
!! at every time step). Once the boundary condition scheme can store fields
!! on native altitude grids, this can be changed again, but I doubt that it makes
!! much difference.
!! The old code had a "feature" which has been corrected in this version: when two
!! lines in the ascii file would point to the same lon and lat coordinates, the 
!! minlevel and maxlevel fields used to be overwritten (so the first volcano would
!! emit at the levels of the second one). The new code can accumulate emission fluxes
!! for all volcanoes independently.
!! Currently, a climatology is used for the emissions by 'explosive' volcanoes. This makes
!! them so-called 'continously explosive' or 'steady-state explosive' volcanoes. In contrast
!! to the so-called 'instantenous explosive volcanoes', these volcanoes are in a continuous 
!! state of explosion. 
!! \bibliographic_references
!! None
!!
!! \belongs_to
!!  HAMMOZ
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mo_hammoz_emi_volcano

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: init_emi_volcano
  PUBLIC :: read_emi_volcano

  INTEGER            :: ibc_volcc, ibc_volce      ! Index to boundary conditions

CONTAINS

  SUBROUTINE init_emi_volcano(jg, ktype)

  USE mo_exception,            ONLY: finish
  USE mo_boundary_condition,   ONLY: bc_find, bc_modify, BC_EVERYWHERE

  IMPLICIT NONE

  INTEGER, INTENT( IN )          :: jg     ,& ! domain/grid index
                                  & ktype     ! type of emissions to initialize:
                                              ! 1 = continuous degassing
                                              ! 2 = explosive

  CHARACTER(LEN=64)            :: bc_name
  INTEGER                      :: ibc     ! index to boundary condition

  IF (ktype < 1 .OR. ktype > 2) CALL finish('init_emi_volcano',      &
                                            'Invalid value of ktype! (Must be 1 or 2)')
  !-- locate corresponding boundary condition
  !   bc_define was called from init_emissions in mo_emi_interface
  IF (ktype == 1) THEN
    bc_name = 'VOLCC emissions of SO2'
  ELSE
    bc_name = 'VOLCE emissions of SO2'
  END IF
  CALL bc_find(jg, bc_name, ibc)
  IF (ibc < 1) CALL finish('init_emi_volcano', 'Cannot find boundary condition for '//TRIM(bc_name))
  IF (ktype == 1) THEN
    ibc_volcc = ibc
  ELSE
    ibc_volce = ibc
  END IF
  !-- modify boundary condition to fit the specific needs of this module
  CALL bc_modify(jg, ibc, bc_domain=BC_EVERYWHERE, bc_ndims=3, ef_actual_unit='kg m-2 s-1')


  END SUBROUTINE init_emi_volcano

  !---------------------------------------------------------------------------------------
  ! read the ascii file with the volcanic SO2 emission fluxes and distribute values on
  ! ECHAM 3D grid
  !!! NOTE: in future versions of HAMMOZ the values shall be stored on a native altitude 
  !!! grid and interpolated onto ECHAM grid at each time step (this corresponds to the ECHAM philosophy)
  !!! Need to decide on spacing of native grid (dz = 200 m ?, top = 12 km?)
  SUBROUTINE read_emi_volcano( jg, p_patch, pareacella, pclon, pclat, pzh, ktype, lprint_info )

  USE mo_kind,                 ONLY: dp
  USE mo_exception,            ONLY: finish, message, message_text, em_info, em_error, em_warn
  USE mo_parallel_config,      ONLY: nproma, p_test_run
  USE mo_run_config,           ONLY: nlev
  USE mo_mpi,                  ONLY: p_pe, p_io, p_bcast, p_comm_work
  USE mo_model_domain,         ONLY: t_patch
  USE mo_gnat_gridsearch,      ONLY: t_gnat_tree, gnat_init_grid, gnat_query_containing_triangles, &
                                   & gnat_merge_distributed_queries, gk
  USE mo_grid_config,          ONLY: grid_sphere_radius
  USE mo_impl_constants,       ONLY: min_rlcell_int, grf_bdywidth_c
  USE mo_loopindices,          ONLY: get_indices_c
  USE mo_sync,                 ONLY: global_sum_array

  USE mo_boundary_condition,   ONLY: bc_set

  IMPLICIT NONE

  TYPE(t_patch), TARGET, INTENT( IN )    :: p_patch

  INTEGER, INTENT( IN )          :: jg     ! grid/domain index
  INTEGER, INTENT( IN )          :: ktype  ! type of emissions to initialize:
                                           ! 1 = continuous degassing
                                           ! 2 = explosive

  REAL(dp), INTENT( IN )         ::         &
                        & pareacella(:,:) , & !< grid box area, dimensions: nproma, nblk
                        & pzh(:,:,:)      , & ! geometric height at half levels, dimensions: nproma, klevp1, nblk 
                        & pclon(:,:)      , & ! longitude, dimensions: nproma, nblk 
                        & pclat(:,:)          ! latitude, dimensions: nproma, nblk 

  ! print some info?
  LOGICAL, INTENT( IN ), OPTIONAL  :: lprint_info

  ! local
  CHARACTER(LEN=256)       :: cfilename, clabel
  CHARACTER(len=22)        :: formstr = '(2(1x,i4),3(1x,e10.4))'
  CHARACTER(len=30)        :: units
  INTEGER                  :: j, jk, jvolc, ibc, ilower, iupper, klon, klat, i, l
                              ! note: iupper and ilower refer to altitude, not level index!

  REAL(dp)                 :: zsumemi, zfac 

  REAL(dp), ALLOCATABLE    :: emivolc(:,:,:)

  REAL(dp), POINTER        :: gl2d(:,:), gl3d(:,:,:)
  REAL(dp), ALLOCATABLE    :: zlats(:), zlons(:), zemiss(:),     &    ! fields for reading
                              zheights_low(:), zheights_high(:)

  LOGICAL, ALLOCATABLE :: ll1(:) !SF #458 for replacing the former WHERE statement

  INTEGER, PARAMETER       :: maxlines=2000      ! max. number of lines in file
  INTEGER, PARAMETER       :: ncols   =5         ! number of columns in file
  INTEGER                  :: nrows              ! number of data values in file
  REAL(dp), ALLOCATABLE    :: zbuf(:,:)

  TYPE (t_gnat_tree)  :: gnat
  REAL(dp) :: start_radius, pi_180
  INTEGER  :: npromz_lonlat, nblks_lonlat

  REAL(gk), ALLOCATABLE    :: llpoints(:,:,:) , &
                            & min_dist(:,:)
 
  INTEGER, ALLOCATABLE     :: tri_idx(:,:,:), & 
                            & global_idx(:)
 
  INTEGER   :: nblks_c

  INTEGER  :: i_nchdom, rl_start, rl_end
  INTEGER  :: jc, jcs, jce         !< cell in row index, start and end indices

  INTEGER  :: i_startblk, i_endblk, jb, lnproma

  INTEGER :: npts_icon, nthis_local_pts 
 
  LOGICAL :: lprint_info_l 
 
  CHARACTER(lEN=*), PARAMETER ::thismodule='mo_hammoz_emi_volcano'

  ! print some info to screen?
  IF ( PRESENT ( lprint_info ) ) THEN
    lprint_info_l = lprint_info
  ELSE
    lprint_info_l = .FALSE.
  END IF

  ! Inquire current grid level and the total number of grid cells (code snippet based on mo_interface_iconam echam)
  i_nchdom = MAX(1,p_patch%n_childdom)
  rl_start = grf_bdywidth_c+1
  rl_end   = min_rlcell_int

  i_startblk = p_patch%cells%start_blk(rl_start,1)
  i_endblk   = p_patch%cells%end_blk(rl_end,i_nchdom)


  nblks_c = p_patch%nblks_c

  pi_180 = ATAN(1._dp)/45._dp

  IF (ktype < 1 .OR. ktype > 2) CALL finish('init_emi_volcano',      &
                                            'Invalid value of ktype! (Must be 1 or 2)')
  !-- allocation & initialisation
  ALLOCATE ( emivolc ( nproma, nlev, nblks_c ) )

  emivolc(:,:,:) = 0._dp

  !-- locate corresponding boundary condition
  IF (ktype == 1) THEN
    ibc = ibc_volcc
    cfilename = 'continuous_volcanos.dat'
    clabel = 'SO2 emissions from continuous volcanic degassing'
  ELSE
    ibc = ibc_volce
    cfilename = 'explosive_volcanos.dat'
    clabel = 'SO2 emissions from explosive volcanic eruptions'
  END IF
  cfilename = TRIM(cfilename)

  CALL message('init_emi_volcano', 'Reading volcanic SO2 emissions from file '//TRIM(cfilename), &
               level=em_info)
  
  ALLOCATE (zbuf(maxlines,ncols)) 
  IF ( p_pe == p_io ) THEN
    CALL read_ascii_file(cfilename, ncols, maxlines, nrows, zbuf, formstr, 'init_emi_volcano', units)
    IF (TRIM(ADJUSTL(units)) .ne. 'kg y-1') THEN
      WRITE(message_text,'(a,a)') 'units of input file should be [kg y-1], units found are: ', TRIM(units)
      CALL finish('read_emi_volcano', message_text)
    END IF
  END IF 
  CALL p_bcast(zbuf, p_io, p_comm_work)
  CALL p_bcast(nrows, p_io, p_comm_work)


  ! distribute zbuf into vectors
  ALLOCATE( zlons(nrows)         ,&
          & zlats(nrows)         ,&
          & zemiss(nrows)        ,&
          & zheights_low(nrows)  ,&
          & zheights_high(nrows)  )
  
  zlons(:)         = zbuf(1:nrows,1)
  zlats(:)         = zbuf(1:nrows,2)
  zemiss(:)        = zbuf(1:nrows,3)
  zheights_low(:)  = zbuf(1:nrows,4)
  zheights_high(:) = zbuf(1:nrows,5)
  

  ! get total emissions
  zsumemi=0._dp
  DO j=1,nrows
    zsumemi = zsumemi + zemiss(j)
  END DO
  WRITE(message_text,'(a,e25.15,a)') 'Global total '//TRIM(clabel)//' = ',zsumemi,' kg(SO2) year-1'
  CALL message('read_emi_volcano', message_text, level=em_info)


  ! distribute emissions onto ICON grid
  !--- find indices
  start_radius = grid_sphere_radius

  nblks_lonlat  = (nrows - 1)/nproma + 1          ! number of blocks
  npromz_lonlat = nrows - (nblks_lonlat-1)*nproma ! end point in last block

  ALLOCATE( llpoints( nproma, nblks_lonlat, 2) ,&
          & tri_idx(2, nproma, nblks_lonlat )  ,&
          & min_dist( nproma, nblks_lonlat)    ,&
          & global_idx( nrows ) )

  
  global_idx(:) = -1
  tri_idx(:,:,:) = -1

  min_dist=90._gk

  llpoints(:,:,1) = RESHAPE(REAL(pi_180 * zlons, gk), (/ nproma, nblks_lonlat /), (/ 0._gk /) )
  llpoints(:,:,2) = RESHAPE(REAL(pi_180 * zlats, gk), (/ nproma, nblks_lonlat /), (/ 0._gk /) )

  CALL  gnat_init_grid(gnat, p_patch)

  CALL gnat_query_containing_triangles(gnat, p_patch, llpoints(:,:,:),            &
       &                 nproma, nblks_lonlat, npromz_lonlat, start_radius,       &
       &                 p_test_run, tri_idx(:,:,:), min_dist(:,:)                )


  CALL gnat_merge_distributed_queries ( p_patch, nrows, nproma, nblks_lonlat       ,&
                                      & min_dist, tri_idx(:,:,:), llpoints(:,:,:)  ,&
                                      & global_idx(:), nthis_local_pts              )

  ! if requested, print info to screen
  IF ( lprint_info_l ) THEN
    l=1
    CALL message(thismodule,'Placing volcanoes', level=em_info)
    DO j=1,nblks_lonlat
      DO i=1,nproma
        IF ( tri_idx(1, i,j) > 0 .AND. l <= nthis_local_pts  ) THEN 
            WRITE(message_text,FMT='(i5.5,2(1X,F7.2),A5,2(1X,F7.2))')               &
             &     p_pe, zlons(global_idx(l)),  zlats(global_idx(l))               ,&
             &  ' --- ', pclon( tri_idx(1,i,j),tri_idx(2,i,j))/pi_180              ,& 
             &           pclat( tri_idx(1,i,j),tri_idx(2,i,j))/pi_180         
            l=l+1 
            CALL message(thismodule, message_text, all_print=.TRUE., level=em_info)
        END IF
      END DO
    END DO            
  END IF

  npts_icon=global_sum_array(nthis_local_pts)
  IF ( npts_icon > nrows ) THEN
    WRITE(message_text,*) 'Too many volcanoes placed on grid', nrows, '<', npts_icon 
    CALL finish(thismodule, message_text)
  END IF
  IF ( npts_icon < nrows ) THEN
    WRITE(message_text,*) 'Not all volcanoes placed on grid', nrows, '>', npts_icon 
    CALL finish(thismodule, message_text)
  END IF

  ! vertical distribution
  l=1
  DO j=1,nblks_lonlat
    DO i=1,nproma
      IF ( tri_idx(1, i,j) > 0  .AND. l <= nthis_local_pts ) THEN   
        jc=tri_idx(1,i,j)
        jb=tri_idx(2,i,j)
        jvolc=global_idx(l)
        !--- Find model levels corresponding to min and max altitude of emissions  
        ilower = -1
        DO jk = 1, nlev
          IF (zheights_low(jvolc) <= pzh(jc,jk,jb)) THEN
            ilower=jk+1
          END IF
        END DO
        IF (ilower > nlev) ilower = nlev
        
        iupper = nlev
        DO jk = nlev, 1, -1
          IF (zheights_high(jvolc) >= pzh(jc,jk,jb)) THEN
            iupper=jk
          END IF
        END DO 
        IF (ilower == -1) THEN
          WRITE(message_text,'(a,i0,e25.15,a,i0,i0,e25.15)') 'ilower=-1 for jvolc, zheights_low = ',jvolc,zheights_low(jvolc), &
                                                             'jc,jb,zh=',jc,jb,pzh(jc,:,jb)
          CALL message('read_emi_volcano', message_text, level=em_warn)
        END IF

        IF (ilower < iupper) THEN
          WRITE(message_text,'(a,i0)') 'Error in emission levels! File '//TRIM(cfilename)//', line=',jvolc
          CALL message('read_emi_volcano', message_text, level=em_error)
        END IF

        !--- Enter portion of emission flux in each affected grid cell
        !    Unit conversion from [kg(SO2) grid_cell-1 year-1] to [kg(SO2) m-2 s-1]
        zfac = 1._dp/REAL(ilower-iupper+1, kind=dp)
        DO jk=iupper,ilower
           emivolc(jc, jk, jb) = emivolc(jc, jk, jb)             &
                            &   + zfac*zemiss(jvolc)/(pareacella(jc,jb)*86400._dp*365._dp)
        END DO

        l=l+1

      END IF 
    END DO
  END DO

   
  DO jb = i_startblk,i_endblk
    !CALL get_indices_c(p_patch, jb, i_startblk, i_endblk, jcs,jce, rl_start, rl_end)
    CALL bc_set(jg, ibc, nproma, jb, emivolc(:,:,jb))
  END DO

  DEALLOCATE( emivolc, zbuf,zheights_low,zheights_high,zemiss,zlats,zlons, &
            &  llpoints, tri_idx, min_dist, global_idx )


  END SUBROUTINE read_emi_volcano

   
  SUBROUTINE read_ascii_file (filename, ncols, maxcount, ncount, buffer, formatstr, calling_routine, units)
  ! open and read a plain format ascii data file
  ! The file may contain comment lines (starting with '#')
  ! filename, number of columns and maximum number of lines must be provided,
  ! and a buffer array with sufficient capacity (dimension maxlines, ncols).
  ! The subroutine will return the number of lines that were actually read.
  ! Maximum number of columns is 32.
  !
  ! Note: the formatstr is not used at present as this led to an error on the IBM power 6

  USE mo_kind,          ONLY: dp
  USE mo_exception,     ONLY: finish
  USE mo_io_units,      ONLY: find_next_free_unit
  USE mo_mpi,           ONLY: p_pe, p_io

  IMPLICIT NONE

  ! Arguments
  CHARACTER(len=*), INTENT( IN )    :: filename, formatstr, calling_routine
  INTEGER, INTENT( IN )             :: ncols, maxcount
  INTEGER, INTENT( OUT )            :: ncount
  REAL(dp), INTENT( INOUT )         :: buffer(maxcount, ncols)
  CHARACTER(len=*), INTENT(inout) :: units

  ! Local variables
  INTEGER             :: iunit, ierr, ilines
  LOGICAL             :: lex
  INTEGER, PARAMETER  :: icolmax = 32
  REAL(dp)            :: zvalbuf(icolmax)
  CHARACTER(len=255)  :: linebuf
  
  REAL(dp), PARAMETER :: missv = -999.e7_dp 

  buffer(:,:) = 0._dp
  ncount = 0           ! number of lines infile
  ilines = 0           ! number of lines in file (including comments)
  linebuf = '                                                                                      '

  zvalbuf = missv

  IF (p_pe == p_io ) THEN
    iunit = find_next_free_unit (20, 99)
    INQUIRE (file=filename,exist=lex)
    IF (.NOT. lex) THEN
      CALL finish( calling_routine,   &
            'Failed to open file '//TRIM(filename)//'. File does not exist or is not accessible.')
    END IF
    ! Open file
    OPEN (iunit,file=TRIM(filename),form='FORMATTED',iostat=ierr)
    IF (ierr /= 0) CALL finish('read_ascii_file', 'Could not open file '//TRIM(filename))
    REWIND(iunit)
    DO WHILE (ierr == 0)
      READ(iunit,FMT='(a)',IOSTAT=ierr) linebuf
      ilines = ilines + 1
      IF (ierr < 0) EXIT     ! eof reached
      IF (ierr > 0) THEN
        write(linebuf,'(i0)') ilines
        CALL finish( calling_routine, &
                'Failed to read line '//TRIM(linebuf)//' in file '//TRIM(filename) )
      END IF
      ! analyze ascii line
      IF (linebuf(1:1) == '#') CYCLE   ! comment line
      IF (linebuf(1:6) == 'units:') THEN
        units=TRIM(linebuf(7:))
        CYCLE
      ENDIF
!       READ(linebuf,FMT=formatstr,IOSTAT=ierr) zvalbuf(1:ncols)
      READ(linebuf,FMT=*,IOSTAT=ierr) zvalbuf(1:ncols)
      IF (ierr /= 0) THEN
        write(linebuf,'(i0)') ilines
        CALL finish( calling_routine, &
                     'Failed to read data in line '//TRIM(linebuf)//' in file '//TRIM(filename) )
      END IF
      ncount = ncount + 1
      IF (ncount > maxcount) THEN
        write(linebuf,'(i0)') maxcount
        CALL finish( calling_routine, &
                    'Error reading file '//TRIM(filename)//': number of lines exceeds '//linebuf )
      END IF
      buffer(ncount, 1:ncols) = zvalbuf(1:ncols)
    END DO

    CLOSE(iunit)
  END IF

  END SUBROUTINE read_ascii_file

END MODULE mo_hammoz_emi_volcano


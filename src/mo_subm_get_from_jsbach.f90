!! SPDX-License-Identifier: BSD-3-Clause
!! Copyright (c) 2021 hammoz
MODULE mo_subm_get_from_jsbach

  USE mo_kind,               ONLY: wp
  USE mo_boundary_condition, ONLY: bc_set
  USE mo_exception,          ONLY: finish, message_text

  IMPLICIT NONE

  PRIVATE
    
  PUBLIC :: get_soil_moisture_from_jsbach,          &
            get_soil_field_capacity_from_jsbach,    &  
            get_snow_cover_bc_from_jsbach,          &
            get_surface_wet_fraction_from_jsbach,   &
            get_forest_fraction_from_jsbach,        &
            get_wsn_critical_from_jsbach,           &
            get_lai_from_jsbach,                    &
            get_heating_by_melting_snow_from_jsbach

  CHARACTER(LEN=*), PARAMETER :: thismodule='mo_subm_get_from_jsbach'

  REAL(wp), POINTER :: ws_l_jsb(:,:,:)          => NULL(),  & ! pointer to 3-d soil moisture in jsbach
                       ws_fc_l_jsb(:,:,:)       => NULL(),  & ! pointer to 3-d soil capacity in jsbach
                       sfract_srf_jsb(:,:)      => NULL(),  & ! pointer to surface snow fraction in jsbach
                       wfract_srf_jsb(:,:)      => NULL(),  & ! pointer to surface wet fraction in jsbach
                       fract_forest_jsb(:,:)    => NULL(),  & ! pointer to forest fraction in jsbach
                       lai_jsb(:,:)             => NULL(),  & ! pointer to leaf area index in jsbach
                       q_snowmelt_jsb(:,:)      => NULL()     ! pointer to heating by melting of snow

  INTERFACE  set_ptr_jsb
    MODULE PROCEDURE set_ptr2d_jsb
    MODULE PROCEDURE set_ptr3d_jsb
  END INTERFACE  set_ptr_jsb

CONTAINS

  SUBROUTINE get_soil_moisture_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow
    
    IF ( .NOT. ASSOCIATED( ws_l_jsb ) ) THEN
      CALL set_ptr_jsb(  'hydro_w_soil_sl_box','hydro', ws_l_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow,  ws_l_jsb(:,1,krow) )
    
  END SUBROUTINE get_soil_moisture_from_jsbach

 SUBROUTINE get_soil_field_capacity_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow
    
    IF ( .NOT. ASSOCIATED( ws_fc_l_jsb ) ) THEN
      CALL set_ptr_jsb(  'hydro_w_soil_fc_sl_box','hydro', ws_fc_l_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow,  ws_fc_l_jsb(:,1,krow) )
    
  END SUBROUTINE get_soil_field_capacity_from_jsbach
 
  SUBROUTINE get_snow_cover_bc_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow
    
    IF ( .NOT. ASSOCIATED( sfract_srf_jsb ) ) THEN
      CALL set_ptr_jsb(  'hydro_fract_snow_box','hydro', sfract_srf_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow, sfract_srf_jsb(:,krow) )
    
  END SUBROUTINE get_snow_cover_bc_from_jsbach

  SUBROUTINE get_surface_wet_fraction_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow

    IF ( .NOT. ASSOCIATED( wfract_srf_jsb ) ) THEN
      CALL set_ptr_jsb(  'hydro_fract_water_box','hydro', wfract_srf_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow, wfract_srf_jsb(:,krow) )

  END SUBROUTINE get_surface_wet_fraction_from_jsbach

  SUBROUTINE get_forest_fraction_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow

    IF ( .NOT. ASSOCIATED( fract_forest_jsb ) ) THEN
      CALL set_ptr_jsb(  'pheno_fract_forest_box','pheno', fract_forest_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow, fract_forest_jsb(:,krow) )

  END SUBROUTINE get_forest_fraction_from_jsbach
 
  SUBROUTINE get_lai_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow
    
    IF ( .NOT. ASSOCIATED( lai_jsb ) ) THEN
      CALL set_ptr_jsb(  'pheno_lai_box','pheno', lai_jsb )
    END IF

    CALL bc_set(jg, jbc, kproma, krow,  lai_jsb(:,krow) )
    
  END SUBROUTINE get_lai_from_jsbach

 SUBROUTINE get_heating_by_melting_snow_from_jsbach( jg, jbc, kproma, krow )

    INTEGER,  INTENT( IN  )  :: jg, jbc, kproma, krow
    
    IF ( .NOT. ASSOCIATED( q_snowmelt_jsb ) ) THEN
      CALL set_ptr_jsb(  'hydro_snowmelt_box','hydro',  q_snowmelt_jsb)
    END IF

    CALL bc_set(jg, jbc, kproma, krow,  q_snowmelt_jsb(:,krow) )
    
  END SUBROUTINE get_heating_by_melting_snow_from_jsbach
!=====================================================================================

  FUNCTION get_wsn_critical_from_jsbach() RESULT( wsn_critical_out )
!cms++ temporary
  REAL(wp)                      :: wsn_critical_out
  wsn_critical_out=0._wp 
  CALL finish(thismodule, "NEEDS TO BE IMPLEMENTED") 
!cms-- temporary

!!$#ifndef __NO_JSBACH__
!!$    ! wsn_critical is read in via namelist
!!$    USE mo_jsb_class,        ONLY: get_model
!!$    USE mo_jsb_model_class,  ONLY: t_jsb_model
!!$    USE mo_SRF_config_class, ONLY: t_SRF_config
!!$    USE mo_jsb_process_factory, ONLY: SRF, Get_process_config
!!$    REAL(wp)                      :: wsn_critical_out
!!$    TYPE(t_jsb_model),  POINTER   :: model
!!$    TYPE(t_SRF_config), POINTER   :: SRF__conf
!!$    model => get_model(1) ! 1=model id
!!$    CALL Get_process_config(model%processes(SRF)%p%config, SRF__conf)
!!$    IF ( SRF__conf%wsn_critical <= 0._wp ) THEN
!!$      CALL finish(thismodule,'Failed to obtain wsn_critical from JSBACH')
!!$    END IF
!!$    wsn_critical_out=SRF__conf%wsn_critical
!!$#else
!!$      REAL( wp ) :: wsn_critical_out
!!$      wsn_critical_out=0._wp 
!!$      CALL finish(thismodule, "Please compile with jsbach") 
!!$#endif 
  END FUNCTION get_wsn_critical_from_jsbach

!=====================================================================================

  SUBROUTINE set_ptr2d_jsb( varname, component, ptr ) 

    USE mo_linked_list,     ONLY: t_var_list
    USE mo_var_list,        ONLY: get_var_list, get_var

#ifndef __NO_JSBACH__
    USE mo_jsb_control,     ONLY: jsb_models_nml
#endif

    CHARACTER(LEN=*),  INTENT( IN )     :: varname,  component
    REAL(wp), POINTER, INTENT( OUT )    :: ptr(:,:) 

    TYPE(t_var_list), POINTER    :: jsb_var_list

    CHARACTER(LEN=50) :: jsb_list_name

#ifndef __NO_JSBACH__
     WRITE(jsb_list_name,'(A)') TRIM(jsb_models_nml(1)%model_shortname)//'_'//TRIM(component)
     CALL get_var_list( jsb_var_list, TRIM(jsb_list_name) )
     IF ( .NOT. ASSOCIATED( jsb_var_list ) ) THEN
       WRITE(message_text,'(2A)') TRIM(jsb_list_name),' not found!'
       CALL finish(thismodule, message_text)
     END IF

     CALL get_var( jsb_var_list, varname, ptr )
     IF ( .NOT. ASSOCIATED(  ptr ) ) THEN
       WRITE(message_text,'(4A)') 'jsbach variable ',TRIM(varname),' not in var list ', &
             TRIM(jsb_list_name)
       CALL finish(thismodule, message_text)
     END IF 
#else
     CALL finish(thismodule,"pointer to jsbach variable requested - please compile with jsbach")
#endif

  END SUBROUTINE set_ptr2d_jsb

!=====================================================================================

  SUBROUTINE set_ptr3d_jsb( varname, component, ptr ) 

    USE mo_linked_list,     ONLY: t_var_list
    USE mo_var_list,        ONLY: get_var_list, get_var

#ifndef __NO_JSBACH__
    USE mo_jsb_control,     ONLY: jsb_models_nml
#endif

    CHARACTER(LEN=*),  INTENT( IN )     :: varname,  component
    REAL(wp), POINTER, INTENT( OUT )    :: ptr(:,:,:) 

    TYPE(t_var_list), POINTER    :: jsb_var_list

    CHARACTER(LEN=50) :: jsb_list_name

#ifndef __NO_JSBACH__
     WRITE(jsb_list_name,'(a)') TRIM(jsb_models_nml(1)%model_shortname)//'_'//TRIM(component)
     CALL get_var_list( jsb_var_list, TRIM(jsb_list_name) )
     IF ( .NOT. ASSOCIATED( jsb_var_list ) ) THEN
       WRITE(message_text,'(a)') TRIM(jsb_list_name),' not found!'
       CALL finish(thismodule, message_text)
     END IF

     CALL get_var( jsb_var_list, varname, ptr )
     IF ( .NOT. ASSOCIATED(  ptr ) ) THEN
       WRITE(message_text,'(a)') 'jsbach varable ',varname,' not in var list ', &
             TRIM(jsb_list_name)
       CALL finish(thismodule, message_text)
     END IF 
#else
     CALL finish(thismodule,"pointer to jsbach varaiable requested - please compile with jsbach"
#endif

  END SUBROUTINE set_ptr3d_jsb

END MODULE mo_subm_get_from_jsbach

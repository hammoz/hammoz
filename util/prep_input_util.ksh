mk_destdir(){
 dest_file=$1
 dest_dir=${dest_file%/*}
 if [ ! -d $dest_dir ]; then
   echo mkdir -p $dest_dir
   mkdir -p $dest_dir
 fi
}

cp_only(){
 echo $idir $odir
 cp_in_file=$idir/$1
 cp_out_file=$odir/$1 

 mk_destdir $cp_out_file
 rsync -av $cp_in_file $cp_out_file
}

## generic #######################

cdo_remap_f(){
  echo cdo remap,$icon_grid_file,$remap_file $re_ifile $re_ofile
  cdo remap,$icon_grid_file,$remap_file $re_ifile $re_ofile
  check_integral
}

cdo_remap(){
  re_ifile=$idir/$1
  re_ofile=$odir/$2
  remap_file=$3
  mk_destdir $re_ofile
  cdo_remap_f
}
   

cdo_remap_fp(){
  mk_destdir $re_ofile
  re_ifile=$1
  re_ofile=$2
  remap_file=$3
  mk_destdir $re_ofile
  cdo_remap_f
}


cdo_remap_masked(){
  rem_ifile=$idir/$1
  rem_ofile=$odir/$2
  remap_file=$3
  mk_destdir $rem_ofile
  l_ifile_name=${rem_ifile##*/}
  l_ofile_name=${rem_ofile##*/}
  l_opath=${rem_ofile%/*}
  tempfile1=${l_opath}/${l_ifile_name%.nc}_TEMP.nc
  tempfile2=${l_opath}/${l_ofile_name%.nc}_TEMP.nc

  cdo setmisstoc,-999. $rem_ifile $tempfile1
  cdo_remap_fp $tempfile1 $tempfile2 $remap_file
  cdo setctomiss,-999. $tempfile2 $rem_ofile
  rm -f $tempfile1 $tempfile2
}

## specific ##############################
cdo_remap_nn(){
  cdo_remap $1 $2 $remap_nn_file
}

cdo_remap_nn_fp(){
  cdo_remap_fp $1 $2 $remap_nn_file
}

cdo_remap_nn_masked(){
  cdo_remap_masked $1 $2 $remap_nn_file
}


cdo_remap_con(){
  cdo_remap $1 $2 $remap_con_file
}

cdo_remap_con_fp(){
  cdo_remap_fp $1 $2 $remap_con_file
}

#cdo_remap_con_masked(){ # not working
#  cdo_remap_masked $1 $2 $remap_con_file
#}


####################################################
ln_file(){
 link_path=$1
 link_source=$2
 link_dest=$3
 opwd=$(pwd)
 cd $odir/$link_path 
 if [ -h $link_dest ]; then
  rm -f $link_dest
 else
   if [ -e $link_dest ]; then
     echo ERROR prep_input_util.ksh: file $link_dest exists and is not a symbolic link
     exit 5 
   fi
 fi
 ln -s $link_source $link_dest
 cd $opwd
}


#################################
create_local_grid_files(){
  if [[ ! -d local ]]; then
     mkdir local
  fi
  cd local
  if [ ! -e ${grid_name_out}_cell.nc ]; then
    cdo selname,cell_area  $icon_grid_file0 ${grid_name_out}_cell.nc
    # nearest neighbor remap file:
    cdo gennn,${grid_name_out}_cell.nc  t63grid.nc  remapnn_t63grid_${grid_name_out}_cell.nc
    # 1st order conservative remap file:
    cdo gencon,${grid_name_out}_cell.nc  t63grid.nc  remapcon_t63grid_${grid_name_out}_cell.nc
  fi
  icon_grid_file=$(pwd)/${grid_name_out}_cell.nc
  remap_nn_file=$(pwd)/remapnn_t63grid_${grid_name_out}_cell.nc
  remap_con_file=$(pwd)/remapcon_t63grid_${grid_name_out}_cell.nc
  cd ..
 
}


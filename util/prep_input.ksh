#!/bin/ksh

#set -ex
set -ex

. ./prep_input_util.ksh

. ./dust_util.ksh
. ./accmip_interpolated_util.ksh
. ./aerocom_II_util.ksh
. ./gfas_util.ksh
. ./dms_util.ksh

. ./check_util.ksh

vers=v0002_01

idir=$HOME/input/hammoz_input/echam/$vers
odir=$HOME/input/hammoz_input/icon/$vers

hres_in=T63 
grid_name_out=icon_grid_0005_R02B04_G

num_lev=47 # vertical levels

icon_grid_file0=$HOME/input/ICON/grids/private/mpim/icon_preprocessing/source/grids/${grid_name_out}.nc

# create files for remapping from T63
create_local_grid_files

#  lcheck=1: compute totals before and after re-gridding and write to file "check.out"
lcheck=1  
ofile_check=check.out

if [ ! -d $odir ]; then
  mkdir -p $odir
fi

if [ -e $ofile_check ]; then
  rm -f $ofile_check
fi

remap_gfas
exit 5

############### start here ###########

cp_only hammoz/lut_kappa.nc 
cp_only hammoz/lut_optical_properties_M7.nc
cp_only hammoz/lut_optical_properties_lw_salsa.nc
cp_only hammoz/lut_optical_properties_lw_M7.nc
cp_only hammoz/lut_optical_properties_salsa.nc

# dust
remap_dust

# accmip_interpolated_emissions
remap_accmip_interpolated

#aerocom_II
remap_aerocom_II

# gfas
remap_gfas

#DMS
remap_dms

#volcanoes
cp_only emissions_inventories/aerocom_II/continuous_volcanos.dat
cp_only emissions_inventories/aerocom_II/explosive_volcanos.dat

#soil and surface properties file (for drydep)
irdir=hammoz/${hres_in}
ordir=hammoz/${grid_name_out}
cdo_remap_nn $irdir/soilpHfrac_${hres_in}.nc  $ordir/soilpHfrac_${grid_name_out}.nc
cdo_remap_nn $irdir/xtsurf_v2_${hres_in}.nc $ordir/xtsurf_v2_${grid_name_out}.nc

irdir=echam6/${hres_in}
ordir=echam6/${grid_name_out}
cdo_remap_nn $irdir/${hres_in}GR15_VGRATCLIM.nc $ordir/VGRATCLIM_${grid_name_out}.nc

# ion production rate from cosmic rays 
cp_only hammoz/solmin.txt
cp_only hammoz/solmax.txt

# PARNUC neutral and charge nucleation scheme
cp_only hammoz/parnuc.15H2SO4.A0.total.nc

# oxidant concentrations (see vert_interp directory for hints on how this was prepared) 
ifile=$odir/temp/${hres_in}/ham_oxidants_monthly_${hres_in}L${num_lev}_macc_ICON_vertical_grid.nc
ofile=$odir/hammoz/${grid_name_out}/ham_oxidants_monthly_${grid_name_out}L${num_lev}h_macc.nc
cdo_remap_con_fp $ifile $ofile


if [[ $lcheck -eq 1 ]]; then
  emacs $ofile_check
fi

exit


